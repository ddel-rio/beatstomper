using Mono = UnityEngine.MonoBehaviour;
using Coroutine = System.Collections.IEnumerator;

namespace Antipixel
{
    public static class MonoBehaviourExtensions
    {
        public static void StartCoroutines(this Mono mono, params Coroutine[] coroutines) => coroutines.Each(i => mono.StartCoroutine(i));
        public static void StopCoroutines(this Mono mono, params Coroutine[] coroutines) => coroutines.Each(i => mono.StopCoroutine(i));
    }
}