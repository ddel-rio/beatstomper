namespace Antipixel
{
	public enum SpatialProperty
	{
		POSITION,
		ROTATION,
		SCALE
	}
}