using AddComponentMenu = UnityEngine.AddComponentMenu;

namespace Antipixel
{
    /// <summary>
    /// 
    /// </summary>
    [AddComponentMenu("Antipixel/Patterns/Dont Destroy On Load")]
    public class DontDestroyOnLoad : BaseObject { private void Awake() => DontDestroyOnLoad(gameObject); }
}