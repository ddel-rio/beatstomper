#if UNITY_EDITOR
using UnityEditor;

namespace Antipixel.Editor
{
	[CustomEditor(typeof(ObjectPool), true), CanEditMultipleObjects]
	public class ObjectPoolInspector : BaseInspector { }
}
#endif