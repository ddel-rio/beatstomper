namespace Antipixel
{
	public enum PlayMode
	{
		NONE,
		ONCE,
		LOOP,
		PING_PONG,
		RANDOM
	}
}