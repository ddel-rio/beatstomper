namespace Antipixel
{
	public enum DetectionMode
	{
		ANYTHING,
		LAYER,
		TAG,
		COMPONENT,
		NAME
	}
}