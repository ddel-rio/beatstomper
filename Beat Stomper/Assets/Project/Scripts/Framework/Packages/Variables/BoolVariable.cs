using UnityEngine;

namespace Antipixel
{
	[CreateAssetMenu(fileName = "New Bool", menuName = "Antipixel/Variables/Bool")]
	public class BoolVariable : GlobalVariable<bool> { }
}