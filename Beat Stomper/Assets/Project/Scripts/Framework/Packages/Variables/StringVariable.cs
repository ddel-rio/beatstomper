using UnityEngine;

namespace Antipixel
{
	[CreateAssetMenu(fileName = "New String", menuName = "Antipixel/Variables/String")]
	public class StringVariable : GlobalVariable<string> { }
}