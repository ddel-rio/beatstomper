using UnityEngine;
using Antipixel;

namespace BeatStomper
{
    /// <summary>
    /// It is responsible for analyzing the rhythm of the music.
    /// </summary>
    [AddComponentMenu("Beat Stomper/Music Controller"), RequireComponent(typeof(AudioSource))]
    public class MusicController : BaseObject
    {
        #region Const Variables
        public const float REFRESH_RATE = 0.03f;
        private const float MIN_BEAT = 0.01f;
		#endregion Const Variables


		#region Public Variables
		public FloatVariable beat;
        public int sampleDataLength = 1024;
        #endregion Public Variables


        #region Private Variables
        private AudioSource source;
        private float[] data;
        private float t;
        #endregion Private Variables


        #region Unity Methods
        private void Awake()
        {
            source = GetComponent<AudioSource>();

            data = new float[sampleDataLength];
        }

        private void Update()
        {
            if ((t += DeltaTime) > REFRESH_RATE)
            {
                t = 0f;
                UpdateBeat();
            }
        }
        #endregion Unity Methods


        #region Main Methods
        private void UpdateBeat()
        {
            if (source.clip == null || !source.isPlaying) return;

            float beat = 0f;

            source.clip.GetData(data, source.timeSamples);
            data.Each(i => beat += i.Abs());
            beat /= sampleDataLength;

            if (beat < 0.01f) beat = MIN_BEAT;

            this.beat.Value = beat;
        }
        #endregion Main Methods
    }
}