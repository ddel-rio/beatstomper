using UnityEngine;
using UnityEngine.Events;
using Antipixel;

namespace BeatStomper
{
	/// <summary>
	/// It controls the direction of the bullets when firing and counts the impacts.
	/// </summary>
	[AddComponentMenu("Beat Stomper/Gun")]
	public class Gun : BaseObject
	{
		#region Public Variables
		public Transform cursor;
		public Bullet bullet;
		public float force;
		public int impacts;
		#endregion Public Variables


		#region Private Variables
		private int count;
		private TrailRenderer bulletTrail;
		private Rigidbody bulletRigidbody;
		private Collider bulletCollider;
		#endregion Private Variable


		#region Events
		public UnityEvent OnShoot, OnRecharge;
		#endregion Events


		#region Properties
		public int Count
		{
			get => count;
			protected set
			{
				count = value;

				if (count >= impacts.Abs())
				{
					count = 0;
					Recharge();
				}
			}
		}
		public bool Shooting { get; protected set; }
		#endregion Properties


		#region Unity Methods
		private void Awake()
		{
			bulletRigidbody = bullet.GetComponent<Rigidbody>();
			bulletTrail = bullet.GetComponentInChildren<TrailRenderer>();
			bulletCollider = bullet.GetComponentInChildren<Collider>();

			bullet.gun = this;
		}

		private void Update()
		{
			if (cursor != null)
			{
				Physics.Raycast(Position, transform.forward, out RaycastHit hit);
				cursor.position = hit.point;
			}
		}
		#endregion Unity Methods


		#region Main Methods
		public void Shoot()
		{
			if (Shooting) return;
			Shooting = true;

			bulletCollider.enabled = true;
			bullet.transform.SetParent(null);
			bullet.force = force.Abs();
			bulletRigidbody.AddForce(Forward);
			bulletTrail.enabled = true;
			OnShoot?.Invoke();
		}
		public void Recharge()
		{
			Shooting = false;
			count = 0;
			bulletCollider.enabled = false;
			bullet.Parent = transform;
			bullet.LocalPosition = Vector3.zero;
			bulletRigidbody.velocity = Vector3.zero;
			bulletTrail.Clear();
			bulletTrail.enabled = false;
			OnRecharge?.Invoke();
		}
		#endregion Main Methods


		#region Utility Methods
		public void Impact() => Count++;
		#endregion Utility Methods
	}
}