using UnityEngine;
using UnityEngine.Events;
using Antipixel;

namespace BeatStomper
{
	/// <summary>
	/// Move the cube to the rhythm of the music.
	/// </summary>
	[AddComponentMenu("Beat Stomper/Cube"), RequireComponent(typeof(Rigidbody))]
	public class Cube : BaseObject, ISpawneable
	{
		#region Const Variables
		private const float MIN_VELOCITY = 0.1f;
		#endregion Const Variables


		#region Public Variables
		public FloatVariable beat;
		public FloatVariable gameSpeed;
		public IntVariable score;
		public SoundEffect correctSFX;
		public SoundEffect incorrectSFX;
		public string particlesName;
		#endregion Public Variables


		#region Private Variables
		private Rigidbody rb;
		private ObjectPool particles;
		private float speed;
		#endregion Private Variables


		#region Events
		public UnityEvent OnSpawnEvent, OnDespawnEvent;
		#endregion Events


		#region Properties
		private Vector3 RandomDirection
		{
			get
			{
				float x = Random.Range(-1f, 1f);
				float y = Random.Range(-1f, 1f);
				float z = Random.Range(-1f, 1f);
				return new Vector3(x, y, z);
			}
		}
		#endregion Properties


		#region Unity Methods
		private void Awake()
		{
			rb = GetComponent<Rigidbody>();

			//TODO: Optimize
			particles = GameObject.Find(particlesName).GetComponent<ObjectPool>();
		}

		private void Start()
		{
			speed = GameManager.Instance.Data.speed;

			float min = GameManager.Instance.Data.minSize;
			float max = GameManager.Instance.Data.maxSize;
			float scale = Random.Range(min, max);
			LocalScale = Vector3.one * scale;
		}

		private void Update()
		{
			float force = beat.Value * speed;

			if (force <= 0f) force = MIN_VELOCITY;

			rb.velocity = force * gameSpeed.Value * rb.velocity.normalized;
		}

		private void OnCollisionEnter(Collision collision)
		{
			Bullet bullet = collision.gameObject.GetComponent<Bullet>();

			if (bullet != null) OnBulletEnter(bullet.tag);
		}

		private void OnMouseOver()
		{
			if (Input.GetMouseButtonDown(0)) OnBulletEnter("Blue");
			else if (Input.GetMouseButtonDown(1)) OnBulletEnter("Red");
		}
		#endregion Unity Methods


		#region Main Methods
		public void OnSpawn()
		{
			Rotation = Random.rotation;
			rb.velocity = RandomDirection;

			OnSpawnEvent?.Invoke();
		}
		public void OnDespawn()
		{
			SpawnParticles();

			Spawner.Instance.Spawn(gameObject);

			OnDespawnEvent?.Invoke();
		}

		private void OnBulletEnter(string bulletTag)
		{
			PlaySound(bulletTag);
			AddPoints(bulletTag);
			OnDespawn();
		}

		private void PlaySound(string bulletTag)
		{
			SoundEffect sfx = CompareTag(bulletTag) ? correctSFX : incorrectSFX;
			SoundController sfxController = sfx.Play(sfx.clips.Random());
			sfxController.Position = Position;
		}
		private void AddPoints(string bulletTag)
		{
			int correctRed = GameManager.Instance.Data.correctRed;
			int correctBlue = GameManager.Instance.Data.correctBlue;
			int incorrectRed = GameManager.Instance.Data.incorrectRed;
			int incorrectBlue = GameManager.Instance.Data.incorrectBlue;
			int points;

			if (CompareTag(bulletTag)) 
				points = bulletTag == "Red" ? correctRed : correctBlue;
			else 
				points = bulletTag == "Red" ? incorrectRed : incorrectBlue;

			score.Add(points);
		}
		private void SpawnParticles()
		{
			GameObject obj = particles.Dequeue()
				?? Instantiate(particles.prefabs.Random());

			obj.transform.position = Position;
		}
		#endregion Main Methods
	}
}