using UnityEngine;
using Antipixel;

namespace BeatStomper
{
	/// <summary>
	/// It adjusts the speed of the bullet and the rotation of the particles when hitting a wall.
	/// </summary>
	[AddComponentMenu("Beat Stomper/Bullet"), RequireComponent(typeof(Rigidbody))]
	public class Bullet : BaseObject
	{
		#region Public Variables
		public FloatVariable gameSpeed;
		public ObjectPool particles;
		public SoundEffect wallSFX;

		[HideInInspector] public float force;
		[HideInInspector] public Gun gun;
		#endregion Public Variables


		#region Private Variables
		private Rigidbody rb;
		#endregion Private Variables


		#region Unity Methods
		private void Awake() => rb = GetComponent<Rigidbody>();

		private void Update()
		{
			Vector3 velocity = rb.velocity;
			velocity.Normalize();

			rb.velocity = force * gameSpeed.Value * velocity;
		}

		private void OnCollisionEnter(Collision collision)
		{
			if (collision.gameObject.CompareTag("Wall"))
			{
				Vector3 offset = -collision.transform.forward * 0.15f;
				Vector3 position = collision.GetContact(0).point + offset;
				Quaternion rotation = collision.transform.rotation;

				GameObject obj = particles.Dequeue();
				obj.transform.position = position;
				obj.transform.rotation = rotation;

				SoundController sfx = wallSFX.Play(wallSFX.clips.Random());
				sfx.Position = position;

				gun.Impact();
			}
		}
		#endregion Unity Methods
	}
}