using UnityEngine;
using Antipixel;

namespace BeatStomper
{
    /// <summary>
    /// Deactivates the particle automatically upon completion.
    /// </summary>
    [AddComponentMenu("Beat Stomper/Particle Controller")]
    public class ParticleController : BaseObject, IPoolObject
    {
        #region Private Variables
        private ParticleSystem ps;
        #endregion Private Variables


        #region Properties
        public ObjectPool Pool { get; set; }
        #endregion Properties


        #region Unity Methods
        private void Awake() => ps = GetComponentInChildren<ParticleSystem>();

        private void Update()
        {
            if (!ps.IsAlive())
            {
                if (Pool != null) Pool.Enqueue(gameObject);
                else Destroy(gameObject);

            }
        }
        #endregion Unity Methods


        #region Main Methods
        public void OnDequeue() => ps.Play();
        public void OnEnqueue() => ps.Stop();
        #endregion Main Methods
    }
}