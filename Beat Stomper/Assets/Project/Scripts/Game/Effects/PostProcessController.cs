using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using Antipixel;

namespace BeatStomper
{
    /// <summary>
    /// Controls the intensity of post-processing according to the rhythm of the music.
    /// </summary>
    [AddComponentMenu("Beat Stomper/Post Process Controller")]
    public class PostProcessController : BaseObject
    {
        #region Public Variables
        public FloatVariable beat;
        public float force = 1f;
        public float speed = 1f;
        #endregion Public Variables


        #region Private Variables
        private PostProcessVolume volume;
        private Bloom bloom;
        private ColorGrading colorGrading;
		#endregion Private Variables


		#region Unity Methods
		private void Awake()
		{
            volume = GetComponent<PostProcessVolume>();
            bloom = volume.profile.GetSetting<Bloom>();
            colorGrading = volume.profile.GetSetting<ColorGrading>();
        }

        private void Update()
        {
            if (GameManager.Instance.InGame)
            {
                float target = force.Abs() * beat.Value;
                float value = Mathf.Lerp(bloom.intensity, target, Time.deltaTime * speed.Abs());
                SetBloom(value);
            }
        }
        #endregion Unity Methods


        #region Utility Methods
        public void SetBloom(float value) => bloom.intensity.Override(value);

        public void SetHueSift(float value) => colorGrading.hueShift.Override(value);
        #endregion Utility Methods
    }
}