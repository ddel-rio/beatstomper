using UnityEngine;
using TMPro;
using Antipixel;

namespace BeatStomper
{
	/// <summary>
	/// It displays the remaining time of the song.
	/// </summary>
	[AddComponentMenu("Beat Stomper/Time Controller")]
	public class TimeController : BaseObject
	{
		#region Public Variables
		public TMP_Text timeText;
		public AudioSource music;
		#endregion Public Variables


		#region Properties
		public float Time { get; private set; }
		public int Seconds => ToSeconds(Time);
		public int Minutes => ToMinutes(Time);
		public int Hours => ToHours(Time);
		#endregion Properties


		#region Unity Methods
		private void Update()
		{
			if (music.clip == null) return;

			Time = (music.clip.length - music.time).Abs();
			timeText.text = string.Format("{0:00} : {1:00}", Minutes, Seconds);

			if (!music.isPlaying || Time <= 0.01f)
				GameManager.Instance.FinishLevel();
		}
		#endregion Unity Methods


		#region Utility Methods
		public void SetTimeScale(float t) => TimeScale = t;

		public static int ToMilliseconds(float t) => System.TimeSpan.FromSeconds(t).Milliseconds;
		public static int ToSeconds(float t) => (int)t % 60;
		public static int ToMinutes(float t) => ((int)t / 60) % 60;
		public static int ToHours(float t) => (int)t / 3600;
		#endregion Utility Methods
	}
}