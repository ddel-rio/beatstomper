using UnityEngine;
using TMPro;
using Antipixel;

namespace BeatStomper
{
	/// <summary>
	/// It updates the game score in real time.
	/// </summary>
	[AddComponentMenu("Beat Stomper/Score Controller")]
	public class ScoreController : BaseObject
	{
		#region Public Variables
		public IntVariable score;
		public TMP_Text scoreText;
		#endregion Public Variables


		#region Unity Methods
		protected override void OnEnable()
		{
			base.OnEnable();

			score.OnValueChanged.AddListener(OnScoreChanged);
		}
		protected override void OnDisable()
		{
			base.OnDisable();

			score.OnValueChanged.RemoveListener(OnScoreChanged);
		}
		#endregion Unity Methods


		#region Main Methods
		private void OnScoreChanged()
		{
			if (score.Value < 0) score.Value = 0;

			scoreText.text = $"{score.Value:0000}";
		}
		#endregion Main Methods
	}
}