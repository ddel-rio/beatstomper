namespace BeatStomper
{
	public interface ISpawneable
	{
		#region Main Methods
		void OnSpawn();
		void OnDespawn();
		#endregion Main Methods
	}
}