using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Antipixel;
using Coroutine = System.Collections.IEnumerator;

namespace BeatStomper
{
	/// <summary>
	/// It creates targets in the designated area.
	/// </summary>
	[AddComponentMenu("Beat Stomper/Spawner")]
	public class Spawner : Singleton<Spawner>
	{
		#region Public Variables
		public Color color;

		public float size = 10f;
		public GameObject[] cubes;
		#endregion Public Variables


		#region Private Variables
		private List<GameObject> cubeList = new List<GameObject>();
		#endregion Private Variables


		#region Events
		public UnityEvent<GameObject> OnSpawn;
		#endregion Events


		#region Main Methods
		public void SpawnAll()
		{
			DestroyAllInmediate();

			StopAllCoroutines();
			StartCoroutine(SpawnCoroutine());
		}
		public void DestroyAll()
		{
			StopAllCoroutines();
			StartCoroutine(DestroyCoroutine());
		}

		public void Spawn(GameObject cube = null, int id = -1)
		{
			float diameter = size.Abs() / 2;
			Vector3 point = Vector3.zero.Random(-diameter, diameter);

			if (cube == null)
				cube = Instantiate(cubes.Random(), Position, Quaternion.identity, transform);

			if (!cubeList.Contains(cube)) cubeList.Add(cube);

			if (id >= 0) cube.name = $"{id} cube_{cube.tag}";

			cube.transform.position = Position + point;

			cube.GetComponent<ISpawneable>()?.OnSpawn();

			OnSpawn?.Invoke(cube);
		}
		private Coroutine SpawnCoroutine()
		{
			if (cubes.IsNullOrEmpty()) yield break;

			int min = GameManager.Instance.Data.minAmount;
			int max = GameManager.Instance.Data.maxAmount;
			float amount = Random.Range(min.Abs(), max.Abs());

			float delay = GameManager.Instance.Data.delay;

			for (int i = 0; i < amount; i++)
			{
				Spawn(id: i + 1);

				if (delay != 0f)
					yield return new WaitForSeconds(delay.Abs());
			}
		}
		private Coroutine DestroyCoroutine()
		{
			float delay = GameManager.Instance.Data.delay;

			while (transform.childCount > 0)
			{
				Transform child = transform.GetChild(0);
				child.GetComponent<ISpawneable>()?.OnDespawn();
				Destroy(child.gameObject);

				if (delay != 0f)
					yield return new WaitForSeconds(delay.Abs());
			}
		}
		public void DestroyAllInmediate()
		{
			while (cubeList.Count > 0)
			{
				GameObject cube = cubeList[0];
				cubeList.Remove(cube);
				Destroy(cube);
			}
		}

#if UNITY_EDITOR
		private void OnDrawGizmos()
		{
			Vector3 cube = Vector3.one * size;

			Gizmos.color = color;
			Gizmos.DrawWireCube(Position, cube);
		}
#endif
		#endregion Main Methods
	}
}