using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using Antipixel;

namespace BeatStomper
{
	/// <summary>
	/// It displays the highscore of a song and allows you to start the game.
	/// </summary>
	[AddComponentMenu("Beat Stomper/Level Button")]
	public class LevelButton : VRButton, IPointerDownHandler
	{
		#region Public Variables
		[Header("Parameters")]
		public LevelData levelData;
		public TMP_Text highscore;
		#endregion Public Variables


		#region Unity Methods
		protected override void OnEnable()
		{
			base.OnEnable();

			levelData.UpdateHighscore();
			highscore.text = levelData.Highscore.ToString("0000");
		}

		public override void OnPointerDown(PointerEventData eventData)
		{
			base.OnPointerDown(eventData);

			GameManager.Instance.StartLevel(levelData);
		}
		#endregion Unity Methods
	}
}