using UnityEngine;
using Antipixel;

namespace BeatStomper
{
	/// <summary>
	/// It manages the main flow of the game.
	/// </summary>
	[AddComponentMenu("Beat Stomper/Game Manager")]
	public class GameManager : Singleton<GameManager>
	{
		#region Public Variables
		public AudioSource music;
		public GameEvent pauseEvent, resumeEvent;
		public Gun redGun;
		public Gun blueGun;
		public GameObject cursors;
		#endregion Public Variables


		#region Properties
		public LevelData Data { get; private set; }
		public bool InPause { get; private set; }
		public bool InGame => Data != null;
		#endregion Properties


		#region Unity Methods
		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				if (InGame) PauseLevel();
				else Quit();
			}
		}
		#endregion Unity Methods


		#region Main Methods
		public void StartLevel(LevelData data)
		{
			if (InGame) return;

			Data = data;

			redGun.force = data.forceRed;
			redGun.impacts = data.impactsRed;

			blueGun.force = data.forceBlue;
			blueGun.impacts = data.impactsBlue;

			music.clip = Data.clip;
			music.Play();
			Data.Load();

			cursors.SetActive(false);
		}
		public void FinishLevel()
		{
			if (!InGame) return;

			music.Pause();
			music.clip = null;

			Data.Save();
			Data = null;

			InPause = false;
			cursors.SetActive(true);

			resumeEvent?.Invoke();
		}
		public void PauseLevel()
		{
			if (!InGame) return;

			InPause = !InPause;
			cursors.SetActive(InPause);

			if (InPause) pauseEvent?.Invoke();
			else resumeEvent?.Invoke();
		}
		#endregion Main Methods


		#region Utility Methods
		public void Quit() => Application.Quit();
		#endregion Utility Methods
	}
}