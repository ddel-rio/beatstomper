using UnityEngine;
using UnityEngine.Events;
using Antipixel;

namespace BeatStomper
{
	/// <summary>
	/// It is responsible for storing, loading and saving the information of a custom level.
	/// </summary>
	[CreateAssetMenu(fileName = "New Level Data", menuName = "Beat Stomper/Level Data")]
	public class LevelData : ScriptableObject
	{
		#region Public Variables
		[Header("Music")]
		public AudioClip clip;
		public IntVariable score;

		[Header("Points")]
		public int correctRed = 10;
		public int correctBlue = 10;
		public int incorrectRed = -15;
		public int incorrectBlue = -15;

		[Header("Weapons")]
		public float forceRed = 80;
		public float forceBlue = 100;
		[Min(1)] public int impactsRed = 2;
		[Min(1)]public int impactsBlue = 1;

		[Header("Cubes")]
		public float speed = 20;
		public float minSize = 0.35f;
		public float maxSize = 1.5f;

		[Header("Generation")]
		[Min(0f)] public float delay = 0.01f;
		[Min(0)] public int minAmount = 100;
		[Min(0)]public int maxAmount = 150;
		#endregion Public Variables


		#region Events
		[Header("Events")]
		public UnityEvent<LevelData> OnLoad;
		public UnityEvent<LevelData> OnSave;
		#endregion Events


		#region Properties
		public int Highscore { get; private set; }
		#endregion Properties


		#region Unity Methods
		private void OnEnable() => UpdateHighscore();
		#endregion Unity Methods


		#region Main Methods
		public void Load()
		{
			OnLoad?.Invoke(this);
		}
		public void Save()
		{
			if (score.Value > Highscore)
				PlayerPrefs.SetInt(clip.name, score.Value);

			OnSave?.Invoke(this);
		}
		#endregion Main Methods


		#region Utility Methods
		public void UpdateHighscore() => Highscore = PlayerPrefs.GetInt(clip.name, 0);
		#endregion Utility Methods
	}
}