﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 Antipixel.ObjectPool BeatStomper.ParticleController::get_Pool()
extern void ParticleController_get_Pool_m0B06778AC43BA0AEEBCCA2900A6774581F31E4FA (void);
// 0x00000002 System.Void BeatStomper.ParticleController::set_Pool(Antipixel.ObjectPool)
extern void ParticleController_set_Pool_m358926DCB555DCBF8394FB3B12239690FFB76A1F (void);
// 0x00000003 System.Void BeatStomper.ParticleController::Awake()
extern void ParticleController_Awake_m29376B5F72D6077B80BE7D5F8C8892E886FC4DCD (void);
// 0x00000004 System.Void BeatStomper.ParticleController::Update()
extern void ParticleController_Update_m3A2B6284877A76F56251767E3A9A3B3D4461D21A (void);
// 0x00000005 System.Void BeatStomper.ParticleController::OnDequeue()
extern void ParticleController_OnDequeue_mEC3009D5E29579D8EF5475ECD345D93EF267F770 (void);
// 0x00000006 System.Void BeatStomper.ParticleController::OnEnqueue()
extern void ParticleController_OnEnqueue_m81224518EDECF925FD5A06DF09B44451D42ADA3C (void);
// 0x00000007 System.Void BeatStomper.ParticleController::.ctor()
extern void ParticleController__ctor_m0633F31711BB00EF50713AB5B22CE542317B65CA (void);
// 0x00000008 System.Void BeatStomper.PostProcessController::Awake()
extern void PostProcessController_Awake_m18224D4ECE4ADE07D51912F70C510CDA9385FBE1 (void);
// 0x00000009 System.Void BeatStomper.PostProcessController::Update()
extern void PostProcessController_Update_m750C394ACB8A886BC03DB7BA14F3F1018C3178E5 (void);
// 0x0000000A System.Void BeatStomper.PostProcessController::SetBloom(System.Single)
extern void PostProcessController_SetBloom_mECA5606372B87F08CBE37CF05B5A020B1465F52A (void);
// 0x0000000B System.Void BeatStomper.PostProcessController::SetHueSift(System.Single)
extern void PostProcessController_SetHueSift_m069DF7F6712E437ACC67C101C5CA3E25A0577BD1 (void);
// 0x0000000C System.Void BeatStomper.PostProcessController::.ctor()
extern void PostProcessController__ctor_mD6096DAC35986FB83D918E201EFACEA87B3362B8 (void);
// 0x0000000D BeatStomper.LevelData BeatStomper.GameManager::get_Data()
extern void GameManager_get_Data_m1B7A20844D301ED83EA5355B5B9F96F368A317D7 (void);
// 0x0000000E System.Void BeatStomper.GameManager::set_Data(BeatStomper.LevelData)
extern void GameManager_set_Data_m10FF27828C8F24B6064BEFEB9A77DFD4BA72DE66 (void);
// 0x0000000F System.Boolean BeatStomper.GameManager::get_InPause()
extern void GameManager_get_InPause_m9B5C419C01769D13A140FE124FEF1A0CBBA89E16 (void);
// 0x00000010 System.Void BeatStomper.GameManager::set_InPause(System.Boolean)
extern void GameManager_set_InPause_mF8A3587DFF67477D609E2F31EDC831AF9EB91828 (void);
// 0x00000011 System.Boolean BeatStomper.GameManager::get_InGame()
extern void GameManager_get_InGame_m8CDCCC555CBB172BBC6EBF3DD196A5730275C983 (void);
// 0x00000012 System.Void BeatStomper.GameManager::Update()
extern void GameManager_Update_mD4DAB5B2B55C8FC09A4A83300080BB6CDD8F076E (void);
// 0x00000013 System.Void BeatStomper.GameManager::StartLevel(BeatStomper.LevelData)
extern void GameManager_StartLevel_m4380820698DCABE87C9DBE61E2D12332BEB52F7B (void);
// 0x00000014 System.Void BeatStomper.GameManager::FinishLevel()
extern void GameManager_FinishLevel_m7B1C70C0CD9A2761ACC7477E73BDE041B1EF788B (void);
// 0x00000015 System.Void BeatStomper.GameManager::PauseLevel()
extern void GameManager_PauseLevel_m93C71900FB2B24F9FC850973EBC24EDCB893F373 (void);
// 0x00000016 System.Void BeatStomper.GameManager::Quit()
extern void GameManager_Quit_m1A7331B8821593C7880728ECEAA0E5493E2F44BA (void);
// 0x00000017 System.Void BeatStomper.GameManager::.ctor()
extern void GameManager__ctor_mA8F33402ED72F4180497DFBBB64C9E0742DEBE7F (void);
// 0x00000018 System.Void BeatStomper.Bullet::Awake()
extern void Bullet_Awake_m4FCE183D29F640F111675F01B12EAC89C4B22E19 (void);
// 0x00000019 System.Void BeatStomper.Bullet::Update()
extern void Bullet_Update_mA5757BC0CB0507844B3310C69ADF3D9D391FBBC7 (void);
// 0x0000001A System.Void BeatStomper.Bullet::OnCollisionEnter(UnityEngine.Collision)
extern void Bullet_OnCollisionEnter_m9BF981EF5E0A67A59DCE73C558CDF75360320E7F (void);
// 0x0000001B System.Void BeatStomper.Bullet::.ctor()
extern void Bullet__ctor_m132BDF6FEF611A1E6B334D228E87E49D1D53D4FB (void);
// 0x0000001C UnityEngine.Vector3 BeatStomper.Cube::get_RandomDirection()
extern void Cube_get_RandomDirection_m27BC8E6E72F066450EAEC6399293F4568E92BFFC (void);
// 0x0000001D System.Void BeatStomper.Cube::Awake()
extern void Cube_Awake_mCE941CDF22321DC6C423F528FBB8E0B4951B0F29 (void);
// 0x0000001E System.Void BeatStomper.Cube::Start()
extern void Cube_Start_m2A25EAF78003BA33903B40E51000823DA27F8DF3 (void);
// 0x0000001F System.Void BeatStomper.Cube::Update()
extern void Cube_Update_m3F07C0887F14FD2E43EC78936FBA9CAEBBCB8941 (void);
// 0x00000020 System.Void BeatStomper.Cube::OnCollisionEnter(UnityEngine.Collision)
extern void Cube_OnCollisionEnter_mEA6A3A75CDDDF0D4EB6C8FAAB7142DBAA7FA68D9 (void);
// 0x00000021 System.Void BeatStomper.Cube::OnMouseOver()
extern void Cube_OnMouseOver_m7C9C220668780939FD6EB78341DCF7113FF1C46E (void);
// 0x00000022 System.Void BeatStomper.Cube::OnSpawn()
extern void Cube_OnSpawn_m1992987C3225752BEA165419742F92F62EF154EA (void);
// 0x00000023 System.Void BeatStomper.Cube::OnDespawn()
extern void Cube_OnDespawn_mEE24FB22421A57CB99E6881125690B75490594D7 (void);
// 0x00000024 System.Void BeatStomper.Cube::OnBulletEnter(System.String)
extern void Cube_OnBulletEnter_mBDA7A8D8AB2DB7175B5E31EE3B529F202CCF4D87 (void);
// 0x00000025 System.Void BeatStomper.Cube::PlaySound(System.String)
extern void Cube_PlaySound_mB0746572FA0856B26403004F688370B93AE2F2D2 (void);
// 0x00000026 System.Void BeatStomper.Cube::AddPoints(System.String)
extern void Cube_AddPoints_m4BAC60CDECF8DB1BBD39B2F6C56D652000BDE540 (void);
// 0x00000027 System.Void BeatStomper.Cube::SpawnParticles()
extern void Cube_SpawnParticles_m427C1167901688F2E814939ED0B9BBEC4153D854 (void);
// 0x00000028 System.Void BeatStomper.Cube::.ctor()
extern void Cube__ctor_m2A7803FF04152687F05DA562F82EFE6592272E74 (void);
// 0x00000029 System.Int32 BeatStomper.Gun::get_Count()
extern void Gun_get_Count_mC8CD9E13E2877EE32624CD22196363AF1EB89CEE (void);
// 0x0000002A System.Void BeatStomper.Gun::set_Count(System.Int32)
extern void Gun_set_Count_m2ACD1D4356A345CED530C14386215068BD205BB5 (void);
// 0x0000002B System.Boolean BeatStomper.Gun::get_Shooting()
extern void Gun_get_Shooting_mBADD5CED17DC9B9D31EE0353C4704F57F58C9EA0 (void);
// 0x0000002C System.Void BeatStomper.Gun::set_Shooting(System.Boolean)
extern void Gun_set_Shooting_m0BD4A2DC332628D534B55DF5C302EC80B227765D (void);
// 0x0000002D System.Void BeatStomper.Gun::Awake()
extern void Gun_Awake_m06158D47E0711A65233ECABF37C44C36CDC1F2C8 (void);
// 0x0000002E System.Void BeatStomper.Gun::Update()
extern void Gun_Update_mE05214C9787B3B3784EAD7A34614E1AB0CCAB3CB (void);
// 0x0000002F System.Void BeatStomper.Gun::Shoot()
extern void Gun_Shoot_m0AE27B949F9360CFD9BE7F213B15F0912CB7BFAB (void);
// 0x00000030 System.Void BeatStomper.Gun::Recharge()
extern void Gun_Recharge_m72768A8E18D441549C643CD77733B96E065FF4A4 (void);
// 0x00000031 System.Void BeatStomper.Gun::Impact()
extern void Gun_Impact_mDAC21E6EF5B11FE2250C90D787C2A22B8E6A0BA9 (void);
// 0x00000032 System.Void BeatStomper.Gun::.ctor()
extern void Gun__ctor_mE80C579632188F705E8DC6BD150A49367DA45030 (void);
// 0x00000033 System.Void BeatStomper.InputController::Awake()
extern void InputController_Awake_m8F546CB1F5CDDA6A4BF69D0E7FF6AB6D7524DE35 (void);
// 0x00000034 System.Void BeatStomper.InputController::UIPress(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void InputController_UIPress_m4060901763F0481C72D4A8B58363192A2356800F (void);
// 0x00000035 System.Void BeatStomper.InputController::SelectAction(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void InputController_SelectAction_mF962A9399E9E984B78475C420E2CDEB64F650901 (void);
// 0x00000036 System.Void BeatStomper.InputController::.ctor()
extern void InputController__ctor_m0156272BB19875EAD5F27C03D7FFBD7898A756FC (void);
// 0x00000037 System.Void BeatStomper.LevelButton::OnEnable()
extern void LevelButton_OnEnable_m57E796FE0FE3DEA874FA9893B26F4FAF28406D12 (void);
// 0x00000038 System.Void BeatStomper.LevelButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void LevelButton_OnPointerDown_mD5F389789EA2217C1C85225FCD8F4628F5F013D0 (void);
// 0x00000039 System.Void BeatStomper.LevelButton::.ctor()
extern void LevelButton__ctor_m91E9F5D19F55E791E8515D3E61A0BACFE1557384 (void);
// 0x0000003A System.Void BeatStomper.VRButton::Awake()
extern void VRButton_Awake_m2900AD33AD24614A691BA0CC5E9FFCF23DE6E30C (void);
// 0x0000003B System.Void BeatStomper.VRButton::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void VRButton_OnPointerEnter_m7F5F4F2AA3352B2BE85093B78F99E26333E95E33 (void);
// 0x0000003C System.Void BeatStomper.VRButton::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void VRButton_OnPointerExit_m14E0DA2C2E440A2F12690D8C47855009B8D05078 (void);
// 0x0000003D System.Void BeatStomper.VRButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void VRButton_OnPointerDown_mB89C9A1636DA34FE1AEC4736A5980AC0A80D15B4 (void);
// 0x0000003E System.Void BeatStomper.VRButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void VRButton_OnPointerUp_m784D90E425B8ED9F4E781C7C8E5CAF2CEA95AB45 (void);
// 0x0000003F System.Void BeatStomper.VRButton::SetColor(System.String)
extern void VRButton_SetColor_mC3D442A42FA253085BE2448CF59A6914EFC99D7B (void);
// 0x00000040 System.Void BeatStomper.VRButton::SetAlpha(System.Single)
extern void VRButton_SetAlpha_m11A349121F86B8707A4E990799F5D13404442A20 (void);
// 0x00000041 System.Void BeatStomper.VRButton::.ctor()
extern void VRButton__ctor_m0A3B56E207EA6B56EC58EF84556202F196187BA3 (void);
// 0x00000042 System.Int32 BeatStomper.LevelData::get_Highscore()
extern void LevelData_get_Highscore_m2572FFE3A1F87CF7E6385159C9E0D5C161B9E73D (void);
// 0x00000043 System.Void BeatStomper.LevelData::set_Highscore(System.Int32)
extern void LevelData_set_Highscore_m4109F0FD95BBEEC422595FCB902CCD18B4AF6516 (void);
// 0x00000044 System.Void BeatStomper.LevelData::OnEnable()
extern void LevelData_OnEnable_mE6558C9F3B02E7D4AD874108E257386598F7F456 (void);
// 0x00000045 System.Void BeatStomper.LevelData::Load()
extern void LevelData_Load_m72D2FEC010DAD223B81294E1C757AE0100FA6BE1 (void);
// 0x00000046 System.Void BeatStomper.LevelData::Save()
extern void LevelData_Save_m4AA36FA2C5EF158B1B6A5FBEE71DFF304F36BE12 (void);
// 0x00000047 System.Void BeatStomper.LevelData::UpdateHighscore()
extern void LevelData_UpdateHighscore_m550E4B7B81B669676FF6F6203003C8AA2C9DD2F5 (void);
// 0x00000048 System.Void BeatStomper.LevelData::.ctor()
extern void LevelData__ctor_mBB39471DB33A2368F5B8DFC1BE9C708E15AD5F18 (void);
// 0x00000049 System.Void BeatStomper.MusicController::Awake()
extern void MusicController_Awake_m5842FDD34281242311239EE95004646AACD3B3BB (void);
// 0x0000004A System.Void BeatStomper.MusicController::Update()
extern void MusicController_Update_m62E28EE20351C09B1168417360DA6661EEB4C9FC (void);
// 0x0000004B System.Void BeatStomper.MusicController::UpdateBeat()
extern void MusicController_UpdateBeat_mE6A62B955883B44800968118A538BA62F24C4E35 (void);
// 0x0000004C System.Void BeatStomper.MusicController::.ctor()
extern void MusicController__ctor_m29CA728523FBCB22CF88C2CC204B3B0D95A80840 (void);
// 0x0000004D System.Void BeatStomper.MusicController/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m70EF91528F4625CF3804D71CCE324EA918FE85A2 (void);
// 0x0000004E System.Void BeatStomper.MusicController/<>c__DisplayClass9_0::<UpdateBeat>b__0(System.Single)
extern void U3CU3Ec__DisplayClass9_0_U3CUpdateBeatU3Eb__0_m3AC7B52282F74AFEB5C6C83DADBD6187EDA572AC (void);
// 0x0000004F System.Void BeatStomper.ScoreController::OnEnable()
extern void ScoreController_OnEnable_m20F4873CF9BD0CF8A3C7C61A4183DEB41B3C7096 (void);
// 0x00000050 System.Void BeatStomper.ScoreController::OnDisable()
extern void ScoreController_OnDisable_mDA3F18BD97F1C612A66ED21ED01BE837F8FBF418 (void);
// 0x00000051 System.Void BeatStomper.ScoreController::OnScoreChanged()
extern void ScoreController_OnScoreChanged_m73FE8D3BEB281BA7CCA470C38EE6076B1CD8CF14 (void);
// 0x00000052 System.Void BeatStomper.ScoreController::.ctor()
extern void ScoreController__ctor_m97FC5B6AB63C562F0087BB740F93797A7ED0F292 (void);
// 0x00000053 System.Single BeatStomper.TimeController::get_Time()
extern void TimeController_get_Time_mADF42903A4A10FC3BCF2C091D9B9B7206210E99F (void);
// 0x00000054 System.Void BeatStomper.TimeController::set_Time(System.Single)
extern void TimeController_set_Time_mEFD6F7166CF29EE301EF91611B3CFD4F0621AFFD (void);
// 0x00000055 System.Int32 BeatStomper.TimeController::get_Seconds()
extern void TimeController_get_Seconds_m8FA56562BE4FB01246CBD1E457D6752D3A359291 (void);
// 0x00000056 System.Int32 BeatStomper.TimeController::get_Minutes()
extern void TimeController_get_Minutes_mB29BC57DA7F16ED2855FA94E34D315EA62BE29F0 (void);
// 0x00000057 System.Int32 BeatStomper.TimeController::get_Hours()
extern void TimeController_get_Hours_m4C075B395256362FCD069671E8457AAB3F82CEA6 (void);
// 0x00000058 System.Void BeatStomper.TimeController::Update()
extern void TimeController_Update_mC0C3116DD026DA9EABDD834B79B6BDCBF36C84DB (void);
// 0x00000059 System.Void BeatStomper.TimeController::SetTimeScale(System.Single)
extern void TimeController_SetTimeScale_mA652C46D5D716A1A37925C9FC37DD29E3535ED24 (void);
// 0x0000005A System.Int32 BeatStomper.TimeController::ToMilliseconds(System.Single)
extern void TimeController_ToMilliseconds_mB67ED4001123AC4FAF261F599543BF91D7170E81 (void);
// 0x0000005B System.Int32 BeatStomper.TimeController::ToSeconds(System.Single)
extern void TimeController_ToSeconds_m70F0E689BA9E29EBEBDFE235E6BC0818911580B6 (void);
// 0x0000005C System.Int32 BeatStomper.TimeController::ToMinutes(System.Single)
extern void TimeController_ToMinutes_m400581149D4680D87BFBBBB2A7FA6B98F0A98593 (void);
// 0x0000005D System.Int32 BeatStomper.TimeController::ToHours(System.Single)
extern void TimeController_ToHours_m93609E7A33D1A8206259522127355AF070AFA723 (void);
// 0x0000005E System.Void BeatStomper.TimeController::.ctor()
extern void TimeController__ctor_mDD0BE3B85D1E5D7F250639DCDD441B1AFBB4974A (void);
// 0x0000005F System.Void BeatStomper.ISpawneable::OnSpawn()
// 0x00000060 System.Void BeatStomper.ISpawneable::OnDespawn()
// 0x00000061 System.Void BeatStomper.Spawner::SpawnAll()
extern void Spawner_SpawnAll_m83588F5243F87B9301F4C0EF45CDD38ACF128EDD (void);
// 0x00000062 System.Void BeatStomper.Spawner::DestroyAll()
extern void Spawner_DestroyAll_mFA3864B932AB7448F73B82F69103A26CF07351F5 (void);
// 0x00000063 System.Void BeatStomper.Spawner::Spawn(UnityEngine.GameObject,System.Int32)
extern void Spawner_Spawn_m5221CBF6B539DFAAEA5147E69E4B73A0E0BB8B33 (void);
// 0x00000064 System.Collections.IEnumerator BeatStomper.Spawner::SpawnCoroutine()
extern void Spawner_SpawnCoroutine_m549896FE8F0AD5E66BAB22A71859C5AA2247FA06 (void);
// 0x00000065 System.Collections.IEnumerator BeatStomper.Spawner::DestroyCoroutine()
extern void Spawner_DestroyCoroutine_m6300439302DA4A94603DB451CD02278474719315 (void);
// 0x00000066 System.Void BeatStomper.Spawner::DestroyAllInmediate()
extern void Spawner_DestroyAllInmediate_m7DFB6877DD888DFA5D9A202612775044FDD6BEA8 (void);
// 0x00000067 System.Void BeatStomper.Spawner::.ctor()
extern void Spawner__ctor_m0A241A8ACE7B1C9B32C725C5294BF528C688C2DD (void);
// 0x00000068 System.Void BeatStomper.Spawner/<SpawnCoroutine>d__8::.ctor(System.Int32)
extern void U3CSpawnCoroutineU3Ed__8__ctor_mFD1313F3B32AB15AB720B852BC14652BED23B696 (void);
// 0x00000069 System.Void BeatStomper.Spawner/<SpawnCoroutine>d__8::System.IDisposable.Dispose()
extern void U3CSpawnCoroutineU3Ed__8_System_IDisposable_Dispose_m0BB4E8AAEB33F52178252D89D5FBE31D4BB5FEBD (void);
// 0x0000006A System.Boolean BeatStomper.Spawner/<SpawnCoroutine>d__8::MoveNext()
extern void U3CSpawnCoroutineU3Ed__8_MoveNext_m6CB42CFB24272863AFEFB7C845340CFE7D5D832D (void);
// 0x0000006B System.Object BeatStomper.Spawner/<SpawnCoroutine>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnCoroutineU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mADB539B627C96166A7BA5D42F374E9D2D5524F01 (void);
// 0x0000006C System.Void BeatStomper.Spawner/<SpawnCoroutine>d__8::System.Collections.IEnumerator.Reset()
extern void U3CSpawnCoroutineU3Ed__8_System_Collections_IEnumerator_Reset_m8BC70F40D5205DA9353062F93CBC3ABD94A7847E (void);
// 0x0000006D System.Object BeatStomper.Spawner/<SpawnCoroutine>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnCoroutineU3Ed__8_System_Collections_IEnumerator_get_Current_m4FC7E752EC618871E9FDD1D27472408107BB2BA3 (void);
// 0x0000006E System.Void BeatStomper.Spawner/<DestroyCoroutine>d__9::.ctor(System.Int32)
extern void U3CDestroyCoroutineU3Ed__9__ctor_m57FF4656A8EB51BC48F56B2FC64168D12A859A14 (void);
// 0x0000006F System.Void BeatStomper.Spawner/<DestroyCoroutine>d__9::System.IDisposable.Dispose()
extern void U3CDestroyCoroutineU3Ed__9_System_IDisposable_Dispose_m6B5A6979C4B1ACCDFBC8D2E62078CD5475E04D17 (void);
// 0x00000070 System.Boolean BeatStomper.Spawner/<DestroyCoroutine>d__9::MoveNext()
extern void U3CDestroyCoroutineU3Ed__9_MoveNext_m8D643328D92BA2765AFA47EAB852C5777698D027 (void);
// 0x00000071 System.Object BeatStomper.Spawner/<DestroyCoroutine>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDestroyCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD98FDE193B09AB90ECDCEB68BCAC51245E244437 (void);
// 0x00000072 System.Void BeatStomper.Spawner/<DestroyCoroutine>d__9::System.Collections.IEnumerator.Reset()
extern void U3CDestroyCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m581161E85D7AF70C0E9FAACF3662C90C27D64497 (void);
// 0x00000073 System.Object BeatStomper.Spawner/<DestroyCoroutine>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CDestroyCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mEBFEDA69992E0957E47A514D7843E67440004F28 (void);
// 0x00000074 UnityEngine.Vector3 Antipixel.BaseObject::get_Position()
extern void BaseObject_get_Position_m7AE6BB5BE85CCD47BF7EAC2C584B1F0746929EC5 (void);
// 0x00000075 System.Void Antipixel.BaseObject::set_Position(UnityEngine.Vector3)
extern void BaseObject_set_Position_m7E1D383DB5AEF04D1ACD7A7B03EACA26E6428E36 (void);
// 0x00000076 UnityEngine.Vector3 Antipixel.BaseObject::get_LocalPosition()
extern void BaseObject_get_LocalPosition_mFFFEF286A58FE523AF682DD90F11AD9ADAF905FF (void);
// 0x00000077 System.Void Antipixel.BaseObject::set_LocalPosition(UnityEngine.Vector3)
extern void BaseObject_set_LocalPosition_mA59860CEF00056DADD8F92FFD43DB3F3D7D47E44 (void);
// 0x00000078 UnityEngine.Quaternion Antipixel.BaseObject::get_Rotation()
extern void BaseObject_get_Rotation_mFFE5AA30B1292F3DD1D0867DA83919DF46D7FFFE (void);
// 0x00000079 System.Void Antipixel.BaseObject::set_Rotation(UnityEngine.Quaternion)
extern void BaseObject_set_Rotation_mD43B9E997F0E9F1842C4E9DD53832CA390781E5C (void);
// 0x0000007A UnityEngine.Quaternion Antipixel.BaseObject::get_LocalRotation()
extern void BaseObject_get_LocalRotation_m8F279FF823BEA265BDE3A543D1811BA9E02E6018 (void);
// 0x0000007B System.Void Antipixel.BaseObject::set_LocalRotation(UnityEngine.Quaternion)
extern void BaseObject_set_LocalRotation_m3B3234A33D7495F7358D8372E1EEF88644625EA1 (void);
// 0x0000007C UnityEngine.Vector3 Antipixel.BaseObject::get_EulerAngles()
extern void BaseObject_get_EulerAngles_m4C75100AB63C4D883BE0CFD9092103B64AD8CE39 (void);
// 0x0000007D System.Void Antipixel.BaseObject::set_EulerAngles(UnityEngine.Vector3)
extern void BaseObject_set_EulerAngles_mF5EA5BA17396010995F4076E47C14AF884790198 (void);
// 0x0000007E UnityEngine.Vector3 Antipixel.BaseObject::get_LocalEulerAngles()
extern void BaseObject_get_LocalEulerAngles_m3FFF7EF3D870713E154B1A3A82BEBB9B535560BD (void);
// 0x0000007F System.Void Antipixel.BaseObject::set_LocalEulerAngles(UnityEngine.Vector3)
extern void BaseObject_set_LocalEulerAngles_m8B16C3B61365DEC43F26ADDBF840087C1C9BF7F0 (void);
// 0x00000080 UnityEngine.Vector3 Antipixel.BaseObject::get_LocalScale()
extern void BaseObject_get_LocalScale_mB6C8FFDB501BBD4BD07249224175FADAB27A6E2B (void);
// 0x00000081 System.Void Antipixel.BaseObject::set_LocalScale(UnityEngine.Vector3)
extern void BaseObject_set_LocalScale_m8ACDEB672E59314F464FF722A1DBA12EA99665C7 (void);
// 0x00000082 UnityEngine.Vector3 Antipixel.BaseObject::get_Up()
extern void BaseObject_get_Up_mA66C52F29744F085D1CF290F417199CE276E2409 (void);
// 0x00000083 UnityEngine.Vector3 Antipixel.BaseObject::get_Down()
extern void BaseObject_get_Down_m5689477F664DB189CAE7E0CECD556630ADAE1366 (void);
// 0x00000084 UnityEngine.Vector3 Antipixel.BaseObject::get_Right()
extern void BaseObject_get_Right_m7E1F1CF78C8670C35D87D85A00B481A822E84955 (void);
// 0x00000085 UnityEngine.Vector3 Antipixel.BaseObject::get_Left()
extern void BaseObject_get_Left_m1EF5D3BD56D0D9D3C4B749E63A931DD87F511B37 (void);
// 0x00000086 UnityEngine.Vector3 Antipixel.BaseObject::get_Forward()
extern void BaseObject_get_Forward_m7FE88E21064598941612F011110C794EA86C0E06 (void);
// 0x00000087 UnityEngine.Vector3 Antipixel.BaseObject::get_Backward()
extern void BaseObject_get_Backward_m2154BA82F184B6E33A95914B8229941627137BC7 (void);
// 0x00000088 UnityEngine.Matrix4x4 Antipixel.BaseObject::get_LocalToWorldMatrix()
extern void BaseObject_get_LocalToWorldMatrix_m7A4A6B908F53320AF255770075B0FE046C9974C9 (void);
// 0x00000089 UnityEngine.Matrix4x4 Antipixel.BaseObject::get_WorldToLocalMatrix()
extern void BaseObject_get_WorldToLocalMatrix_m1B96B14434F54B7CB0D745CB548DCD116D7FB1F9 (void);
// 0x0000008A UnityEngine.Transform Antipixel.BaseObject::get_Parent()
extern void BaseObject_get_Parent_mA292DBB33B6A0AA9814BDE3A3CFD0B4709F97D21 (void);
// 0x0000008B System.Void Antipixel.BaseObject::set_Parent(UnityEngine.Transform)
extern void BaseObject_set_Parent_mC6EABFF6CC009C6DAF9A539FEE72E2835028C0C0 (void);
// 0x0000008C System.Single Antipixel.BaseObject::get_DeltaTime()
extern void BaseObject_get_DeltaTime_m7EEF23BEA3DE2EFCFA6921482E348D68061B2512 (void);
// 0x0000008D System.Single Antipixel.BaseObject::get_FixedDeltaTime()
extern void BaseObject_get_FixedDeltaTime_m0AECE944F2F2092516EBBD737AA81B6E4D510619 (void);
// 0x0000008E System.Single Antipixel.BaseObject::get_TimeScale()
extern void BaseObject_get_TimeScale_m6C94F720CDBA8D697FCEA43DB76033EC4C27D393 (void);
// 0x0000008F System.Void Antipixel.BaseObject::set_TimeScale(System.Single)
extern void BaseObject_set_TimeScale_m31FE6FC2D09D950DBA500ACE423047A60697C879 (void);
// 0x00000090 System.Void Antipixel.BaseObject::OnEnable()
extern void BaseObject_OnEnable_mAAEC97877F51526B7B4254A12E56AFA127398F11 (void);
// 0x00000091 System.Void Antipixel.BaseObject::OnDisable()
extern void BaseObject_OnDisable_mCF640D0075473EE81BC0CB8DB871F3F4BB5BC309 (void);
// 0x00000092 System.Void Antipixel.BaseObject::OnDestroy()
extern void BaseObject_OnDestroy_mCA33A7FBEA0BFB8E57356896278762025AFE0BFA (void);
// 0x00000093 System.Void Antipixel.BaseObject::StartCoroutines(System.Collections.IEnumerator[])
extern void BaseObject_StartCoroutines_mBCE8DB63DA71A918AEC8B7699C5DF55CC03BBFB5 (void);
// 0x00000094 System.Void Antipixel.BaseObject::StopCoroutines(System.Collections.IEnumerator[])
extern void BaseObject_StopCoroutines_m8CC10400B83317C0C06FEB3C7C9A07835E9E8C6B (void);
// 0x00000095 System.Void Antipixel.BaseObject::Destroy(System.Single)
extern void BaseObject_Destroy_mCD035DDCB058CA7CABCB664E068B453165F94CA1 (void);
// 0x00000096 System.Void Antipixel.BaseObject::DestroyComponent(System.Single)
extern void BaseObject_DestroyComponent_mD4BEB2ADDD4DF961DB7998CDE330E91E2F0EDB3E (void);
// 0x00000097 System.Void Antipixel.BaseObject::Print(System.String)
extern void BaseObject_Print_mC5715684D6CCC739DA6FFA99439436052FC555D7 (void);
// 0x00000098 System.Void Antipixel.BaseObject::StartEditorCoroutine(System.Collections.IEnumerator)
extern void BaseObject_StartEditorCoroutine_mBE60871B4C4B357673389EE1E009FB64173D45AF (void);
// 0x00000099 System.Void Antipixel.BaseObject::StopAllEditorCoroutines()
extern void BaseObject_StopAllEditorCoroutines_m5C95986F2F04FD440A11FB4A83D0274E097AA193 (void);
// 0x0000009A System.Void Antipixel.BaseObject::RunEditor(Antipixel.VoidDelegate,Antipixel.VoidDelegate)
extern void BaseObject_RunEditor_m60311491B0A8355FFEBADA2B99932C3B3B7BC443 (void);
// 0x0000009B System.Void Antipixel.BaseObject::.ctor()
extern void BaseObject__ctor_m47A4666F079EBAC9ECD7953A322CFAD68FF22E85 (void);
// 0x0000009C UnityEngine.Collider[] Antipixel.CollisionShape::get_Collider3Ds()
extern void CollisionShape_get_Collider3Ds_m3B1665EFB9A0842AA9165F01D75180104EB8C5BE (void);
// 0x0000009D System.Void Antipixel.CollisionShape::set_Collider3Ds(UnityEngine.Collider[])
extern void CollisionShape_set_Collider3Ds_m1AFDAE818DEE62300CC1B3CDC7DFB3BED593ABF4 (void);
// 0x0000009E UnityEngine.Collider2D[] Antipixel.CollisionShape::get_Collider2Ds()
extern void CollisionShape_get_Collider2Ds_mAE9734F5A82AD0A329CE26024BF9933AD710BA6A (void);
// 0x0000009F System.Void Antipixel.CollisionShape::set_Collider2Ds(UnityEngine.Collider2D[])
extern void CollisionShape_set_Collider2Ds_mF8CEBB4D0E430D3CF05648F0AB850A693B5DAD08 (void);
// 0x000000A0 UnityEngine.GameObject Antipixel.CollisionShape::GetObject(T)
// 0x000000A1 System.Boolean Antipixel.CollisionShape::IsCollider3D(T)
// 0x000000A2 System.Boolean Antipixel.CollisionShape::IsCollider2D(T)
// 0x000000A3 System.Void Antipixel.CollisionShape::.ctor()
extern void CollisionShape__ctor_mE82546223CE3E1F315A4ED40288EF1F368EC7C73 (void);
// 0x000000A4 System.Void Antipixel.Drawing::.ctor()
extern void Drawing__ctor_m09C827CAA867C9FB7D8B8655E63C738CDA97029C (void);
// 0x000000A5 UnityEngine.Color Antipixel.Colors::get_Black()
extern void Colors_get_Black_mAA43214B64EB771E63E91EE7E9599E2E0A11FC80 (void);
// 0x000000A6 System.Void Antipixel.Colors::set_Black(UnityEngine.Color)
extern void Colors_set_Black_m9E303B4234A56F5CD0197FF9897B983560774886 (void);
// 0x000000A7 UnityEngine.Color Antipixel.Colors::get_DarkBlue()
extern void Colors_get_DarkBlue_m8DA4C1F9CBC8876B06B560F692F0831F6345DAEE (void);
// 0x000000A8 System.Void Antipixel.Colors::set_DarkBlue(UnityEngine.Color)
extern void Colors_set_DarkBlue_mBAC57C2DC67AC8B0347BDB973978785F1678EC14 (void);
// 0x000000A9 UnityEngine.Color Antipixel.Colors::get_DarkGreen()
extern void Colors_get_DarkGreen_mE6CDB2C12ED02884C481D3BAF1F72BAC600488BB (void);
// 0x000000AA System.Void Antipixel.Colors::set_DarkGreen(UnityEngine.Color)
extern void Colors_set_DarkGreen_mD5AA93D92E07A08BE6E682548086EB0820B86FB6 (void);
// 0x000000AB UnityEngine.Color Antipixel.Colors::get_DarkAqua()
extern void Colors_get_DarkAqua_m4459622E91F8B377D8D5B05D8EFEB3AE9BDFA07C (void);
// 0x000000AC System.Void Antipixel.Colors::set_DarkAqua(UnityEngine.Color)
extern void Colors_set_DarkAqua_mE14F05D00B594DAA31F6C81B46898A7DD8638299 (void);
// 0x000000AD UnityEngine.Color Antipixel.Colors::get_DarkRed()
extern void Colors_get_DarkRed_m16C2014D3A2802DC6DD092DED66B4CBAAFAFD168 (void);
// 0x000000AE System.Void Antipixel.Colors::set_DarkRed(UnityEngine.Color)
extern void Colors_set_DarkRed_mB1216D5C094AEB9BD4F8B12CFFCFE6742B73DA1C (void);
// 0x000000AF UnityEngine.Color Antipixel.Colors::get_DarkPurple()
extern void Colors_get_DarkPurple_m8C8C52E33789C5ADF3BEA2194F801B9582BF569B (void);
// 0x000000B0 System.Void Antipixel.Colors::set_DarkPurple(UnityEngine.Color)
extern void Colors_set_DarkPurple_mF9F855C820B992667C9F465994F2E53E3B27EE85 (void);
// 0x000000B1 UnityEngine.Color Antipixel.Colors::get_Gold()
extern void Colors_get_Gold_m746E9979E1BA38E14C359340DB2C31DECD25830C (void);
// 0x000000B2 System.Void Antipixel.Colors::set_Gold(UnityEngine.Color)
extern void Colors_set_Gold_m55DF49C197C4B7CE5169F28E327E8EBCCCD94DB8 (void);
// 0x000000B3 UnityEngine.Color Antipixel.Colors::get_Gray()
extern void Colors_get_Gray_mF8233FB0AA5C8525006ECFC4244624DA321EB2DE (void);
// 0x000000B4 System.Void Antipixel.Colors::set_Gray(UnityEngine.Color)
extern void Colors_set_Gray_m3D5F0D102AC9B1EE88DE051F78CE60D11F8D31A2 (void);
// 0x000000B5 UnityEngine.Color Antipixel.Colors::get_DarkGray()
extern void Colors_get_DarkGray_m13B0103AEC08C4A8877BC17C1221EA2D347516BB (void);
// 0x000000B6 System.Void Antipixel.Colors::set_DarkGray(UnityEngine.Color)
extern void Colors_set_DarkGray_mD084F5B587B5EDA7A9787747E78705EC4C2DA8AD (void);
// 0x000000B7 UnityEngine.Color Antipixel.Colors::get_Blue()
extern void Colors_get_Blue_m616240B6A1145A2B771AC140E5C18C530A83DB8F (void);
// 0x000000B8 System.Void Antipixel.Colors::set_Blue(UnityEngine.Color)
extern void Colors_set_Blue_m9F4079D5C6866780DCAF058D8853DC5EA7C59829 (void);
// 0x000000B9 UnityEngine.Color Antipixel.Colors::get_Green()
extern void Colors_get_Green_m17BAC4AE541DEB4D615F8DEA46DA076E06875CE6 (void);
// 0x000000BA System.Void Antipixel.Colors::set_Green(UnityEngine.Color)
extern void Colors_set_Green_mA92664CE44C2759E464DEA4006F1FC05ED0F7701 (void);
// 0x000000BB UnityEngine.Color Antipixel.Colors::get_Aqua()
extern void Colors_get_Aqua_m892EB4654255457958BB0B3048B20944844D93A9 (void);
// 0x000000BC System.Void Antipixel.Colors::set_Aqua(UnityEngine.Color)
extern void Colors_set_Aqua_m0BEB1267F6AAA73B2131D10894F9204DF1F6AC86 (void);
// 0x000000BD UnityEngine.Color Antipixel.Colors::get_Red()
extern void Colors_get_Red_mFFD3D6960EADDC9B5C6F040C34EDBCEE4F794B46 (void);
// 0x000000BE System.Void Antipixel.Colors::set_Red(UnityEngine.Color)
extern void Colors_set_Red_m25C2338D066C18BA0D592069DD25349B753D3A1B (void);
// 0x000000BF UnityEngine.Color Antipixel.Colors::get_LightPurple()
extern void Colors_get_LightPurple_mE52D43E2B72D28197B362D188A81BE9D2DADE22A (void);
// 0x000000C0 System.Void Antipixel.Colors::set_LightPurple(UnityEngine.Color)
extern void Colors_set_LightPurple_m225FBECE0B2B2EE07B94DF923B764495B33675E7 (void);
// 0x000000C1 UnityEngine.Color Antipixel.Colors::get_Yellow()
extern void Colors_get_Yellow_m0ECD8AE27E256F35C20B31606FE4A8014A50435E (void);
// 0x000000C2 System.Void Antipixel.Colors::set_Yellow(UnityEngine.Color)
extern void Colors_set_Yellow_mB22D624E1720AF8E55B12A07535B36FF12645B40 (void);
// 0x000000C3 UnityEngine.Color Antipixel.Colors::get_White()
extern void Colors_get_White_mF9CC4AE9901408F590F58BF6D909A81DF4C1C1AA (void);
// 0x000000C4 System.Void Antipixel.Colors::set_White(UnityEngine.Color)
extern void Colors_set_White_mA6F60FAC4252EB2DB23A4C64D4EF9303EFD286EF (void);
// 0x000000C5 System.Void Antipixel.Colors::.cctor()
extern void Colors__cctor_m765004F428930D4FFDCC04FF16F6947414EFC7FE (void);
// 0x000000C6 System.Void Antipixel.CustomEvent::Invoke()
extern void CustomEvent_Invoke_m014EA27E7156075411591D61E2A4BBDD5EC387F2 (void);
// 0x000000C7 System.Void Antipixel.CustomEvent::Add(UnityEngine.Events.UnityAction)
extern void CustomEvent_Add_mD5C2D900F70B02F0A57A119F02171137AD2955B3 (void);
// 0x000000C8 System.Void Antipixel.CustomEvent::Remove(UnityEngine.Events.UnityAction)
extern void CustomEvent_Remove_mD880E0C9BA5956F20D593E38D26D7F814AEC348E (void);
// 0x000000C9 System.Void Antipixel.CustomEvent::RemoveAll()
extern void CustomEvent_RemoveAll_m1CA74C54DC3E71461A8D3D9547C83CD7BB7E8A6B (void);
// 0x000000CA System.Void Antipixel.CustomEvent::.ctor()
extern void CustomEvent__ctor_mB49879EC16C85491C6862F3E939259B16D306C8B (void);
// 0x000000CB System.Void Antipixel.CustomEvent`1::Invoke(T)
// 0x000000CC System.Void Antipixel.CustomEvent`1::Add(UnityEngine.Events.UnityAction`1<T>)
// 0x000000CD System.Void Antipixel.CustomEvent`1::Remove(UnityEngine.Events.UnityAction`1<T>)
// 0x000000CE System.Void Antipixel.CustomEvent`1::RemoveAll()
// 0x000000CF System.Void Antipixel.CustomEvent`1::.ctor()
// 0x000000D0 System.Void Antipixel.CustomEvent`2::Invoke(T1,T2)
// 0x000000D1 System.Void Antipixel.CustomEvent`2::Add(UnityEngine.Events.UnityAction`2<T1,T2>)
// 0x000000D2 System.Void Antipixel.CustomEvent`2::Remove(UnityEngine.Events.UnityAction`2<T1,T2>)
// 0x000000D3 System.Void Antipixel.CustomEvent`2::RemoveAll()
// 0x000000D4 System.Void Antipixel.CustomEvent`2::.ctor()
// 0x000000D5 System.Void Antipixel.CustomEvent`3::Invoke(T1,T2,T3)
// 0x000000D6 System.Void Antipixel.CustomEvent`3::Add(UnityEngine.Events.UnityAction`3<T1,T2,T3>)
// 0x000000D7 System.Void Antipixel.CustomEvent`3::Remove(UnityEngine.Events.UnityAction`3<T1,T2,T3>)
// 0x000000D8 System.Void Antipixel.CustomEvent`3::RemoveAll()
// 0x000000D9 System.Void Antipixel.CustomEvent`3::.ctor()
// 0x000000DA System.Void Antipixel.VoidDelegate::.ctor(System.Object,System.IntPtr)
extern void VoidDelegate__ctor_m7950233DE26ED051442B194D37AB825F3DCEABC3 (void);
// 0x000000DB System.Void Antipixel.VoidDelegate::Invoke()
extern void VoidDelegate_Invoke_m6096ED17DE0AC6CD36268570A59BD0EC5FC83F21 (void);
// 0x000000DC System.IAsyncResult Antipixel.VoidDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void VoidDelegate_BeginInvoke_m12499E8AE27BE16D896A952166CF1600D057A707 (void);
// 0x000000DD System.Void Antipixel.VoidDelegate::EndInvoke(System.IAsyncResult)
extern void VoidDelegate_EndInvoke_m30CFE9257A2E32980561C4E0158D864E1301AA13 (void);
// 0x000000DE System.Void Antipixel.BoolDelegate::.ctor(System.Object,System.IntPtr)
extern void BoolDelegate__ctor_mEC48165B85F15F4A91A18BA56B0D601EC177B92C (void);
// 0x000000DF System.Void Antipixel.BoolDelegate::Invoke(System.Boolean)
extern void BoolDelegate_Invoke_m42FDBE077B7C59544E0295CE0D9A8178A7DBAB18 (void);
// 0x000000E0 System.IAsyncResult Antipixel.BoolDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void BoolDelegate_BeginInvoke_m532E96CD6930ABE5AF7021E0C554527471DAEEB4 (void);
// 0x000000E1 System.Void Antipixel.BoolDelegate::EndInvoke(System.IAsyncResult)
extern void BoolDelegate_EndInvoke_mCAEE4CAB9769815B8B1CDC70731EC656065ACB83 (void);
// 0x000000E2 System.Void Antipixel.BoolArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void BoolArrayDelegate__ctor_mBE7388BB27A21BFE987542AB42E2BF85A008B6B0 (void);
// 0x000000E3 System.Void Antipixel.BoolArrayDelegate::Invoke(System.Boolean[])
extern void BoolArrayDelegate_Invoke_mA11AD0B332BC5E3B5C24C71B8CAFE20D1E2275A1 (void);
// 0x000000E4 System.IAsyncResult Antipixel.BoolArrayDelegate::BeginInvoke(System.Boolean[],System.AsyncCallback,System.Object)
extern void BoolArrayDelegate_BeginInvoke_m9C669BD67A84C61F56DD323292A03E016BCCB02B (void);
// 0x000000E5 System.Void Antipixel.BoolArrayDelegate::EndInvoke(System.IAsyncResult)
extern void BoolArrayDelegate_EndInvoke_mA9F2B229FC55C7598F47250453EAAF78A7AC139D (void);
// 0x000000E6 System.Void Antipixel.IntDelegate::.ctor(System.Object,System.IntPtr)
extern void IntDelegate__ctor_mB66123A448E9716B3472483E8EDBDE50295A3D66 (void);
// 0x000000E7 System.Void Antipixel.IntDelegate::Invoke(System.Int32)
extern void IntDelegate_Invoke_m3A5BCA3E9D512370EF335A2A799D998F8A37A914 (void);
// 0x000000E8 System.IAsyncResult Antipixel.IntDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void IntDelegate_BeginInvoke_m17DDFA43456E5E178CC51788A8F179D72F2F743B (void);
// 0x000000E9 System.Void Antipixel.IntDelegate::EndInvoke(System.IAsyncResult)
extern void IntDelegate_EndInvoke_mED24C739361754D3CD350BBD13ACC8A0CA99897F (void);
// 0x000000EA System.Void Antipixel.IntArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void IntArrayDelegate__ctor_m7B9F16F3B20BE5F03853B30FEB0F2B7149F44769 (void);
// 0x000000EB System.Void Antipixel.IntArrayDelegate::Invoke(System.Int32[])
extern void IntArrayDelegate_Invoke_m7E55C92EA3FD93FEBF6FD7345283E3B59700156C (void);
// 0x000000EC System.IAsyncResult Antipixel.IntArrayDelegate::BeginInvoke(System.Int32[],System.AsyncCallback,System.Object)
extern void IntArrayDelegate_BeginInvoke_mBFAA3380C107477ADB3B46990B8F3EC2E6D52654 (void);
// 0x000000ED System.Void Antipixel.IntArrayDelegate::EndInvoke(System.IAsyncResult)
extern void IntArrayDelegate_EndInvoke_mA37FE2661919D8BC59302F215C1BC0A5B2F1FF20 (void);
// 0x000000EE System.Void Antipixel.FloatDelegate::.ctor(System.Object,System.IntPtr)
extern void FloatDelegate__ctor_mB46C30EBCF6D5E0F19DE50275398CC00952FC67D (void);
// 0x000000EF System.Void Antipixel.FloatDelegate::Invoke(System.Single)
extern void FloatDelegate_Invoke_m8EFC1624F9C80D660DB698FE428E33A7E5A7E99C (void);
// 0x000000F0 System.IAsyncResult Antipixel.FloatDelegate::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern void FloatDelegate_BeginInvoke_m1A951666FADDD7978E25AFB4FAF53E3CAE278A68 (void);
// 0x000000F1 System.Void Antipixel.FloatDelegate::EndInvoke(System.IAsyncResult)
extern void FloatDelegate_EndInvoke_m8286B1661AF0172D10D2543BD4144828C43203C5 (void);
// 0x000000F2 System.Void Antipixel.FloatArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void FloatArrayDelegate__ctor_m7D53F48DCE46003A51E86613848B984E3E5AD50F (void);
// 0x000000F3 System.Void Antipixel.FloatArrayDelegate::Invoke(System.Single[])
extern void FloatArrayDelegate_Invoke_m76C354CC0AAEE7867B76947ED05CF5A13520165C (void);
// 0x000000F4 System.IAsyncResult Antipixel.FloatArrayDelegate::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern void FloatArrayDelegate_BeginInvoke_m369658D336E2B19B4D4EEC8A7CA870C0C8B93A79 (void);
// 0x000000F5 System.Void Antipixel.FloatArrayDelegate::EndInvoke(System.IAsyncResult)
extern void FloatArrayDelegate_EndInvoke_m6F76435C32C1A52F68D71F859FF77FAF37C72C14 (void);
// 0x000000F6 System.Void Antipixel.StringDelegate::.ctor(System.Object,System.IntPtr)
extern void StringDelegate__ctor_mA585DDB44B4299CE6AD537E8FB411C2C83150814 (void);
// 0x000000F7 System.Void Antipixel.StringDelegate::Invoke(System.String)
extern void StringDelegate_Invoke_m9DAE66215C755173DC8AB9866035A02B186F08A3 (void);
// 0x000000F8 System.IAsyncResult Antipixel.StringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void StringDelegate_BeginInvoke_m8DA504D8DD817639D1B9D78EABD5E0F4529B889F (void);
// 0x000000F9 System.Void Antipixel.StringDelegate::EndInvoke(System.IAsyncResult)
extern void StringDelegate_EndInvoke_m10598AC661815E120F22B609C3FBB38A05DEE46D (void);
// 0x000000FA System.Void Antipixel.StringArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void StringArrayDelegate__ctor_m19F078B326B837F13B8755ABE24F761193EE79BC (void);
// 0x000000FB System.Void Antipixel.StringArrayDelegate::Invoke(System.String[])
extern void StringArrayDelegate_Invoke_mA87EDDB2C81B80136B4C2AE9BE9FC2E6CF5D2EDE (void);
// 0x000000FC System.IAsyncResult Antipixel.StringArrayDelegate::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern void StringArrayDelegate_BeginInvoke_mA7B86158C7A4D8F73B13BE537D5C12EBC7520403 (void);
// 0x000000FD System.Void Antipixel.StringArrayDelegate::EndInvoke(System.IAsyncResult)
extern void StringArrayDelegate_EndInvoke_mB5E33A5A40ECA89FFA5C32719077F8966AF501CE (void);
// 0x000000FE System.Void Antipixel.Vector2Delegate::.ctor(System.Object,System.IntPtr)
extern void Vector2Delegate__ctor_m5929CE6BA421E01AFEA8E9CC7247336D15672ED0 (void);
// 0x000000FF System.Void Antipixel.Vector2Delegate::Invoke(UnityEngine.Vector2)
extern void Vector2Delegate_Invoke_m40084D19AB1810DD18A2DE85971D81C44F090186 (void);
// 0x00000100 System.IAsyncResult Antipixel.Vector2Delegate::BeginInvoke(UnityEngine.Vector2,System.AsyncCallback,System.Object)
extern void Vector2Delegate_BeginInvoke_m9FC2D224DF30AC033ACE6A8FA91C1C2582D8DA53 (void);
// 0x00000101 System.Void Antipixel.Vector2Delegate::EndInvoke(System.IAsyncResult)
extern void Vector2Delegate_EndInvoke_mC33210574438DB1D37B3A9345FF3FBCF904A417E (void);
// 0x00000102 System.Void Antipixel.Vector2ArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void Vector2ArrayDelegate__ctor_mBBEF38BCBBCCCFF48969BD2BA206DA2151DA46F9 (void);
// 0x00000103 System.Void Antipixel.Vector2ArrayDelegate::Invoke(UnityEngine.Vector2[])
extern void Vector2ArrayDelegate_Invoke_m0408E9D9EA55359646D32F3C2F93E6E8800D8E59 (void);
// 0x00000104 System.IAsyncResult Antipixel.Vector2ArrayDelegate::BeginInvoke(UnityEngine.Vector2[],System.AsyncCallback,System.Object)
extern void Vector2ArrayDelegate_BeginInvoke_mE2CDB9719F54880CAEC34D021C3BE4EE67FDA791 (void);
// 0x00000105 System.Void Antipixel.Vector2ArrayDelegate::EndInvoke(System.IAsyncResult)
extern void Vector2ArrayDelegate_EndInvoke_m59B4666ED75A8A83D98097F74D88BAB0E741EC4D (void);
// 0x00000106 System.Void Antipixel.Vector2IntDelegate::.ctor(System.Object,System.IntPtr)
extern void Vector2IntDelegate__ctor_m4648FE29D7715F80CC3EC50C59A4B1DCDDCE28F2 (void);
// 0x00000107 System.Void Antipixel.Vector2IntDelegate::Invoke(UnityEngine.Vector2Int)
extern void Vector2IntDelegate_Invoke_m7A24D3F523DA17A96A69F5159D6213E463BE2261 (void);
// 0x00000108 System.IAsyncResult Antipixel.Vector2IntDelegate::BeginInvoke(UnityEngine.Vector2Int,System.AsyncCallback,System.Object)
extern void Vector2IntDelegate_BeginInvoke_mBF7E66FC2EB86046F34D3FD6CC58990130DC8ABC (void);
// 0x00000109 System.Void Antipixel.Vector2IntDelegate::EndInvoke(System.IAsyncResult)
extern void Vector2IntDelegate_EndInvoke_m45E015471892AAD6BC5383E7CCD3C494A1F2743C (void);
// 0x0000010A System.Void Antipixel.Vector2IntArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void Vector2IntArrayDelegate__ctor_m8102BAB394D6BD23A34B4EFE6E33173715DA0CDE (void);
// 0x0000010B System.Void Antipixel.Vector2IntArrayDelegate::Invoke(UnityEngine.Vector2Int[])
extern void Vector2IntArrayDelegate_Invoke_m9A76FF54455B5D9143B4D847BFA8C692FBBFBE8C (void);
// 0x0000010C System.IAsyncResult Antipixel.Vector2IntArrayDelegate::BeginInvoke(UnityEngine.Vector2Int[],System.AsyncCallback,System.Object)
extern void Vector2IntArrayDelegate_BeginInvoke_m83BAC5A6B81E7CA5F684A7F2C106F60CEAF9887E (void);
// 0x0000010D System.Void Antipixel.Vector2IntArrayDelegate::EndInvoke(System.IAsyncResult)
extern void Vector2IntArrayDelegate_EndInvoke_m80F4F7624D3DC592F9D29F4F5261230CDE2EA37E (void);
// 0x0000010E System.Void Antipixel.Vector3Delegate::.ctor(System.Object,System.IntPtr)
extern void Vector3Delegate__ctor_mABF3B11E697E9CB84440D16D34457AF79EC6AB18 (void);
// 0x0000010F System.Void Antipixel.Vector3Delegate::Invoke(UnityEngine.Vector3)
extern void Vector3Delegate_Invoke_m2E1FE499B528540B997ED3ED28F45D10FC4B3ACF (void);
// 0x00000110 System.IAsyncResult Antipixel.Vector3Delegate::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern void Vector3Delegate_BeginInvoke_m64E7C53CA5D3F750F0C7F8814A5F1A53F7E28101 (void);
// 0x00000111 System.Void Antipixel.Vector3Delegate::EndInvoke(System.IAsyncResult)
extern void Vector3Delegate_EndInvoke_mBC52FE53D1D3901016ACF2F250F6A6B46043C1A9 (void);
// 0x00000112 System.Void Antipixel.Vector3ArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void Vector3ArrayDelegate__ctor_mCCED617103C6C75CF3E35BBF269254714018DA7E (void);
// 0x00000113 System.Void Antipixel.Vector3ArrayDelegate::Invoke(UnityEngine.Vector3[])
extern void Vector3ArrayDelegate_Invoke_m6F3ABDC2368326546F178AE8D79C0A259A092610 (void);
// 0x00000114 System.IAsyncResult Antipixel.Vector3ArrayDelegate::BeginInvoke(UnityEngine.Vector3[],System.AsyncCallback,System.Object)
extern void Vector3ArrayDelegate_BeginInvoke_m0CDE8CE73B87DDCBB08F9D606761FC4F09B24CB2 (void);
// 0x00000115 System.Void Antipixel.Vector3ArrayDelegate::EndInvoke(System.IAsyncResult)
extern void Vector3ArrayDelegate_EndInvoke_m468A2DBA283096D682C1342DA56BB728CAC13EDD (void);
// 0x00000116 System.Void Antipixel.Vector3IntDelegate::.ctor(System.Object,System.IntPtr)
extern void Vector3IntDelegate__ctor_m87374C0012EEB6DCC65D7908C909016B663096BB (void);
// 0x00000117 System.Void Antipixel.Vector3IntDelegate::Invoke(UnityEngine.Vector3Int)
extern void Vector3IntDelegate_Invoke_m071BB8F2EE2C8E9A522870892BB622554EEC7FD4 (void);
// 0x00000118 System.IAsyncResult Antipixel.Vector3IntDelegate::BeginInvoke(UnityEngine.Vector3Int,System.AsyncCallback,System.Object)
extern void Vector3IntDelegate_BeginInvoke_m43FF9548E554CE2EFF997F070D4983C419574E46 (void);
// 0x00000119 System.Void Antipixel.Vector3IntDelegate::EndInvoke(System.IAsyncResult)
extern void Vector3IntDelegate_EndInvoke_m14506CD00EFDFEE2CA0CBCFEFFC6AB73EA5584F8 (void);
// 0x0000011A System.Void Antipixel.Vector3IntArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void Vector3IntArrayDelegate__ctor_mCDE0F7AC584D8699F9E08E6173189B9085EEDD03 (void);
// 0x0000011B System.Void Antipixel.Vector3IntArrayDelegate::Invoke(UnityEngine.Vector3Int[])
extern void Vector3IntArrayDelegate_Invoke_mE9B9FFCDBA03B3AF41772E30BFD6F22AF51DA803 (void);
// 0x0000011C System.IAsyncResult Antipixel.Vector3IntArrayDelegate::BeginInvoke(UnityEngine.Vector3Int[],System.AsyncCallback,System.Object)
extern void Vector3IntArrayDelegate_BeginInvoke_mC0B471935451CE04EE16174A20B063DE5FE24B1B (void);
// 0x0000011D System.Void Antipixel.Vector3IntArrayDelegate::EndInvoke(System.IAsyncResult)
extern void Vector3IntArrayDelegate_EndInvoke_m1195C8A98AA0B6616F8475CA156F8C186F51CD31 (void);
// 0x0000011E System.Void Antipixel.TransformDelegate::.ctor(System.Object,System.IntPtr)
extern void TransformDelegate__ctor_m0F84912797F2FCFEB440CF71A65580A478B5EF4F (void);
// 0x0000011F System.Void Antipixel.TransformDelegate::Invoke(UnityEngine.Transform)
extern void TransformDelegate_Invoke_mB72C8967B05B61A8A00AF506C76820DCF7A4F821 (void);
// 0x00000120 System.IAsyncResult Antipixel.TransformDelegate::BeginInvoke(UnityEngine.Transform,System.AsyncCallback,System.Object)
extern void TransformDelegate_BeginInvoke_m54D6BF951C1888F5F65AA38492C43CA94E267332 (void);
// 0x00000121 System.Void Antipixel.TransformDelegate::EndInvoke(System.IAsyncResult)
extern void TransformDelegate_EndInvoke_mCD2FCCD308D9EFE4B79A6983446C22ED58533E96 (void);
// 0x00000122 System.Void Antipixel.TransformArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void TransformArrayDelegate__ctor_mCAD18F5418E37E57BFFF0CF8AFA0D5D7A3226587 (void);
// 0x00000123 System.Void Antipixel.TransformArrayDelegate::Invoke(UnityEngine.Transform[])
extern void TransformArrayDelegate_Invoke_m88EFE1E75448407EF7E62E636383BA10CFACA56A (void);
// 0x00000124 System.IAsyncResult Antipixel.TransformArrayDelegate::BeginInvoke(UnityEngine.Transform[],System.AsyncCallback,System.Object)
extern void TransformArrayDelegate_BeginInvoke_m26F4181AADACD9290933A1DD285978007489734E (void);
// 0x00000125 System.Void Antipixel.TransformArrayDelegate::EndInvoke(System.IAsyncResult)
extern void TransformArrayDelegate_EndInvoke_m4BCA72D8BBEA73D8CB812CF8E0CFFF84B9F0F894 (void);
// 0x00000126 System.Void Antipixel.GameObjectDelegate::.ctor(System.Object,System.IntPtr)
extern void GameObjectDelegate__ctor_m092E78062C3B8435F1697BDFCCB11493753D1EB0 (void);
// 0x00000127 System.Void Antipixel.GameObjectDelegate::Invoke(UnityEngine.GameObject)
extern void GameObjectDelegate_Invoke_mA4400413FFBE6065A4746D0F1030E79583DCB024 (void);
// 0x00000128 System.IAsyncResult Antipixel.GameObjectDelegate::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern void GameObjectDelegate_BeginInvoke_m07E72E1F7C0595789B68C9944A7CE75F56C9D96E (void);
// 0x00000129 System.Void Antipixel.GameObjectDelegate::EndInvoke(System.IAsyncResult)
extern void GameObjectDelegate_EndInvoke_m1492660531B2A720A29566D1F9B86E2CCB9B6C28 (void);
// 0x0000012A System.Void Antipixel.GameObjectArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void GameObjectArrayDelegate__ctor_mFBB3D98DECA01FFACBF90201BEC91BCA03CB4CE7 (void);
// 0x0000012B System.Void Antipixel.GameObjectArrayDelegate::Invoke(UnityEngine.GameObject[])
extern void GameObjectArrayDelegate_Invoke_mC0B0785A0EDF48F5828B8AB450E1B681F4731537 (void);
// 0x0000012C System.IAsyncResult Antipixel.GameObjectArrayDelegate::BeginInvoke(UnityEngine.GameObject[],System.AsyncCallback,System.Object)
extern void GameObjectArrayDelegate_BeginInvoke_mE8EE1196D3601A095C1FB843BED62B43EAEEBE01 (void);
// 0x0000012D System.Void Antipixel.GameObjectArrayDelegate::EndInvoke(System.IAsyncResult)
extern void GameObjectArrayDelegate_EndInvoke_m36F74F779EE19B4448C644BE237EDDA9BF174F70 (void);
// 0x0000012E System.Void Antipixel.ObjectDelegate::.ctor(System.Object,System.IntPtr)
extern void ObjectDelegate__ctor_mB7CD12C819B3106338279C57810CE13001FA6436 (void);
// 0x0000012F System.Void Antipixel.ObjectDelegate::Invoke(System.Object)
extern void ObjectDelegate_Invoke_m51A1D39A91F655ED91264002A3421158449E1032 (void);
// 0x00000130 System.IAsyncResult Antipixel.ObjectDelegate::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void ObjectDelegate_BeginInvoke_m62BE701F20E6EE510C962CCCC1ECD50A4D6D42FE (void);
// 0x00000131 System.Void Antipixel.ObjectDelegate::EndInvoke(System.IAsyncResult)
extern void ObjectDelegate_EndInvoke_mBAA3916B3D5846DBD8C6395EF89FEE83A9713E80 (void);
// 0x00000132 System.Void Antipixel.ObjectArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void ObjectArrayDelegate__ctor_m4D56E525595BC926599D9E81B3C95998CEB8E72F (void);
// 0x00000133 System.Void Antipixel.ObjectArrayDelegate::Invoke(System.Object[])
extern void ObjectArrayDelegate_Invoke_mFA894DBE1F767B5D0936039F950C3C2090359A13 (void);
// 0x00000134 System.IAsyncResult Antipixel.ObjectArrayDelegate::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
extern void ObjectArrayDelegate_BeginInvoke_m18BF3734C385C2983B42607835BAEEEBFAC5938F (void);
// 0x00000135 System.Void Antipixel.ObjectArrayDelegate::EndInvoke(System.IAsyncResult)
extern void ObjectArrayDelegate_EndInvoke_m906802594040CFD5F89D47A787F627BD5ABDA008 (void);
// 0x00000136 System.Void Antipixel.GenericDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x00000137 System.Void Antipixel.GenericDelegate`1::Invoke(T)
// 0x00000138 System.IAsyncResult Antipixel.GenericDelegate`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x00000139 System.Void Antipixel.GenericDelegate`1::EndInvoke(System.IAsyncResult)
// 0x0000013A System.Void Antipixel.GenericArrayDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x0000013B System.Void Antipixel.GenericArrayDelegate`1::Invoke(T[])
// 0x0000013C System.IAsyncResult Antipixel.GenericArrayDelegate`1::BeginInvoke(T[],System.AsyncCallback,System.Object)
// 0x0000013D System.Void Antipixel.GenericArrayDelegate`1::EndInvoke(System.IAsyncResult)
// 0x0000013E System.Void Antipixel.ValidInputDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x0000013F T Antipixel.ValidInputDelegate`1::Invoke(System.String)
// 0x00000140 System.IAsyncResult Antipixel.ValidInputDelegate`1::BeginInvoke(System.String,System.AsyncCallback,System.Object)
// 0x00000141 T Antipixel.ValidInputDelegate`1::EndInvoke(System.IAsyncResult)
// 0x00000142 System.Void Antipixel.FieldDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x00000143 T Antipixel.FieldDelegate`1::Invoke(System.String,T,UnityEngine.GUILayoutOption[])
// 0x00000144 System.IAsyncResult Antipixel.FieldDelegate`1::BeginInvoke(System.String,T,UnityEngine.GUILayoutOption[],System.AsyncCallback,System.Object)
// 0x00000145 T Antipixel.FieldDelegate`1::EndInvoke(System.IAsyncResult)
// 0x00000146 System.Boolean Antipixel.AnimatorExtensions::HasParameter(UnityEngine.Animator,System.String)
extern void AnimatorExtensions_HasParameter_m65C2A853A157D11E89D30CA699DF352245CA502D (void);
// 0x00000147 System.Void Antipixel.AnimatorExtensions/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m27874F4F6A337094F29A7C671C81E7EA67C4B39F (void);
// 0x00000148 System.Boolean Antipixel.AnimatorExtensions/<>c__DisplayClass0_0::<HasParameter>b__0(UnityEngine.AnimatorControllerParameter)
extern void U3CU3Ec__DisplayClass0_0_U3CHasParameterU3Eb__0_m5F8484DAE810EBA9E24771165E0D50B90D374F4B (void);
// 0x00000149 T[] Antipixel.ArrayExtensions::Shuffle(T[])
// 0x0000014A T Antipixel.ArrayExtensions::Random(T[])
// 0x0000014B T[] Antipixel.ArrayExtensions::Add(T[],T,System.Boolean)
// 0x0000014C T[] Antipixel.ArrayExtensions::Add(T[],T[])
// 0x0000014D T[] Antipixel.ArrayExtensions::Insert(T[],T,System.Int32)
// 0x0000014E T[] Antipixel.ArrayExtensions::Insert(T[],T[],System.Int32)
// 0x0000014F T[] Antipixel.ArrayExtensions::Fusion(T[],T[])
// 0x00000150 U[] Antipixel.ArrayExtensions::Get(T[],System.Func`2<T,U>)
// 0x00000151 T[] Antipixel.ArrayExtensions::RemoveAt(T[],System.Int32)
// 0x00000152 T[] Antipixel.ArrayExtensions::Remove(T[],T)
// 0x00000153 T[] Antipixel.ArrayExtensions::Remove(T[],T[])
// 0x00000154 T[] Antipixel.ArrayExtensions::Remove(T[],System.Int32,System.Int32)
// 0x00000155 T[] Antipixel.ArrayExtensions::FindAll(T[],T)
// 0x00000156 T[] Antipixel.ArrayExtensions::FindAll(T[],T[])
// 0x00000157 T[] Antipixel.ArrayExtensions::FindAll(T[],System.Predicate`1<T>)
// 0x00000158 T Antipixel.ArrayExtensions::First(T[])
// 0x00000159 T Antipixel.ArrayExtensions::Last(T[])
// 0x0000015A T Antipixel.ArrayExtensions::Find(T[],T)
// 0x0000015B T Antipixel.ArrayExtensions::Find(T[],System.Predicate`1<T>)
// 0x0000015C U Antipixel.ArrayExtensions::Maximum(T[],System.Func`2<T,U>)
// 0x0000015D U Antipixel.ArrayExtensions::Minimum(T[],System.Func`2<T,U>)
// 0x0000015E System.Int32 Antipixel.ArrayExtensions::Index(T[],T)
// 0x0000015F System.Int32 Antipixel.ArrayExtensions::Index(T[],System.Predicate`1<T>)
// 0x00000160 System.Boolean Antipixel.ArrayExtensions::Contains(T[],T)
// 0x00000161 System.Boolean Antipixel.ArrayExtensions::Contains(T[],System.Predicate`1<T>)
// 0x00000162 System.Void Antipixel.ArrayExtensions::Each(T[],System.Action`1<T>)
// 0x00000163 System.Void Antipixel.ArrayExtensions::Sort(T[])
// 0x00000164 System.Void Antipixel.ArrayExtensions::Reverse(T[])
// 0x00000165 System.Void Antipixel.ArrayExtensions::Swap(T[],T,T)
// 0x00000166 System.Void Antipixel.ArrayExtensions::Swap(T[],System.Int32,System.Int32)
// 0x00000167 System.Boolean Antipixel.ArrayExtensions::InBounds(T[],System.Int32)
// 0x00000168 System.Boolean Antipixel.ArrayExtensions::IsNull(T[],System.Int32)
// 0x00000169 System.Boolean Antipixel.ArrayExtensions::IsNull(T[])
// 0x0000016A System.Boolean Antipixel.ArrayExtensions::IsEmpty(T[])
// 0x0000016B System.Boolean Antipixel.ArrayExtensions::IsNullOrEmpty(T[])
// 0x0000016C System.Void Antipixel.ArrayExtensions/<>c__DisplayClass9_0`1::.ctor()
// 0x0000016D System.Boolean Antipixel.ArrayExtensions/<>c__DisplayClass9_0`1::<Remove>b__0(T)
// 0x0000016E System.Void Antipixel.ArrayExtensions/<>c__DisplayClass10_0`1::.ctor()
// 0x0000016F System.Boolean Antipixel.ArrayExtensions/<>c__DisplayClass10_0`1::<Remove>b__0(T)
// 0x00000170 System.Void Antipixel.ArrayExtensions/<>c__DisplayClass11_0`1::.ctor()
// 0x00000171 System.Boolean Antipixel.ArrayExtensions/<>c__DisplayClass11_0`1::<Remove>b__0(T)
// 0x00000172 System.Void Antipixel.ArrayExtensions/<>c__DisplayClass12_0`1::.ctor()
// 0x00000173 System.Boolean Antipixel.ArrayExtensions/<>c__DisplayClass12_0`1::<FindAll>b__0(T)
// 0x00000174 System.Void Antipixel.ArrayExtensions/<>c__DisplayClass13_0`1::.ctor()
// 0x00000175 System.Boolean Antipixel.ArrayExtensions/<>c__DisplayClass13_0`1::<FindAll>b__0(T)
// 0x00000176 System.Void Antipixel.ArrayExtensions/<>c__DisplayClass17_0`1::.ctor()
// 0x00000177 System.Boolean Antipixel.ArrayExtensions/<>c__DisplayClass17_0`1::<Find>b__0(T)
// 0x00000178 System.Void Antipixel.ArrayExtensions/<>c__DisplayClass21_0`1::.ctor()
// 0x00000179 System.Boolean Antipixel.ArrayExtensions/<>c__DisplayClass21_0`1::<Index>b__0(T)
// 0x0000017A System.Void Antipixel.ArrayExtensions/<>c__DisplayClass23_0`1::.ctor()
// 0x0000017B System.Boolean Antipixel.ArrayExtensions/<>c__DisplayClass23_0`1::<Contains>b__0(T)
// 0x0000017C UnityEngine.Color Antipixel.ColorExtensions::Set(UnityEngine.Color,System.Int32,System.Int32,System.Int32,System.Int32)
extern void ColorExtensions_Set_m6AFA22FCF6B5EC500255C9FEEE6935D9E4021BD7 (void);
// 0x0000017D UnityEngine.Color Antipixel.ColorExtensions::SetR(UnityEngine.Color,System.Int32)
extern void ColorExtensions_SetR_mFFA620CE382CA0CA7BB52C3ABD3CD737E5EFB6E2 (void);
// 0x0000017E UnityEngine.Color Antipixel.ColorExtensions::SetR(UnityEngine.Color,System.Single)
extern void ColorExtensions_SetR_m09BA6159DEDCCBE2DBEAB9C49F22CA6007323C63 (void);
// 0x0000017F UnityEngine.Color Antipixel.ColorExtensions::SetG(UnityEngine.Color,System.Int32)
extern void ColorExtensions_SetG_mBB4DF67245D4EB6913B2D3E5D852969D83A16498 (void);
// 0x00000180 UnityEngine.Color Antipixel.ColorExtensions::SetG(UnityEngine.Color,System.Single)
extern void ColorExtensions_SetG_m8DF4C4FC264EFDF9FCF6F714AE3B5ECD98E9BF0D (void);
// 0x00000181 UnityEngine.Color Antipixel.ColorExtensions::SetB(UnityEngine.Color,System.Int32)
extern void ColorExtensions_SetB_m84DBE0E438C84E3D584E97C58B969A8979DA5DD9 (void);
// 0x00000182 UnityEngine.Color Antipixel.ColorExtensions::SetB(UnityEngine.Color,System.Single)
extern void ColorExtensions_SetB_m9A58A226B85B62796E7A68CF55DFCC9C2BBA2993 (void);
// 0x00000183 UnityEngine.Color Antipixel.ColorExtensions::SetA(UnityEngine.Color,System.Int32)
extern void ColorExtensions_SetA_mA169AE6EED6A543E3ABD2A113EF5308D4BC1287F (void);
// 0x00000184 UnityEngine.Color Antipixel.ColorExtensions::SetA(UnityEngine.Color,System.Single)
extern void ColorExtensions_SetA_m963C90656799BD83F9FE4C1FAAB5A3FA9DAE1608 (void);
// 0x00000185 System.String Antipixel.ColorExtensions::ToHex(UnityEngine.Color32)
extern void ColorExtensions_ToHex_m59386FA193D9DD3507810E2C7AFE2F712D5C2290 (void);
// 0x00000186 System.Single Antipixel.FloatExtensions::Abs(System.Single)
extern void FloatExtensions_Abs_mC78DD95AA8FAA3082D8C41884BDD06D5FA9ABFBF (void);
// 0x00000187 System.Boolean Antipixel.FloatExtensions::IsBetween(System.Single,System.Single,System.Single)
extern void FloatExtensions_IsBetween_mC6515CC784721FCA19FE9D96C18637B07F3B0AD3 (void);
// 0x00000188 System.Boolean Antipixel.FloatExtensions::IsOut(System.Single,System.Single,System.Single)
extern void FloatExtensions_IsOut_m2EB229AB49E1964E39BFC1D5A29275493E142A75 (void);
// 0x00000189 System.Collections.IEnumerator Antipixel.FloatExtensions::Lerp(System.Single,System.Single,Antipixel.FloatDelegate)
extern void FloatExtensions_Lerp_m4FFD0C6736D360C71851DEC02866847F925BE1D0 (void);
// 0x0000018A System.Void Antipixel.FloatExtensions/<Lerp>d__3::.ctor(System.Int32)
extern void U3CLerpU3Ed__3__ctor_m17137A1ECDE2DC1B5E0B05AC6B12D70873286ED9 (void);
// 0x0000018B System.Void Antipixel.FloatExtensions/<Lerp>d__3::System.IDisposable.Dispose()
extern void U3CLerpU3Ed__3_System_IDisposable_Dispose_m216C87FD0A759BDA982FC212D1E1551A499A8C4C (void);
// 0x0000018C System.Boolean Antipixel.FloatExtensions/<Lerp>d__3::MoveNext()
extern void U3CLerpU3Ed__3_MoveNext_mF0B2E7693E9CE0A65132E31167588E767612B27D (void);
// 0x0000018D System.Object Antipixel.FloatExtensions/<Lerp>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLerpU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16B7E50FF3A9EF14AFAC9E1E0DC2CF5F1B9DB67A (void);
// 0x0000018E System.Void Antipixel.FloatExtensions/<Lerp>d__3::System.Collections.IEnumerator.Reset()
extern void U3CLerpU3Ed__3_System_Collections_IEnumerator_Reset_mC4E3EC46B4A0646C6737E4924FCDADD09FEE512E (void);
// 0x0000018F System.Object Antipixel.FloatExtensions/<Lerp>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CLerpU3Ed__3_System_Collections_IEnumerator_get_Current_m4BB5AD0EB5DE1B3306ECFF7DC883012CCF9DE601 (void);
// 0x00000190 System.Void Antipixel.ImageExtensions::SetR(UnityEngine.UI.Image,System.Int32)
extern void ImageExtensions_SetR_mFAEBA61A20E6428F999A17E03AF36B2503A2C431 (void);
// 0x00000191 System.Void Antipixel.ImageExtensions::SetR(UnityEngine.UI.Image,System.Single)
extern void ImageExtensions_SetR_m629E89B98E610E8CB83CF42E6367B7D3A14A6A69 (void);
// 0x00000192 System.Void Antipixel.ImageExtensions::SetG(UnityEngine.UI.Image,System.Int32)
extern void ImageExtensions_SetG_m382DA12A44594B14636CEADB0818D97BE178936B (void);
// 0x00000193 System.Void Antipixel.ImageExtensions::SetG(UnityEngine.UI.Image,System.Single)
extern void ImageExtensions_SetG_mD5DE779F22269932C230DA10B6E58E160887C71E (void);
// 0x00000194 System.Void Antipixel.ImageExtensions::SetB(UnityEngine.UI.Image,System.Int32)
extern void ImageExtensions_SetB_m988B7B9526387E85ED2B17DA538E59E90091ADEA (void);
// 0x00000195 System.Void Antipixel.ImageExtensions::SetB(UnityEngine.UI.Image,System.Single)
extern void ImageExtensions_SetB_m49026135CCEAD64A953F960D16CC47AFEA57D973 (void);
// 0x00000196 System.Void Antipixel.ImageExtensions::SetA(UnityEngine.UI.Image,System.Int32)
extern void ImageExtensions_SetA_m70F77E629B8B45D00ABF861A762F2BA764880F67 (void);
// 0x00000197 System.Void Antipixel.ImageExtensions::SetA(UnityEngine.UI.Image,System.Single)
extern void ImageExtensions_SetA_mAA23378BAB12FA75FF36348E3C2742CB8FBE263A (void);
// 0x00000198 System.Void Antipixel.ImageExtensions::SetColor(UnityEngine.UI.Image,UnityEngine.Color)
extern void ImageExtensions_SetColor_m5E5CCF0BA62A2AEB34DCA633FD9B4E63A4FB1E61 (void);
// 0x00000199 System.Void Antipixel.ImageExtensions::SetColor(UnityEngine.UI.Image,System.String)
extern void ImageExtensions_SetColor_mBEE20DCF91C9119E14EADCA21171A4690B603412 (void);
// 0x0000019A System.Int32 Antipixel.IntExtensions::Abs(System.Int32)
extern void IntExtensions_Abs_m09093FE68577FEDF3E9026C4D1A27B3CCEAA3193 (void);
// 0x0000019B System.Boolean Antipixel.IntExtensions::IsBetween(System.Int32,System.Int32,System.Int32)
extern void IntExtensions_IsBetween_mC4EDD39F5B7C8CC2ED88735414F86E9FA009A509 (void);
// 0x0000019C System.Boolean Antipixel.IntExtensions::IsOut(System.Int32,System.Int32,System.Int32)
extern void IntExtensions_IsOut_m5C3BBD203A78F7FFA3B5B0C3C569CFE888A67C21 (void);
// 0x0000019D System.Collections.Generic.List`1<T> Antipixel.ListExtensions::Shuffle(System.Collections.Generic.List`1<T>)
// 0x0000019E T Antipixel.ListExtensions::Random(System.Collections.Generic.List`1<T>)
// 0x0000019F T Antipixel.ListExtensions::First(System.Collections.Generic.List`1<T>)
// 0x000001A0 T Antipixel.ListExtensions::Last(System.Collections.Generic.List`1<T>)
// 0x000001A1 System.Void Antipixel.MonoBehaviourExtensions::StartCoroutines(UnityEngine.MonoBehaviour,System.Collections.IEnumerator[])
extern void MonoBehaviourExtensions_StartCoroutines_mDCD7FE5F426A916EBB16514473D000A7FDF5DC91 (void);
// 0x000001A2 System.Void Antipixel.MonoBehaviourExtensions::StopCoroutines(UnityEngine.MonoBehaviour,System.Collections.IEnumerator[])
extern void MonoBehaviourExtensions_StopCoroutines_mB4D9D1AF8DBE31F6B589A8CFB44D9993B14054E7 (void);
// 0x000001A3 System.Void Antipixel.MonoBehaviourExtensions/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mB311E0656BA272229CEFB3E895FEFD7FF745C6FF (void);
// 0x000001A4 System.Void Antipixel.MonoBehaviourExtensions/<>c__DisplayClass0_0::<StartCoroutines>b__0(System.Collections.IEnumerator)
extern void U3CU3Ec__DisplayClass0_0_U3CStartCoroutinesU3Eb__0_mECF4977979F1F705DBBE402A2FFE4AB5FC6CB4CE (void);
// 0x000001A5 System.Void Antipixel.MonoBehaviourExtensions/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m66F5CC081468949403B33CB6EFEF55C5911BA1A0 (void);
// 0x000001A6 System.Void Antipixel.MonoBehaviourExtensions/<>c__DisplayClass1_0::<StopCoroutines>b__0(System.Collections.IEnumerator)
extern void U3CU3Ec__DisplayClass1_0_U3CStopCoroutinesU3Eb__0_m5F184D5EC9100BF9B0A9EA473F7BEFC3CED5AF19 (void);
// 0x000001A7 System.Void Antipixel.ObjectExtensions::Log(System.Action`2<System.String,UnityEngine.Object>,UnityEngine.Object,System.String,System.Object[])
extern void ObjectExtensions_Log_mE2360D17840FEC4124F24EA2B46D2FFB0838D5A9 (void);
// 0x000001A8 System.Void Antipixel.ObjectExtensions::Log(UnityEngine.Object,System.Object[])
extern void ObjectExtensions_Log_m61CF3C51D0F65245B5E8D769788F35EE3339022B (void);
// 0x000001A9 System.Void Antipixel.ObjectExtensions::LogNotification(UnityEngine.Object,System.Object[])
extern void ObjectExtensions_LogNotification_m0968F3F09B27243BA90F851446E9D978A2031EDD (void);
// 0x000001AA System.Void Antipixel.ObjectExtensions::LogSuccess(UnityEngine.Object,System.Object[])
extern void ObjectExtensions_LogSuccess_mDE4CFC8CEFB0AF4619AC6871A70F09E8FE2F19C8 (void);
// 0x000001AB System.Void Antipixel.ObjectExtensions::LogWarning(UnityEngine.Object,System.Object[])
extern void ObjectExtensions_LogWarning_mDB995A3CE3662832CD9097225D442C73A6FF2D4D (void);
// 0x000001AC System.Void Antipixel.ObjectExtensions::LogError(UnityEngine.Object,System.Object[])
extern void ObjectExtensions_LogError_m953CE7BACA44BA1932BF876D8B1ADC9612450256 (void);
// 0x000001AD System.Boolean Antipixel.QuaternionExtensions::IsNaN(UnityEngine.Quaternion)
extern void QuaternionExtensions_IsNaN_mC9FE45291D8D4253F5D7D887A9A1C466DEE349F9 (void);
// 0x000001AE UnityEngine.Quaternion Antipixel.QuaternionExtensions::Zero(UnityEngine.Quaternion)
extern void QuaternionExtensions_Zero_m0F12B09FBEB3B559E1D64904A9121BF1B38ACF23 (void);
// 0x000001AF UnityEngine.Quaternion Antipixel.QuaternionExtensions::Up(UnityEngine.Quaternion)
extern void QuaternionExtensions_Up_mBB3A995893817FEBD828D45099EDFC5A84BBEF5D (void);
// 0x000001B0 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Up(UnityEngine.Quaternion,System.Int32)
extern void QuaternionExtensions_Up_mC52895A6ACD4B3F51662DB8C74F910B1C0CC90B8 (void);
// 0x000001B1 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Up(UnityEngine.Quaternion,System.Single)
extern void QuaternionExtensions_Up_m06E359EA967177DD74342E90C37DC889F6F83F00 (void);
// 0x000001B2 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Up90(UnityEngine.Quaternion)
extern void QuaternionExtensions_Up90_mABB571A0047C0DAE30CCC9BB65A52CB2815F8A9C (void);
// 0x000001B3 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Up180(UnityEngine.Quaternion)
extern void QuaternionExtensions_Up180_mE140FDDE5BE7988867B33A14E09B28D74EA89A9F (void);
// 0x000001B4 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Down(UnityEngine.Quaternion)
extern void QuaternionExtensions_Down_m153776DFF67216F955E7138922B3D4146382320E (void);
// 0x000001B5 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Down(UnityEngine.Quaternion,System.Int32)
extern void QuaternionExtensions_Down_m90995D193F285DB9A33D12E5BB909990F3A7FD6E (void);
// 0x000001B6 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Down(UnityEngine.Quaternion,System.Single)
extern void QuaternionExtensions_Down_mEB06E21CAB8262A461537A80F8F98992C94F510F (void);
// 0x000001B7 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Down90(UnityEngine.Quaternion)
extern void QuaternionExtensions_Down90_m0A7CFA1EFE1E1DE498622A5B60387EBB13509B1F (void);
// 0x000001B8 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Down180(UnityEngine.Quaternion)
extern void QuaternionExtensions_Down180_m21AC9C0569C619D1EA8F56B7FAC4C4D0DEC5C4C0 (void);
// 0x000001B9 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Left(UnityEngine.Quaternion)
extern void QuaternionExtensions_Left_m947B5E58A6C2DFCF74DD4A8756424E221C66966D (void);
// 0x000001BA UnityEngine.Quaternion Antipixel.QuaternionExtensions::Left(UnityEngine.Quaternion,System.Int32)
extern void QuaternionExtensions_Left_mDA42E54985429B0F3E23DC9BDF3862B3A0DF41FD (void);
// 0x000001BB UnityEngine.Quaternion Antipixel.QuaternionExtensions::Left(UnityEngine.Quaternion,System.Single)
extern void QuaternionExtensions_Left_m5C2BBF623267EB0C3EB1240265BC924DF4C35EB5 (void);
// 0x000001BC UnityEngine.Quaternion Antipixel.QuaternionExtensions::Left90(UnityEngine.Quaternion)
extern void QuaternionExtensions_Left90_mBCEC0324C5BE378CCF01882DB9C49E3C88D7C453 (void);
// 0x000001BD UnityEngine.Quaternion Antipixel.QuaternionExtensions::Left180(UnityEngine.Quaternion)
extern void QuaternionExtensions_Left180_m10F0F71ABE7471EE31D93784997104EE94F6C43B (void);
// 0x000001BE UnityEngine.Quaternion Antipixel.QuaternionExtensions::Right(UnityEngine.Quaternion)
extern void QuaternionExtensions_Right_m776B9F0569D3F2244DD5A97A4E89DF3AB16EF818 (void);
// 0x000001BF UnityEngine.Quaternion Antipixel.QuaternionExtensions::Right(UnityEngine.Quaternion,System.Int32)
extern void QuaternionExtensions_Right_m9453E79E22C616CB1986E5C1EA0D039AEAEF6C95 (void);
// 0x000001C0 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Right(UnityEngine.Quaternion,System.Single)
extern void QuaternionExtensions_Right_m14FF1F9324335431168EF73FF6041792A6EA04DF (void);
// 0x000001C1 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Right90(UnityEngine.Quaternion)
extern void QuaternionExtensions_Right90_m9BD36C19ED4067966DE28AD70E61D36681B2BFE4 (void);
// 0x000001C2 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Right180(UnityEngine.Quaternion)
extern void QuaternionExtensions_Right180_m405E1E80C2FE3F6C2E7942AB09D4B7D85151825C (void);
// 0x000001C3 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Forward(UnityEngine.Quaternion)
extern void QuaternionExtensions_Forward_mDCE957BA188393041286E10CE0FE787BD523BB88 (void);
// 0x000001C4 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Forward(UnityEngine.Quaternion,System.Int32)
extern void QuaternionExtensions_Forward_m7000615890CAEC4AC45C91A66A68BD52BBA39BC4 (void);
// 0x000001C5 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Forward(UnityEngine.Quaternion,System.Single)
extern void QuaternionExtensions_Forward_m65940725192A7DA0D955C067F3BFEE3E6DA86B90 (void);
// 0x000001C6 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Forward90(UnityEngine.Quaternion)
extern void QuaternionExtensions_Forward90_m80D0BF85B5BB5CF21A19B54D20E554C6B34BDD67 (void);
// 0x000001C7 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Forward180(UnityEngine.Quaternion)
extern void QuaternionExtensions_Forward180_m199E6031F53F23DE333755BD198D81DAC664D883 (void);
// 0x000001C8 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Backward(UnityEngine.Quaternion)
extern void QuaternionExtensions_Backward_m52DF472DFAAFDE4E506D3A7BB06C9E75D3299C73 (void);
// 0x000001C9 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Backward(UnityEngine.Quaternion,System.Int32)
extern void QuaternionExtensions_Backward_m70BA954DD7F1A36CA375FDCDBF0D441AF5D0880B (void);
// 0x000001CA UnityEngine.Quaternion Antipixel.QuaternionExtensions::Backward(UnityEngine.Quaternion,System.Single)
extern void QuaternionExtensions_Backward_m24D1D22CDA1132F002B0CF76948A15ED2C5A5F58 (void);
// 0x000001CB UnityEngine.Quaternion Antipixel.QuaternionExtensions::Backward90(UnityEngine.Quaternion)
extern void QuaternionExtensions_Backward90_mE14A87E0BEA5D911553B940C41D7E599759B7832 (void);
// 0x000001CC UnityEngine.Quaternion Antipixel.QuaternionExtensions::Backward180(UnityEngine.Quaternion)
extern void QuaternionExtensions_Backward180_m6FB603BF64C76C05CCE51CEC35248AAA3D18B3CD (void);
// 0x000001CD UnityEngine.Quaternion Antipixel.QuaternionExtensions::Add(UnityEngine.Quaternion,System.Int32)
extern void QuaternionExtensions_Add_m4CAA4D499323047A559C20D57473B45815F0E648 (void);
// 0x000001CE UnityEngine.Quaternion Antipixel.QuaternionExtensions::Add(UnityEngine.Quaternion,System.Single)
extern void QuaternionExtensions_Add_m336CEF5746B11CFA9D83A148A0DE496862D7ECCB (void);
// 0x000001CF UnityEngine.Quaternion Antipixel.QuaternionExtensions::Add(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void QuaternionExtensions_Add_m7084BA1762C4F4381CF2FCF9635E888E1F498D9C (void);
// 0x000001D0 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Add(UnityEngine.Quaternion,UnityEngine.Vector3Int)
extern void QuaternionExtensions_Add_mE46FCEB42C92B5163F71B313073F2E814BF0E50D (void);
// 0x000001D1 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Add(UnityEngine.Quaternion,UnityEngine.Vector3)
extern void QuaternionExtensions_Add_mF713C20D639724911F419DEC5DE3EEB0FD18AACA (void);
// 0x000001D2 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Subtract(UnityEngine.Quaternion,System.Int32)
extern void QuaternionExtensions_Subtract_m4CF2B58CBBCE57D0DBB4AC1F1D5F777888657F04 (void);
// 0x000001D3 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Subtract(UnityEngine.Quaternion,System.Single)
extern void QuaternionExtensions_Subtract_m2B59E0F86BD21F529B10BB208801F15F424484A8 (void);
// 0x000001D4 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Subtract(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void QuaternionExtensions_Subtract_m2C3BAB564A9569BE9BB425835FFB040873AFB8C3 (void);
// 0x000001D5 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Subtract(UnityEngine.Quaternion,UnityEngine.Vector3Int)
extern void QuaternionExtensions_Subtract_m4F04ED216472FDD481FA50F2DE5B53808BC31365 (void);
// 0x000001D6 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Subtract(UnityEngine.Quaternion,UnityEngine.Vector3)
extern void QuaternionExtensions_Subtract_mB6942626E492BEB8FE232C43F6D497D96FFF01E9 (void);
// 0x000001D7 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Multiply(UnityEngine.Quaternion,System.Int32)
extern void QuaternionExtensions_Multiply_m67D9D8633883D8E551D25EB7269C46FF725782F7 (void);
// 0x000001D8 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Multiply(UnityEngine.Quaternion,System.Single)
extern void QuaternionExtensions_Multiply_mAA96F55CF284FD2C99FD1035CC1CCE8673E662BD (void);
// 0x000001D9 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void QuaternionExtensions_Multiply_m9227C99233A1B254EFBFA0117E89363C1C0A0BE8 (void);
// 0x000001DA UnityEngine.Quaternion Antipixel.QuaternionExtensions::Multiply(UnityEngine.Quaternion,UnityEngine.Vector3Int)
extern void QuaternionExtensions_Multiply_mB7D0A683328EB6D69EAD60E2095E48AFBFACAB27 (void);
// 0x000001DB UnityEngine.Quaternion Antipixel.QuaternionExtensions::Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern void QuaternionExtensions_Multiply_m8E8913D8912390CE6C9EDE0E562AC4FF1E7AE2E6 (void);
// 0x000001DC UnityEngine.Quaternion Antipixel.QuaternionExtensions::Divide(UnityEngine.Quaternion,System.Int32)
extern void QuaternionExtensions_Divide_m5EFFEB89FB0B5889C6AC64A4326A492F1A02D02C (void);
// 0x000001DD UnityEngine.Quaternion Antipixel.QuaternionExtensions::Divide(UnityEngine.Quaternion,System.Single)
extern void QuaternionExtensions_Divide_m9EA4388DBF3ABE2DFA8ED007A4F6331228410D36 (void);
// 0x000001DE UnityEngine.Quaternion Antipixel.QuaternionExtensions::Divide(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void QuaternionExtensions_Divide_m357BD1B27735729427DA56C846D3019977D260F2 (void);
// 0x000001DF UnityEngine.Quaternion Antipixel.QuaternionExtensions::Divide(UnityEngine.Quaternion,UnityEngine.Vector3Int)
extern void QuaternionExtensions_Divide_m07FA50A7EBDF4523A9309D15094C76030A78D215 (void);
// 0x000001E0 UnityEngine.Quaternion Antipixel.QuaternionExtensions::Divide(UnityEngine.Quaternion,UnityEngine.Vector3)
extern void QuaternionExtensions_Divide_mE439CBA5A34972F59CEF509C0490D8C7C9C5309E (void);
// 0x000001E1 System.Void Antipixel.Rigidbody2DExtensions::AddExplosionForce(UnityEngine.Rigidbody2D,System.Single,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.ForceMode2D)
extern void Rigidbody2DExtensions_AddExplosionForce_m8E9BDAD79B943D7B97E0FC5EE0EE5AD0599454DE (void);
// 0x000001E2 System.Void Antipixel.SpriteRendererExtensions::SetR(UnityEngine.SpriteRenderer,System.Int32)
extern void SpriteRendererExtensions_SetR_m8BFF8AFDC263CA0BC4A40FC5D06B0A32E1E76A69 (void);
// 0x000001E3 System.Void Antipixel.SpriteRendererExtensions::SetR(UnityEngine.SpriteRenderer,System.Single)
extern void SpriteRendererExtensions_SetR_m6EEF8BA1DE7852327357E5A7A721337679DA62EF (void);
// 0x000001E4 System.Void Antipixel.SpriteRendererExtensions::SetG(UnityEngine.SpriteRenderer,System.Int32)
extern void SpriteRendererExtensions_SetG_mB5A0183B6B695701304581A15FB9ECCB39CBFA8B (void);
// 0x000001E5 System.Void Antipixel.SpriteRendererExtensions::SetG(UnityEngine.SpriteRenderer,System.Single)
extern void SpriteRendererExtensions_SetG_mAC079782001B1E6832445F6F80514071D8901385 (void);
// 0x000001E6 System.Void Antipixel.SpriteRendererExtensions::SetB(UnityEngine.SpriteRenderer,System.Int32)
extern void SpriteRendererExtensions_SetB_m9284EF24294550B3ED66698DC6DEA83EEC5B182E (void);
// 0x000001E7 System.Void Antipixel.SpriteRendererExtensions::SetB(UnityEngine.SpriteRenderer,System.Single)
extern void SpriteRendererExtensions_SetB_m757487CD1F97AA94AAB9BCF75CAC56833504184A (void);
// 0x000001E8 System.Void Antipixel.SpriteRendererExtensions::SetA(UnityEngine.SpriteRenderer,System.Int32)
extern void SpriteRendererExtensions_SetA_mC94B3648CF295E68D03A4179CA232D1A2F06EC20 (void);
// 0x000001E9 System.Void Antipixel.SpriteRendererExtensions::SetA(UnityEngine.SpriteRenderer,System.Single)
extern void SpriteRendererExtensions_SetA_m54BAD889A436F0F488D6F45B573F1FBF94916770 (void);
// 0x000001EA System.Void Antipixel.SpriteRendererExtensions::SetColor(UnityEngine.SpriteRenderer,UnityEngine.Color)
extern void SpriteRendererExtensions_SetColor_m7CD01B62F84106674BA6973641776B6CD3A6A7FA (void);
// 0x000001EB System.Void Antipixel.SpriteRendererExtensions::SetColor(UnityEngine.SpriteRenderer,System.String)
extern void SpriteRendererExtensions_SetColor_m5C7E8A42F030A1A8A564332F06936ED007048C95 (void);
// 0x000001EC System.String[] Antipixel.StringExtensions::Divide(System.String,System.Int32)
extern void StringExtensions_Divide_m2BCC8258257E8616F53F6DA9B04E10FE1936641D (void);
// 0x000001ED System.String Antipixel.StringExtensions::Color(System.String,System.String)
extern void StringExtensions_Color_mB8C917AEDFC621F07E4ED594A654FD6A40A7ED11 (void);
// 0x000001EE UnityEngine.Color32 Antipixel.StringExtensions::ToColor(System.String)
extern void StringExtensions_ToColor_mFE27F82901872E6F765BC8EDC12B048CC0DD1B74 (void);
// 0x000001EF T Antipixel.StringExtensions::ToEnum(System.String,System.Type)
// 0x000001F0 System.Boolean Antipixel.StringExtensions::IsHex(System.String)
extern void StringExtensions_IsHex_mDDC2251E712409026DB88D83386F4B84A26D1022 (void);
// 0x000001F1 System.Boolean Antipixel.StringExtensions::IsAlpha(System.String)
extern void StringExtensions_IsAlpha_m74204F1AA74D46F02EA8E89C5C00B403EE6D30F7 (void);
// 0x000001F2 System.Boolean Antipixel.StringExtensions::IsNumeric(System.String)
extern void StringExtensions_IsNumeric_mFF043632B4F5E1742BE82FC3B006578C92B0E04A (void);
// 0x000001F3 System.Boolean Antipixel.StringExtensions::IsAlphanumeric(System.String)
extern void StringExtensions_IsAlphanumeric_m901B743AF3B9C962E9C4453323D1E865F752B66D (void);
// 0x000001F4 System.Boolean Antipixel.StringExtensions::IsValidInput(System.String,Antipixel.ValidInputDelegate`1<U>)
// 0x000001F5 System.Void Antipixel.TMPTextExtensions::SetR(TMPro.TMP_Text,System.Int32)
extern void TMPTextExtensions_SetR_mB4B2957AC500366941B17B7568231D351317482B (void);
// 0x000001F6 System.Void Antipixel.TMPTextExtensions::SetR(TMPro.TMP_Text,System.Single)
extern void TMPTextExtensions_SetR_m94BBFE8352782D5DD510CBFE8BF650B31F7AA115 (void);
// 0x000001F7 System.Void Antipixel.TMPTextExtensions::SetG(TMPro.TMP_Text,System.Int32)
extern void TMPTextExtensions_SetG_m2039EFA377D9DF24CA2C824EC1185737E8E92DCD (void);
// 0x000001F8 System.Void Antipixel.TMPTextExtensions::SetG(TMPro.TMP_Text,System.Single)
extern void TMPTextExtensions_SetG_m96C93CAA18EF4F9ED87F9617D2EE1453B1C014F6 (void);
// 0x000001F9 System.Void Antipixel.TMPTextExtensions::SetB(TMPro.TMP_Text,System.Int32)
extern void TMPTextExtensions_SetB_mE795683BBB5EC70F256F18F1622BF46966D49690 (void);
// 0x000001FA System.Void Antipixel.TMPTextExtensions::SetB(TMPro.TMP_Text,System.Single)
extern void TMPTextExtensions_SetB_m059A5D2AF784EE97EC7B7E52F7CCFFFBAE403E0E (void);
// 0x000001FB System.Void Antipixel.TMPTextExtensions::SetA(TMPro.TMP_Text,System.Int32)
extern void TMPTextExtensions_SetA_m291C7CDB9C0C7C0AF7F7B2B009E7D68EA1EECC5C (void);
// 0x000001FC System.Void Antipixel.TMPTextExtensions::SetA(TMPro.TMP_Text,System.Single)
extern void TMPTextExtensions_SetA_mE8D0A7075CD7AA9D35E37F50AF49BF1DA9443ED4 (void);
// 0x000001FD System.Void Antipixel.TMPTextExtensions::SetColor(TMPro.TMP_Text,UnityEngine.Color)
extern void TMPTextExtensions_SetColor_m37619C9062B93B5A410F9336C050FBCFE614670A (void);
// 0x000001FE System.Void Antipixel.TMPTextExtensions::SetColor(TMPro.TMP_Text,System.String)
extern void TMPTextExtensions_SetColor_m7553DCBF072EBFC73BEF0EDD99102742A475F0E0 (void);
// 0x000001FF System.Void Antipixel.TextExtensions::SetR(UnityEngine.UI.Text,System.Int32)
extern void TextExtensions_SetR_m3C5E804D9E55689CB388ECDEEB0084E1474BA8DA (void);
// 0x00000200 System.Void Antipixel.TextExtensions::SetR(UnityEngine.UI.Text,System.Single)
extern void TextExtensions_SetR_m1EADCE814318C3D20603DFBAC59C2A7FFAF7E8BB (void);
// 0x00000201 System.Void Antipixel.TextExtensions::SetG(UnityEngine.UI.Text,System.Int32)
extern void TextExtensions_SetG_m83D9666D54A195A448B8403A01224410D1000F6C (void);
// 0x00000202 System.Void Antipixel.TextExtensions::SetG(UnityEngine.UI.Text,System.Single)
extern void TextExtensions_SetG_mC90BD24E6A8221AE2369CD5AF190FA771DA10C78 (void);
// 0x00000203 System.Void Antipixel.TextExtensions::SetB(UnityEngine.UI.Text,System.Int32)
extern void TextExtensions_SetB_m48F032B423EBAF11900517D6C1E1525D2ED1DCB3 (void);
// 0x00000204 System.Void Antipixel.TextExtensions::SetB(UnityEngine.UI.Text,System.Single)
extern void TextExtensions_SetB_mBC36FDFBD6A12DCB5FEFC72DEECF90D16A65A49A (void);
// 0x00000205 System.Void Antipixel.TextExtensions::SetA(UnityEngine.UI.Text,System.Int32)
extern void TextExtensions_SetA_m85362D6B891357D09337508D77EA2FEA3D219566 (void);
// 0x00000206 System.Void Antipixel.TextExtensions::SetA(UnityEngine.UI.Text,System.Single)
extern void TextExtensions_SetA_m40873B329DFE4CCB63BE5BC83FEE7136DAC64C9D (void);
// 0x00000207 System.Void Antipixel.TextExtensions::SetColor(UnityEngine.UI.Text,UnityEngine.Color)
extern void TextExtensions_SetColor_m0C70BA3F433326639E054B20FD170A50D91D0A50 (void);
// 0x00000208 System.Void Antipixel.TextExtensions::SetColor(UnityEngine.UI.Text,System.String)
extern void TextExtensions_SetColor_m71E41A2A11BF53243929659738C52ADA9F1DC9D3 (void);
// 0x00000209 UnityEngine.Vector3 Antipixel.TransformExtensions::OrbitAround(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void TransformExtensions_OrbitAround_m427AFBBED01A222959A499573BF3416EEB0BBA8C (void);
// 0x0000020A UnityEngine.Vector2 Antipixel.Vector2Extensions::SetX(UnityEngine.Vector2,System.Single)
extern void Vector2Extensions_SetX_m628D5C7426C250CF1AA42EB7B4D5CD4C2AEF6FD2 (void);
// 0x0000020B UnityEngine.Vector2 Antipixel.Vector2Extensions::SetY(UnityEngine.Vector2,System.Single)
extern void Vector2Extensions_SetY_m4E8FBC009EFF33EF0A41B94012A34C4CBE0A2FB9 (void);
// 0x0000020C UnityEngine.Vector2 Antipixel.Vector2Extensions::Add(UnityEngine.Vector2,System.Single)
extern void Vector2Extensions_Add_mF90553B085B216B8063C319E7F5C8204150ED6A1 (void);
// 0x0000020D UnityEngine.Vector2 Antipixel.Vector2Extensions::Add(UnityEngine.Vector2,System.Single,System.Single)
extern void Vector2Extensions_Add_m762B3B8DF1C23DE0AA2C3560276B2A8B41B63D4B (void);
// 0x0000020E UnityEngine.Vector2 Antipixel.Vector2Extensions::AddX(UnityEngine.Vector2,System.Single)
extern void Vector2Extensions_AddX_m758A794B218B5C423592D51E268B663BBCC11962 (void);
// 0x0000020F UnityEngine.Vector2 Antipixel.Vector2Extensions::AddY(UnityEngine.Vector2,System.Single)
extern void Vector2Extensions_AddY_mBB7E924E34BC6B1692D46C6056AFFECA78C72D1A (void);
// 0x00000210 UnityEngine.Vector2 Antipixel.Vector2Extensions::Multiply(UnityEngine.Vector2,System.Single,System.Single)
extern void Vector2Extensions_Multiply_mF1DFE84C51DCB8461D57DFEA89F8271451231BBE (void);
// 0x00000211 UnityEngine.Vector2 Antipixel.Vector2Extensions::MultiplyX(UnityEngine.Vector2,System.Single)
extern void Vector2Extensions_MultiplyX_mBDA6E3039AB952E5CBBA6392DCA76DBB53AD320A (void);
// 0x00000212 UnityEngine.Vector2 Antipixel.Vector2Extensions::MultiplyY(UnityEngine.Vector2,System.Single)
extern void Vector2Extensions_MultiplyY_m5D71509EA53C9E5E1EB99824003A55F582038DCE (void);
// 0x00000213 UnityEngine.Vector2 Antipixel.Vector2Extensions::FlipX(UnityEngine.Vector2)
extern void Vector2Extensions_FlipX_m207B2ECA3AFA9705787AF81675F193DDFEED1274 (void);
// 0x00000214 UnityEngine.Vector2 Antipixel.Vector2Extensions::FlipX(UnityEngine.Vector2,System.Single)
extern void Vector2Extensions_FlipX_m9B3AD699449ACAEBC20A9FAF8E18C10D86D7AC8B (void);
// 0x00000215 UnityEngine.Vector2 Antipixel.Vector2Extensions::FlipY(UnityEngine.Vector2)
extern void Vector2Extensions_FlipY_m3D57D6A2C8E86B7932C2E2E31818ECDC8E7394E2 (void);
// 0x00000216 UnityEngine.Vector2 Antipixel.Vector2Extensions::FlipY(UnityEngine.Vector2,System.Single)
extern void Vector2Extensions_FlipY_mE5CE03C2A7D315D3A99DD3A328A4AD31437FBA22 (void);
// 0x00000217 UnityEngine.Vector2 Antipixel.Vector2Extensions::Clamp(UnityEngine.Vector2,System.Single,System.Single)
extern void Vector2Extensions_Clamp_m45CA9923A6E901E9382DDC7FE420214A9BFFD02B (void);
// 0x00000218 UnityEngine.Vector2 Antipixel.Vector2Extensions::Clamp01(UnityEngine.Vector2)
extern void Vector2Extensions_Clamp01_mAC727F3DC44C853FDA19B266270580714034A817 (void);
// 0x00000219 UnityEngine.Vector2 Antipixel.Vector2Extensions::Abs(UnityEngine.Vector2)
extern void Vector2Extensions_Abs_mD3F2F9E177B6FB04F7D6FF4C1C5AE863A9717B5D (void);
// 0x0000021A System.Boolean Antipixel.Vector2Extensions::IsNaN(UnityEngine.Vector2)
extern void Vector2Extensions_IsNaN_m2CEE8D61FDD43F32C53233AA95A589ED3E033EE4 (void);
// 0x0000021B System.Boolean Antipixel.Vector2Extensions::IsBetween(UnityEngine.Vector2,System.Single,System.Single)
extern void Vector2Extensions_IsBetween_m12B3F6E076D165F71A3EA8653FF0D395D939065F (void);
// 0x0000021C System.Single Antipixel.Vector2Extensions::Random(UnityEngine.Vector2)
extern void Vector2Extensions_Random_m41E2C59D0F97940AAF95FFBFB970BD4D4F609284 (void);
// 0x0000021D UnityEngine.Vector2 Antipixel.Vector2Extensions::Random(UnityEngine.Vector2,System.Single,System.Single,System.Boolean)
extern void Vector2Extensions_Random_m75CA36F81F110C038800A22EAFDE25668ABA9B25 (void);
// 0x0000021E UnityEngine.Vector2Int Antipixel.Vector2Extensions::SetX(UnityEngine.Vector2Int,System.Int32)
extern void Vector2Extensions_SetX_m595EC12BB39254EA57783C91AD3ADCB4A653C8B0 (void);
// 0x0000021F UnityEngine.Vector2Int Antipixel.Vector2Extensions::SetY(UnityEngine.Vector2Int,System.Int32)
extern void Vector2Extensions_SetY_m0A7A88B104C6A031184C0EC70E9BB8834E818C6E (void);
// 0x00000220 UnityEngine.Vector2Int Antipixel.Vector2Extensions::Add(UnityEngine.Vector2Int,System.Int32)
extern void Vector2Extensions_Add_m2C033D4117520A9FEE90D070A9102DD3C70BE2E6 (void);
// 0x00000221 UnityEngine.Vector2Int Antipixel.Vector2Extensions::Add(UnityEngine.Vector2Int,System.Int32,System.Int32)
extern void Vector2Extensions_Add_m7FA63B8E9AF73DA9D6F1839128884FB5C83A2DEA (void);
// 0x00000222 UnityEngine.Vector2Int Antipixel.Vector2Extensions::AddX(UnityEngine.Vector2Int,System.Int32)
extern void Vector2Extensions_AddX_m0DB873F80F8A413EE690783034D438225AB3D164 (void);
// 0x00000223 UnityEngine.Vector2Int Antipixel.Vector2Extensions::AddY(UnityEngine.Vector2Int,System.Int32)
extern void Vector2Extensions_AddY_m27B53E8C4E342CDC2365F88B86F21E7DE407D9F4 (void);
// 0x00000224 UnityEngine.Vector2Int Antipixel.Vector2Extensions::Multiply(UnityEngine.Vector2Int,System.Int32,System.Int32)
extern void Vector2Extensions_Multiply_mAB1021AA7564D90AF150F1F171BAD924F3B38450 (void);
// 0x00000225 UnityEngine.Vector2Int Antipixel.Vector2Extensions::MultiplyX(UnityEngine.Vector2Int,System.Int32)
extern void Vector2Extensions_MultiplyX_m972CCB60D21C531E0021216D2E4E147A7EA7C0A6 (void);
// 0x00000226 UnityEngine.Vector2Int Antipixel.Vector2Extensions::MultiplyY(UnityEngine.Vector2Int,System.Int32)
extern void Vector2Extensions_MultiplyY_mF5AE7390ADC21BD6BAA2F0307910342360EE53ED (void);
// 0x00000227 UnityEngine.Vector2Int Antipixel.Vector2Extensions::FlipX(UnityEngine.Vector2Int)
extern void Vector2Extensions_FlipX_m60E7C5F685BB334192C017E7B59F26459A37B4E1 (void);
// 0x00000228 UnityEngine.Vector2Int Antipixel.Vector2Extensions::FlipY(UnityEngine.Vector2Int)
extern void Vector2Extensions_FlipY_mD2C6E9C5B1C152F266DE5AA51DDB1F299294DAE5 (void);
// 0x00000229 UnityEngine.Vector2Int Antipixel.Vector2Extensions::Clamp(UnityEngine.Vector2Int,System.Single,System.Single)
extern void Vector2Extensions_Clamp_mDDF1AEFA36425F97961E262C791A7118CFA080F8 (void);
// 0x0000022A UnityEngine.Vector2Int Antipixel.Vector2Extensions::Clamp01(UnityEngine.Vector2Int)
extern void Vector2Extensions_Clamp01_m420330594EDA2001E2A9A5E43542B6C2934CC1DE (void);
// 0x0000022B UnityEngine.Vector2Int Antipixel.Vector2Extensions::Abs(UnityEngine.Vector2Int)
extern void Vector2Extensions_Abs_m443A22A719ECBC24B13A8C98DF0F643BB6699F35 (void);
// 0x0000022C System.Boolean Antipixel.Vector2Extensions::IsNaN(UnityEngine.Vector2Int)
extern void Vector2Extensions_IsNaN_m3BD979CB285D0FE1B7C92EC7C09A98DE3FEC439A (void);
// 0x0000022D System.Boolean Antipixel.Vector2Extensions::IsBetween(UnityEngine.Vector2Int,System.Int32,System.Int32)
extern void Vector2Extensions_IsBetween_m9BDF92254390AE9F6712D1911CFD269800874943 (void);
// 0x0000022E System.Int32 Antipixel.Vector2Extensions::Random(UnityEngine.Vector2Int)
extern void Vector2Extensions_Random_mC14C459831FE9B2D765229782B770F82DB2DC5D7 (void);
// 0x0000022F UnityEngine.Vector2Int Antipixel.Vector2Extensions::Random(UnityEngine.Vector2Int,System.Int32,System.Int32,System.Boolean)
extern void Vector2Extensions_Random_mD7F87347B2F86A02C3D17910862F3FF2361A7B44 (void);
// 0x00000230 UnityEngine.Vector3 Antipixel.Vector3Extensions::SetX(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_SetX_m81CF6035CEB02FC9564EC3FDA6F022DAFC315718 (void);
// 0x00000231 UnityEngine.Vector3 Antipixel.Vector3Extensions::SetY(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_SetY_m80B1B14A61DFD9644F2738CFF13D2C0AE4E1B47C (void);
// 0x00000232 UnityEngine.Vector3 Antipixel.Vector3Extensions::SetZ(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_SetZ_mA4B15836E624115C548DC4DBC9C231B915425170 (void);
// 0x00000233 UnityEngine.Vector3 Antipixel.Vector3Extensions::Add(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_Add_m129731B6B0B7D716826167689EF58602DE19DEB0 (void);
// 0x00000234 UnityEngine.Vector3 Antipixel.Vector3Extensions::Add(UnityEngine.Vector3,System.Single,System.Single,System.Single)
extern void Vector3Extensions_Add_m427C7F3B429DB4D3D6885A799BCB22A8BB746D10 (void);
// 0x00000235 UnityEngine.Vector3 Antipixel.Vector3Extensions::AddX(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_AddX_m091C22A33637623414F59CF8E760945DC978FE0A (void);
// 0x00000236 UnityEngine.Vector3 Antipixel.Vector3Extensions::AddY(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_AddY_m3DC1D188941755AA4B0EC34B71E1EB4F9443EE3F (void);
// 0x00000237 UnityEngine.Vector3 Antipixel.Vector3Extensions::AddZ(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_AddZ_m167854E93C83055C59F075127BC017ACC4BFCE30 (void);
// 0x00000238 UnityEngine.Vector3 Antipixel.Vector3Extensions::Multiply(UnityEngine.Vector3,System.Single,System.Single,System.Single)
extern void Vector3Extensions_Multiply_m59A3EB0E5AC45440069BA4638FE5574337F16C24 (void);
// 0x00000239 UnityEngine.Vector3 Antipixel.Vector3Extensions::MultiplyX(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_MultiplyX_m01BB07BBFF31318D2D904375EA6229EC98269AE7 (void);
// 0x0000023A UnityEngine.Vector3 Antipixel.Vector3Extensions::MultiplyY(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_MultiplyY_mD8206F4F19AC05F89F814A1A3A1FC066B8FAD3F7 (void);
// 0x0000023B UnityEngine.Vector3 Antipixel.Vector3Extensions::MultiplyZ(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_MultiplyZ_m4125A5E09C216CAB14745C8B807416602546717B (void);
// 0x0000023C UnityEngine.Vector3 Antipixel.Vector3Extensions::FlipX(UnityEngine.Vector3)
extern void Vector3Extensions_FlipX_mE1C6AA3FB735AE73620ACAD1B024CE661DED4950 (void);
// 0x0000023D UnityEngine.Vector3 Antipixel.Vector3Extensions::FlipX(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_FlipX_m1A831DF6FF2320D87367883EC610D1C0F9ADFFF3 (void);
// 0x0000023E UnityEngine.Vector3 Antipixel.Vector3Extensions::FlipY(UnityEngine.Vector3)
extern void Vector3Extensions_FlipY_m947BF3D1C40A38EFBA386A04E0AE3D2BBEA34C5F (void);
// 0x0000023F UnityEngine.Vector3 Antipixel.Vector3Extensions::FlipY(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_FlipY_m8B0B2087D7210C8C7E8478EE593DC76808C60F24 (void);
// 0x00000240 UnityEngine.Vector3 Antipixel.Vector3Extensions::FlipZ(UnityEngine.Vector3)
extern void Vector3Extensions_FlipZ_mB79D528327D548D16D0EDF4054F0B123CBBE7A53 (void);
// 0x00000241 UnityEngine.Vector3 Antipixel.Vector3Extensions::FlipZ(UnityEngine.Vector3,System.Single)
extern void Vector3Extensions_FlipZ_mC50F3E941648FC54AECB6258E6A700F509223729 (void);
// 0x00000242 UnityEngine.Vector3 Antipixel.Vector3Extensions::Clamp(UnityEngine.Vector3,System.Single,System.Single)
extern void Vector3Extensions_Clamp_m560F956CF30672AAEBDE86DAE2151F22C3BA4650 (void);
// 0x00000243 UnityEngine.Vector3 Antipixel.Vector3Extensions::Clamp01(UnityEngine.Vector3)
extern void Vector3Extensions_Clamp01_mA5FB3155EFFD290F0EF094B52B33D1073D6B031B (void);
// 0x00000244 UnityEngine.Vector3 Antipixel.Vector3Extensions::Abs(UnityEngine.Vector3)
extern void Vector3Extensions_Abs_mE81EE8A5C7EE28268CF0D1B91AC614564E3F772F (void);
// 0x00000245 System.Boolean Antipixel.Vector3Extensions::IsNaN(UnityEngine.Vector3)
extern void Vector3Extensions_IsNaN_m8BBB8CB0F0A766C32ECD86496E0C09DF33EFCF6D (void);
// 0x00000246 System.Boolean Antipixel.Vector3Extensions::IsBetween(UnityEngine.Vector3,System.Single,System.Single)
extern void Vector3Extensions_IsBetween_m7AF18A71EECB9DC544FE13BACD8BF22CE82558EC (void);
// 0x00000247 UnityEngine.Vector3 Antipixel.Vector3Extensions::Random(UnityEngine.Vector3,System.Single,System.Single,System.Boolean)
extern void Vector3Extensions_Random_m4DEBFEDD768D01361BD810FD1C154E142F2FEA3B (void);
// 0x00000248 UnityEngine.Vector3Int Antipixel.Vector3Extensions::SetX(UnityEngine.Vector3Int,System.Int32)
extern void Vector3Extensions_SetX_m163A6AC84AEE45EF5BC9FAE6C13455CE7742CC6D (void);
// 0x00000249 UnityEngine.Vector3Int Antipixel.Vector3Extensions::SetY(UnityEngine.Vector3Int,System.Int32)
extern void Vector3Extensions_SetY_m63B70C6B67A6D6B7ACA684FC65DC0D7B7FC7A9D0 (void);
// 0x0000024A UnityEngine.Vector3Int Antipixel.Vector3Extensions::SetZ(UnityEngine.Vector3Int,System.Int32)
extern void Vector3Extensions_SetZ_m5EE056686D97AAD916B9C0B85DD362B93F993C51 (void);
// 0x0000024B UnityEngine.Vector3Int Antipixel.Vector3Extensions::Add(UnityEngine.Vector3Int,System.Int32)
extern void Vector3Extensions_Add_mA7D11EB57D79A27310CA41C27F96C8E4351784A6 (void);
// 0x0000024C UnityEngine.Vector3Int Antipixel.Vector3Extensions::Add(UnityEngine.Vector3Int,System.Int32,System.Int32,System.Int32)
extern void Vector3Extensions_Add_mB57C1DC5B450AF7750A847B09F1FD340260D8287 (void);
// 0x0000024D UnityEngine.Vector3Int Antipixel.Vector3Extensions::AddX(UnityEngine.Vector3Int,System.Int32)
extern void Vector3Extensions_AddX_mC7DED0E7E3A689D23932A95CD5E6635344009FEF (void);
// 0x0000024E UnityEngine.Vector3Int Antipixel.Vector3Extensions::AddY(UnityEngine.Vector3Int,System.Int32)
extern void Vector3Extensions_AddY_m70CF1032AA0D84F1DE1BF94260CB0797023B4E7A (void);
// 0x0000024F UnityEngine.Vector3Int Antipixel.Vector3Extensions::AddZ(UnityEngine.Vector3Int,System.Int32)
extern void Vector3Extensions_AddZ_mF3ADC52DBBE7EA3D772E254573B7F3BB28C4525F (void);
// 0x00000250 UnityEngine.Vector3Int Antipixel.Vector3Extensions::Multiply(UnityEngine.Vector3Int,System.Int32,System.Int32,System.Int32)
extern void Vector3Extensions_Multiply_m395208DA68CD04BC047C164060D1AD22FBE4AA40 (void);
// 0x00000251 UnityEngine.Vector3Int Antipixel.Vector3Extensions::MultiplyX(UnityEngine.Vector3Int,System.Int32)
extern void Vector3Extensions_MultiplyX_mCFBFA0C826EE539A6EB978C03C7C3C4A79C68082 (void);
// 0x00000252 UnityEngine.Vector3Int Antipixel.Vector3Extensions::MultiplyY(UnityEngine.Vector3Int,System.Int32)
extern void Vector3Extensions_MultiplyY_m49A31473D6CD98AA93115BAFAC87F01A5C742071 (void);
// 0x00000253 UnityEngine.Vector3Int Antipixel.Vector3Extensions::MultiplyZ(UnityEngine.Vector3Int,System.Int32)
extern void Vector3Extensions_MultiplyZ_m534D5268445D560788BC0051115F7FDDF7586DCA (void);
// 0x00000254 UnityEngine.Vector3Int Antipixel.Vector3Extensions::FlipX(UnityEngine.Vector3Int)
extern void Vector3Extensions_FlipX_m43688C951EC6BD33AB086671DFA5FF4107126D63 (void);
// 0x00000255 UnityEngine.Vector3Int Antipixel.Vector3Extensions::FlipY(UnityEngine.Vector3Int)
extern void Vector3Extensions_FlipY_mE1F7250AB3D50229D14DED692C2DB294658043F1 (void);
// 0x00000256 UnityEngine.Vector3Int Antipixel.Vector3Extensions::FlipZ(UnityEngine.Vector3Int)
extern void Vector3Extensions_FlipZ_m82CBD59675BB1FA10165B5BF48472E1F2B213340 (void);
// 0x00000257 UnityEngine.Vector3Int Antipixel.Vector3Extensions::Clamp(UnityEngine.Vector3Int,System.Single,System.Single)
extern void Vector3Extensions_Clamp_mB2DF32F829692E3ADE4E5BD158D2CFF86AFE7D3A (void);
// 0x00000258 UnityEngine.Vector3Int Antipixel.Vector3Extensions::Clamp01(UnityEngine.Vector3Int)
extern void Vector3Extensions_Clamp01_m2EA27A8E453821993BEB3EAB65B067EB8B11409C (void);
// 0x00000259 UnityEngine.Vector3Int Antipixel.Vector3Extensions::Abs(UnityEngine.Vector3Int)
extern void Vector3Extensions_Abs_m64AA0B24024192CF433F91B6B6EB9C7A402FDDD6 (void);
// 0x0000025A UnityEngine.Vector3Int Antipixel.Vector3Extensions::Random(UnityEngine.Vector3Int,System.Int32,System.Int32,System.Boolean)
extern void Vector3Extensions_Random_m1C1CF68F0D0FC1AB9A6B6210ABD7B74CD803628F (void);
// 0x0000025B System.Boolean Antipixel.Vector3Extensions::IsNaN(UnityEngine.Vector3Int)
extern void Vector3Extensions_IsNaN_m6487769847AF0456E47459EF5D4EEE557AC23DE9 (void);
// 0x0000025C System.Boolean Antipixel.Vector3Extensions::IsBetween(UnityEngine.Vector3Int,System.Int32,System.Int32)
extern void Vector3Extensions_IsBetween_mF858EF76A0CE1AEEA83E725B0DE0E53256D609D6 (void);
// 0x0000025D UnityEngine.Vector4 Antipixel.Vector4Extensions::SetX(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_SetX_m4488FC494CB2AF61DA8CA4B27325D5C6347D2A46 (void);
// 0x0000025E UnityEngine.Vector4 Antipixel.Vector4Extensions::SetY(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_SetY_m88F360368C14B924564A67CB65176F828F88E65E (void);
// 0x0000025F UnityEngine.Vector4 Antipixel.Vector4Extensions::SetZ(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_SetZ_m5B05CBDF245A95D31DF067551B66F7D95CDA821D (void);
// 0x00000260 UnityEngine.Vector4 Antipixel.Vector4Extensions::SetW(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_SetW_m58268842AF83B79D88E1ECD3769B6E04CA223956 (void);
// 0x00000261 UnityEngine.Vector4 Antipixel.Vector4Extensions::Add(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_Add_m75644FE0B835214634EB48EF8A397874D6040CC1 (void);
// 0x00000262 UnityEngine.Vector4 Antipixel.Vector4Extensions::Add(UnityEngine.Vector4,System.Single,System.Single,System.Single,System.Single)
extern void Vector4Extensions_Add_mB764E997232736FAF86908C91A26F422CF99150B (void);
// 0x00000263 UnityEngine.Vector4 Antipixel.Vector4Extensions::AddX(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_AddX_m921E63F49FDA98902D1A3AFB611E42B1699733B0 (void);
// 0x00000264 UnityEngine.Vector4 Antipixel.Vector4Extensions::AddY(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_AddY_mC95FB316204D4DDEDE5E75E5C3B06E22E4C34C2D (void);
// 0x00000265 UnityEngine.Vector4 Antipixel.Vector4Extensions::AddZ(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_AddZ_m888AD463EF6716CE6A65360AD29EF75F9664C824 (void);
// 0x00000266 UnityEngine.Vector4 Antipixel.Vector4Extensions::AddW(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_AddW_m3B5CBAAD90A8722B6884F299766CD5CDBBCBA98B (void);
// 0x00000267 UnityEngine.Vector4 Antipixel.Vector4Extensions::Multiply(UnityEngine.Vector4,System.Single,System.Single,System.Single,System.Single)
extern void Vector4Extensions_Multiply_m28E187B218F792582C81A26C2AA4C2BEED3AAD81 (void);
// 0x00000268 UnityEngine.Vector4 Antipixel.Vector4Extensions::MultiplyX(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_MultiplyX_mAA0033CABA21A8A4AC3D5A25F96427DA23981E04 (void);
// 0x00000269 UnityEngine.Vector4 Antipixel.Vector4Extensions::MultiplyY(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_MultiplyY_m2F195487328CDFEB4AB2F8CAC2F0528974012832 (void);
// 0x0000026A UnityEngine.Vector4 Antipixel.Vector4Extensions::MultiplyZ(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_MultiplyZ_m6B49DF8E2A118803DED30A63BF92F00DCD59B9D4 (void);
// 0x0000026B UnityEngine.Vector4 Antipixel.Vector4Extensions::MultiplyW(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_MultiplyW_m82DD4E43D50E114A94CAF9C07CE2FB82C366A77F (void);
// 0x0000026C UnityEngine.Vector4 Antipixel.Vector4Extensions::FlipX(UnityEngine.Vector4)
extern void Vector4Extensions_FlipX_m4FE0F1E12B406F3FD14AF98556E925E81AE7789C (void);
// 0x0000026D UnityEngine.Vector4 Antipixel.Vector4Extensions::FlipX(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_FlipX_m075997348588562CD28F7D3792725F51CEB40220 (void);
// 0x0000026E UnityEngine.Vector4 Antipixel.Vector4Extensions::FlipY(UnityEngine.Vector4)
extern void Vector4Extensions_FlipY_m18F8D19C98CC1A8DFDE8784E33C29DE27D86E9B5 (void);
// 0x0000026F UnityEngine.Vector4 Antipixel.Vector4Extensions::FlipY(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_FlipY_m47B8E95F76846CF7B4FB52E781616DD2099A81BE (void);
// 0x00000270 UnityEngine.Vector4 Antipixel.Vector4Extensions::FlipZ(UnityEngine.Vector4)
extern void Vector4Extensions_FlipZ_m7EA7F38B981936AED7E919AF0CD5280696E98560 (void);
// 0x00000271 UnityEngine.Vector4 Antipixel.Vector4Extensions::FlipZ(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_FlipZ_m4CCFC1573B94ADC023952CEDF009A143E999204B (void);
// 0x00000272 UnityEngine.Vector4 Antipixel.Vector4Extensions::FlipW(UnityEngine.Vector4)
extern void Vector4Extensions_FlipW_m74E8AD6CD37BF06788D3E664C8308BF37BA4F0F3 (void);
// 0x00000273 UnityEngine.Vector4 Antipixel.Vector4Extensions::FlipW(UnityEngine.Vector4,System.Single)
extern void Vector4Extensions_FlipW_mBC9BDF2CE1DF86A8EE1FDCAC8EE5707C80636DA8 (void);
// 0x00000274 UnityEngine.Vector4 Antipixel.Vector4Extensions::Clamp(UnityEngine.Vector4,System.Single,System.Single)
extern void Vector4Extensions_Clamp_m4D46C45F7D1CD35ECDCD4FAACB54E027772BCF3E (void);
// 0x00000275 UnityEngine.Vector4 Antipixel.Vector4Extensions::Clamp01(UnityEngine.Vector4)
extern void Vector4Extensions_Clamp01_m5D536C744535AA0842C612FB34084D106E758199 (void);
// 0x00000276 UnityEngine.Vector4 Antipixel.Vector4Extensions::Abs(UnityEngine.Vector4)
extern void Vector4Extensions_Abs_m4934C92C8D119F43FBE87A923B0D6F69D70FCBFC (void);
// 0x00000277 System.Boolean Antipixel.Vector4Extensions::IsNaN(UnityEngine.Vector4)
extern void Vector4Extensions_IsNaN_m0D9B34D85906B29320C3A779695A5F3C49545615 (void);
// 0x00000278 System.Boolean Antipixel.Vector4Extensions::IsBetween(UnityEngine.Vector4,System.Single,System.Single)
extern void Vector4Extensions_IsBetween_m7760AE4387A1C698F72BC1EDEEBECF87039470AD (void);
// 0x00000279 UnityEngine.Vector4 Antipixel.Vector4Extensions::Random(UnityEngine.Vector4,System.Single,System.Single,System.Boolean)
extern void Vector4Extensions_Random_m75E196B5C836C8D8C82CB32CF7A9D8F1BD024D4E (void);
// 0x0000027A UnityEngine.Vector3 Antipixel.BoxDetector::get_Size()
extern void BoxDetector_get_Size_mB4D022DCB52FA854F4040BC282E98BE19BA03A28 (void);
// 0x0000027B System.Void Antipixel.BoxDetector::UpdateDetector()
extern void BoxDetector_UpdateDetector_m607BE07DF6D72784AE66C6680543003902E464F0 (void);
// 0x0000027C System.Void Antipixel.BoxDetector::.ctor()
extern void BoxDetector__ctor_m3EE1E981229B09669B64587D6990ECE5C9F3BD5D (void);
// 0x0000027D UnityEngine.Events.UnityEvent`1<UnityEngine.Collision> Antipixel.CollisionDetector::get_OnEnterCollision()
extern void CollisionDetector_get_OnEnterCollision_m552AE5930F972CF920E0767FECEEA6E8E404CCDB (void);
// 0x0000027E System.Void Antipixel.CollisionDetector::set_OnEnterCollision(UnityEngine.Events.UnityEvent`1<UnityEngine.Collision>)
extern void CollisionDetector_set_OnEnterCollision_m6F9F5443517CAE3BA616DAC973C338491D720DDA (void);
// 0x0000027F UnityEngine.Events.UnityEvent`1<UnityEngine.Collision> Antipixel.CollisionDetector::get_OnExitCollision()
extern void CollisionDetector_get_OnExitCollision_mFCE873DF650709BA935EA417D700D44B124233F7 (void);
// 0x00000280 System.Void Antipixel.CollisionDetector::set_OnExitCollision(UnityEngine.Events.UnityEvent`1<UnityEngine.Collision>)
extern void CollisionDetector_set_OnExitCollision_mABEA96E854787882105ECE55D298B4F958EFF49D (void);
// 0x00000281 UnityEngine.Events.UnityEvent`1<UnityEngine.Collision2D> Antipixel.CollisionDetector::get_OnEnterCollision2D()
extern void CollisionDetector_get_OnEnterCollision2D_m65D34D5EE180EDF0087CD7B17B03C5F86BB83627 (void);
// 0x00000282 System.Void Antipixel.CollisionDetector::set_OnEnterCollision2D(UnityEngine.Events.UnityEvent`1<UnityEngine.Collision2D>)
extern void CollisionDetector_set_OnEnterCollision2D_mB1E65053C541FA44FFD1ACAE3AB159B5755A91D8 (void);
// 0x00000283 UnityEngine.Events.UnityEvent`1<UnityEngine.Collision2D> Antipixel.CollisionDetector::get_OnExitCollision2D()
extern void CollisionDetector_get_OnExitCollision2D_mA070591D65C0365EB7EEB1938FB063D8F4D1E57D (void);
// 0x00000284 System.Void Antipixel.CollisionDetector::set_OnExitCollision2D(UnityEngine.Events.UnityEvent`1<UnityEngine.Collision2D>)
extern void CollisionDetector_set_OnExitCollision2D_mAB23E7E7ABB23329F387123FD0F55B8393DE80B1 (void);
// 0x00000285 System.Boolean Antipixel.CollisionDetector::get_IsCollisionDetector()
extern void CollisionDetector_get_IsCollisionDetector_m35663F8771BDB74AD0DA95938E9536DC8A529D94 (void);
// 0x00000286 System.Void Antipixel.CollisionDetector::OnCollisionEnter(UnityEngine.Collision)
extern void CollisionDetector_OnCollisionEnter_m3756EDA1589191C18F207910528A4C06C2003A4D (void);
// 0x00000287 System.Void Antipixel.CollisionDetector::OnCollisionExit(UnityEngine.Collision)
extern void CollisionDetector_OnCollisionExit_mE6D4224BC6FDC46A28707E8AF6C9B0CC3141EC6D (void);
// 0x00000288 System.Void Antipixel.CollisionDetector::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void CollisionDetector_OnCollisionEnter2D_m1111D6F15643850AA6BDC77F2D0975C3A9D0EC5A (void);
// 0x00000289 System.Void Antipixel.CollisionDetector::OnCollisionExit2D(UnityEngine.Collision2D)
extern void CollisionDetector_OnCollisionExit2D_m0E3BC8BDA6EEEE7D4D174554317DD6EE08612B97 (void);
// 0x0000028A System.Void Antipixel.CollisionDetector::OnTriggerEnter(UnityEngine.Collider)
extern void CollisionDetector_OnTriggerEnter_mA51E7E61F41ACF372AA145DD733C7E9A2E6CAD2E (void);
// 0x0000028B System.Void Antipixel.CollisionDetector::OnTriggerExit(UnityEngine.Collider)
extern void CollisionDetector_OnTriggerExit_m1F3341D73495D924B2983F058F12C2C848E6015F (void);
// 0x0000028C System.Void Antipixel.CollisionDetector::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CollisionDetector_OnTriggerEnter2D_mFAE14ECA33644098950358D44C7DB17C80DB5689 (void);
// 0x0000028D System.Void Antipixel.CollisionDetector::OnTriggerExit2D(UnityEngine.Collider2D)
extern void CollisionDetector_OnTriggerExit2D_mE7131A286E64473EC1A5FF59480CA8230C579599 (void);
// 0x0000028E System.Void Antipixel.CollisionDetector::Process(T,System.Boolean)
// 0x0000028F System.Void Antipixel.CollisionDetector::.ctor()
extern void CollisionDetector__ctor_m7B3FDF5BF025B1FA6711DF9304CDE492A0BE7AA5 (void);
// 0x00000290 System.Void Antipixel.ColliderDelegate::.ctor(System.Object,System.IntPtr)
extern void ColliderDelegate__ctor_m6A49AADFADF039C7804E2EB75027E7135F0BA1E1 (void);
// 0x00000291 System.Void Antipixel.ColliderDelegate::Invoke(UnityEngine.Collider)
extern void ColliderDelegate_Invoke_m7CF676CBC8ABBD9A051D549BB367ECC23EEA64B8 (void);
// 0x00000292 System.IAsyncResult Antipixel.ColliderDelegate::BeginInvoke(UnityEngine.Collider,System.AsyncCallback,System.Object)
extern void ColliderDelegate_BeginInvoke_m7A29544F198EC37BDACA4C76CAF5D6D35A95D60D (void);
// 0x00000293 System.Void Antipixel.ColliderDelegate::EndInvoke(System.IAsyncResult)
extern void ColliderDelegate_EndInvoke_mFF8F287FDE5778370E1DEA0F1CB8229BC3C1F70E (void);
// 0x00000294 System.Void Antipixel.ColliderArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void ColliderArrayDelegate__ctor_m769A8069342C6A4AF776BAC3B53D74C3256B9724 (void);
// 0x00000295 System.Void Antipixel.ColliderArrayDelegate::Invoke(UnityEngine.Collider[])
extern void ColliderArrayDelegate_Invoke_mCB9ADCD520138A95966828AC016976926ACA6DD3 (void);
// 0x00000296 System.IAsyncResult Antipixel.ColliderArrayDelegate::BeginInvoke(UnityEngine.Collider[],System.AsyncCallback,System.Object)
extern void ColliderArrayDelegate_BeginInvoke_m7CE4DC0A2E7BFFD899EDA7BE2A43C31EEB319079 (void);
// 0x00000297 System.Void Antipixel.ColliderArrayDelegate::EndInvoke(System.IAsyncResult)
extern void ColliderArrayDelegate_EndInvoke_mE7E299D64FEB85DB476E0DADB6F913FD10FC9A51 (void);
// 0x00000298 System.Void Antipixel.Collider2DDelegate::.ctor(System.Object,System.IntPtr)
extern void Collider2DDelegate__ctor_mEFBDC98962613FC73036038D992851373229154C (void);
// 0x00000299 System.Void Antipixel.Collider2DDelegate::Invoke(UnityEngine.Collider2D)
extern void Collider2DDelegate_Invoke_mC04C22342134DDD09470CE9883C2585191EE7EB4 (void);
// 0x0000029A System.IAsyncResult Antipixel.Collider2DDelegate::BeginInvoke(UnityEngine.Collider2D,System.AsyncCallback,System.Object)
extern void Collider2DDelegate_BeginInvoke_mB9B67346D802B8179DABB929611AC6A8CECB700C (void);
// 0x0000029B System.Void Antipixel.Collider2DDelegate::EndInvoke(System.IAsyncResult)
extern void Collider2DDelegate_EndInvoke_m431BF66AD4E0BF102FE519E36BB818EC1B37A2F1 (void);
// 0x0000029C System.Void Antipixel.Collider2DArrayDelegate::.ctor(System.Object,System.IntPtr)
extern void Collider2DArrayDelegate__ctor_m667B1E6867506A59717A4D5159935EBDA5B0923E (void);
// 0x0000029D System.Void Antipixel.Collider2DArrayDelegate::Invoke(UnityEngine.Collider2D[])
extern void Collider2DArrayDelegate_Invoke_m00E88726219FCF5254279C67D028EA9FB2B0A282 (void);
// 0x0000029E System.IAsyncResult Antipixel.Collider2DArrayDelegate::BeginInvoke(UnityEngine.Collider2D[],System.AsyncCallback,System.Object)
extern void Collider2DArrayDelegate_BeginInvoke_mE223482AB75085AF2CF5F001EE225DC09DE4721F (void);
// 0x0000029F System.Void Antipixel.Collider2DArrayDelegate::EndInvoke(System.IAsyncResult)
extern void Collider2DArrayDelegate_EndInvoke_mCB4B5992EEFB58E55183D083EA17B769DE820378 (void);
// 0x000002A0 System.Void Antipixel.CollisionDelegate::.ctor(System.Object,System.IntPtr)
extern void CollisionDelegate__ctor_m882FA50F7D18B3F52DF5E432B8122A810321650C (void);
// 0x000002A1 System.Void Antipixel.CollisionDelegate::Invoke(UnityEngine.Collision)
extern void CollisionDelegate_Invoke_m1D5B8620A4B95B12EB1755F84836C5F16BFA2240 (void);
// 0x000002A2 System.IAsyncResult Antipixel.CollisionDelegate::BeginInvoke(UnityEngine.Collision,System.AsyncCallback,System.Object)
extern void CollisionDelegate_BeginInvoke_m86ECE784A5F2ACBA080C9640A2E70109ED7CF490 (void);
// 0x000002A3 System.Void Antipixel.CollisionDelegate::EndInvoke(System.IAsyncResult)
extern void CollisionDelegate_EndInvoke_m1A19646EF8AB7825C5388F99BA0A8F0E8F4CA653 (void);
// 0x000002A4 System.Void Antipixel.Collision2DDelegate::.ctor(System.Object,System.IntPtr)
extern void Collision2DDelegate__ctor_mFEC810C6FC228CE0A4A0B4B214C34A9AE3ACA6ED (void);
// 0x000002A5 System.Void Antipixel.Collision2DDelegate::Invoke(UnityEngine.Collision2D)
extern void Collision2DDelegate_Invoke_mD8DA2B3A541FBCE892CFB5B42542EDA744393BDF (void);
// 0x000002A6 System.IAsyncResult Antipixel.Collision2DDelegate::BeginInvoke(UnityEngine.Collision2D,System.AsyncCallback,System.Object)
extern void Collision2DDelegate_BeginInvoke_mEC2E882368777A0AF1DE500828688F6BD10D6BC4 (void);
// 0x000002A7 System.Void Antipixel.Collision2DDelegate::EndInvoke(System.IAsyncResult)
extern void Collision2DDelegate_EndInvoke_m3AF2FAD2EF4F88ADA1DFECFEF611B89F517AABCA (void);
// 0x000002A8 System.Boolean Antipixel.Detector::get_IsCollisionDetector()
extern void Detector_get_IsCollisionDetector_mF21FA01B1A8AB55B2CC0403AF849AAB2EDB119B8 (void);
// 0x000002A9 System.Void Antipixel.Detector::Awake()
extern void Detector_Awake_m0A846996A9D08EBAC4C9A92A6BCE97B5F695DB90 (void);
// 0x000002AA System.Void Antipixel.Detector::UpdateDetector()
extern void Detector_UpdateDetector_m7309EC77733C6EF5D9437EE76653D34C59489D85 (void);
// 0x000002AB UnityEngine.Collider[] Antipixel.Detector::Detect3D(UnityEngine.Collider[])
extern void Detector_Detect3D_mF66CDDE559E00F2E71CE982F5DD9FD788F45E971 (void);
// 0x000002AC UnityEngine.Collider2D[] Antipixel.Detector::Detect2D(UnityEngine.Collider2D[])
extern void Detector_Detect2D_m96B9CB1FABEA1A54DF93D046C09171C785766430 (void);
// 0x000002AD System.Boolean Antipixel.Detector::IsValidCollider(T)
// 0x000002AE System.Void Antipixel.Detector::.ctor()
extern void Detector__ctor_m0EFEB7B47AEDAE653674618F31E1E97CC52B08F3 (void);
// 0x000002AF System.Boolean Antipixel.Detector::<Detect3D>b__16_0(UnityEngine.Collider)
extern void Detector_U3CDetect3DU3Eb__16_0_m4470C692412E4DB45B299A874A4D6EF37BB71B35 (void);
// 0x000002B0 System.Boolean Antipixel.Detector::<Detect2D>b__17_0(UnityEngine.Collider2D)
extern void Detector_U3CDetect2DU3Eb__17_0_mA99BEFCE95AB60BBC004150C2CC3C982C547F218 (void);
// 0x000002B1 System.Single Antipixel.RayDetector::get_Distance()
extern void RayDetector_get_Distance_m4A3300B64FAFBAAEB601B0FB59E46ED8C9A5F1B3 (void);
// 0x000002B2 System.Void Antipixel.RayDetector::UpdateDetector()
extern void RayDetector_UpdateDetector_m01CB5A7255CD9F60D423F77ACF19383E7504FB12 (void);
// 0x000002B3 System.Void Antipixel.RayDetector::.ctor()
extern void RayDetector__ctor_m08080361A45A38E1DCA019D5AC99918728BA2A78 (void);
// 0x000002B4 System.Single Antipixel.SphereDetector::get_Radius()
extern void SphereDetector_get_Radius_m66781B323F6657FFA37AB47221954DA1ADF2496A (void);
// 0x000002B5 System.Void Antipixel.SphereDetector::UpdateDetector()
extern void SphereDetector_UpdateDetector_mD3ECA8C2483C419DA4D0BE31E8E6563215BEDD5B (void);
// 0x000002B6 System.Void Antipixel.SphereDetector::.ctor()
extern void SphereDetector__ctor_mA336CA54662CE11F66CD3D171400637458AF05B4 (void);
// 0x000002B7 Antipixel.ShakeData Antipixel.ShakeController::get_Data()
extern void ShakeController_get_Data_m9194007F30FE902F418E3719CC6589BAACBD2A97 (void);
// 0x000002B8 System.Void Antipixel.ShakeController::set_Data(Antipixel.ShakeData)
extern void ShakeController_set_Data_mCC4E4F1BDCA181EAA83C186945435EC73923CF95 (void);
// 0x000002B9 UnityEngine.Vector3 Antipixel.ShakeController::get_Noise()
extern void ShakeController_get_Noise_m6430E01E097318FB62F2392358856AD0436AA177 (void);
// 0x000002BA System.Void Antipixel.ShakeController::set_Noise(UnityEngine.Vector3)
extern void ShakeController_set_Noise_mFB37CA32FC05BAE468506C2ED111B29AED5194BA (void);
// 0x000002BB System.Boolean Antipixel.ShakeController::get_IsRunning()
extern void ShakeController_get_IsRunning_m92F84540AF648FBB151514751AF29A6EF75EBDBB (void);
// 0x000002BC System.Void Antipixel.ShakeController::.ctor(Antipixel.ShakeData)
extern void ShakeController__ctor_m4F9985FE2005869CE7B3D81C863AF79277752F70 (void);
// 0x000002BD System.Void Antipixel.ShakeController::Update()
extern void ShakeController_Update_m0492DE796E36B7C61A757F31552E83AA86DE3F16 (void);
// 0x000002BE System.Void Antipixel.ShakeData::Shake(UnityEngine.Transform)
extern void ShakeData_Shake_m6B701676891B6E72AC734B1DED499ADC2CE2327D (void);
// 0x000002BF System.Void Antipixel.ShakeData::.ctor()
extern void ShakeData__ctor_mDCEE67EACA53EA1EC11EE8E97BBF10BB0E4C7D71 (void);
// 0x000002C0 Antipixel.ShakeController[] Antipixel.Shaker::get_Shakes()
extern void Shaker_get_Shakes_m16E0A104B3A0951DF9BA6D427289A34165C88736 (void);
// 0x000002C1 System.Void Antipixel.Shaker::set_Shakes(Antipixel.ShakeController[])
extern void Shaker_set_Shakes_m69643E7D4F2FEC0A5B164550D74979D12E0D60D1 (void);
// 0x000002C2 System.Void Antipixel.Shaker::LateUpdate()
extern void Shaker_LateUpdate_mA91DE50B6F15DDBDE3EE4881BBE00EE30E862242 (void);
// 0x000002C3 System.Void Antipixel.Shaker::Shake(Antipixel.ShakeData)
extern void Shaker_Shake_m864C274A3EDE0FB0CBE2CD10ACCA77BE9D69C26C (void);
// 0x000002C4 System.Void Antipixel.Shaker::.ctor()
extern void Shaker__ctor_mC4282CF1A05F4DB3F8440D13B561F2A3814A6CC3 (void);
// 0x000002C5 Antipixel.SoundEffect Antipixel.SoundController::get_Sound()
extern void SoundController_get_Sound_mBA2B474E8303D1A09B3EFB797D4ECC3C0932FC73 (void);
// 0x000002C6 System.Void Antipixel.SoundController::set_Sound(Antipixel.SoundEffect)
extern void SoundController_set_Sound_mAFFB12A928462D41B9316599893546B4D6FC993E (void);
// 0x000002C7 UnityEngine.AudioSource Antipixel.SoundController::get_Source()
extern void SoundController_get_Source_m17A382415CE8079DBE7279043BF410B844F45448 (void);
// 0x000002C8 System.Void Antipixel.SoundController::set_Source(UnityEngine.AudioSource)
extern void SoundController_set_Source_m6B3FF429CE8B1FEEEE895EC0E499F15E53FE049A (void);
// 0x000002C9 System.Single Antipixel.SoundController::get_Duration()
extern void SoundController_get_Duration_m9C3DB8864780E96F6C6342E86ABB9F0A72C0BE7D (void);
// 0x000002CA System.Boolean Antipixel.SoundController::get_IsBackward()
extern void SoundController_get_IsBackward_m469505E5F0A7E71BB2D9179C4623C16636EF0952 (void);
// 0x000002CB System.Single Antipixel.SoundController::get_StartTime()
extern void SoundController_get_StartTime_mD5BAF345D317D2F644A8D5CE7DF4BE95511E8437 (void);
// 0x000002CC System.Single Antipixel.SoundController::get_EndTime()
extern void SoundController_get_EndTime_mFB82D49A344708B8E82CA1997205114C7B3A7A69 (void);
// 0x000002CD System.Boolean Antipixel.SoundController::get_HasFinished()
extern void SoundController_get_HasFinished_mA9561FBEC3B4C809DF111896361ED8A09CC4FB3C (void);
// 0x000002CE System.Boolean Antipixel.SoundController::get_NextCycle()
extern void SoundController_get_NextCycle_mF4C2639975FE01B34976AB1A6821B3446EF578EC (void);
// 0x000002CF System.Void Antipixel.SoundController::OnEnable()
extern void SoundController_OnEnable_m98E90F08F90C75EE7539925D17017DFFD05DC259 (void);
// 0x000002D0 System.Void Antipixel.SoundController::OnDestroy()
extern void SoundController_OnDestroy_m0C9BA0F93801AC2ABDD3C0806B88CD42A29FD4B4 (void);
// 0x000002D1 System.Void Antipixel.SoundController::Play(Antipixel.SoundEffect,UnityEngine.AudioClip,System.Boolean)
extern void SoundController_Play_m4837D8DEECAFD9F4357C30E573BA5645710A58CF (void);
// 0x000002D2 System.Void Antipixel.SoundController::Stop()
extern void SoundController_Stop_m1F27729E29ED19DC0D35E17D65D11A5933CE585D (void);
// 0x000002D3 System.Void Antipixel.SoundController::Kill()
extern void SoundController_Kill_m572F633CB8D3E2C7EEC0445BCE7B5F6487FF471B (void);
// 0x000002D4 System.Collections.IEnumerator Antipixel.SoundController::InCoroutine(Antipixel.SoundEffect,UnityEngine.AudioClip,System.Boolean)
extern void SoundController_InCoroutine_m33D2FDA6432F130AC83F2D296F45F31229B72508 (void);
// 0x000002D5 System.Collections.IEnumerator Antipixel.SoundController::OutCoroutine()
extern void SoundController_OutCoroutine_m73884E8C7CEFB14569C11DB03BC001D6916673AA (void);
// 0x000002D6 System.Collections.IEnumerator Antipixel.SoundController::SFXUpdate()
extern void SoundController_SFXUpdate_m03BAC1FA20CDAF18E9942DA9944BDFFE203C6778 (void);
// 0x000002D7 System.Void Antipixel.SoundController::NextClip()
extern void SoundController_NextClip_m6F911CB0F2FA9F294BC99015625CE630D6583200 (void);
// 0x000002D8 System.Void Antipixel.SoundController::NextSound()
extern void SoundController_NextSound_mF9C8EB89D877A86D446E1AC8372DFA7AFDA29E69 (void);
// 0x000002D9 System.Void Antipixel.SoundController::SetDynamicValue(UnityEngine.AnimationCurve,Antipixel.FloatDelegate,System.Single,System.Boolean)
extern void SoundController_SetDynamicValue_m8F92564F13BB98C8AFF8AF0ADD67826E91360149 (void);
// 0x000002DA System.Collections.IEnumerator Antipixel.SoundController::CurveCoroutine(UnityEngine.AnimationCurve,Antipixel.FloatDelegate,System.Boolean)
extern void SoundController_CurveCoroutine_m9A698305897A7796C9BE34C2CEAD4189984F72EE (void);
// 0x000002DB System.Void Antipixel.SoundController::SetVolume(System.Single)
extern void SoundController_SetVolume_m7BF6D26DDDD7B18C61DE3C78AFD94E93F8EEADC3 (void);
// 0x000002DC System.Void Antipixel.SoundController::SetPitch(System.Single)
extern void SoundController_SetPitch_mA07E8D1827E24CC9C574041E0FDE6E6C2932F727 (void);
// 0x000002DD System.Void Antipixel.SoundController::Destroy()
extern void SoundController_Destroy_m8579BCB0F9CF0758AF91D215B4EAA49E7B49DC69 (void);
// 0x000002DE System.Void Antipixel.SoundController::DestroyImmediate()
extern void SoundController_DestroyImmediate_m0F89A9443503D9DB00D1F1A23EF7AFDA3F052514 (void);
// 0x000002DF System.Void Antipixel.SoundController::PlayIndex(System.Int32)
extern void SoundController_PlayIndex_m0D94D984F156F00A5C2AD307A61190F668A4DA2B (void);
// 0x000002E0 System.Void Antipixel.SoundController::.ctor()
extern void SoundController__ctor_m84C8C661E258C5BC4A73D8BC619584092CDE19DC (void);
// 0x000002E1 System.Void Antipixel.SoundController/<InCoroutine>d__36::.ctor(System.Int32)
extern void U3CInCoroutineU3Ed__36__ctor_m768A77D62609B5219C8B27E84141983AAD3A3D76 (void);
// 0x000002E2 System.Void Antipixel.SoundController/<InCoroutine>d__36::System.IDisposable.Dispose()
extern void U3CInCoroutineU3Ed__36_System_IDisposable_Dispose_mB6F727542D4FE84C7FA54DB3A654762DF56997E3 (void);
// 0x000002E3 System.Boolean Antipixel.SoundController/<InCoroutine>d__36::MoveNext()
extern void U3CInCoroutineU3Ed__36_MoveNext_mDB4319ADA06596B069A328CDC8BA345232D0F830 (void);
// 0x000002E4 System.Object Antipixel.SoundController/<InCoroutine>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInCoroutineU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68B293F1F04F174F8AB3D39F3117B329F637258B (void);
// 0x000002E5 System.Void Antipixel.SoundController/<InCoroutine>d__36::System.Collections.IEnumerator.Reset()
extern void U3CInCoroutineU3Ed__36_System_Collections_IEnumerator_Reset_m1ABD37031B2464A2450F841151AE1ADF40CE3551 (void);
// 0x000002E6 System.Object Antipixel.SoundController/<InCoroutine>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CInCoroutineU3Ed__36_System_Collections_IEnumerator_get_Current_m721A47FB34BEB99D558CDB207E02E86FBDBBA28C (void);
// 0x000002E7 System.Void Antipixel.SoundController/<OutCoroutine>d__37::.ctor(System.Int32)
extern void U3COutCoroutineU3Ed__37__ctor_mBF75D8F91859D02800985B782AD9B9061034A4CC (void);
// 0x000002E8 System.Void Antipixel.SoundController/<OutCoroutine>d__37::System.IDisposable.Dispose()
extern void U3COutCoroutineU3Ed__37_System_IDisposable_Dispose_mFC1E65CC0E5BED49103F47FA6074CB7CD88B2EFD (void);
// 0x000002E9 System.Boolean Antipixel.SoundController/<OutCoroutine>d__37::MoveNext()
extern void U3COutCoroutineU3Ed__37_MoveNext_mD557DBAA1C27F0238D43AB6780F8BEABDA679822 (void);
// 0x000002EA System.Object Antipixel.SoundController/<OutCoroutine>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COutCoroutineU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA32CEBA53B1DE20AF3F1994A7936BE75142B8F71 (void);
// 0x000002EB System.Void Antipixel.SoundController/<OutCoroutine>d__37::System.Collections.IEnumerator.Reset()
extern void U3COutCoroutineU3Ed__37_System_Collections_IEnumerator_Reset_m7107C4BB8F4BD42DC550CA60ABB427255BAA6F27 (void);
// 0x000002EC System.Object Antipixel.SoundController/<OutCoroutine>d__37::System.Collections.IEnumerator.get_Current()
extern void U3COutCoroutineU3Ed__37_System_Collections_IEnumerator_get_Current_m6A992C1DF0F0D804C810939A396E501E79B35003 (void);
// 0x000002ED System.Void Antipixel.SoundController/<SFXUpdate>d__38::.ctor(System.Int32)
extern void U3CSFXUpdateU3Ed__38__ctor_m07428BBF9A91E3E67C36ED99F3FA91529FE4F06F (void);
// 0x000002EE System.Void Antipixel.SoundController/<SFXUpdate>d__38::System.IDisposable.Dispose()
extern void U3CSFXUpdateU3Ed__38_System_IDisposable_Dispose_m9EE4DCFF6221AAE6E63389B625CC72E7BEC38C04 (void);
// 0x000002EF System.Boolean Antipixel.SoundController/<SFXUpdate>d__38::MoveNext()
extern void U3CSFXUpdateU3Ed__38_MoveNext_mE77ED25AAF32A9A646C77E42EE3E0D99231F1C68 (void);
// 0x000002F0 System.Object Antipixel.SoundController/<SFXUpdate>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSFXUpdateU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84E0A548F2DB3379A1F64D00FED15D945D35E777 (void);
// 0x000002F1 System.Void Antipixel.SoundController/<SFXUpdate>d__38::System.Collections.IEnumerator.Reset()
extern void U3CSFXUpdateU3Ed__38_System_Collections_IEnumerator_Reset_m440C938D24AC7FC03EAB463992F1640D4A9E1780 (void);
// 0x000002F2 System.Object Antipixel.SoundController/<SFXUpdate>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CSFXUpdateU3Ed__38_System_Collections_IEnumerator_get_Current_m386B1BBB881C24922403C0C99C403CF1BF7237D2 (void);
// 0x000002F3 System.Void Antipixel.SoundController/<CurveCoroutine>d__42::.ctor(System.Int32)
extern void U3CCurveCoroutineU3Ed__42__ctor_m7CBE424F8485C6F65BF9B61DC65E567D584FF738 (void);
// 0x000002F4 System.Void Antipixel.SoundController/<CurveCoroutine>d__42::System.IDisposable.Dispose()
extern void U3CCurveCoroutineU3Ed__42_System_IDisposable_Dispose_mAE910B8021B1C839BDD46C8091E823CCC14A1B49 (void);
// 0x000002F5 System.Boolean Antipixel.SoundController/<CurveCoroutine>d__42::MoveNext()
extern void U3CCurveCoroutineU3Ed__42_MoveNext_m6733CCF6B67C1743C31F070C67F5401BB0E7E29E (void);
// 0x000002F6 System.Object Antipixel.SoundController/<CurveCoroutine>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCurveCoroutineU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3D1317D299D2A110D9B57D9017DDA06E96B745D (void);
// 0x000002F7 System.Void Antipixel.SoundController/<CurveCoroutine>d__42::System.Collections.IEnumerator.Reset()
extern void U3CCurveCoroutineU3Ed__42_System_Collections_IEnumerator_Reset_mE505FA3A27B3D11891AC8BF8BB1BBB63FC2627D8 (void);
// 0x000002F8 System.Object Antipixel.SoundController/<CurveCoroutine>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CCurveCoroutineU3Ed__42_System_Collections_IEnumerator_get_Current_m5F69CE3B305929E8E2786EE42A96D602A656CCF8 (void);
// 0x000002F9 UnityEngine.AudioClip Antipixel.SoundEffect::get_Item(System.Int32)
extern void SoundEffect_get_Item_m69B105B90FD522E4153E2F829B887499168C7635 (void);
// 0x000002FA UnityEngine.AudioClip Antipixel.SoundEffect::get_Item(System.String)
extern void SoundEffect_get_Item_m135B3869341995A0DCB489A34C5E79AA1D158443 (void);
// 0x000002FB Antipixel.SoundController Antipixel.SoundEffect::get_Controller()
extern void SoundEffect_get_Controller_m2A5FF64D3317DA9A1E49055A1B6CB8579591E804 (void);
// 0x000002FC System.Void Antipixel.SoundEffect::set_Controller(Antipixel.SoundController)
extern void SoundEffect_set_Controller_mEE20B5A111058B0F288672C8808E32209332B7F6 (void);
// 0x000002FD System.Boolean Antipixel.SoundEffect::get_Error()
extern void SoundEffect_get_Error_mFEABA33E81935C46112A8DDF0D973B2852350326 (void);
// 0x000002FE System.Void Antipixel.SoundEffect::PlayRandomIn(UnityEngine.AudioSource)
extern void SoundEffect_PlayRandomIn_mF43DE75B7245572B9F7DB5200CFA5B4DE3F2C8EC (void);
// 0x000002FF System.Void Antipixel.SoundEffect::PlayRandom()
extern void SoundEffect_PlayRandom_m530BC6833E850408512284CDBF86236C9987DC09 (void);
// 0x00000300 System.Void Antipixel.SoundEffect::PlayIndex(System.Int32)
extern void SoundEffect_PlayIndex_m94A45DD4084396B8DF0EAB04210DD96D7BAD0C79 (void);
// 0x00000301 System.Void Antipixel.SoundEffect::PlayClip(System.String)
extern void SoundEffect_PlayClip_m1957A2B18F0811B95AD20300A82EF5A936B7BAAD (void);
// 0x00000302 Antipixel.SoundController Antipixel.SoundEffect::Play(UnityEngine.AudioSource)
extern void SoundEffect_Play_m6EF2B92FF56F18C888F23662FEC3AEA9E335050A (void);
// 0x00000303 Antipixel.SoundController Antipixel.SoundEffect::Play(System.Int32,UnityEngine.AudioSource)
extern void SoundEffect_Play_m2CF720A7BC1E4FA2CC85B1CFBDEF354ACE5677E6 (void);
// 0x00000304 Antipixel.SoundController Antipixel.SoundEffect::Play(System.String,UnityEngine.AudioSource)
extern void SoundEffect_Play_m7B96EA771ACD5C8D1EF818FEF2770136C15E61A4 (void);
// 0x00000305 Antipixel.SoundController Antipixel.SoundEffect::Play(UnityEngine.AudioClip,UnityEngine.AudioSource)
extern void SoundEffect_Play_mE74F7C13B5A95ADC64B2973E37D3AB61DEF48AF5 (void);
// 0x00000306 System.Int32 Antipixel.SoundEffect::GetIndex(System.String)
extern void SoundEffect_GetIndex_mD0F75D1E7B19347B10798286084AFAB82D57E8A2 (void);
// 0x00000307 System.Void Antipixel.SoundEffect::.ctor()
extern void SoundEffect__ctor_m49005690D984553914506D352F50192754D6537B (void);
// 0x00000308 System.Void Antipixel.SoundEffect/<>c__DisplayClass47_0::.ctor()
extern void U3CU3Ec__DisplayClass47_0__ctor_m6C23AC52ACCA7C462C82CD39CB45DB3427ADE30E (void);
// 0x00000309 System.Boolean Antipixel.SoundEffect/<>c__DisplayClass47_0::<GetIndex>b__0(UnityEngine.AudioClip)
extern void U3CU3Ec__DisplayClass47_0_U3CGetIndexU3Eb__0_m199A71E692136F86F0B35A56BE0D38895B8D6818 (void);
// 0x0000030A System.Single Antipixel.LookAt::get_Speed()
extern void LookAt_get_Speed_m9418E49B8898A0AD0E92C550F40745BB6E45CBE6 (void);
// 0x0000030B UnityEngine.Quaternion Antipixel.LookAt::get_TargetRotation()
extern void LookAt_get_TargetRotation_m1ADB344A9AE82FF57A1B5FFF29D74F03416E3FAA (void);
// 0x0000030C System.Void Antipixel.LookAt::Update()
extern void LookAt_Update_mBC3D46F3B84AE879F0EF9649DAD6B64735B557DA (void);
// 0x0000030D System.Void Antipixel.LookAt::.ctor()
extern void LookAt__ctor_mE8DBF3B4883F69B24A36C8428FE103BFBA57AB74 (void);
// 0x0000030E System.Single Antipixel.MoveTo::get_Speed()
extern void MoveTo_get_Speed_m5CE20DE31090C365A809C4814241E58A6456A822 (void);
// 0x0000030F System.Single Antipixel.MoveTo::get_Smoothing()
extern void MoveTo_get_Smoothing_m9FECF580ECE2A01F1DFE0B93DEFA888E11942560 (void);
// 0x00000310 UnityEngine.Vector3 Antipixel.MoveTo::get_TargetPosition()
extern void MoveTo_get_TargetPosition_mA98ABA6DF2938E1A36F3EEE8E08F33B666DC7925 (void);
// 0x00000311 System.Void Antipixel.MoveTo::Update()
extern void MoveTo_Update_m2B309169E7792C401C35CFBAF1776A9AD252FBA2 (void);
// 0x00000312 System.Void Antipixel.MoveTo::.ctor()
extern void MoveTo__ctor_m0684D0AEDB2BAE41341F5DB7BC8B0BC41B9DCAED (void);
// 0x00000313 System.Single Antipixel.RotateTo::get_Speed()
extern void RotateTo_get_Speed_m1857A806B518A4A34DE5962DD195B1C03A153F52 (void);
// 0x00000314 UnityEngine.Quaternion Antipixel.RotateTo::get_TargetRotation()
extern void RotateTo_get_TargetRotation_mFBF2EF8345414D007674CA0480793DDB86B65BB4 (void);
// 0x00000315 System.Void Antipixel.RotateTo::Update()
extern void RotateTo_Update_m2CADFB9435A25DBC1669633D653915EB5DA60E85 (void);
// 0x00000316 System.Void Antipixel.RotateTo::.ctor()
extern void RotateTo__ctor_mE35DB069013408C9EC36F9BBE147E03BEA08A9D8 (void);
// 0x00000317 UnityEngine.Vector3 Antipixel.Rotator::get_TargetRotation()
extern void Rotator_get_TargetRotation_m9D1C0510BACFEE4358EAB03243F43CAEA4685368 (void);
// 0x00000318 System.Void Antipixel.Rotator::Update()
extern void Rotator_Update_mF2BB0FE6370B1439D77B80A8F0CF7AC10A4A5287 (void);
// 0x00000319 System.Void Antipixel.Rotator::.ctor()
extern void Rotator__ctor_m3B6AE4A7DDE871917541466518501F2B7B6CD41C (void);
// 0x0000031A System.Single Antipixel.ScaleTo::get_Speed()
extern void ScaleTo_get_Speed_mA689A8A49E807E1F15E73B4E52EDEA0203880B01 (void);
// 0x0000031B System.Single Antipixel.ScaleTo::get_Smoothing()
extern void ScaleTo_get_Smoothing_mABBEB27D5CC1AFEA98B4753342569471AE406B8A (void);
// 0x0000031C UnityEngine.Vector3 Antipixel.ScaleTo::get_TargetScale()
extern void ScaleTo_get_TargetScale_m17099FC6B10897A55CD83A2A834BB551553F5B7F (void);
// 0x0000031D System.Void Antipixel.ScaleTo::Update()
extern void ScaleTo_Update_m03A242B1B818A3E8E5A2CEA092B83156DE23840A (void);
// 0x0000031E System.Void Antipixel.ScaleTo::.ctor()
extern void ScaleTo__ctor_m36019B37912FBE1FA556233AB0122EF3207C3E43 (void);
// 0x0000031F System.Void Antipixel.BoolVariable::.ctor()
extern void BoolVariable__ctor_mD14F45A12E9FFA94936118A266DF045FC4DF10AF (void);
// 0x00000320 System.Void Antipixel.FloatVariable::Add(System.Single)
extern void FloatVariable_Add_m5B456DEB93879200BC28CF1FBA2B38CCFA85AAE0 (void);
// 0x00000321 Antipixel.FloatVariable Antipixel.FloatVariable::op_Addition(Antipixel.FloatVariable,System.Single)
extern void FloatVariable_op_Addition_mBF6655C2DB6CFFA0EEA4F397358B402E0D086FEA (void);
// 0x00000322 Antipixel.FloatVariable Antipixel.FloatVariable::op_Subtraction(Antipixel.FloatVariable,System.Single)
extern void FloatVariable_op_Subtraction_m3B5630FE735D50F374C57AEFEEF660EAF06483B6 (void);
// 0x00000323 Antipixel.FloatVariable Antipixel.FloatVariable::op_Multiply(Antipixel.FloatVariable,System.Single)
extern void FloatVariable_op_Multiply_m4D6D1FCF6C0DE556990DE4708411C766EA8319C7 (void);
// 0x00000324 Antipixel.FloatVariable Antipixel.FloatVariable::op_Division(Antipixel.FloatVariable,System.Single)
extern void FloatVariable_op_Division_m9602BB2482BE59266CA57244C9AA59E7A261D2CF (void);
// 0x00000325 Antipixel.FloatVariable Antipixel.FloatVariable::op_Addition(Antipixel.FloatVariable,Antipixel.FloatVariable)
extern void FloatVariable_op_Addition_mD50137843BDA8E864BD9DB2659E918C1D374BD3B (void);
// 0x00000326 Antipixel.FloatVariable Antipixel.FloatVariable::op_Subtraction(Antipixel.FloatVariable,Antipixel.FloatVariable)
extern void FloatVariable_op_Subtraction_mEB87A5D62BEA760279F2F7FB0568CBABA26A162D (void);
// 0x00000327 Antipixel.FloatVariable Antipixel.FloatVariable::op_Multiply(Antipixel.FloatVariable,Antipixel.FloatVariable)
extern void FloatVariable_op_Multiply_m716497B1C8DE764D605AF2D916B1F3940A3E1F5B (void);
// 0x00000328 Antipixel.FloatVariable Antipixel.FloatVariable::op_Division(Antipixel.FloatVariable,Antipixel.FloatVariable)
extern void FloatVariable_op_Division_m6D4C1637B219DCA426560F650E2B96060610C4D8 (void);
// 0x00000329 System.Void Antipixel.FloatVariable::.ctor()
extern void FloatVariable__ctor_m93D45630DB0F4D7AAD19DB346F4297AE1A44A760 (void);
// 0x0000032A T Antipixel.GlobalVariable`1::get_Value()
// 0x0000032B System.Void Antipixel.GlobalVariable`1::set_Value(T)
// 0x0000032C System.Void Antipixel.GlobalVariable`1::OnDisable()
// 0x0000032D System.Void Antipixel.GlobalVariable`1::OnValidate()
// 0x0000032E System.Void Antipixel.GlobalVariable`1::InvokeOnValueChanged()
// 0x0000032F System.Void Antipixel.GlobalVariable`1::Set(T)
// 0x00000330 System.Void Antipixel.GlobalVariable`1::.ctor()
// 0x00000331 System.Void Antipixel.IntVariable::Add(System.Int32)
extern void IntVariable_Add_mBBC2E2C5D8726FA0F1ABBABDE4FEB33646154A5E (void);
// 0x00000332 Antipixel.IntVariable Antipixel.IntVariable::op_Addition(Antipixel.IntVariable,System.Int32)
extern void IntVariable_op_Addition_mE2BF57251522059E120FF17E4BC35FE7B935C042 (void);
// 0x00000333 Antipixel.IntVariable Antipixel.IntVariable::op_Subtraction(Antipixel.IntVariable,System.Int32)
extern void IntVariable_op_Subtraction_mDFC56E17993AE9B6C5C621632C2631C0699839CE (void);
// 0x00000334 Antipixel.IntVariable Antipixel.IntVariable::op_Multiply(Antipixel.IntVariable,System.Int32)
extern void IntVariable_op_Multiply_mF011812E491834C5417BCA65DB25F0372D444B2E (void);
// 0x00000335 Antipixel.IntVariable Antipixel.IntVariable::op_Division(Antipixel.IntVariable,System.Int32)
extern void IntVariable_op_Division_m99247BAA544F386EB770A63F26F03A9C3B10C052 (void);
// 0x00000336 Antipixel.IntVariable Antipixel.IntVariable::op_Addition(Antipixel.IntVariable,Antipixel.IntVariable)
extern void IntVariable_op_Addition_mE0E344A514006ED1AEC5A42E316D5138A5E7AC4B (void);
// 0x00000337 Antipixel.IntVariable Antipixel.IntVariable::op_Subtraction(Antipixel.IntVariable,Antipixel.IntVariable)
extern void IntVariable_op_Subtraction_m2630096420772D4B14385981DC0E10525277B65A (void);
// 0x00000338 Antipixel.IntVariable Antipixel.IntVariable::op_Multiply(Antipixel.IntVariable,Antipixel.IntVariable)
extern void IntVariable_op_Multiply_m43EB1A78A4A9B570D6827FA993DCD462FB8D6A4F (void);
// 0x00000339 Antipixel.IntVariable Antipixel.IntVariable::op_Division(Antipixel.IntVariable,Antipixel.IntVariable)
extern void IntVariable_op_Division_m6383014092BDA355E4B3DDEBFFC64103E4D793A8 (void);
// 0x0000033A System.Void Antipixel.IntVariable::.ctor()
extern void IntVariable__ctor_m50F51827F27895196C2E311C0EB56298C47F6CAD (void);
// 0x0000033B System.Void Antipixel.StringVariable::.ctor()
extern void StringVariable__ctor_m2988EA2BE9AE0E3D0E30946C1C41EE814A55F67D (void);
// 0x0000033C System.Void Antipixel.Vector2Variable::SetX(System.Single)
extern void Vector2Variable_SetX_m15ADCDF86DDC4706D1E1D6B74C8BE868F9A90EF8 (void);
// 0x0000033D System.Void Antipixel.Vector2Variable::SetY(System.Single)
extern void Vector2Variable_SetY_mD3B43ACD1746989585B78E36DA47B5655DC07A91 (void);
// 0x0000033E System.Void Antipixel.Vector2Variable::AddX(System.Single)
extern void Vector2Variable_AddX_m73A9F9C9139661A0126FCF2C443E2E1E21A565F2 (void);
// 0x0000033F System.Void Antipixel.Vector2Variable::AddY(System.Single)
extern void Vector2Variable_AddY_m55EC955063EC3F9B5B14992601CE235432B7810D (void);
// 0x00000340 System.Void Antipixel.Vector2Variable::.ctor()
extern void Vector2Variable__ctor_mF5AA10818B992D586D2853794B89BA90B10D51D4 (void);
// 0x00000341 System.Void Antipixel.Vector3Variable::SetX(System.Single)
extern void Vector3Variable_SetX_m07B06FB8C18083FFEA91A1828BF49B0954E50F60 (void);
// 0x00000342 System.Void Antipixel.Vector3Variable::SetY(System.Single)
extern void Vector3Variable_SetY_mBC40D07E3788F502248ED568A2AFC91CECF67A78 (void);
// 0x00000343 System.Void Antipixel.Vector3Variable::SetZ(System.Single)
extern void Vector3Variable_SetZ_mFC69837F80992649B90D4119FD593BFBF79CD848 (void);
// 0x00000344 System.Void Antipixel.Vector3Variable::AddX(System.Single)
extern void Vector3Variable_AddX_mE545DD6AC8FEF2316F3788F523CF017FCF764D6E (void);
// 0x00000345 System.Void Antipixel.Vector3Variable::AddY(System.Single)
extern void Vector3Variable_AddY_m42C6A8EE087C6CBFAAE48BFB80946475A5AC32E4 (void);
// 0x00000346 System.Void Antipixel.Vector3Variable::AddZ(System.Single)
extern void Vector3Variable_AddZ_m95137F714AEF4F5F78379EED6F602AA7711BE854 (void);
// 0x00000347 System.Void Antipixel.Vector3Variable::.ctor()
extern void Vector3Variable__ctor_m2979A9357BD6ED39CD3635260F58DF042E64E3EB (void);
// 0x00000348 Antipixel.ObjectPool Antipixel.IPoolObject::get_Pool()
// 0x00000349 System.Void Antipixel.IPoolObject::set_Pool(Antipixel.ObjectPool)
// 0x0000034A System.Void Antipixel.IPoolObject::OnDequeue()
// 0x0000034B System.Void Antipixel.IPoolObject::OnEnqueue()
// 0x0000034C System.Void Antipixel.ObjectPool::Start()
extern void ObjectPool_Start_mC04977F651D01E5EBE3C84188CAD56EC1A48C9CC (void);
// 0x0000034D UnityEngine.GameObject Antipixel.ObjectPool::Dequeue()
extern void ObjectPool_Dequeue_m736202A6620AE05FCE63CF7F6FC15EF883698503 (void);
// 0x0000034E System.Void Antipixel.ObjectPool::Enqueue(UnityEngine.GameObject)
extern void ObjectPool_Enqueue_m3A2786BBF13BB8F25AA204BE1201D509E5155DF3 (void);
// 0x0000034F System.Void Antipixel.ObjectPool::.ctor()
extern void ObjectPool__ctor_m8461AF302657087B4A4F82A95B9447902E90B8AE (void);
// 0x00000350 System.Void Antipixel.GameEvent::Invoke()
extern void GameEvent_Invoke_m9360B4D0BAE97638A3A92CB2805395CBB8688115 (void);
// 0x00000351 System.Void Antipixel.GameEvent::Add(Antipixel.Observer)
extern void GameEvent_Add_m3CA7FC021B7E3BEB833AA0D2326559E1DC064B05 (void);
// 0x00000352 System.Void Antipixel.GameEvent::Remove(Antipixel.Observer)
extern void GameEvent_Remove_m1C44B6790B5F0CFE6FF60568BAFDBBA4C5DCCBE5 (void);
// 0x00000353 System.Void Antipixel.GameEvent::.ctor()
extern void GameEvent__ctor_mB7045D8F0BD412B320784ACD83A9CB45C1F42886 (void);
// 0x00000354 System.Void Antipixel.GameEvent/<>c::.cctor()
extern void U3CU3Ec__cctor_m0BEBFDC09B869F2217AEA0AE9E7775DDB7C82298 (void);
// 0x00000355 System.Void Antipixel.GameEvent/<>c::.ctor()
extern void U3CU3Ec__ctor_m3DEBF43DF5455203C2B58D1AE91257157103A4EC (void);
// 0x00000356 System.Void Antipixel.GameEvent/<>c::<Invoke>b__1_0(Antipixel.Observer)
extern void U3CU3Ec_U3CInvokeU3Eb__1_0_m649CDAA769E218D85F5471A6B16840D2D223173B (void);
// 0x00000357 System.Void Antipixel.Observer::OnEnable()
extern void Observer_OnEnable_m5B151472F76C5DE6AE8B6BB5B45AE70160BF1AFC (void);
// 0x00000358 System.Void Antipixel.Observer::OnDisable()
extern void Observer_OnDisable_mDAAE03EDDFBA78A4FF22976F8391237FBB77DB5F (void);
// 0x00000359 System.Void Antipixel.Observer::Invoke()
extern void Observer_Invoke_m4358633664613294F5001BDEA0BC39B54DCA4F15 (void);
// 0x0000035A System.Void Antipixel.Observer::.ctor()
extern void Observer__ctor_m5ECFC7CAD6CA5F7ECA8B03AD538185027CE1DA5F (void);
// 0x0000035B System.Void Antipixel.DontDestroyOnLoad::Awake()
extern void DontDestroyOnLoad_Awake_m84E57F8144BEC6705DE625AC66710F4495674FAB (void);
// 0x0000035C System.Void Antipixel.DontDestroyOnLoad::.ctor()
extern void DontDestroyOnLoad__ctor_mF051046ADED5EC6EF0FDEDA1273D0D9BF77E99F1 (void);
// 0x0000035D T Antipixel.Singleton`1::get_Instance()
// 0x0000035E System.Void Antipixel.Singleton`1::set_Instance(T)
// 0x0000035F System.Void Antipixel.Singleton`1::Awake()
// 0x00000360 System.Void Antipixel.Singleton`1::.ctor()
static Il2CppMethodPointer s_methodPointers[864] = 
{
	ParticleController_get_Pool_m0B06778AC43BA0AEEBCCA2900A6774581F31E4FA,
	ParticleController_set_Pool_m358926DCB555DCBF8394FB3B12239690FFB76A1F,
	ParticleController_Awake_m29376B5F72D6077B80BE7D5F8C8892E886FC4DCD,
	ParticleController_Update_m3A2B6284877A76F56251767E3A9A3B3D4461D21A,
	ParticleController_OnDequeue_mEC3009D5E29579D8EF5475ECD345D93EF267F770,
	ParticleController_OnEnqueue_m81224518EDECF925FD5A06DF09B44451D42ADA3C,
	ParticleController__ctor_m0633F31711BB00EF50713AB5B22CE542317B65CA,
	PostProcessController_Awake_m18224D4ECE4ADE07D51912F70C510CDA9385FBE1,
	PostProcessController_Update_m750C394ACB8A886BC03DB7BA14F3F1018C3178E5,
	PostProcessController_SetBloom_mECA5606372B87F08CBE37CF05B5A020B1465F52A,
	PostProcessController_SetHueSift_m069DF7F6712E437ACC67C101C5CA3E25A0577BD1,
	PostProcessController__ctor_mD6096DAC35986FB83D918E201EFACEA87B3362B8,
	GameManager_get_Data_m1B7A20844D301ED83EA5355B5B9F96F368A317D7,
	GameManager_set_Data_m10FF27828C8F24B6064BEFEB9A77DFD4BA72DE66,
	GameManager_get_InPause_m9B5C419C01769D13A140FE124FEF1A0CBBA89E16,
	GameManager_set_InPause_mF8A3587DFF67477D609E2F31EDC831AF9EB91828,
	GameManager_get_InGame_m8CDCCC555CBB172BBC6EBF3DD196A5730275C983,
	GameManager_Update_mD4DAB5B2B55C8FC09A4A83300080BB6CDD8F076E,
	GameManager_StartLevel_m4380820698DCABE87C9DBE61E2D12332BEB52F7B,
	GameManager_FinishLevel_m7B1C70C0CD9A2761ACC7477E73BDE041B1EF788B,
	GameManager_PauseLevel_m93C71900FB2B24F9FC850973EBC24EDCB893F373,
	GameManager_Quit_m1A7331B8821593C7880728ECEAA0E5493E2F44BA,
	GameManager__ctor_mA8F33402ED72F4180497DFBBB64C9E0742DEBE7F,
	Bullet_Awake_m4FCE183D29F640F111675F01B12EAC89C4B22E19,
	Bullet_Update_mA5757BC0CB0507844B3310C69ADF3D9D391FBBC7,
	Bullet_OnCollisionEnter_m9BF981EF5E0A67A59DCE73C558CDF75360320E7F,
	Bullet__ctor_m132BDF6FEF611A1E6B334D228E87E49D1D53D4FB,
	Cube_get_RandomDirection_m27BC8E6E72F066450EAEC6399293F4568E92BFFC,
	Cube_Awake_mCE941CDF22321DC6C423F528FBB8E0B4951B0F29,
	Cube_Start_m2A25EAF78003BA33903B40E51000823DA27F8DF3,
	Cube_Update_m3F07C0887F14FD2E43EC78936FBA9CAEBBCB8941,
	Cube_OnCollisionEnter_mEA6A3A75CDDDF0D4EB6C8FAAB7142DBAA7FA68D9,
	Cube_OnMouseOver_m7C9C220668780939FD6EB78341DCF7113FF1C46E,
	Cube_OnSpawn_m1992987C3225752BEA165419742F92F62EF154EA,
	Cube_OnDespawn_mEE24FB22421A57CB99E6881125690B75490594D7,
	Cube_OnBulletEnter_mBDA7A8D8AB2DB7175B5E31EE3B529F202CCF4D87,
	Cube_PlaySound_mB0746572FA0856B26403004F688370B93AE2F2D2,
	Cube_AddPoints_m4BAC60CDECF8DB1BBD39B2F6C56D652000BDE540,
	Cube_SpawnParticles_m427C1167901688F2E814939ED0B9BBEC4153D854,
	Cube__ctor_m2A7803FF04152687F05DA562F82EFE6592272E74,
	Gun_get_Count_mC8CD9E13E2877EE32624CD22196363AF1EB89CEE,
	Gun_set_Count_m2ACD1D4356A345CED530C14386215068BD205BB5,
	Gun_get_Shooting_mBADD5CED17DC9B9D31EE0353C4704F57F58C9EA0,
	Gun_set_Shooting_m0BD4A2DC332628D534B55DF5C302EC80B227765D,
	Gun_Awake_m06158D47E0711A65233ECABF37C44C36CDC1F2C8,
	Gun_Update_mE05214C9787B3B3784EAD7A34614E1AB0CCAB3CB,
	Gun_Shoot_m0AE27B949F9360CFD9BE7F213B15F0912CB7BFAB,
	Gun_Recharge_m72768A8E18D441549C643CD77733B96E065FF4A4,
	Gun_Impact_mDAC21E6EF5B11FE2250C90D787C2A22B8E6A0BA9,
	Gun__ctor_mE80C579632188F705E8DC6BD150A49367DA45030,
	InputController_Awake_m8F546CB1F5CDDA6A4BF69D0E7FF6AB6D7524DE35,
	InputController_UIPress_m4060901763F0481C72D4A8B58363192A2356800F,
	InputController_SelectAction_mF962A9399E9E984B78475C420E2CDEB64F650901,
	InputController__ctor_m0156272BB19875EAD5F27C03D7FFBD7898A756FC,
	LevelButton_OnEnable_m57E796FE0FE3DEA874FA9893B26F4FAF28406D12,
	LevelButton_OnPointerDown_mD5F389789EA2217C1C85225FCD8F4628F5F013D0,
	LevelButton__ctor_m91E9F5D19F55E791E8515D3E61A0BACFE1557384,
	VRButton_Awake_m2900AD33AD24614A691BA0CC5E9FFCF23DE6E30C,
	VRButton_OnPointerEnter_m7F5F4F2AA3352B2BE85093B78F99E26333E95E33,
	VRButton_OnPointerExit_m14E0DA2C2E440A2F12690D8C47855009B8D05078,
	VRButton_OnPointerDown_mB89C9A1636DA34FE1AEC4736A5980AC0A80D15B4,
	VRButton_OnPointerUp_m784D90E425B8ED9F4E781C7C8E5CAF2CEA95AB45,
	VRButton_SetColor_mC3D442A42FA253085BE2448CF59A6914EFC99D7B,
	VRButton_SetAlpha_m11A349121F86B8707A4E990799F5D13404442A20,
	VRButton__ctor_m0A3B56E207EA6B56EC58EF84556202F196187BA3,
	LevelData_get_Highscore_m2572FFE3A1F87CF7E6385159C9E0D5C161B9E73D,
	LevelData_set_Highscore_m4109F0FD95BBEEC422595FCB902CCD18B4AF6516,
	LevelData_OnEnable_mE6558C9F3B02E7D4AD874108E257386598F7F456,
	LevelData_Load_m72D2FEC010DAD223B81294E1C757AE0100FA6BE1,
	LevelData_Save_m4AA36FA2C5EF158B1B6A5FBEE71DFF304F36BE12,
	LevelData_UpdateHighscore_m550E4B7B81B669676FF6F6203003C8AA2C9DD2F5,
	LevelData__ctor_mBB39471DB33A2368F5B8DFC1BE9C708E15AD5F18,
	MusicController_Awake_m5842FDD34281242311239EE95004646AACD3B3BB,
	MusicController_Update_m62E28EE20351C09B1168417360DA6661EEB4C9FC,
	MusicController_UpdateBeat_mE6A62B955883B44800968118A538BA62F24C4E35,
	MusicController__ctor_m29CA728523FBCB22CF88C2CC204B3B0D95A80840,
	U3CU3Ec__DisplayClass9_0__ctor_m70EF91528F4625CF3804D71CCE324EA918FE85A2,
	U3CU3Ec__DisplayClass9_0_U3CUpdateBeatU3Eb__0_m3AC7B52282F74AFEB5C6C83DADBD6187EDA572AC,
	ScoreController_OnEnable_m20F4873CF9BD0CF8A3C7C61A4183DEB41B3C7096,
	ScoreController_OnDisable_mDA3F18BD97F1C612A66ED21ED01BE837F8FBF418,
	ScoreController_OnScoreChanged_m73FE8D3BEB281BA7CCA470C38EE6076B1CD8CF14,
	ScoreController__ctor_m97FC5B6AB63C562F0087BB740F93797A7ED0F292,
	TimeController_get_Time_mADF42903A4A10FC3BCF2C091D9B9B7206210E99F,
	TimeController_set_Time_mEFD6F7166CF29EE301EF91611B3CFD4F0621AFFD,
	TimeController_get_Seconds_m8FA56562BE4FB01246CBD1E457D6752D3A359291,
	TimeController_get_Minutes_mB29BC57DA7F16ED2855FA94E34D315EA62BE29F0,
	TimeController_get_Hours_m4C075B395256362FCD069671E8457AAB3F82CEA6,
	TimeController_Update_mC0C3116DD026DA9EABDD834B79B6BDCBF36C84DB,
	TimeController_SetTimeScale_mA652C46D5D716A1A37925C9FC37DD29E3535ED24,
	TimeController_ToMilliseconds_mB67ED4001123AC4FAF261F599543BF91D7170E81,
	TimeController_ToSeconds_m70F0E689BA9E29EBEBDFE235E6BC0818911580B6,
	TimeController_ToMinutes_m400581149D4680D87BFBBBB2A7FA6B98F0A98593,
	TimeController_ToHours_m93609E7A33D1A8206259522127355AF070AFA723,
	TimeController__ctor_mDD0BE3B85D1E5D7F250639DCDD441B1AFBB4974A,
	NULL,
	NULL,
	Spawner_SpawnAll_m83588F5243F87B9301F4C0EF45CDD38ACF128EDD,
	Spawner_DestroyAll_mFA3864B932AB7448F73B82F69103A26CF07351F5,
	Spawner_Spawn_m5221CBF6B539DFAAEA5147E69E4B73A0E0BB8B33,
	Spawner_SpawnCoroutine_m549896FE8F0AD5E66BAB22A71859C5AA2247FA06,
	Spawner_DestroyCoroutine_m6300439302DA4A94603DB451CD02278474719315,
	Spawner_DestroyAllInmediate_m7DFB6877DD888DFA5D9A202612775044FDD6BEA8,
	Spawner__ctor_m0A241A8ACE7B1C9B32C725C5294BF528C688C2DD,
	U3CSpawnCoroutineU3Ed__8__ctor_mFD1313F3B32AB15AB720B852BC14652BED23B696,
	U3CSpawnCoroutineU3Ed__8_System_IDisposable_Dispose_m0BB4E8AAEB33F52178252D89D5FBE31D4BB5FEBD,
	U3CSpawnCoroutineU3Ed__8_MoveNext_m6CB42CFB24272863AFEFB7C845340CFE7D5D832D,
	U3CSpawnCoroutineU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mADB539B627C96166A7BA5D42F374E9D2D5524F01,
	U3CSpawnCoroutineU3Ed__8_System_Collections_IEnumerator_Reset_m8BC70F40D5205DA9353062F93CBC3ABD94A7847E,
	U3CSpawnCoroutineU3Ed__8_System_Collections_IEnumerator_get_Current_m4FC7E752EC618871E9FDD1D27472408107BB2BA3,
	U3CDestroyCoroutineU3Ed__9__ctor_m57FF4656A8EB51BC48F56B2FC64168D12A859A14,
	U3CDestroyCoroutineU3Ed__9_System_IDisposable_Dispose_m6B5A6979C4B1ACCDFBC8D2E62078CD5475E04D17,
	U3CDestroyCoroutineU3Ed__9_MoveNext_m8D643328D92BA2765AFA47EAB852C5777698D027,
	U3CDestroyCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD98FDE193B09AB90ECDCEB68BCAC51245E244437,
	U3CDestroyCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m581161E85D7AF70C0E9FAACF3662C90C27D64497,
	U3CDestroyCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mEBFEDA69992E0957E47A514D7843E67440004F28,
	BaseObject_get_Position_m7AE6BB5BE85CCD47BF7EAC2C584B1F0746929EC5,
	BaseObject_set_Position_m7E1D383DB5AEF04D1ACD7A7B03EACA26E6428E36,
	BaseObject_get_LocalPosition_mFFFEF286A58FE523AF682DD90F11AD9ADAF905FF,
	BaseObject_set_LocalPosition_mA59860CEF00056DADD8F92FFD43DB3F3D7D47E44,
	BaseObject_get_Rotation_mFFE5AA30B1292F3DD1D0867DA83919DF46D7FFFE,
	BaseObject_set_Rotation_mD43B9E997F0E9F1842C4E9DD53832CA390781E5C,
	BaseObject_get_LocalRotation_m8F279FF823BEA265BDE3A543D1811BA9E02E6018,
	BaseObject_set_LocalRotation_m3B3234A33D7495F7358D8372E1EEF88644625EA1,
	BaseObject_get_EulerAngles_m4C75100AB63C4D883BE0CFD9092103B64AD8CE39,
	BaseObject_set_EulerAngles_mF5EA5BA17396010995F4076E47C14AF884790198,
	BaseObject_get_LocalEulerAngles_m3FFF7EF3D870713E154B1A3A82BEBB9B535560BD,
	BaseObject_set_LocalEulerAngles_m8B16C3B61365DEC43F26ADDBF840087C1C9BF7F0,
	BaseObject_get_LocalScale_mB6C8FFDB501BBD4BD07249224175FADAB27A6E2B,
	BaseObject_set_LocalScale_m8ACDEB672E59314F464FF722A1DBA12EA99665C7,
	BaseObject_get_Up_mA66C52F29744F085D1CF290F417199CE276E2409,
	BaseObject_get_Down_m5689477F664DB189CAE7E0CECD556630ADAE1366,
	BaseObject_get_Right_m7E1F1CF78C8670C35D87D85A00B481A822E84955,
	BaseObject_get_Left_m1EF5D3BD56D0D9D3C4B749E63A931DD87F511B37,
	BaseObject_get_Forward_m7FE88E21064598941612F011110C794EA86C0E06,
	BaseObject_get_Backward_m2154BA82F184B6E33A95914B8229941627137BC7,
	BaseObject_get_LocalToWorldMatrix_m7A4A6B908F53320AF255770075B0FE046C9974C9,
	BaseObject_get_WorldToLocalMatrix_m1B96B14434F54B7CB0D745CB548DCD116D7FB1F9,
	BaseObject_get_Parent_mA292DBB33B6A0AA9814BDE3A3CFD0B4709F97D21,
	BaseObject_set_Parent_mC6EABFF6CC009C6DAF9A539FEE72E2835028C0C0,
	BaseObject_get_DeltaTime_m7EEF23BEA3DE2EFCFA6921482E348D68061B2512,
	BaseObject_get_FixedDeltaTime_m0AECE944F2F2092516EBBD737AA81B6E4D510619,
	BaseObject_get_TimeScale_m6C94F720CDBA8D697FCEA43DB76033EC4C27D393,
	BaseObject_set_TimeScale_m31FE6FC2D09D950DBA500ACE423047A60697C879,
	BaseObject_OnEnable_mAAEC97877F51526B7B4254A12E56AFA127398F11,
	BaseObject_OnDisable_mCF640D0075473EE81BC0CB8DB871F3F4BB5BC309,
	BaseObject_OnDestroy_mCA33A7FBEA0BFB8E57356896278762025AFE0BFA,
	BaseObject_StartCoroutines_mBCE8DB63DA71A918AEC8B7699C5DF55CC03BBFB5,
	BaseObject_StopCoroutines_m8CC10400B83317C0C06FEB3C7C9A07835E9E8C6B,
	BaseObject_Destroy_mCD035DDCB058CA7CABCB664E068B453165F94CA1,
	BaseObject_DestroyComponent_mD4BEB2ADDD4DF961DB7998CDE330E91E2F0EDB3E,
	BaseObject_Print_mC5715684D6CCC739DA6FFA99439436052FC555D7,
	BaseObject_StartEditorCoroutine_mBE60871B4C4B357673389EE1E009FB64173D45AF,
	BaseObject_StopAllEditorCoroutines_m5C95986F2F04FD440A11FB4A83D0274E097AA193,
	BaseObject_RunEditor_m60311491B0A8355FFEBADA2B99932C3B3B7BC443,
	BaseObject__ctor_m47A4666F079EBAC9ECD7953A322CFAD68FF22E85,
	CollisionShape_get_Collider3Ds_m3B1665EFB9A0842AA9165F01D75180104EB8C5BE,
	CollisionShape_set_Collider3Ds_m1AFDAE818DEE62300CC1B3CDC7DFB3BED593ABF4,
	CollisionShape_get_Collider2Ds_mAE9734F5A82AD0A329CE26024BF9933AD710BA6A,
	CollisionShape_set_Collider2Ds_mF8CEBB4D0E430D3CF05648F0AB850A693B5DAD08,
	NULL,
	NULL,
	NULL,
	CollisionShape__ctor_mE82546223CE3E1F315A4ED40288EF1F368EC7C73,
	Drawing__ctor_m09C827CAA867C9FB7D8B8655E63C738CDA97029C,
	Colors_get_Black_mAA43214B64EB771E63E91EE7E9599E2E0A11FC80,
	Colors_set_Black_m9E303B4234A56F5CD0197FF9897B983560774886,
	Colors_get_DarkBlue_m8DA4C1F9CBC8876B06B560F692F0831F6345DAEE,
	Colors_set_DarkBlue_mBAC57C2DC67AC8B0347BDB973978785F1678EC14,
	Colors_get_DarkGreen_mE6CDB2C12ED02884C481D3BAF1F72BAC600488BB,
	Colors_set_DarkGreen_mD5AA93D92E07A08BE6E682548086EB0820B86FB6,
	Colors_get_DarkAqua_m4459622E91F8B377D8D5B05D8EFEB3AE9BDFA07C,
	Colors_set_DarkAqua_mE14F05D00B594DAA31F6C81B46898A7DD8638299,
	Colors_get_DarkRed_m16C2014D3A2802DC6DD092DED66B4CBAAFAFD168,
	Colors_set_DarkRed_mB1216D5C094AEB9BD4F8B12CFFCFE6742B73DA1C,
	Colors_get_DarkPurple_m8C8C52E33789C5ADF3BEA2194F801B9582BF569B,
	Colors_set_DarkPurple_mF9F855C820B992667C9F465994F2E53E3B27EE85,
	Colors_get_Gold_m746E9979E1BA38E14C359340DB2C31DECD25830C,
	Colors_set_Gold_m55DF49C197C4B7CE5169F28E327E8EBCCCD94DB8,
	Colors_get_Gray_mF8233FB0AA5C8525006ECFC4244624DA321EB2DE,
	Colors_set_Gray_m3D5F0D102AC9B1EE88DE051F78CE60D11F8D31A2,
	Colors_get_DarkGray_m13B0103AEC08C4A8877BC17C1221EA2D347516BB,
	Colors_set_DarkGray_mD084F5B587B5EDA7A9787747E78705EC4C2DA8AD,
	Colors_get_Blue_m616240B6A1145A2B771AC140E5C18C530A83DB8F,
	Colors_set_Blue_m9F4079D5C6866780DCAF058D8853DC5EA7C59829,
	Colors_get_Green_m17BAC4AE541DEB4D615F8DEA46DA076E06875CE6,
	Colors_set_Green_mA92664CE44C2759E464DEA4006F1FC05ED0F7701,
	Colors_get_Aqua_m892EB4654255457958BB0B3048B20944844D93A9,
	Colors_set_Aqua_m0BEB1267F6AAA73B2131D10894F9204DF1F6AC86,
	Colors_get_Red_mFFD3D6960EADDC9B5C6F040C34EDBCEE4F794B46,
	Colors_set_Red_m25C2338D066C18BA0D592069DD25349B753D3A1B,
	Colors_get_LightPurple_mE52D43E2B72D28197B362D188A81BE9D2DADE22A,
	Colors_set_LightPurple_m225FBECE0B2B2EE07B94DF923B764495B33675E7,
	Colors_get_Yellow_m0ECD8AE27E256F35C20B31606FE4A8014A50435E,
	Colors_set_Yellow_mB22D624E1720AF8E55B12A07535B36FF12645B40,
	Colors_get_White_mF9CC4AE9901408F590F58BF6D909A81DF4C1C1AA,
	Colors_set_White_mA6F60FAC4252EB2DB23A4C64D4EF9303EFD286EF,
	Colors__cctor_m765004F428930D4FFDCC04FF16F6947414EFC7FE,
	CustomEvent_Invoke_m014EA27E7156075411591D61E2A4BBDD5EC387F2,
	CustomEvent_Add_mD5C2D900F70B02F0A57A119F02171137AD2955B3,
	CustomEvent_Remove_mD880E0C9BA5956F20D593E38D26D7F814AEC348E,
	CustomEvent_RemoveAll_m1CA74C54DC3E71461A8D3D9547C83CD7BB7E8A6B,
	CustomEvent__ctor_mB49879EC16C85491C6862F3E939259B16D306C8B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	VoidDelegate__ctor_m7950233DE26ED051442B194D37AB825F3DCEABC3,
	VoidDelegate_Invoke_m6096ED17DE0AC6CD36268570A59BD0EC5FC83F21,
	VoidDelegate_BeginInvoke_m12499E8AE27BE16D896A952166CF1600D057A707,
	VoidDelegate_EndInvoke_m30CFE9257A2E32980561C4E0158D864E1301AA13,
	BoolDelegate__ctor_mEC48165B85F15F4A91A18BA56B0D601EC177B92C,
	BoolDelegate_Invoke_m42FDBE077B7C59544E0295CE0D9A8178A7DBAB18,
	BoolDelegate_BeginInvoke_m532E96CD6930ABE5AF7021E0C554527471DAEEB4,
	BoolDelegate_EndInvoke_mCAEE4CAB9769815B8B1CDC70731EC656065ACB83,
	BoolArrayDelegate__ctor_mBE7388BB27A21BFE987542AB42E2BF85A008B6B0,
	BoolArrayDelegate_Invoke_mA11AD0B332BC5E3B5C24C71B8CAFE20D1E2275A1,
	BoolArrayDelegate_BeginInvoke_m9C669BD67A84C61F56DD323292A03E016BCCB02B,
	BoolArrayDelegate_EndInvoke_mA9F2B229FC55C7598F47250453EAAF78A7AC139D,
	IntDelegate__ctor_mB66123A448E9716B3472483E8EDBDE50295A3D66,
	IntDelegate_Invoke_m3A5BCA3E9D512370EF335A2A799D998F8A37A914,
	IntDelegate_BeginInvoke_m17DDFA43456E5E178CC51788A8F179D72F2F743B,
	IntDelegate_EndInvoke_mED24C739361754D3CD350BBD13ACC8A0CA99897F,
	IntArrayDelegate__ctor_m7B9F16F3B20BE5F03853B30FEB0F2B7149F44769,
	IntArrayDelegate_Invoke_m7E55C92EA3FD93FEBF6FD7345283E3B59700156C,
	IntArrayDelegate_BeginInvoke_mBFAA3380C107477ADB3B46990B8F3EC2E6D52654,
	IntArrayDelegate_EndInvoke_mA37FE2661919D8BC59302F215C1BC0A5B2F1FF20,
	FloatDelegate__ctor_mB46C30EBCF6D5E0F19DE50275398CC00952FC67D,
	FloatDelegate_Invoke_m8EFC1624F9C80D660DB698FE428E33A7E5A7E99C,
	FloatDelegate_BeginInvoke_m1A951666FADDD7978E25AFB4FAF53E3CAE278A68,
	FloatDelegate_EndInvoke_m8286B1661AF0172D10D2543BD4144828C43203C5,
	FloatArrayDelegate__ctor_m7D53F48DCE46003A51E86613848B984E3E5AD50F,
	FloatArrayDelegate_Invoke_m76C354CC0AAEE7867B76947ED05CF5A13520165C,
	FloatArrayDelegate_BeginInvoke_m369658D336E2B19B4D4EEC8A7CA870C0C8B93A79,
	FloatArrayDelegate_EndInvoke_m6F76435C32C1A52F68D71F859FF77FAF37C72C14,
	StringDelegate__ctor_mA585DDB44B4299CE6AD537E8FB411C2C83150814,
	StringDelegate_Invoke_m9DAE66215C755173DC8AB9866035A02B186F08A3,
	StringDelegate_BeginInvoke_m8DA504D8DD817639D1B9D78EABD5E0F4529B889F,
	StringDelegate_EndInvoke_m10598AC661815E120F22B609C3FBB38A05DEE46D,
	StringArrayDelegate__ctor_m19F078B326B837F13B8755ABE24F761193EE79BC,
	StringArrayDelegate_Invoke_mA87EDDB2C81B80136B4C2AE9BE9FC2E6CF5D2EDE,
	StringArrayDelegate_BeginInvoke_mA7B86158C7A4D8F73B13BE537D5C12EBC7520403,
	StringArrayDelegate_EndInvoke_mB5E33A5A40ECA89FFA5C32719077F8966AF501CE,
	Vector2Delegate__ctor_m5929CE6BA421E01AFEA8E9CC7247336D15672ED0,
	Vector2Delegate_Invoke_m40084D19AB1810DD18A2DE85971D81C44F090186,
	Vector2Delegate_BeginInvoke_m9FC2D224DF30AC033ACE6A8FA91C1C2582D8DA53,
	Vector2Delegate_EndInvoke_mC33210574438DB1D37B3A9345FF3FBCF904A417E,
	Vector2ArrayDelegate__ctor_mBBEF38BCBBCCCFF48969BD2BA206DA2151DA46F9,
	Vector2ArrayDelegate_Invoke_m0408E9D9EA55359646D32F3C2F93E6E8800D8E59,
	Vector2ArrayDelegate_BeginInvoke_mE2CDB9719F54880CAEC34D021C3BE4EE67FDA791,
	Vector2ArrayDelegate_EndInvoke_m59B4666ED75A8A83D98097F74D88BAB0E741EC4D,
	Vector2IntDelegate__ctor_m4648FE29D7715F80CC3EC50C59A4B1DCDDCE28F2,
	Vector2IntDelegate_Invoke_m7A24D3F523DA17A96A69F5159D6213E463BE2261,
	Vector2IntDelegate_BeginInvoke_mBF7E66FC2EB86046F34D3FD6CC58990130DC8ABC,
	Vector2IntDelegate_EndInvoke_m45E015471892AAD6BC5383E7CCD3C494A1F2743C,
	Vector2IntArrayDelegate__ctor_m8102BAB394D6BD23A34B4EFE6E33173715DA0CDE,
	Vector2IntArrayDelegate_Invoke_m9A76FF54455B5D9143B4D847BFA8C692FBBFBE8C,
	Vector2IntArrayDelegate_BeginInvoke_m83BAC5A6B81E7CA5F684A7F2C106F60CEAF9887E,
	Vector2IntArrayDelegate_EndInvoke_m80F4F7624D3DC592F9D29F4F5261230CDE2EA37E,
	Vector3Delegate__ctor_mABF3B11E697E9CB84440D16D34457AF79EC6AB18,
	Vector3Delegate_Invoke_m2E1FE499B528540B997ED3ED28F45D10FC4B3ACF,
	Vector3Delegate_BeginInvoke_m64E7C53CA5D3F750F0C7F8814A5F1A53F7E28101,
	Vector3Delegate_EndInvoke_mBC52FE53D1D3901016ACF2F250F6A6B46043C1A9,
	Vector3ArrayDelegate__ctor_mCCED617103C6C75CF3E35BBF269254714018DA7E,
	Vector3ArrayDelegate_Invoke_m6F3ABDC2368326546F178AE8D79C0A259A092610,
	Vector3ArrayDelegate_BeginInvoke_m0CDE8CE73B87DDCBB08F9D606761FC4F09B24CB2,
	Vector3ArrayDelegate_EndInvoke_m468A2DBA283096D682C1342DA56BB728CAC13EDD,
	Vector3IntDelegate__ctor_m87374C0012EEB6DCC65D7908C909016B663096BB,
	Vector3IntDelegate_Invoke_m071BB8F2EE2C8E9A522870892BB622554EEC7FD4,
	Vector3IntDelegate_BeginInvoke_m43FF9548E554CE2EFF997F070D4983C419574E46,
	Vector3IntDelegate_EndInvoke_m14506CD00EFDFEE2CA0CBCFEFFC6AB73EA5584F8,
	Vector3IntArrayDelegate__ctor_mCDE0F7AC584D8699F9E08E6173189B9085EEDD03,
	Vector3IntArrayDelegate_Invoke_mE9B9FFCDBA03B3AF41772E30BFD6F22AF51DA803,
	Vector3IntArrayDelegate_BeginInvoke_mC0B471935451CE04EE16174A20B063DE5FE24B1B,
	Vector3IntArrayDelegate_EndInvoke_m1195C8A98AA0B6616F8475CA156F8C186F51CD31,
	TransformDelegate__ctor_m0F84912797F2FCFEB440CF71A65580A478B5EF4F,
	TransformDelegate_Invoke_mB72C8967B05B61A8A00AF506C76820DCF7A4F821,
	TransformDelegate_BeginInvoke_m54D6BF951C1888F5F65AA38492C43CA94E267332,
	TransformDelegate_EndInvoke_mCD2FCCD308D9EFE4B79A6983446C22ED58533E96,
	TransformArrayDelegate__ctor_mCAD18F5418E37E57BFFF0CF8AFA0D5D7A3226587,
	TransformArrayDelegate_Invoke_m88EFE1E75448407EF7E62E636383BA10CFACA56A,
	TransformArrayDelegate_BeginInvoke_m26F4181AADACD9290933A1DD285978007489734E,
	TransformArrayDelegate_EndInvoke_m4BCA72D8BBEA73D8CB812CF8E0CFFF84B9F0F894,
	GameObjectDelegate__ctor_m092E78062C3B8435F1697BDFCCB11493753D1EB0,
	GameObjectDelegate_Invoke_mA4400413FFBE6065A4746D0F1030E79583DCB024,
	GameObjectDelegate_BeginInvoke_m07E72E1F7C0595789B68C9944A7CE75F56C9D96E,
	GameObjectDelegate_EndInvoke_m1492660531B2A720A29566D1F9B86E2CCB9B6C28,
	GameObjectArrayDelegate__ctor_mFBB3D98DECA01FFACBF90201BEC91BCA03CB4CE7,
	GameObjectArrayDelegate_Invoke_mC0B0785A0EDF48F5828B8AB450E1B681F4731537,
	GameObjectArrayDelegate_BeginInvoke_mE8EE1196D3601A095C1FB843BED62B43EAEEBE01,
	GameObjectArrayDelegate_EndInvoke_m36F74F779EE19B4448C644BE237EDDA9BF174F70,
	ObjectDelegate__ctor_mB7CD12C819B3106338279C57810CE13001FA6436,
	ObjectDelegate_Invoke_m51A1D39A91F655ED91264002A3421158449E1032,
	ObjectDelegate_BeginInvoke_m62BE701F20E6EE510C962CCCC1ECD50A4D6D42FE,
	ObjectDelegate_EndInvoke_mBAA3916B3D5846DBD8C6395EF89FEE83A9713E80,
	ObjectArrayDelegate__ctor_m4D56E525595BC926599D9E81B3C95998CEB8E72F,
	ObjectArrayDelegate_Invoke_mFA894DBE1F767B5D0936039F950C3C2090359A13,
	ObjectArrayDelegate_BeginInvoke_m18BF3734C385C2983B42607835BAEEEBFAC5938F,
	ObjectArrayDelegate_EndInvoke_m906802594040CFD5F89D47A787F627BD5ABDA008,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AnimatorExtensions_HasParameter_m65C2A853A157D11E89D30CA699DF352245CA502D,
	U3CU3Ec__DisplayClass0_0__ctor_m27874F4F6A337094F29A7C671C81E7EA67C4B39F,
	U3CU3Ec__DisplayClass0_0_U3CHasParameterU3Eb__0_m5F8484DAE810EBA9E24771165E0D50B90D374F4B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ColorExtensions_Set_m6AFA22FCF6B5EC500255C9FEEE6935D9E4021BD7,
	ColorExtensions_SetR_mFFA620CE382CA0CA7BB52C3ABD3CD737E5EFB6E2,
	ColorExtensions_SetR_m09BA6159DEDCCBE2DBEAB9C49F22CA6007323C63,
	ColorExtensions_SetG_mBB4DF67245D4EB6913B2D3E5D852969D83A16498,
	ColorExtensions_SetG_m8DF4C4FC264EFDF9FCF6F714AE3B5ECD98E9BF0D,
	ColorExtensions_SetB_m84DBE0E438C84E3D584E97C58B969A8979DA5DD9,
	ColorExtensions_SetB_m9A58A226B85B62796E7A68CF55DFCC9C2BBA2993,
	ColorExtensions_SetA_mA169AE6EED6A543E3ABD2A113EF5308D4BC1287F,
	ColorExtensions_SetA_m963C90656799BD83F9FE4C1FAAB5A3FA9DAE1608,
	ColorExtensions_ToHex_m59386FA193D9DD3507810E2C7AFE2F712D5C2290,
	FloatExtensions_Abs_mC78DD95AA8FAA3082D8C41884BDD06D5FA9ABFBF,
	FloatExtensions_IsBetween_mC6515CC784721FCA19FE9D96C18637B07F3B0AD3,
	FloatExtensions_IsOut_m2EB229AB49E1964E39BFC1D5A29275493E142A75,
	FloatExtensions_Lerp_m4FFD0C6736D360C71851DEC02866847F925BE1D0,
	U3CLerpU3Ed__3__ctor_m17137A1ECDE2DC1B5E0B05AC6B12D70873286ED9,
	U3CLerpU3Ed__3_System_IDisposable_Dispose_m216C87FD0A759BDA982FC212D1E1551A499A8C4C,
	U3CLerpU3Ed__3_MoveNext_mF0B2E7693E9CE0A65132E31167588E767612B27D,
	U3CLerpU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16B7E50FF3A9EF14AFAC9E1E0DC2CF5F1B9DB67A,
	U3CLerpU3Ed__3_System_Collections_IEnumerator_Reset_mC4E3EC46B4A0646C6737E4924FCDADD09FEE512E,
	U3CLerpU3Ed__3_System_Collections_IEnumerator_get_Current_m4BB5AD0EB5DE1B3306ECFF7DC883012CCF9DE601,
	ImageExtensions_SetR_mFAEBA61A20E6428F999A17E03AF36B2503A2C431,
	ImageExtensions_SetR_m629E89B98E610E8CB83CF42E6367B7D3A14A6A69,
	ImageExtensions_SetG_m382DA12A44594B14636CEADB0818D97BE178936B,
	ImageExtensions_SetG_mD5DE779F22269932C230DA10B6E58E160887C71E,
	ImageExtensions_SetB_m988B7B9526387E85ED2B17DA538E59E90091ADEA,
	ImageExtensions_SetB_m49026135CCEAD64A953F960D16CC47AFEA57D973,
	ImageExtensions_SetA_m70F77E629B8B45D00ABF861A762F2BA764880F67,
	ImageExtensions_SetA_mAA23378BAB12FA75FF36348E3C2742CB8FBE263A,
	ImageExtensions_SetColor_m5E5CCF0BA62A2AEB34DCA633FD9B4E63A4FB1E61,
	ImageExtensions_SetColor_mBEE20DCF91C9119E14EADCA21171A4690B603412,
	IntExtensions_Abs_m09093FE68577FEDF3E9026C4D1A27B3CCEAA3193,
	IntExtensions_IsBetween_mC4EDD39F5B7C8CC2ED88735414F86E9FA009A509,
	IntExtensions_IsOut_m5C3BBD203A78F7FFA3B5B0C3C569CFE888A67C21,
	NULL,
	NULL,
	NULL,
	NULL,
	MonoBehaviourExtensions_StartCoroutines_mDCD7FE5F426A916EBB16514473D000A7FDF5DC91,
	MonoBehaviourExtensions_StopCoroutines_mB4D9D1AF8DBE31F6B589A8CFB44D9993B14054E7,
	U3CU3Ec__DisplayClass0_0__ctor_mB311E0656BA272229CEFB3E895FEFD7FF745C6FF,
	U3CU3Ec__DisplayClass0_0_U3CStartCoroutinesU3Eb__0_mECF4977979F1F705DBBE402A2FFE4AB5FC6CB4CE,
	U3CU3Ec__DisplayClass1_0__ctor_m66F5CC081468949403B33CB6EFEF55C5911BA1A0,
	U3CU3Ec__DisplayClass1_0_U3CStopCoroutinesU3Eb__0_m5F184D5EC9100BF9B0A9EA473F7BEFC3CED5AF19,
	ObjectExtensions_Log_mE2360D17840FEC4124F24EA2B46D2FFB0838D5A9,
	ObjectExtensions_Log_m61CF3C51D0F65245B5E8D769788F35EE3339022B,
	ObjectExtensions_LogNotification_m0968F3F09B27243BA90F851446E9D978A2031EDD,
	ObjectExtensions_LogSuccess_mDE4CFC8CEFB0AF4619AC6871A70F09E8FE2F19C8,
	ObjectExtensions_LogWarning_mDB995A3CE3662832CD9097225D442C73A6FF2D4D,
	ObjectExtensions_LogError_m953CE7BACA44BA1932BF876D8B1ADC9612450256,
	QuaternionExtensions_IsNaN_mC9FE45291D8D4253F5D7D887A9A1C466DEE349F9,
	QuaternionExtensions_Zero_m0F12B09FBEB3B559E1D64904A9121BF1B38ACF23,
	QuaternionExtensions_Up_mBB3A995893817FEBD828D45099EDFC5A84BBEF5D,
	QuaternionExtensions_Up_mC52895A6ACD4B3F51662DB8C74F910B1C0CC90B8,
	QuaternionExtensions_Up_m06E359EA967177DD74342E90C37DC889F6F83F00,
	QuaternionExtensions_Up90_mABB571A0047C0DAE30CCC9BB65A52CB2815F8A9C,
	QuaternionExtensions_Up180_mE140FDDE5BE7988867B33A14E09B28D74EA89A9F,
	QuaternionExtensions_Down_m153776DFF67216F955E7138922B3D4146382320E,
	QuaternionExtensions_Down_m90995D193F285DB9A33D12E5BB909990F3A7FD6E,
	QuaternionExtensions_Down_mEB06E21CAB8262A461537A80F8F98992C94F510F,
	QuaternionExtensions_Down90_m0A7CFA1EFE1E1DE498622A5B60387EBB13509B1F,
	QuaternionExtensions_Down180_m21AC9C0569C619D1EA8F56B7FAC4C4D0DEC5C4C0,
	QuaternionExtensions_Left_m947B5E58A6C2DFCF74DD4A8756424E221C66966D,
	QuaternionExtensions_Left_mDA42E54985429B0F3E23DC9BDF3862B3A0DF41FD,
	QuaternionExtensions_Left_m5C2BBF623267EB0C3EB1240265BC924DF4C35EB5,
	QuaternionExtensions_Left90_mBCEC0324C5BE378CCF01882DB9C49E3C88D7C453,
	QuaternionExtensions_Left180_m10F0F71ABE7471EE31D93784997104EE94F6C43B,
	QuaternionExtensions_Right_m776B9F0569D3F2244DD5A97A4E89DF3AB16EF818,
	QuaternionExtensions_Right_m9453E79E22C616CB1986E5C1EA0D039AEAEF6C95,
	QuaternionExtensions_Right_m14FF1F9324335431168EF73FF6041792A6EA04DF,
	QuaternionExtensions_Right90_m9BD36C19ED4067966DE28AD70E61D36681B2BFE4,
	QuaternionExtensions_Right180_m405E1E80C2FE3F6C2E7942AB09D4B7D85151825C,
	QuaternionExtensions_Forward_mDCE957BA188393041286E10CE0FE787BD523BB88,
	QuaternionExtensions_Forward_m7000615890CAEC4AC45C91A66A68BD52BBA39BC4,
	QuaternionExtensions_Forward_m65940725192A7DA0D955C067F3BFEE3E6DA86B90,
	QuaternionExtensions_Forward90_m80D0BF85B5BB5CF21A19B54D20E554C6B34BDD67,
	QuaternionExtensions_Forward180_m199E6031F53F23DE333755BD198D81DAC664D883,
	QuaternionExtensions_Backward_m52DF472DFAAFDE4E506D3A7BB06C9E75D3299C73,
	QuaternionExtensions_Backward_m70BA954DD7F1A36CA375FDCDBF0D441AF5D0880B,
	QuaternionExtensions_Backward_m24D1D22CDA1132F002B0CF76948A15ED2C5A5F58,
	QuaternionExtensions_Backward90_mE14A87E0BEA5D911553B940C41D7E599759B7832,
	QuaternionExtensions_Backward180_m6FB603BF64C76C05CCE51CEC35248AAA3D18B3CD,
	QuaternionExtensions_Add_m4CAA4D499323047A559C20D57473B45815F0E648,
	QuaternionExtensions_Add_m336CEF5746B11CFA9D83A148A0DE496862D7ECCB,
	QuaternionExtensions_Add_m7084BA1762C4F4381CF2FCF9635E888E1F498D9C,
	QuaternionExtensions_Add_mE46FCEB42C92B5163F71B313073F2E814BF0E50D,
	QuaternionExtensions_Add_mF713C20D639724911F419DEC5DE3EEB0FD18AACA,
	QuaternionExtensions_Subtract_m4CF2B58CBBCE57D0DBB4AC1F1D5F777888657F04,
	QuaternionExtensions_Subtract_m2B59E0F86BD21F529B10BB208801F15F424484A8,
	QuaternionExtensions_Subtract_m2C3BAB564A9569BE9BB425835FFB040873AFB8C3,
	QuaternionExtensions_Subtract_m4F04ED216472FDD481FA50F2DE5B53808BC31365,
	QuaternionExtensions_Subtract_mB6942626E492BEB8FE232C43F6D497D96FFF01E9,
	QuaternionExtensions_Multiply_m67D9D8633883D8E551D25EB7269C46FF725782F7,
	QuaternionExtensions_Multiply_mAA96F55CF284FD2C99FD1035CC1CCE8673E662BD,
	QuaternionExtensions_Multiply_m9227C99233A1B254EFBFA0117E89363C1C0A0BE8,
	QuaternionExtensions_Multiply_mB7D0A683328EB6D69EAD60E2095E48AFBFACAB27,
	QuaternionExtensions_Multiply_m8E8913D8912390CE6C9EDE0E562AC4FF1E7AE2E6,
	QuaternionExtensions_Divide_m5EFFEB89FB0B5889C6AC64A4326A492F1A02D02C,
	QuaternionExtensions_Divide_m9EA4388DBF3ABE2DFA8ED007A4F6331228410D36,
	QuaternionExtensions_Divide_m357BD1B27735729427DA56C846D3019977D260F2,
	QuaternionExtensions_Divide_m07FA50A7EBDF4523A9309D15094C76030A78D215,
	QuaternionExtensions_Divide_mE439CBA5A34972F59CEF509C0490D8C7C9C5309E,
	Rigidbody2DExtensions_AddExplosionForce_m8E9BDAD79B943D7B97E0FC5EE0EE5AD0599454DE,
	SpriteRendererExtensions_SetR_m8BFF8AFDC263CA0BC4A40FC5D06B0A32E1E76A69,
	SpriteRendererExtensions_SetR_m6EEF8BA1DE7852327357E5A7A721337679DA62EF,
	SpriteRendererExtensions_SetG_mB5A0183B6B695701304581A15FB9ECCB39CBFA8B,
	SpriteRendererExtensions_SetG_mAC079782001B1E6832445F6F80514071D8901385,
	SpriteRendererExtensions_SetB_m9284EF24294550B3ED66698DC6DEA83EEC5B182E,
	SpriteRendererExtensions_SetB_m757487CD1F97AA94AAB9BCF75CAC56833504184A,
	SpriteRendererExtensions_SetA_mC94B3648CF295E68D03A4179CA232D1A2F06EC20,
	SpriteRendererExtensions_SetA_m54BAD889A436F0F488D6F45B573F1FBF94916770,
	SpriteRendererExtensions_SetColor_m7CD01B62F84106674BA6973641776B6CD3A6A7FA,
	SpriteRendererExtensions_SetColor_m5C7E8A42F030A1A8A564332F06936ED007048C95,
	StringExtensions_Divide_m2BCC8258257E8616F53F6DA9B04E10FE1936641D,
	StringExtensions_Color_mB8C917AEDFC621F07E4ED594A654FD6A40A7ED11,
	StringExtensions_ToColor_mFE27F82901872E6F765BC8EDC12B048CC0DD1B74,
	NULL,
	StringExtensions_IsHex_mDDC2251E712409026DB88D83386F4B84A26D1022,
	StringExtensions_IsAlpha_m74204F1AA74D46F02EA8E89C5C00B403EE6D30F7,
	StringExtensions_IsNumeric_mFF043632B4F5E1742BE82FC3B006578C92B0E04A,
	StringExtensions_IsAlphanumeric_m901B743AF3B9C962E9C4453323D1E865F752B66D,
	NULL,
	TMPTextExtensions_SetR_mB4B2957AC500366941B17B7568231D351317482B,
	TMPTextExtensions_SetR_m94BBFE8352782D5DD510CBFE8BF650B31F7AA115,
	TMPTextExtensions_SetG_m2039EFA377D9DF24CA2C824EC1185737E8E92DCD,
	TMPTextExtensions_SetG_m96C93CAA18EF4F9ED87F9617D2EE1453B1C014F6,
	TMPTextExtensions_SetB_mE795683BBB5EC70F256F18F1622BF46966D49690,
	TMPTextExtensions_SetB_m059A5D2AF784EE97EC7B7E52F7CCFFFBAE403E0E,
	TMPTextExtensions_SetA_m291C7CDB9C0C7C0AF7F7B2B009E7D68EA1EECC5C,
	TMPTextExtensions_SetA_mE8D0A7075CD7AA9D35E37F50AF49BF1DA9443ED4,
	TMPTextExtensions_SetColor_m37619C9062B93B5A410F9336C050FBCFE614670A,
	TMPTextExtensions_SetColor_m7553DCBF072EBFC73BEF0EDD99102742A475F0E0,
	TextExtensions_SetR_m3C5E804D9E55689CB388ECDEEB0084E1474BA8DA,
	TextExtensions_SetR_m1EADCE814318C3D20603DFBAC59C2A7FFAF7E8BB,
	TextExtensions_SetG_m83D9666D54A195A448B8403A01224410D1000F6C,
	TextExtensions_SetG_mC90BD24E6A8221AE2369CD5AF190FA771DA10C78,
	TextExtensions_SetB_m48F032B423EBAF11900517D6C1E1525D2ED1DCB3,
	TextExtensions_SetB_mBC36FDFBD6A12DCB5FEFC72DEECF90D16A65A49A,
	TextExtensions_SetA_m85362D6B891357D09337508D77EA2FEA3D219566,
	TextExtensions_SetA_m40873B329DFE4CCB63BE5BC83FEE7136DAC64C9D,
	TextExtensions_SetColor_m0C70BA3F433326639E054B20FD170A50D91D0A50,
	TextExtensions_SetColor_m71E41A2A11BF53243929659738C52ADA9F1DC9D3,
	TransformExtensions_OrbitAround_m427AFBBED01A222959A499573BF3416EEB0BBA8C,
	Vector2Extensions_SetX_m628D5C7426C250CF1AA42EB7B4D5CD4C2AEF6FD2,
	Vector2Extensions_SetY_m4E8FBC009EFF33EF0A41B94012A34C4CBE0A2FB9,
	Vector2Extensions_Add_mF90553B085B216B8063C319E7F5C8204150ED6A1,
	Vector2Extensions_Add_m762B3B8DF1C23DE0AA2C3560276B2A8B41B63D4B,
	Vector2Extensions_AddX_m758A794B218B5C423592D51E268B663BBCC11962,
	Vector2Extensions_AddY_mBB7E924E34BC6B1692D46C6056AFFECA78C72D1A,
	Vector2Extensions_Multiply_mF1DFE84C51DCB8461D57DFEA89F8271451231BBE,
	Vector2Extensions_MultiplyX_mBDA6E3039AB952E5CBBA6392DCA76DBB53AD320A,
	Vector2Extensions_MultiplyY_m5D71509EA53C9E5E1EB99824003A55F582038DCE,
	Vector2Extensions_FlipX_m207B2ECA3AFA9705787AF81675F193DDFEED1274,
	Vector2Extensions_FlipX_m9B3AD699449ACAEBC20A9FAF8E18C10D86D7AC8B,
	Vector2Extensions_FlipY_m3D57D6A2C8E86B7932C2E2E31818ECDC8E7394E2,
	Vector2Extensions_FlipY_mE5CE03C2A7D315D3A99DD3A328A4AD31437FBA22,
	Vector2Extensions_Clamp_m45CA9923A6E901E9382DDC7FE420214A9BFFD02B,
	Vector2Extensions_Clamp01_mAC727F3DC44C853FDA19B266270580714034A817,
	Vector2Extensions_Abs_mD3F2F9E177B6FB04F7D6FF4C1C5AE863A9717B5D,
	Vector2Extensions_IsNaN_m2CEE8D61FDD43F32C53233AA95A589ED3E033EE4,
	Vector2Extensions_IsBetween_m12B3F6E076D165F71A3EA8653FF0D395D939065F,
	Vector2Extensions_Random_m41E2C59D0F97940AAF95FFBFB970BD4D4F609284,
	Vector2Extensions_Random_m75CA36F81F110C038800A22EAFDE25668ABA9B25,
	Vector2Extensions_SetX_m595EC12BB39254EA57783C91AD3ADCB4A653C8B0,
	Vector2Extensions_SetY_m0A7A88B104C6A031184C0EC70E9BB8834E818C6E,
	Vector2Extensions_Add_m2C033D4117520A9FEE90D070A9102DD3C70BE2E6,
	Vector2Extensions_Add_m7FA63B8E9AF73DA9D6F1839128884FB5C83A2DEA,
	Vector2Extensions_AddX_m0DB873F80F8A413EE690783034D438225AB3D164,
	Vector2Extensions_AddY_m27B53E8C4E342CDC2365F88B86F21E7DE407D9F4,
	Vector2Extensions_Multiply_mAB1021AA7564D90AF150F1F171BAD924F3B38450,
	Vector2Extensions_MultiplyX_m972CCB60D21C531E0021216D2E4E147A7EA7C0A6,
	Vector2Extensions_MultiplyY_mF5AE7390ADC21BD6BAA2F0307910342360EE53ED,
	Vector2Extensions_FlipX_m60E7C5F685BB334192C017E7B59F26459A37B4E1,
	Vector2Extensions_FlipY_mD2C6E9C5B1C152F266DE5AA51DDB1F299294DAE5,
	Vector2Extensions_Clamp_mDDF1AEFA36425F97961E262C791A7118CFA080F8,
	Vector2Extensions_Clamp01_m420330594EDA2001E2A9A5E43542B6C2934CC1DE,
	Vector2Extensions_Abs_m443A22A719ECBC24B13A8C98DF0F643BB6699F35,
	Vector2Extensions_IsNaN_m3BD979CB285D0FE1B7C92EC7C09A98DE3FEC439A,
	Vector2Extensions_IsBetween_m9BDF92254390AE9F6712D1911CFD269800874943,
	Vector2Extensions_Random_mC14C459831FE9B2D765229782B770F82DB2DC5D7,
	Vector2Extensions_Random_mD7F87347B2F86A02C3D17910862F3FF2361A7B44,
	Vector3Extensions_SetX_m81CF6035CEB02FC9564EC3FDA6F022DAFC315718,
	Vector3Extensions_SetY_m80B1B14A61DFD9644F2738CFF13D2C0AE4E1B47C,
	Vector3Extensions_SetZ_mA4B15836E624115C548DC4DBC9C231B915425170,
	Vector3Extensions_Add_m129731B6B0B7D716826167689EF58602DE19DEB0,
	Vector3Extensions_Add_m427C7F3B429DB4D3D6885A799BCB22A8BB746D10,
	Vector3Extensions_AddX_m091C22A33637623414F59CF8E760945DC978FE0A,
	Vector3Extensions_AddY_m3DC1D188941755AA4B0EC34B71E1EB4F9443EE3F,
	Vector3Extensions_AddZ_m167854E93C83055C59F075127BC017ACC4BFCE30,
	Vector3Extensions_Multiply_m59A3EB0E5AC45440069BA4638FE5574337F16C24,
	Vector3Extensions_MultiplyX_m01BB07BBFF31318D2D904375EA6229EC98269AE7,
	Vector3Extensions_MultiplyY_mD8206F4F19AC05F89F814A1A3A1FC066B8FAD3F7,
	Vector3Extensions_MultiplyZ_m4125A5E09C216CAB14745C8B807416602546717B,
	Vector3Extensions_FlipX_mE1C6AA3FB735AE73620ACAD1B024CE661DED4950,
	Vector3Extensions_FlipX_m1A831DF6FF2320D87367883EC610D1C0F9ADFFF3,
	Vector3Extensions_FlipY_m947BF3D1C40A38EFBA386A04E0AE3D2BBEA34C5F,
	Vector3Extensions_FlipY_m8B0B2087D7210C8C7E8478EE593DC76808C60F24,
	Vector3Extensions_FlipZ_mB79D528327D548D16D0EDF4054F0B123CBBE7A53,
	Vector3Extensions_FlipZ_mC50F3E941648FC54AECB6258E6A700F509223729,
	Vector3Extensions_Clamp_m560F956CF30672AAEBDE86DAE2151F22C3BA4650,
	Vector3Extensions_Clamp01_mA5FB3155EFFD290F0EF094B52B33D1073D6B031B,
	Vector3Extensions_Abs_mE81EE8A5C7EE28268CF0D1B91AC614564E3F772F,
	Vector3Extensions_IsNaN_m8BBB8CB0F0A766C32ECD86496E0C09DF33EFCF6D,
	Vector3Extensions_IsBetween_m7AF18A71EECB9DC544FE13BACD8BF22CE82558EC,
	Vector3Extensions_Random_m4DEBFEDD768D01361BD810FD1C154E142F2FEA3B,
	Vector3Extensions_SetX_m163A6AC84AEE45EF5BC9FAE6C13455CE7742CC6D,
	Vector3Extensions_SetY_m63B70C6B67A6D6B7ACA684FC65DC0D7B7FC7A9D0,
	Vector3Extensions_SetZ_m5EE056686D97AAD916B9C0B85DD362B93F993C51,
	Vector3Extensions_Add_mA7D11EB57D79A27310CA41C27F96C8E4351784A6,
	Vector3Extensions_Add_mB57C1DC5B450AF7750A847B09F1FD340260D8287,
	Vector3Extensions_AddX_mC7DED0E7E3A689D23932A95CD5E6635344009FEF,
	Vector3Extensions_AddY_m70CF1032AA0D84F1DE1BF94260CB0797023B4E7A,
	Vector3Extensions_AddZ_mF3ADC52DBBE7EA3D772E254573B7F3BB28C4525F,
	Vector3Extensions_Multiply_m395208DA68CD04BC047C164060D1AD22FBE4AA40,
	Vector3Extensions_MultiplyX_mCFBFA0C826EE539A6EB978C03C7C3C4A79C68082,
	Vector3Extensions_MultiplyY_m49A31473D6CD98AA93115BAFAC87F01A5C742071,
	Vector3Extensions_MultiplyZ_m534D5268445D560788BC0051115F7FDDF7586DCA,
	Vector3Extensions_FlipX_m43688C951EC6BD33AB086671DFA5FF4107126D63,
	Vector3Extensions_FlipY_mE1F7250AB3D50229D14DED692C2DB294658043F1,
	Vector3Extensions_FlipZ_m82CBD59675BB1FA10165B5BF48472E1F2B213340,
	Vector3Extensions_Clamp_mB2DF32F829692E3ADE4E5BD158D2CFF86AFE7D3A,
	Vector3Extensions_Clamp01_m2EA27A8E453821993BEB3EAB65B067EB8B11409C,
	Vector3Extensions_Abs_m64AA0B24024192CF433F91B6B6EB9C7A402FDDD6,
	Vector3Extensions_Random_m1C1CF68F0D0FC1AB9A6B6210ABD7B74CD803628F,
	Vector3Extensions_IsNaN_m6487769847AF0456E47459EF5D4EEE557AC23DE9,
	Vector3Extensions_IsBetween_mF858EF76A0CE1AEEA83E725B0DE0E53256D609D6,
	Vector4Extensions_SetX_m4488FC494CB2AF61DA8CA4B27325D5C6347D2A46,
	Vector4Extensions_SetY_m88F360368C14B924564A67CB65176F828F88E65E,
	Vector4Extensions_SetZ_m5B05CBDF245A95D31DF067551B66F7D95CDA821D,
	Vector4Extensions_SetW_m58268842AF83B79D88E1ECD3769B6E04CA223956,
	Vector4Extensions_Add_m75644FE0B835214634EB48EF8A397874D6040CC1,
	Vector4Extensions_Add_mB764E997232736FAF86908C91A26F422CF99150B,
	Vector4Extensions_AddX_m921E63F49FDA98902D1A3AFB611E42B1699733B0,
	Vector4Extensions_AddY_mC95FB316204D4DDEDE5E75E5C3B06E22E4C34C2D,
	Vector4Extensions_AddZ_m888AD463EF6716CE6A65360AD29EF75F9664C824,
	Vector4Extensions_AddW_m3B5CBAAD90A8722B6884F299766CD5CDBBCBA98B,
	Vector4Extensions_Multiply_m28E187B218F792582C81A26C2AA4C2BEED3AAD81,
	Vector4Extensions_MultiplyX_mAA0033CABA21A8A4AC3D5A25F96427DA23981E04,
	Vector4Extensions_MultiplyY_m2F195487328CDFEB4AB2F8CAC2F0528974012832,
	Vector4Extensions_MultiplyZ_m6B49DF8E2A118803DED30A63BF92F00DCD59B9D4,
	Vector4Extensions_MultiplyW_m82DD4E43D50E114A94CAF9C07CE2FB82C366A77F,
	Vector4Extensions_FlipX_m4FE0F1E12B406F3FD14AF98556E925E81AE7789C,
	Vector4Extensions_FlipX_m075997348588562CD28F7D3792725F51CEB40220,
	Vector4Extensions_FlipY_m18F8D19C98CC1A8DFDE8784E33C29DE27D86E9B5,
	Vector4Extensions_FlipY_m47B8E95F76846CF7B4FB52E781616DD2099A81BE,
	Vector4Extensions_FlipZ_m7EA7F38B981936AED7E919AF0CD5280696E98560,
	Vector4Extensions_FlipZ_m4CCFC1573B94ADC023952CEDF009A143E999204B,
	Vector4Extensions_FlipW_m74E8AD6CD37BF06788D3E664C8308BF37BA4F0F3,
	Vector4Extensions_FlipW_mBC9BDF2CE1DF86A8EE1FDCAC8EE5707C80636DA8,
	Vector4Extensions_Clamp_m4D46C45F7D1CD35ECDCD4FAACB54E027772BCF3E,
	Vector4Extensions_Clamp01_m5D536C744535AA0842C612FB34084D106E758199,
	Vector4Extensions_Abs_m4934C92C8D119F43FBE87A923B0D6F69D70FCBFC,
	Vector4Extensions_IsNaN_m0D9B34D85906B29320C3A779695A5F3C49545615,
	Vector4Extensions_IsBetween_m7760AE4387A1C698F72BC1EDEEBECF87039470AD,
	Vector4Extensions_Random_m75E196B5C836C8D8C82CB32CF7A9D8F1BD024D4E,
	BoxDetector_get_Size_mB4D022DCB52FA854F4040BC282E98BE19BA03A28,
	BoxDetector_UpdateDetector_m607BE07DF6D72784AE66C6680543003902E464F0,
	BoxDetector__ctor_m3EE1E981229B09669B64587D6990ECE5C9F3BD5D,
	CollisionDetector_get_OnEnterCollision_m552AE5930F972CF920E0767FECEEA6E8E404CCDB,
	CollisionDetector_set_OnEnterCollision_m6F9F5443517CAE3BA616DAC973C338491D720DDA,
	CollisionDetector_get_OnExitCollision_mFCE873DF650709BA935EA417D700D44B124233F7,
	CollisionDetector_set_OnExitCollision_mABEA96E854787882105ECE55D298B4F958EFF49D,
	CollisionDetector_get_OnEnterCollision2D_m65D34D5EE180EDF0087CD7B17B03C5F86BB83627,
	CollisionDetector_set_OnEnterCollision2D_mB1E65053C541FA44FFD1ACAE3AB159B5755A91D8,
	CollisionDetector_get_OnExitCollision2D_mA070591D65C0365EB7EEB1938FB063D8F4D1E57D,
	CollisionDetector_set_OnExitCollision2D_mAB23E7E7ABB23329F387123FD0F55B8393DE80B1,
	CollisionDetector_get_IsCollisionDetector_m35663F8771BDB74AD0DA95938E9536DC8A529D94,
	CollisionDetector_OnCollisionEnter_m3756EDA1589191C18F207910528A4C06C2003A4D,
	CollisionDetector_OnCollisionExit_mE6D4224BC6FDC46A28707E8AF6C9B0CC3141EC6D,
	CollisionDetector_OnCollisionEnter2D_m1111D6F15643850AA6BDC77F2D0975C3A9D0EC5A,
	CollisionDetector_OnCollisionExit2D_m0E3BC8BDA6EEEE7D4D174554317DD6EE08612B97,
	CollisionDetector_OnTriggerEnter_mA51E7E61F41ACF372AA145DD733C7E9A2E6CAD2E,
	CollisionDetector_OnTriggerExit_m1F3341D73495D924B2983F058F12C2C848E6015F,
	CollisionDetector_OnTriggerEnter2D_mFAE14ECA33644098950358D44C7DB17C80DB5689,
	CollisionDetector_OnTriggerExit2D_mE7131A286E64473EC1A5FF59480CA8230C579599,
	NULL,
	CollisionDetector__ctor_m7B3FDF5BF025B1FA6711DF9304CDE492A0BE7AA5,
	ColliderDelegate__ctor_m6A49AADFADF039C7804E2EB75027E7135F0BA1E1,
	ColliderDelegate_Invoke_m7CF676CBC8ABBD9A051D549BB367ECC23EEA64B8,
	ColliderDelegate_BeginInvoke_m7A29544F198EC37BDACA4C76CAF5D6D35A95D60D,
	ColliderDelegate_EndInvoke_mFF8F287FDE5778370E1DEA0F1CB8229BC3C1F70E,
	ColliderArrayDelegate__ctor_m769A8069342C6A4AF776BAC3B53D74C3256B9724,
	ColliderArrayDelegate_Invoke_mCB9ADCD520138A95966828AC016976926ACA6DD3,
	ColliderArrayDelegate_BeginInvoke_m7CE4DC0A2E7BFFD899EDA7BE2A43C31EEB319079,
	ColliderArrayDelegate_EndInvoke_mE7E299D64FEB85DB476E0DADB6F913FD10FC9A51,
	Collider2DDelegate__ctor_mEFBDC98962613FC73036038D992851373229154C,
	Collider2DDelegate_Invoke_mC04C22342134DDD09470CE9883C2585191EE7EB4,
	Collider2DDelegate_BeginInvoke_mB9B67346D802B8179DABB929611AC6A8CECB700C,
	Collider2DDelegate_EndInvoke_m431BF66AD4E0BF102FE519E36BB818EC1B37A2F1,
	Collider2DArrayDelegate__ctor_m667B1E6867506A59717A4D5159935EBDA5B0923E,
	Collider2DArrayDelegate_Invoke_m00E88726219FCF5254279C67D028EA9FB2B0A282,
	Collider2DArrayDelegate_BeginInvoke_mE223482AB75085AF2CF5F001EE225DC09DE4721F,
	Collider2DArrayDelegate_EndInvoke_mCB4B5992EEFB58E55183D083EA17B769DE820378,
	CollisionDelegate__ctor_m882FA50F7D18B3F52DF5E432B8122A810321650C,
	CollisionDelegate_Invoke_m1D5B8620A4B95B12EB1755F84836C5F16BFA2240,
	CollisionDelegate_BeginInvoke_m86ECE784A5F2ACBA080C9640A2E70109ED7CF490,
	CollisionDelegate_EndInvoke_m1A19646EF8AB7825C5388F99BA0A8F0E8F4CA653,
	Collision2DDelegate__ctor_mFEC810C6FC228CE0A4A0B4B214C34A9AE3ACA6ED,
	Collision2DDelegate_Invoke_mD8DA2B3A541FBCE892CFB5B42542EDA744393BDF,
	Collision2DDelegate_BeginInvoke_mEC2E882368777A0AF1DE500828688F6BD10D6BC4,
	Collision2DDelegate_EndInvoke_m3AF2FAD2EF4F88ADA1DFECFEF611B89F517AABCA,
	Detector_get_IsCollisionDetector_mF21FA01B1A8AB55B2CC0403AF849AAB2EDB119B8,
	Detector_Awake_m0A846996A9D08EBAC4C9A92A6BCE97B5F695DB90,
	Detector_UpdateDetector_m7309EC77733C6EF5D9437EE76653D34C59489D85,
	Detector_Detect3D_mF66CDDE559E00F2E71CE982F5DD9FD788F45E971,
	Detector_Detect2D_m96B9CB1FABEA1A54DF93D046C09171C785766430,
	NULL,
	Detector__ctor_m0EFEB7B47AEDAE653674618F31E1E97CC52B08F3,
	Detector_U3CDetect3DU3Eb__16_0_m4470C692412E4DB45B299A874A4D6EF37BB71B35,
	Detector_U3CDetect2DU3Eb__17_0_mA99BEFCE95AB60BBC004150C2CC3C982C547F218,
	RayDetector_get_Distance_m4A3300B64FAFBAAEB601B0FB59E46ED8C9A5F1B3,
	RayDetector_UpdateDetector_m01CB5A7255CD9F60D423F77ACF19383E7504FB12,
	RayDetector__ctor_m08080361A45A38E1DCA019D5AC99918728BA2A78,
	SphereDetector_get_Radius_m66781B323F6657FFA37AB47221954DA1ADF2496A,
	SphereDetector_UpdateDetector_mD3ECA8C2483C419DA4D0BE31E8E6563215BEDD5B,
	SphereDetector__ctor_mA336CA54662CE11F66CD3D171400637458AF05B4,
	ShakeController_get_Data_m9194007F30FE902F418E3719CC6589BAACBD2A97,
	ShakeController_set_Data_mCC4E4F1BDCA181EAA83C186945435EC73923CF95,
	ShakeController_get_Noise_m6430E01E097318FB62F2392358856AD0436AA177,
	ShakeController_set_Noise_mFB37CA32FC05BAE468506C2ED111B29AED5194BA,
	ShakeController_get_IsRunning_m92F84540AF648FBB151514751AF29A6EF75EBDBB,
	ShakeController__ctor_m4F9985FE2005869CE7B3D81C863AF79277752F70,
	ShakeController_Update_m0492DE796E36B7C61A757F31552E83AA86DE3F16,
	ShakeData_Shake_m6B701676891B6E72AC734B1DED499ADC2CE2327D,
	ShakeData__ctor_mDCEE67EACA53EA1EC11EE8E97BBF10BB0E4C7D71,
	Shaker_get_Shakes_m16E0A104B3A0951DF9BA6D427289A34165C88736,
	Shaker_set_Shakes_m69643E7D4F2FEC0A5B164550D74979D12E0D60D1,
	Shaker_LateUpdate_mA91DE50B6F15DDBDE3EE4881BBE00EE30E862242,
	Shaker_Shake_m864C274A3EDE0FB0CBE2CD10ACCA77BE9D69C26C,
	Shaker__ctor_mC4282CF1A05F4DB3F8440D13B561F2A3814A6CC3,
	SoundController_get_Sound_mBA2B474E8303D1A09B3EFB797D4ECC3C0932FC73,
	SoundController_set_Sound_mAFFB12A928462D41B9316599893546B4D6FC993E,
	SoundController_get_Source_m17A382415CE8079DBE7279043BF410B844F45448,
	SoundController_set_Source_m6B3FF429CE8B1FEEEE895EC0E499F15E53FE049A,
	SoundController_get_Duration_m9C3DB8864780E96F6C6342E86ABB9F0A72C0BE7D,
	SoundController_get_IsBackward_m469505E5F0A7E71BB2D9179C4623C16636EF0952,
	SoundController_get_StartTime_mD5BAF345D317D2F644A8D5CE7DF4BE95511E8437,
	SoundController_get_EndTime_mFB82D49A344708B8E82CA1997205114C7B3A7A69,
	SoundController_get_HasFinished_mA9561FBEC3B4C809DF111896361ED8A09CC4FB3C,
	SoundController_get_NextCycle_mF4C2639975FE01B34976AB1A6821B3446EF578EC,
	SoundController_OnEnable_m98E90F08F90C75EE7539925D17017DFFD05DC259,
	SoundController_OnDestroy_m0C9BA0F93801AC2ABDD3C0806B88CD42A29FD4B4,
	SoundController_Play_m4837D8DEECAFD9F4357C30E573BA5645710A58CF,
	SoundController_Stop_m1F27729E29ED19DC0D35E17D65D11A5933CE585D,
	SoundController_Kill_m572F633CB8D3E2C7EEC0445BCE7B5F6487FF471B,
	SoundController_InCoroutine_m33D2FDA6432F130AC83F2D296F45F31229B72508,
	SoundController_OutCoroutine_m73884E8C7CEFB14569C11DB03BC001D6916673AA,
	SoundController_SFXUpdate_m03BAC1FA20CDAF18E9942DA9944BDFFE203C6778,
	SoundController_NextClip_m6F911CB0F2FA9F294BC99015625CE630D6583200,
	SoundController_NextSound_mF9C8EB89D877A86D446E1AC8372DFA7AFDA29E69,
	SoundController_SetDynamicValue_m8F92564F13BB98C8AFF8AF0ADD67826E91360149,
	SoundController_CurveCoroutine_m9A698305897A7796C9BE34C2CEAD4189984F72EE,
	SoundController_SetVolume_m7BF6D26DDDD7B18C61DE3C78AFD94E93F8EEADC3,
	SoundController_SetPitch_mA07E8D1827E24CC9C574041E0FDE6E6C2932F727,
	SoundController_Destroy_m8579BCB0F9CF0758AF91D215B4EAA49E7B49DC69,
	SoundController_DestroyImmediate_m0F89A9443503D9DB00D1F1A23EF7AFDA3F052514,
	SoundController_PlayIndex_m0D94D984F156F00A5C2AD307A61190F668A4DA2B,
	SoundController__ctor_m84C8C661E258C5BC4A73D8BC619584092CDE19DC,
	U3CInCoroutineU3Ed__36__ctor_m768A77D62609B5219C8B27E84141983AAD3A3D76,
	U3CInCoroutineU3Ed__36_System_IDisposable_Dispose_mB6F727542D4FE84C7FA54DB3A654762DF56997E3,
	U3CInCoroutineU3Ed__36_MoveNext_mDB4319ADA06596B069A328CDC8BA345232D0F830,
	U3CInCoroutineU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68B293F1F04F174F8AB3D39F3117B329F637258B,
	U3CInCoroutineU3Ed__36_System_Collections_IEnumerator_Reset_m1ABD37031B2464A2450F841151AE1ADF40CE3551,
	U3CInCoroutineU3Ed__36_System_Collections_IEnumerator_get_Current_m721A47FB34BEB99D558CDB207E02E86FBDBBA28C,
	U3COutCoroutineU3Ed__37__ctor_mBF75D8F91859D02800985B782AD9B9061034A4CC,
	U3COutCoroutineU3Ed__37_System_IDisposable_Dispose_mFC1E65CC0E5BED49103F47FA6074CB7CD88B2EFD,
	U3COutCoroutineU3Ed__37_MoveNext_mD557DBAA1C27F0238D43AB6780F8BEABDA679822,
	U3COutCoroutineU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA32CEBA53B1DE20AF3F1994A7936BE75142B8F71,
	U3COutCoroutineU3Ed__37_System_Collections_IEnumerator_Reset_m7107C4BB8F4BD42DC550CA60ABB427255BAA6F27,
	U3COutCoroutineU3Ed__37_System_Collections_IEnumerator_get_Current_m6A992C1DF0F0D804C810939A396E501E79B35003,
	U3CSFXUpdateU3Ed__38__ctor_m07428BBF9A91E3E67C36ED99F3FA91529FE4F06F,
	U3CSFXUpdateU3Ed__38_System_IDisposable_Dispose_m9EE4DCFF6221AAE6E63389B625CC72E7BEC38C04,
	U3CSFXUpdateU3Ed__38_MoveNext_mE77ED25AAF32A9A646C77E42EE3E0D99231F1C68,
	U3CSFXUpdateU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84E0A548F2DB3379A1F64D00FED15D945D35E777,
	U3CSFXUpdateU3Ed__38_System_Collections_IEnumerator_Reset_m440C938D24AC7FC03EAB463992F1640D4A9E1780,
	U3CSFXUpdateU3Ed__38_System_Collections_IEnumerator_get_Current_m386B1BBB881C24922403C0C99C403CF1BF7237D2,
	U3CCurveCoroutineU3Ed__42__ctor_m7CBE424F8485C6F65BF9B61DC65E567D584FF738,
	U3CCurveCoroutineU3Ed__42_System_IDisposable_Dispose_mAE910B8021B1C839BDD46C8091E823CCC14A1B49,
	U3CCurveCoroutineU3Ed__42_MoveNext_m6733CCF6B67C1743C31F070C67F5401BB0E7E29E,
	U3CCurveCoroutineU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3D1317D299D2A110D9B57D9017DDA06E96B745D,
	U3CCurveCoroutineU3Ed__42_System_Collections_IEnumerator_Reset_mE505FA3A27B3D11891AC8BF8BB1BBB63FC2627D8,
	U3CCurveCoroutineU3Ed__42_System_Collections_IEnumerator_get_Current_m5F69CE3B305929E8E2786EE42A96D602A656CCF8,
	SoundEffect_get_Item_m69B105B90FD522E4153E2F829B887499168C7635,
	SoundEffect_get_Item_m135B3869341995A0DCB489A34C5E79AA1D158443,
	SoundEffect_get_Controller_m2A5FF64D3317DA9A1E49055A1B6CB8579591E804,
	SoundEffect_set_Controller_mEE20B5A111058B0F288672C8808E32209332B7F6,
	SoundEffect_get_Error_mFEABA33E81935C46112A8DDF0D973B2852350326,
	SoundEffect_PlayRandomIn_mF43DE75B7245572B9F7DB5200CFA5B4DE3F2C8EC,
	SoundEffect_PlayRandom_m530BC6833E850408512284CDBF86236C9987DC09,
	SoundEffect_PlayIndex_m94A45DD4084396B8DF0EAB04210DD96D7BAD0C79,
	SoundEffect_PlayClip_m1957A2B18F0811B95AD20300A82EF5A936B7BAAD,
	SoundEffect_Play_m6EF2B92FF56F18C888F23662FEC3AEA9E335050A,
	SoundEffect_Play_m2CF720A7BC1E4FA2CC85B1CFBDEF354ACE5677E6,
	SoundEffect_Play_m7B96EA771ACD5C8D1EF818FEF2770136C15E61A4,
	SoundEffect_Play_mE74F7C13B5A95ADC64B2973E37D3AB61DEF48AF5,
	SoundEffect_GetIndex_mD0F75D1E7B19347B10798286084AFAB82D57E8A2,
	SoundEffect__ctor_m49005690D984553914506D352F50192754D6537B,
	U3CU3Ec__DisplayClass47_0__ctor_m6C23AC52ACCA7C462C82CD39CB45DB3427ADE30E,
	U3CU3Ec__DisplayClass47_0_U3CGetIndexU3Eb__0_m199A71E692136F86F0B35A56BE0D38895B8D6818,
	LookAt_get_Speed_m9418E49B8898A0AD0E92C550F40745BB6E45CBE6,
	LookAt_get_TargetRotation_m1ADB344A9AE82FF57A1B5FFF29D74F03416E3FAA,
	LookAt_Update_mBC3D46F3B84AE879F0EF9649DAD6B64735B557DA,
	LookAt__ctor_mE8DBF3B4883F69B24A36C8428FE103BFBA57AB74,
	MoveTo_get_Speed_m5CE20DE31090C365A809C4814241E58A6456A822,
	MoveTo_get_Smoothing_m9FECF580ECE2A01F1DFE0B93DEFA888E11942560,
	MoveTo_get_TargetPosition_mA98ABA6DF2938E1A36F3EEE8E08F33B666DC7925,
	MoveTo_Update_m2B309169E7792C401C35CFBAF1776A9AD252FBA2,
	MoveTo__ctor_m0684D0AEDB2BAE41341F5DB7BC8B0BC41B9DCAED,
	RotateTo_get_Speed_m1857A806B518A4A34DE5962DD195B1C03A153F52,
	RotateTo_get_TargetRotation_mFBF2EF8345414D007674CA0480793DDB86B65BB4,
	RotateTo_Update_m2CADFB9435A25DBC1669633D653915EB5DA60E85,
	RotateTo__ctor_mE35DB069013408C9EC36F9BBE147E03BEA08A9D8,
	Rotator_get_TargetRotation_m9D1C0510BACFEE4358EAB03243F43CAEA4685368,
	Rotator_Update_mF2BB0FE6370B1439D77B80A8F0CF7AC10A4A5287,
	Rotator__ctor_m3B6AE4A7DDE871917541466518501F2B7B6CD41C,
	ScaleTo_get_Speed_mA689A8A49E807E1F15E73B4E52EDEA0203880B01,
	ScaleTo_get_Smoothing_mABBEB27D5CC1AFEA98B4753342569471AE406B8A,
	ScaleTo_get_TargetScale_m17099FC6B10897A55CD83A2A834BB551553F5B7F,
	ScaleTo_Update_m03A242B1B818A3E8E5A2CEA092B83156DE23840A,
	ScaleTo__ctor_m36019B37912FBE1FA556233AB0122EF3207C3E43,
	BoolVariable__ctor_mD14F45A12E9FFA94936118A266DF045FC4DF10AF,
	FloatVariable_Add_m5B456DEB93879200BC28CF1FBA2B38CCFA85AAE0,
	FloatVariable_op_Addition_mBF6655C2DB6CFFA0EEA4F397358B402E0D086FEA,
	FloatVariable_op_Subtraction_m3B5630FE735D50F374C57AEFEEF660EAF06483B6,
	FloatVariable_op_Multiply_m4D6D1FCF6C0DE556990DE4708411C766EA8319C7,
	FloatVariable_op_Division_m9602BB2482BE59266CA57244C9AA59E7A261D2CF,
	FloatVariable_op_Addition_mD50137843BDA8E864BD9DB2659E918C1D374BD3B,
	FloatVariable_op_Subtraction_mEB87A5D62BEA760279F2F7FB0568CBABA26A162D,
	FloatVariable_op_Multiply_m716497B1C8DE764D605AF2D916B1F3940A3E1F5B,
	FloatVariable_op_Division_m6D4C1637B219DCA426560F650E2B96060610C4D8,
	FloatVariable__ctor_m93D45630DB0F4D7AAD19DB346F4297AE1A44A760,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IntVariable_Add_mBBC2E2C5D8726FA0F1ABBABDE4FEB33646154A5E,
	IntVariable_op_Addition_mE2BF57251522059E120FF17E4BC35FE7B935C042,
	IntVariable_op_Subtraction_mDFC56E17993AE9B6C5C621632C2631C0699839CE,
	IntVariable_op_Multiply_mF011812E491834C5417BCA65DB25F0372D444B2E,
	IntVariable_op_Division_m99247BAA544F386EB770A63F26F03A9C3B10C052,
	IntVariable_op_Addition_mE0E344A514006ED1AEC5A42E316D5138A5E7AC4B,
	IntVariable_op_Subtraction_m2630096420772D4B14385981DC0E10525277B65A,
	IntVariable_op_Multiply_m43EB1A78A4A9B570D6827FA993DCD462FB8D6A4F,
	IntVariable_op_Division_m6383014092BDA355E4B3DDEBFFC64103E4D793A8,
	IntVariable__ctor_m50F51827F27895196C2E311C0EB56298C47F6CAD,
	StringVariable__ctor_m2988EA2BE9AE0E3D0E30946C1C41EE814A55F67D,
	Vector2Variable_SetX_m15ADCDF86DDC4706D1E1D6B74C8BE868F9A90EF8,
	Vector2Variable_SetY_mD3B43ACD1746989585B78E36DA47B5655DC07A91,
	Vector2Variable_AddX_m73A9F9C9139661A0126FCF2C443E2E1E21A565F2,
	Vector2Variable_AddY_m55EC955063EC3F9B5B14992601CE235432B7810D,
	Vector2Variable__ctor_mF5AA10818B992D586D2853794B89BA90B10D51D4,
	Vector3Variable_SetX_m07B06FB8C18083FFEA91A1828BF49B0954E50F60,
	Vector3Variable_SetY_mBC40D07E3788F502248ED568A2AFC91CECF67A78,
	Vector3Variable_SetZ_mFC69837F80992649B90D4119FD593BFBF79CD848,
	Vector3Variable_AddX_mE545DD6AC8FEF2316F3788F523CF017FCF764D6E,
	Vector3Variable_AddY_m42C6A8EE087C6CBFAAE48BFB80946475A5AC32E4,
	Vector3Variable_AddZ_m95137F714AEF4F5F78379EED6F602AA7711BE854,
	Vector3Variable__ctor_m2979A9357BD6ED39CD3635260F58DF042E64E3EB,
	NULL,
	NULL,
	NULL,
	NULL,
	ObjectPool_Start_mC04977F651D01E5EBE3C84188CAD56EC1A48C9CC,
	ObjectPool_Dequeue_m736202A6620AE05FCE63CF7F6FC15EF883698503,
	ObjectPool_Enqueue_m3A2786BBF13BB8F25AA204BE1201D509E5155DF3,
	ObjectPool__ctor_m8461AF302657087B4A4F82A95B9447902E90B8AE,
	GameEvent_Invoke_m9360B4D0BAE97638A3A92CB2805395CBB8688115,
	GameEvent_Add_m3CA7FC021B7E3BEB833AA0D2326559E1DC064B05,
	GameEvent_Remove_m1C44B6790B5F0CFE6FF60568BAFDBBA4C5DCCBE5,
	GameEvent__ctor_mB7045D8F0BD412B320784ACD83A9CB45C1F42886,
	U3CU3Ec__cctor_m0BEBFDC09B869F2217AEA0AE9E7775DDB7C82298,
	U3CU3Ec__ctor_m3DEBF43DF5455203C2B58D1AE91257157103A4EC,
	U3CU3Ec_U3CInvokeU3Eb__1_0_m649CDAA769E218D85F5471A6B16840D2D223173B,
	Observer_OnEnable_m5B151472F76C5DE6AE8B6BB5B45AE70160BF1AFC,
	Observer_OnDisable_mDAAE03EDDFBA78A4FF22976F8391237FBB77DB5F,
	Observer_Invoke_m4358633664613294F5001BDEA0BC39B54DCA4F15,
	Observer__ctor_m5ECFC7CAD6CA5F7ECA8B03AD538185027CE1DA5F,
	DontDestroyOnLoad_Awake_m84E57F8144BEC6705DE625AC66710F4495674FAB,
	DontDestroyOnLoad__ctor_mF051046ADED5EC6EF0FDEDA1273D0D9BF77E99F1,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[864] = 
{
	3170,
	2568,
	3229,
	3229,
	3229,
	3229,
	3229,
	3229,
	3229,
	2593,
	2593,
	3229,
	3170,
	2568,
	3197,
	2590,
	3197,
	3229,
	2568,
	3229,
	3229,
	3229,
	3229,
	3229,
	3229,
	2568,
	3229,
	3225,
	3229,
	3229,
	3229,
	2568,
	3229,
	3229,
	3229,
	2568,
	2568,
	2568,
	3229,
	3229,
	3151,
	2551,
	3197,
	2590,
	3229,
	3229,
	3229,
	3229,
	3229,
	3229,
	3229,
	2643,
	2643,
	3229,
	3229,
	2568,
	3229,
	3229,
	2568,
	2568,
	2568,
	2568,
	2568,
	2593,
	3229,
	3151,
	2551,
	3229,
	3229,
	3229,
	3229,
	3229,
	3229,
	3229,
	3229,
	3229,
	3229,
	2593,
	3229,
	3229,
	3229,
	3229,
	3200,
	2593,
	3151,
	3151,
	3151,
	3229,
	2593,
	4731,
	4731,
	4731,
	4731,
	3229,
	3229,
	3229,
	3229,
	3229,
	1453,
	3170,
	3170,
	3229,
	3229,
	2551,
	3229,
	3197,
	3170,
	3229,
	3170,
	2551,
	3229,
	3197,
	3170,
	3229,
	3170,
	3225,
	2618,
	3225,
	2618,
	3182,
	2580,
	3182,
	2580,
	3225,
	2618,
	3225,
	2618,
	3225,
	2618,
	3225,
	3225,
	3225,
	3225,
	3225,
	3225,
	3164,
	3164,
	3170,
	2568,
	3200,
	3200,
	3200,
	2593,
	3229,
	3229,
	3229,
	2568,
	2568,
	2593,
	2593,
	2568,
	2568,
	3229,
	1457,
	3229,
	3170,
	2568,
	3170,
	2568,
	-1,
	-1,
	-1,
	3229,
	3229,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	4983,
	4914,
	5040,
	3229,
	2568,
	2568,
	3229,
	3229,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1455,
	3229,
	1033,
	2568,
	1455,
	2590,
	678,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2551,
	647,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2593,
	680,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2616,
	692,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2617,
	693,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2618,
	694,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2619,
	695,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4434,
	3229,
	2206,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3463,
	4226,
	4227,
	4226,
	4227,
	4226,
	4227,
	4226,
	4227,
	4794,
	4886,
	4092,
	4092,
	4040,
	2551,
	3229,
	3197,
	3170,
	3229,
	3170,
	4549,
	4558,
	4549,
	4558,
	4549,
	4558,
	4549,
	4558,
	4541,
	4554,
	4723,
	4064,
	4064,
	-1,
	-1,
	-1,
	-1,
	4554,
	4554,
	3229,
	2568,
	3229,
	2568,
	3906,
	4554,
	4554,
	4554,
	4554,
	4554,
	4859,
	4834,
	4834,
	4362,
	4364,
	4834,
	4834,
	4834,
	4362,
	4364,
	4834,
	4834,
	4834,
	4362,
	4364,
	4834,
	4834,
	4834,
	4362,
	4364,
	4834,
	4834,
	4834,
	4362,
	4364,
	4834,
	4834,
	4834,
	4362,
	4364,
	4834,
	4834,
	4362,
	4364,
	4363,
	4366,
	4365,
	4362,
	4364,
	4363,
	4366,
	4365,
	4362,
	4364,
	4363,
	4366,
	4365,
	4362,
	4364,
	4363,
	4366,
	4365,
	3459,
	4549,
	4558,
	4549,
	4558,
	4549,
	4558,
	4549,
	4558,
	4541,
	4554,
	4336,
	4340,
	4664,
	-1,
	4858,
	4858,
	4858,
	4858,
	-1,
	4549,
	4558,
	4549,
	4558,
	4549,
	4558,
	4549,
	4558,
	4541,
	4554,
	4549,
	4558,
	4549,
	4558,
	4549,
	4558,
	4549,
	4558,
	4541,
	4554,
	4121,
	4488,
	4488,
	4488,
	4117,
	4488,
	4488,
	4117,
	4488,
	4488,
	4895,
	4488,
	4895,
	4488,
	4117,
	4895,
	4895,
	4864,
	4093,
	4887,
	3849,
	4490,
	4490,
	4490,
	4119,
	4490,
	4490,
	4119,
	4490,
	4490,
	4899,
	4899,
	4120,
	4899,
	4899,
	4865,
	4094,
	4734,
	3850,
	4498,
	4498,
	4498,
	4498,
	3852,
	4498,
	4498,
	4498,
	3852,
	4498,
	4498,
	4498,
	4903,
	4498,
	4903,
	4498,
	4903,
	4498,
	4122,
	4903,
	4903,
	4866,
	4096,
	3851,
	4500,
	4500,
	4500,
	4500,
	3855,
	4500,
	4500,
	4500,
	3855,
	4500,
	4500,
	4500,
	4906,
	4906,
	4906,
	4125,
	4906,
	4906,
	3856,
	4867,
	4100,
	4502,
	4502,
	4502,
	4502,
	4502,
	3587,
	4502,
	4502,
	4502,
	4502,
	3587,
	4502,
	4502,
	4502,
	4502,
	4911,
	4502,
	4911,
	4502,
	4911,
	4502,
	4911,
	4502,
	4126,
	4911,
	4911,
	4868,
	4101,
	3857,
	3225,
	3229,
	3229,
	3170,
	2568,
	3170,
	2568,
	3170,
	2568,
	3170,
	2568,
	3197,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	-1,
	3229,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	1455,
	2568,
	665,
	2568,
	3197,
	3229,
	3229,
	1973,
	1973,
	-1,
	3229,
	2206,
	2206,
	3200,
	3229,
	3229,
	3200,
	3229,
	3229,
	3170,
	2568,
	3225,
	2618,
	3197,
	2568,
	3229,
	2568,
	3229,
	3170,
	2568,
	3229,
	2568,
	3229,
	3170,
	2568,
	3170,
	2568,
	3200,
	3197,
	3200,
	3200,
	3197,
	3197,
	3229,
	3229,
	864,
	3229,
	3229,
	667,
	3170,
	3170,
	3229,
	3229,
	552,
	667,
	2593,
	2593,
	3229,
	3229,
	2551,
	3229,
	2551,
	3229,
	3197,
	3170,
	3229,
	3170,
	2551,
	3229,
	3197,
	3170,
	3229,
	3170,
	2551,
	3229,
	3197,
	3170,
	3229,
	3170,
	2551,
	3229,
	3197,
	3170,
	3229,
	3170,
	1966,
	1973,
	3170,
	2568,
	3197,
	2568,
	3229,
	2551,
	2568,
	1973,
	1023,
	1033,
	1033,
	1803,
	3229,
	3229,
	2206,
	3200,
	3182,
	3229,
	3229,
	3200,
	3200,
	3225,
	3229,
	3229,
	3200,
	3182,
	3229,
	3229,
	3225,
	3229,
	3229,
	3200,
	3200,
	3225,
	3229,
	3229,
	3229,
	2593,
	4342,
	4342,
	4342,
	4342,
	4340,
	4340,
	4340,
	4340,
	3229,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2551,
	4336,
	4336,
	4336,
	4336,
	4340,
	4340,
	4340,
	4340,
	3229,
	3229,
	2593,
	2593,
	2593,
	2593,
	3229,
	2593,
	2593,
	2593,
	2593,
	2593,
	2593,
	3229,
	3170,
	2568,
	3229,
	3229,
	3229,
	3170,
	2568,
	3229,
	3229,
	2568,
	2568,
	3229,
	5040,
	3229,
	2568,
	3229,
	3229,
	3229,
	3229,
	3229,
	3229,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[50] = 
{
	{ 0x02000019, { 5, 3 } },
	{ 0x0200001A, { 8, 3 } },
	{ 0x0200001B, { 11, 3 } },
	{ 0x0200003D, { 88, 1 } },
	{ 0x0200003E, { 89, 1 } },
	{ 0x0200003F, { 90, 1 } },
	{ 0x02000040, { 91, 1 } },
	{ 0x02000041, { 92, 1 } },
	{ 0x02000042, { 93, 1 } },
	{ 0x02000043, { 94, 1 } },
	{ 0x02000044, { 95, 1 } },
	{ 0x0200007A, { 111, 3 } },
	{ 0x02000085, { 114, 4 } },
	{ 0x060000A0, { 0, 3 } },
	{ 0x060000A1, { 3, 1 } },
	{ 0x060000A2, { 4, 1 } },
	{ 0x0600014B, { 14, 2 } },
	{ 0x0600014C, { 16, 1 } },
	{ 0x0600014D, { 17, 1 } },
	{ 0x0600014E, { 18, 1 } },
	{ 0x0600014F, { 19, 2 } },
	{ 0x06000150, { 21, 2 } },
	{ 0x06000151, { 23, 1 } },
	{ 0x06000152, { 24, 7 } },
	{ 0x06000153, { 31, 7 } },
	{ 0x06000154, { 38, 7 } },
	{ 0x06000155, { 45, 6 } },
	{ 0x06000156, { 51, 6 } },
	{ 0x06000157, { 57, 1 } },
	{ 0x0600015A, { 58, 6 } },
	{ 0x0600015B, { 64, 1 } },
	{ 0x0600015C, { 65, 1 } },
	{ 0x0600015D, { 66, 1 } },
	{ 0x0600015E, { 67, 6 } },
	{ 0x0600015F, { 73, 1 } },
	{ 0x06000160, { 74, 6 } },
	{ 0x06000161, { 80, 1 } },
	{ 0x06000162, { 81, 1 } },
	{ 0x06000163, { 82, 1 } },
	{ 0x06000165, { 83, 2 } },
	{ 0x06000168, { 85, 1 } },
	{ 0x0600016B, { 86, 2 } },
	{ 0x0600019D, { 96, 3 } },
	{ 0x0600019E, { 99, 2 } },
	{ 0x0600019F, { 101, 2 } },
	{ 0x060001A0, { 103, 2 } },
	{ 0x060001EF, { 105, 1 } },
	{ 0x060001F4, { 106, 1 } },
	{ 0x0600028E, { 107, 3 } },
	{ 0x060002AD, { 110, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[118] = 
{
	{ (Il2CppRGCTXDataType)3, 24802 },
	{ (Il2CppRGCTXDataType)2, 143 },
	{ (Il2CppRGCTXDataType)3, 24800 },
	{ (Il2CppRGCTXDataType)2, 142 },
	{ (Il2CppRGCTXDataType)2, 141 },
	{ (Il2CppRGCTXDataType)3, 20930 },
	{ (Il2CppRGCTXDataType)3, 20929 },
	{ (Il2CppRGCTXDataType)3, 20931 },
	{ (Il2CppRGCTXDataType)3, 21063 },
	{ (Il2CppRGCTXDataType)3, 21062 },
	{ (Il2CppRGCTXDataType)3, 21064 },
	{ (Il2CppRGCTXDataType)3, 21074 },
	{ (Il2CppRGCTXDataType)3, 21073 },
	{ (Il2CppRGCTXDataType)3, 21075 },
	{ (Il2CppRGCTXDataType)2, 4381 },
	{ (Il2CppRGCTXDataType)2, 77 },
	{ (Il2CppRGCTXDataType)2, 4382 },
	{ (Il2CppRGCTXDataType)2, 4387 },
	{ (Il2CppRGCTXDataType)2, 4388 },
	{ (Il2CppRGCTXDataType)3, 25116 },
	{ (Il2CppRGCTXDataType)3, 25087 },
	{ (Il2CppRGCTXDataType)3, 25051 },
	{ (Il2CppRGCTXDataType)3, 25092 },
	{ (Il2CppRGCTXDataType)3, 24505 },
	{ (Il2CppRGCTXDataType)2, 998 },
	{ (Il2CppRGCTXDataType)3, 133 },
	{ (Il2CppRGCTXDataType)3, 134 },
	{ (Il2CppRGCTXDataType)2, 1723 },
	{ (Il2CppRGCTXDataType)3, 8373 },
	{ (Il2CppRGCTXDataType)3, 25122 },
	{ (Il2CppRGCTXDataType)3, 25089 },
	{ (Il2CppRGCTXDataType)2, 984 },
	{ (Il2CppRGCTXDataType)3, 29 },
	{ (Il2CppRGCTXDataType)3, 30 },
	{ (Il2CppRGCTXDataType)2, 1724 },
	{ (Il2CppRGCTXDataType)3, 8374 },
	{ (Il2CppRGCTXDataType)3, 25123 },
	{ (Il2CppRGCTXDataType)3, 25090 },
	{ (Il2CppRGCTXDataType)2, 985 },
	{ (Il2CppRGCTXDataType)3, 33 },
	{ (Il2CppRGCTXDataType)3, 34 },
	{ (Il2CppRGCTXDataType)2, 1722 },
	{ (Il2CppRGCTXDataType)3, 8372 },
	{ (Il2CppRGCTXDataType)3, 25121 },
	{ (Il2CppRGCTXDataType)3, 25088 },
	{ (Il2CppRGCTXDataType)2, 986 },
	{ (Il2CppRGCTXDataType)3, 37 },
	{ (Il2CppRGCTXDataType)3, 38 },
	{ (Il2CppRGCTXDataType)2, 3404 },
	{ (Il2CppRGCTXDataType)3, 17269 },
	{ (Il2CppRGCTXDataType)3, 21866 },
	{ (Il2CppRGCTXDataType)2, 987 },
	{ (Il2CppRGCTXDataType)3, 41 },
	{ (Il2CppRGCTXDataType)3, 42 },
	{ (Il2CppRGCTXDataType)2, 3405 },
	{ (Il2CppRGCTXDataType)3, 17270 },
	{ (Il2CppRGCTXDataType)3, 21867 },
	{ (Il2CppRGCTXDataType)3, 21865 },
	{ (Il2CppRGCTXDataType)2, 988 },
	{ (Il2CppRGCTXDataType)3, 45 },
	{ (Il2CppRGCTXDataType)3, 46 },
	{ (Il2CppRGCTXDataType)2, 3402 },
	{ (Il2CppRGCTXDataType)3, 17268 },
	{ (Il2CppRGCTXDataType)3, 21862 },
	{ (Il2CppRGCTXDataType)3, 21861 },
	{ (Il2CppRGCTXDataType)3, 25028 },
	{ (Il2CppRGCTXDataType)3, 25032 },
	{ (Il2CppRGCTXDataType)2, 989 },
	{ (Il2CppRGCTXDataType)3, 49 },
	{ (Il2CppRGCTXDataType)3, 50 },
	{ (Il2CppRGCTXDataType)2, 3400 },
	{ (Il2CppRGCTXDataType)3, 17267 },
	{ (Il2CppRGCTXDataType)3, 24479 },
	{ (Il2CppRGCTXDataType)3, 21870 },
	{ (Il2CppRGCTXDataType)2, 990 },
	{ (Il2CppRGCTXDataType)3, 53 },
	{ (Il2CppRGCTXDataType)3, 54 },
	{ (Il2CppRGCTXDataType)2, 3398 },
	{ (Il2CppRGCTXDataType)3, 17266 },
	{ (Il2CppRGCTXDataType)3, 24457 },
	{ (Il2CppRGCTXDataType)3, 21857 },
	{ (Il2CppRGCTXDataType)3, 21882 },
	{ (Il2CppRGCTXDataType)3, 24319 },
	{ (Il2CppRGCTXDataType)3, 24480 },
	{ (Il2CppRGCTXDataType)3, 24520 },
	{ (Il2CppRGCTXDataType)2, 63 },
	{ (Il2CppRGCTXDataType)3, 24489 },
	{ (Il2CppRGCTXDataType)3, 24487 },
	{ (Il2CppRGCTXDataType)2, 716 },
	{ (Il2CppRGCTXDataType)3, 24458 },
	{ (Il2CppRGCTXDataType)3, 24481 },
	{ (Il2CppRGCTXDataType)2, 707 },
	{ (Il2CppRGCTXDataType)3, 24459 },
	{ (Il2CppRGCTXDataType)2, 710 },
	{ (Il2CppRGCTXDataType)2, 712 },
	{ (Il2CppRGCTXDataType)2, 714 },
	{ (Il2CppRGCTXDataType)3, 12575 },
	{ (Il2CppRGCTXDataType)3, 24516 },
	{ (Il2CppRGCTXDataType)3, 25105 },
	{ (Il2CppRGCTXDataType)3, 12578 },
	{ (Il2CppRGCTXDataType)3, 24500 },
	{ (Il2CppRGCTXDataType)3, 12576 },
	{ (Il2CppRGCTXDataType)3, 24474 },
	{ (Il2CppRGCTXDataType)3, 12577 },
	{ (Il2CppRGCTXDataType)3, 24496 },
	{ (Il2CppRGCTXDataType)2, 395 },
	{ (Il2CppRGCTXDataType)3, 21098 },
	{ (Il2CppRGCTXDataType)3, 24934 },
	{ (Il2CppRGCTXDataType)2, 140 },
	{ (Il2CppRGCTXDataType)3, 24795 },
	{ (Il2CppRGCTXDataType)3, 24796 },
	{ (Il2CppRGCTXDataType)3, 9135 },
	{ (Il2CppRGCTXDataType)2, 576 },
	{ (Il2CppRGCTXDataType)3, 9136 },
	{ (Il2CppRGCTXDataType)2, 3545 },
	{ (Il2CppRGCTXDataType)3, 19386 },
	{ (Il2CppRGCTXDataType)2, 663 },
	{ (Il2CppRGCTXDataType)3, 19387 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	864,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	50,
	s_rgctxIndices,
	118,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
