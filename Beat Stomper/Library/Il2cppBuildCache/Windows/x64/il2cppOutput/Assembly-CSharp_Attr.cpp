﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.MinAttribute
struct MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.MinAttribute
struct MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.MinAttribute::min
	float ___min_0;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MinAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * __this, float ___min0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, int32_t ___order1, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void ParticleController_t1321350CE7B8866173FCA8B8C2FE5C1C02B591CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x50\x61\x72\x74\x69\x63\x6C\x65\x20\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72"), NULL);
	}
}
static void ParticleController_t1321350CE7B8866173FCA8B8C2FE5C1C02B591CB_CustomAttributesCacheGenerator_U3CPoolU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParticleController_t1321350CE7B8866173FCA8B8C2FE5C1C02B591CB_CustomAttributesCacheGenerator_ParticleController_get_Pool_m0B06778AC43BA0AEEBCCA2900A6774581F31E4FA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ParticleController_t1321350CE7B8866173FCA8B8C2FE5C1C02B591CB_CustomAttributesCacheGenerator_ParticleController_set_Pool_m358926DCB555DCBF8394FB3B12239690FFB76A1F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PostProcessController_tCE55D91AB09F6CCEECD15317D7D2E3A50625A404_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x50\x6F\x73\x74\x20\x50\x72\x6F\x63\x65\x73\x73\x20\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72"), NULL);
	}
}
static void GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x47\x61\x6D\x65\x20\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
}
static void GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_U3CDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_U3CInPauseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_GameManager_get_Data_m1B7A20844D301ED83EA5355B5B9F96F368A317D7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_GameManager_set_Data_m10FF27828C8F24B6064BEFEB9A77DFD4BA72DE66(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_GameManager_get_InPause_m9B5C419C01769D13A140FE124FEF1A0CBBA89E16(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_GameManager_set_InPause_mF8A3587DFF67477D609E2F31EDC831AF9EB91828(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Bullet_t03F753BFC452ED62E4F21F7475BDC85A461477FB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x42\x75\x6C\x6C\x65\x74"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var), NULL);
	}
}
static void Bullet_t03F753BFC452ED62E4F21F7475BDC85A461477FB_CustomAttributesCacheGenerator_force(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Bullet_t03F753BFC452ED62E4F21F7475BDC85A461477FB_CustomAttributesCacheGenerator_gun(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Cube_tDA5F7EABAB6759295837377645468CACD038E785_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x43\x75\x62\x65"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var), NULL);
	}
}
static void Gun_tD5524FEE60C64BD8012CCA408F20D54E23D7BD7B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x47\x75\x6E"), NULL);
	}
}
static void Gun_tD5524FEE60C64BD8012CCA408F20D54E23D7BD7B_CustomAttributesCacheGenerator_U3CShootingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Gun_tD5524FEE60C64BD8012CCA408F20D54E23D7BD7B_CustomAttributesCacheGenerator_Gun_get_Shooting_mBADD5CED17DC9B9D31EE0353C4704F57F58C9EA0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Gun_tD5524FEE60C64BD8012CCA408F20D54E23D7BD7B_CustomAttributesCacheGenerator_Gun_set_Shooting_m0BD4A2DC332628D534B55DF5C302EC80B227765D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputController_t8DCE042BFEE3216C69A47E8CF5D2AD7BE61FC477_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x49\x6E\x70\x75\x74\x20\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72"), NULL);
	}
}
static void LevelButton_t594042553BBB302EEAA6341F2970689FF9B4748F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x4C\x65\x76\x65\x6C\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void LevelButton_t594042553BBB302EEAA6341F2970689FF9B4748F_CustomAttributesCacheGenerator_levelData(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x72\x61\x6D\x65\x74\x65\x72\x73"), NULL);
	}
}
static void VRButton_t855BDA83829A1E12CF55166756ADB4E0BC8A8AF7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x56\x52\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void VRButton_t855BDA83829A1E12CF55166756ADB4E0BC8A8AF7_CustomAttributesCacheGenerator_OnEnter(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x4C\x65\x76\x65\x6C\x20\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x4C\x65\x76\x65\x6C\x20\x44\x61\x74\x61"), NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_clip(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x73\x69\x63"), NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_correctRed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x69\x6E\x74\x73"), NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_forceRed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x57\x65\x61\x70\x6F\x6E\x73"), NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_impactsRed(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 1.0f, NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_impactsBlue(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 1.0f, NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x75\x62\x65\x73"), NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_delay(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[1];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 0.0f, NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_minAmount(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 0.0f, NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_maxAmount(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 0.0f, NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_OnLoad(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_U3CHighscoreU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_LevelData_get_Highscore_m2572FFE3A1F87CF7E6385159C9E0D5C161B9E73D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_LevelData_set_Highscore_m4109F0FD95BBEEC422595FCB902CCD18B4AF6516(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MusicController_tEB578E45F93A5B0DDC83BF784DDC31E03814123B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x4D\x75\x73\x69\x63\x20\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72"), NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_tA1B456F005FDB7B7B948677C799572E20B4C318E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScoreController_t24C0514AB4B209C5BB043287493DE3EE9FA006C2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x53\x63\x6F\x72\x65\x20\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72"), NULL);
	}
}
static void TimeController_t9377123311BA6E26BA04D94350DEDBADC6224557_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x54\x69\x6D\x65\x20\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72"), NULL);
	}
}
static void TimeController_t9377123311BA6E26BA04D94350DEDBADC6224557_CustomAttributesCacheGenerator_U3CTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimeController_t9377123311BA6E26BA04D94350DEDBADC6224557_CustomAttributesCacheGenerator_TimeController_get_Time_mADF42903A4A10FC3BCF2C091D9B9B7206210E99F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimeController_t9377123311BA6E26BA04D94350DEDBADC6224557_CustomAttributesCacheGenerator_TimeController_set_Time_mEFD6F7166CF29EE301EF91611B3CFD4F0621AFFD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Spawner_t33CA370368AF9D12D62931126F248AF6F1FD73D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x61\x74\x20\x53\x74\x6F\x6D\x70\x65\x72\x2F\x53\x70\x61\x77\x6E\x65\x72"), NULL);
	}
}
static void Spawner_t33CA370368AF9D12D62931126F248AF6F1FD73D4_CustomAttributesCacheGenerator_Spawner_SpawnCoroutine_m549896FE8F0AD5E66BAB22A71859C5AA2247FA06(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_0_0_0_var), NULL);
	}
}
static void Spawner_t33CA370368AF9D12D62931126F248AF6F1FD73D4_CustomAttributesCacheGenerator_Spawner_DestroyCoroutine_m6300439302DA4A94603DB451CD02278474719315(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_0_0_0_var), NULL);
	}
}
static void U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator_U3CSpawnCoroutineU3Ed__8__ctor_mFD1313F3B32AB15AB720B852BC14652BED23B696(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator_U3CSpawnCoroutineU3Ed__8_System_IDisposable_Dispose_m0BB4E8AAEB33F52178252D89D5FBE31D4BB5FEBD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator_U3CSpawnCoroutineU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mADB539B627C96166A7BA5D42F374E9D2D5524F01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator_U3CSpawnCoroutineU3Ed__8_System_Collections_IEnumerator_Reset_m8BC70F40D5205DA9353062F93CBC3ABD94A7847E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator_U3CSpawnCoroutineU3Ed__8_System_Collections_IEnumerator_get_Current_m4FC7E752EC618871E9FDD1D27472408107BB2BA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator_U3CDestroyCoroutineU3Ed__9__ctor_m57FF4656A8EB51BC48F56B2FC64168D12A859A14(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator_U3CDestroyCoroutineU3Ed__9_System_IDisposable_Dispose_m6B5A6979C4B1ACCDFBC8D2E62078CD5475E04D17(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator_U3CDestroyCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD98FDE193B09AB90ECDCEB68BCAC51245E244437(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator_U3CDestroyCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m581161E85D7AF70C0E9FAACF3662C90C27D64497(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator_U3CDestroyCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mEBFEDA69992E0957E47A514D7843E67440004F28(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BaseObject_t2E7E8DF7B6AB09B998D7B321873C12ABF90265CC_CustomAttributesCacheGenerator_OnEnableObject(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BaseObject_t2E7E8DF7B6AB09B998D7B321873C12ABF90265CC_CustomAttributesCacheGenerator_OnDisableObject(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BaseObject_t2E7E8DF7B6AB09B998D7B321873C12ABF90265CC_CustomAttributesCacheGenerator_OnDestroyObject(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BaseObject_t2E7E8DF7B6AB09B998D7B321873C12ABF90265CC_CustomAttributesCacheGenerator_BaseObject_StartCoroutines_mBCE8DB63DA71A918AEC8B7699C5DF55CC03BBFB5____coroutines0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void BaseObject_t2E7E8DF7B6AB09B998D7B321873C12ABF90265CC_CustomAttributesCacheGenerator_BaseObject_StopCoroutines_m8CC10400B83317C0C06FEB3C7C9A07835E9E8C6B____coroutines0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_U3CCollider3DsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_U3CCollider2DsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_CollisionShape_get_Collider3Ds_m3B1665EFB9A0842AA9165F01D75180104EB8C5BE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_CollisionShape_set_Collider3Ds_m1AFDAE818DEE62300CC1B3CDC7DFB3BED593ABF4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_CollisionShape_get_Collider2Ds_mAE9734F5A82AD0A329CE26024BF9933AD710BA6A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_CollisionShape_set_Collider2Ds_mF8CEBB4D0E430D3CF05648F0AB850A693B5DAD08(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CBlackU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkBlueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkGreenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkAquaU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkRedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkPurpleU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CGoldU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CGrayU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkGrayU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CBlueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CGreenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CAquaU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CRedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CLightPurpleU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CYellowU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CWhiteU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Black_mAA43214B64EB771E63E91EE7E9599E2E0A11FC80(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Black_m9E303B4234A56F5CD0197FF9897B983560774886(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkBlue_m8DA4C1F9CBC8876B06B560F692F0831F6345DAEE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkBlue_mBAC57C2DC67AC8B0347BDB973978785F1678EC14(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkGreen_mE6CDB2C12ED02884C481D3BAF1F72BAC600488BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkGreen_mD5AA93D92E07A08BE6E682548086EB0820B86FB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkAqua_m4459622E91F8B377D8D5B05D8EFEB3AE9BDFA07C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkAqua_mE14F05D00B594DAA31F6C81B46898A7DD8638299(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkRed_m16C2014D3A2802DC6DD092DED66B4CBAAFAFD168(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkRed_mB1216D5C094AEB9BD4F8B12CFFCFE6742B73DA1C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkPurple_m8C8C52E33789C5ADF3BEA2194F801B9582BF569B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkPurple_mF9F855C820B992667C9F465994F2E53E3B27EE85(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Gold_m746E9979E1BA38E14C359340DB2C31DECD25830C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Gold_m55DF49C197C4B7CE5169F28E327E8EBCCCD94DB8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Gray_mF8233FB0AA5C8525006ECFC4244624DA321EB2DE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Gray_m3D5F0D102AC9B1EE88DE051F78CE60D11F8D31A2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkGray_m13B0103AEC08C4A8877BC17C1221EA2D347516BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkGray_mD084F5B587B5EDA7A9787747E78705EC4C2DA8AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Blue_m616240B6A1145A2B771AC140E5C18C530A83DB8F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Blue_m9F4079D5C6866780DCAF058D8853DC5EA7C59829(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Green_m17BAC4AE541DEB4D615F8DEA46DA076E06875CE6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Green_mA92664CE44C2759E464DEA4006F1FC05ED0F7701(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Aqua_m892EB4654255457958BB0B3048B20944844D93A9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Aqua_m0BEB1267F6AAA73B2131D10894F9204DF1F6AC86(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Red_mFFD3D6960EADDC9B5C6F040C34EDBCEE4F794B46(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Red_m25C2338D066C18BA0D592069DD25349B753D3A1B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_LightPurple_mE52D43E2B72D28197B362D188A81BE9D2DADE22A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_LightPurple_m225FBECE0B2B2EE07B94DF923B764495B33675E7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Yellow_m0ECD8AE27E256F35C20B31606FE4A8014A50435E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Yellow_mB22D624E1720AF8E55B12A07535B36FF12645B40(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_White_mF9CC4AE9901408F590F58BF6D909A81DF4C1C1AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_White_mA6F60FAC4252EB2DB23A4C64D4EF9303EFD286EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CustomEvent_t5B35408F28C7A79D4ACF840110EF0173112F3E91_CustomAttributesCacheGenerator_Event(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CustomEvent_1_t4EEDFA083CB1C31478A549CDA764DE151FDDD112_CustomAttributesCacheGenerator_Event(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CustomEvent_2_tB9D2FC3CCE07F79933DFE0A2DA47DE2B13BBBEB2_CustomAttributesCacheGenerator_Event(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CustomEvent_3_t9A49C44D538F17341B1A83FC9EE42669C5BE0761_CustomAttributesCacheGenerator_Event(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimatorExtensions_t92B2B45FC1A8E51D177B9E36134F4E64949804DF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AnimatorExtensions_t92B2B45FC1A8E51D177B9E36134F4E64949804DF_CustomAttributesCacheGenerator_AnimatorExtensions_HasParameter_m65C2A853A157D11E89D30CA699DF352245CA502D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_tBFCF0F4DDEB27B1ABFAF894AF738C8D6F3887023_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Shuffle_mA14E8F7784445218B0D50F0B1A0AF2DB48FE6FF3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Random_m60D6955470B9DD6FB35BB678C1EEBC7D4AC9B339(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Add_mF283468779923CF5A38689E03D89F7CC91F9F346(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Add_mD0079738AB79BEE34A4B975850F74693F7F4319A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Insert_m534846A9101A4F536A0387A8F987D3CA89EB60C7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Insert_mC1A161A6D5844A4594D1D5820ABAC8F0CF3891CD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Fusion_m8DCC32AABE8CBCC37FAAB9CF60395638B652C07C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Get_m0B05DFF32B4017A1CED013A30B7B595EFB32FC8F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_RemoveAt_mF370866441AE10765F61DB805DAAAAAA75A25F2E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Remove_m5F45260BA2F1B80FCF7BBE9A24764B0DAC2D1603(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Remove_m49F6441C0BE6CD8F5FE85A7E52B60CBF2F2D8C9E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Remove_m39D0B84C66460E3C0FF4B79FF23EAD4E7EEC3055(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_FindAll_m8F4FA9EC9EDF4E1F20ABB88E88E8A754857A6E97(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_FindAll_m6F1DA872BADCAD84F6E43DF230917B1B2090DD92(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_FindAll_m5ACBEB97B5A4BCB89A561F3B5E85BFCFBF2C13A7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_First_m8B9A2522669365FD7AECA1473602C5C9D68EFE9F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Last_m1E3C4C6F29316F57E3960179CB70422B81D8A2BD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Find_m9F13B38E8CE9C3B3C92C05939F073ABBBE7580CC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Find_mFFAB56812B612DCC241BD058B19C52037A32BD4E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Maximum_mC9AF1E12D99FB1704B2E28ED6F44ECBCD8B8C578(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Minimum_m30DA7B04AF45F7995F87EC801AA040531F1F526C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Index_m226898DBDEAA4EA9CB04C44BF1BCD0EF9DBCB178(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Index_m713BD5160380271C76860C18A541C8F2A6771E8E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Contains_mC735778C64AE14948C6B89EBF82BE6FC70AB06A5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Contains_m3845D3428047503C269BD91DAE6292126C79ED58(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Each_m9118C7E1F1DD142B92653226908C72A78F801276(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Sort_mB9456DFF0945D3B7AA26489EC10F8AA77E99BF85(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Reverse_m3C4C012A50C53098AF290B70A0F80857CC535B3A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Swap_m4AA0F5D4D298ABB291E437E226F4C847735E3B25(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Swap_m94D017C3BCFAF02BA73F2FD0EED60B2BCECFF26F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_InBounds_m1533CBE553FD2EAAD8DE422ADC50627FFACA2C65(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_IsNull_m24145C25BAB281FFB0159AEEABDA43EDFFAFEB65(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_IsNull_mA2AD6F3048466572F7AA2266F2427E5C40D74D7C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_IsEmpty_m66C48A0FBAEEF60C759AF9B2D604F224A704C840(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_IsNullOrEmpty_mC6C8AAD6031CFEA93BC012365F9DBE47C5CAE862(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_1_tE9DDC0D8DB88BFB6C7158AB4A6C3017263E754DB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_1_tA394AF4946184D2047C3EBC69C5944F95A75535D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_1_tC99E4F3D281EBE5CE28DDCA379E11E5E8EB0843E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_1_t0A6B89C40011BC1D309B8DF388FDD33A8671B041_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_1_t111828C5E5D6576384977A84D13DE39135310775_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_1_t3174C326419FEFC31DE9BF72CC0AD50290714DD5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_1_t4141D646C6435C4695063A8AA492C67E99133F8D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_1_t16BA9A85E97E3452774BFB907BC1DB6182396BBB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_Set_m6AFA22FCF6B5EC500255C9FEEE6935D9E4021BD7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetR_mFFA620CE382CA0CA7BB52C3ABD3CD737E5EFB6E2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetR_m09BA6159DEDCCBE2DBEAB9C49F22CA6007323C63(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetG_mBB4DF67245D4EB6913B2D3E5D852969D83A16498(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetG_m8DF4C4FC264EFDF9FCF6F714AE3B5ECD98E9BF0D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetB_m84DBE0E438C84E3D584E97C58B969A8979DA5DD9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetB_m9A58A226B85B62796E7A68CF55DFCC9C2BBA2993(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetA_mA169AE6EED6A543E3ABD2A113EF5308D4BC1287F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetA_m963C90656799BD83F9FE4C1FAAB5A3FA9DAE1608(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_ToHex_m59386FA193D9DD3507810E2C7AFE2F712D5C2290(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FloatExtensions_t11F738799DFB051D1DFE86130BF864B2E07D8BE9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FloatExtensions_t11F738799DFB051D1DFE86130BF864B2E07D8BE9_CustomAttributesCacheGenerator_FloatExtensions_Abs_mC78DD95AA8FAA3082D8C41884BDD06D5FA9ABFBF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FloatExtensions_t11F738799DFB051D1DFE86130BF864B2E07D8BE9_CustomAttributesCacheGenerator_FloatExtensions_IsBetween_mC6515CC784721FCA19FE9D96C18637B07F3B0AD3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FloatExtensions_t11F738799DFB051D1DFE86130BF864B2E07D8BE9_CustomAttributesCacheGenerator_FloatExtensions_IsOut_m2EB229AB49E1964E39BFC1D5A29275493E142A75(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FloatExtensions_t11F738799DFB051D1DFE86130BF864B2E07D8BE9_CustomAttributesCacheGenerator_FloatExtensions_Lerp_m4FFD0C6736D360C71851DEC02866847F925BE1D0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_0_0_0_var), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator_U3CLerpU3Ed__3__ctor_m17137A1ECDE2DC1B5E0B05AC6B12D70873286ED9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator_U3CLerpU3Ed__3_System_IDisposable_Dispose_m216C87FD0A759BDA982FC212D1E1551A499A8C4C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator_U3CLerpU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16B7E50FF3A9EF14AFAC9E1E0DC2CF5F1B9DB67A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator_U3CLerpU3Ed__3_System_Collections_IEnumerator_Reset_mC4E3EC46B4A0646C6737E4924FCDADD09FEE512E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator_U3CLerpU3Ed__3_System_Collections_IEnumerator_get_Current_m4BB5AD0EB5DE1B3306ECFF7DC883012CCF9DE601(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetR_mFAEBA61A20E6428F999A17E03AF36B2503A2C431(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetR_m629E89B98E610E8CB83CF42E6367B7D3A14A6A69(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetG_m382DA12A44594B14636CEADB0818D97BE178936B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetG_mD5DE779F22269932C230DA10B6E58E160887C71E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetB_m988B7B9526387E85ED2B17DA538E59E90091ADEA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetB_m49026135CCEAD64A953F960D16CC47AFEA57D973(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetA_m70F77E629B8B45D00ABF861A762F2BA764880F67(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetA_mAA23378BAB12FA75FF36348E3C2742CB8FBE263A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetColor_m5E5CCF0BA62A2AEB34DCA633FD9B4E63A4FB1E61(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetColor_mBEE20DCF91C9119E14EADCA21171A4690B603412(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IntExtensions_t2AB6759A9C19F84E98B0DEC5AE664D1DEC2B0AE2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IntExtensions_t2AB6759A9C19F84E98B0DEC5AE664D1DEC2B0AE2_CustomAttributesCacheGenerator_IntExtensions_Abs_m09093FE68577FEDF3E9026C4D1A27B3CCEAA3193(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IntExtensions_t2AB6759A9C19F84E98B0DEC5AE664D1DEC2B0AE2_CustomAttributesCacheGenerator_IntExtensions_IsBetween_mC4EDD39F5B7C8CC2ED88735414F86E9FA009A509(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IntExtensions_t2AB6759A9C19F84E98B0DEC5AE664D1DEC2B0AE2_CustomAttributesCacheGenerator_IntExtensions_IsOut_m5C3BBD203A78F7FFA3B5B0C3C569CFE888A67C21(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListExtensions_t0055BC97A88F83F00F9F9026C5ECFAC40F6F0386_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListExtensions_t0055BC97A88F83F00F9F9026C5ECFAC40F6F0386_CustomAttributesCacheGenerator_ListExtensions_Shuffle_m42B1A28752BEA487C5B409C0591671DC5D6853AB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListExtensions_t0055BC97A88F83F00F9F9026C5ECFAC40F6F0386_CustomAttributesCacheGenerator_ListExtensions_Random_m21815B793D9B14F272FC058BF2349E1BA802D725(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListExtensions_t0055BC97A88F83F00F9F9026C5ECFAC40F6F0386_CustomAttributesCacheGenerator_ListExtensions_First_m98FB80BB01DF1AAEAD047BFDCFD5D437EDE76C54(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListExtensions_t0055BC97A88F83F00F9F9026C5ECFAC40F6F0386_CustomAttributesCacheGenerator_ListExtensions_Last_m2C6AF04A05A963E5F83E3D0854A82AEAF963969F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MonoBehaviourExtensions_t7EF7770B6CE16EE2F83ED2EA1627BB0C8DBECD25_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MonoBehaviourExtensions_t7EF7770B6CE16EE2F83ED2EA1627BB0C8DBECD25_CustomAttributesCacheGenerator_MonoBehaviourExtensions_StartCoroutines_mDCD7FE5F426A916EBB16514473D000A7FDF5DC91(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MonoBehaviourExtensions_t7EF7770B6CE16EE2F83ED2EA1627BB0C8DBECD25_CustomAttributesCacheGenerator_MonoBehaviourExtensions_StartCoroutines_mDCD7FE5F426A916EBB16514473D000A7FDF5DC91____coroutines1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void MonoBehaviourExtensions_t7EF7770B6CE16EE2F83ED2EA1627BB0C8DBECD25_CustomAttributesCacheGenerator_MonoBehaviourExtensions_StopCoroutines_mB4D9D1AF8DBE31F6B589A8CFB44D9993B14054E7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void MonoBehaviourExtensions_t7EF7770B6CE16EE2F83ED2EA1627BB0C8DBECD25_CustomAttributesCacheGenerator_MonoBehaviourExtensions_StopCoroutines_mB4D9D1AF8DBE31F6B589A8CFB44D9993B14054E7____coroutines1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_tBDE4A152D6E676F268E9C01EF4EB07F14C11E0D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t860599246BD98A8F8414D681A7233BA1CC9BA6D5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_Log_mE2360D17840FEC4124F24EA2B46D2FFB0838D5A9____msg3(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_Log_m61CF3C51D0F65245B5E8D769788F35EE3339022B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_Log_m61CF3C51D0F65245B5E8D769788F35EE3339022B____msg1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogNotification_m0968F3F09B27243BA90F851446E9D978A2031EDD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogNotification_m0968F3F09B27243BA90F851446E9D978A2031EDD____msg1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogSuccess_mDE4CFC8CEFB0AF4619AC6871A70F09E8FE2F19C8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogSuccess_mDE4CFC8CEFB0AF4619AC6871A70F09E8FE2F19C8____msg1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogWarning_mDB995A3CE3662832CD9097225D442C73A6FF2D4D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogWarning_mDB995A3CE3662832CD9097225D442C73A6FF2D4D____msg1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogError_m953CE7BACA44BA1932BF876D8B1ADC9612450256(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogError_m953CE7BACA44BA1932BF876D8B1ADC9612450256____msg1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Zero_m0F12B09FBEB3B559E1D64904A9121BF1B38ACF23(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Up_mBB3A995893817FEBD828D45099EDFC5A84BBEF5D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Up_mC52895A6ACD4B3F51662DB8C74F910B1C0CC90B8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Up_m06E359EA967177DD74342E90C37DC889F6F83F00(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Up90_mABB571A0047C0DAE30CCC9BB65A52CB2815F8A9C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Up180_mE140FDDE5BE7988867B33A14E09B28D74EA89A9F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Down_m153776DFF67216F955E7138922B3D4146382320E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Down_m90995D193F285DB9A33D12E5BB909990F3A7FD6E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Down_mEB06E21CAB8262A461537A80F8F98992C94F510F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Down90_m0A7CFA1EFE1E1DE498622A5B60387EBB13509B1F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Down180_m21AC9C0569C619D1EA8F56B7FAC4C4D0DEC5C4C0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Left_m947B5E58A6C2DFCF74DD4A8756424E221C66966D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Left_mDA42E54985429B0F3E23DC9BDF3862B3A0DF41FD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Left_m5C2BBF623267EB0C3EB1240265BC924DF4C35EB5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Left90_mBCEC0324C5BE378CCF01882DB9C49E3C88D7C453(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Left180_m10F0F71ABE7471EE31D93784997104EE94F6C43B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Right_m776B9F0569D3F2244DD5A97A4E89DF3AB16EF818(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Right_m9453E79E22C616CB1986E5C1EA0D039AEAEF6C95(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Right_m14FF1F9324335431168EF73FF6041792A6EA04DF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Right90_m9BD36C19ED4067966DE28AD70E61D36681B2BFE4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Right180_m405E1E80C2FE3F6C2E7942AB09D4B7D85151825C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Forward_mDCE957BA188393041286E10CE0FE787BD523BB88(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Forward_m7000615890CAEC4AC45C91A66A68BD52BBA39BC4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Forward_m65940725192A7DA0D955C067F3BFEE3E6DA86B90(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Forward90_m80D0BF85B5BB5CF21A19B54D20E554C6B34BDD67(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Forward180_m199E6031F53F23DE333755BD198D81DAC664D883(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Backward_m52DF472DFAAFDE4E506D3A7BB06C9E75D3299C73(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Backward_m70BA954DD7F1A36CA375FDCDBF0D441AF5D0880B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Backward_m24D1D22CDA1132F002B0CF76948A15ED2C5A5F58(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Backward90_mE14A87E0BEA5D911553B940C41D7E599759B7832(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Backward180_m6FB603BF64C76C05CCE51CEC35248AAA3D18B3CD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Add_m4CAA4D499323047A559C20D57473B45815F0E648(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Add_m336CEF5746B11CFA9D83A148A0DE496862D7ECCB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Add_m7084BA1762C4F4381CF2FCF9635E888E1F498D9C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Add_mE46FCEB42C92B5163F71B313073F2E814BF0E50D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Add_mF713C20D639724911F419DEC5DE3EEB0FD18AACA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Subtract_m4CF2B58CBBCE57D0DBB4AC1F1D5F777888657F04(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Subtract_m2B59E0F86BD21F529B10BB208801F15F424484A8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Subtract_m2C3BAB564A9569BE9BB425835FFB040873AFB8C3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Subtract_m4F04ED216472FDD481FA50F2DE5B53808BC31365(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Subtract_mB6942626E492BEB8FE232C43F6D497D96FFF01E9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Multiply_m67D9D8633883D8E551D25EB7269C46FF725782F7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Multiply_mAA96F55CF284FD2C99FD1035CC1CCE8673E662BD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Multiply_m9227C99233A1B254EFBFA0117E89363C1C0A0BE8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Multiply_mB7D0A683328EB6D69EAD60E2095E48AFBFACAB27(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Multiply_m8E8913D8912390CE6C9EDE0E562AC4FF1E7AE2E6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Divide_m5EFFEB89FB0B5889C6AC64A4326A492F1A02D02C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Divide_m9EA4388DBF3ABE2DFA8ED007A4F6331228410D36(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Divide_m357BD1B27735729427DA56C846D3019977D260F2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Divide_m07FA50A7EBDF4523A9309D15094C76030A78D215(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Divide_mE439CBA5A34972F59CEF509C0490D8C7C9C5309E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Rigidbody2DExtensions_t73F71A7890A5A39EE2BA80FB701ED5967B7825C7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Rigidbody2DExtensions_t73F71A7890A5A39EE2BA80FB701ED5967B7825C7_CustomAttributesCacheGenerator_Rigidbody2DExtensions_AddExplosionForce_m8E9BDAD79B943D7B97E0FC5EE0EE5AD0599454DE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetR_m8BFF8AFDC263CA0BC4A40FC5D06B0A32E1E76A69(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetR_m6EEF8BA1DE7852327357E5A7A721337679DA62EF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetG_mB5A0183B6B695701304581A15FB9ECCB39CBFA8B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetG_mAC079782001B1E6832445F6F80514071D8901385(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetB_m9284EF24294550B3ED66698DC6DEA83EEC5B182E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetB_m757487CD1F97AA94AAB9BCF75CAC56833504184A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetA_mC94B3648CF295E68D03A4179CA232D1A2F06EC20(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetA_m54BAD889A436F0F488D6F45B573F1FBF94916770(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetColor_m7CD01B62F84106674BA6973641776B6CD3A6A7FA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetColor_m5C7E8A42F030A1A8A564332F06936ED007048C95(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_Divide_m2BCC8258257E8616F53F6DA9B04E10FE1936641D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_Color_mB8C917AEDFC621F07E4ED594A654FD6A40A7ED11(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_ToColor_mFE27F82901872E6F765BC8EDC12B048CC0DD1B74(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_ToEnum_mA06CE7F87E469E941591617556D41DABE2ADC755(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_IsHex_mDDC2251E712409026DB88D83386F4B84A26D1022(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_IsAlpha_m74204F1AA74D46F02EA8E89C5C00B403EE6D30F7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_IsNumeric_mFF043632B4F5E1742BE82FC3B006578C92B0E04A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_IsAlphanumeric_m901B743AF3B9C962E9C4453323D1E865F752B66D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_IsValidInput_m88CD9E58498F3640C2DFC0D451C6F5DAEEC39EDB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetR_mB4B2957AC500366941B17B7568231D351317482B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetR_m94BBFE8352782D5DD510CBFE8BF650B31F7AA115(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetG_m2039EFA377D9DF24CA2C824EC1185737E8E92DCD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetG_m96C93CAA18EF4F9ED87F9617D2EE1453B1C014F6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetB_mE795683BBB5EC70F256F18F1622BF46966D49690(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetB_m059A5D2AF784EE97EC7B7E52F7CCFFFBAE403E0E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetA_m291C7CDB9C0C7C0AF7F7B2B009E7D68EA1EECC5C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetA_mE8D0A7075CD7AA9D35E37F50AF49BF1DA9443ED4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetColor_m37619C9062B93B5A410F9336C050FBCFE614670A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetColor_m7553DCBF072EBFC73BEF0EDD99102742A475F0E0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetR_m3C5E804D9E55689CB388ECDEEB0084E1474BA8DA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetR_m1EADCE814318C3D20603DFBAC59C2A7FFAF7E8BB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetG_m83D9666D54A195A448B8403A01224410D1000F6C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetG_mC90BD24E6A8221AE2369CD5AF190FA771DA10C78(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetB_m48F032B423EBAF11900517D6C1E1525D2ED1DCB3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetB_mBC36FDFBD6A12DCB5FEFC72DEECF90D16A65A49A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetA_m85362D6B891357D09337508D77EA2FEA3D219566(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetA_m40873B329DFE4CCB63BE5BC83FEE7136DAC64C9D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetColor_m0C70BA3F433326639E054B20FD170A50D91D0A50(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetColor_m71E41A2A11BF53243929659738C52ADA9F1DC9D3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TransformExtensions_tF7DDA7C52B354E79289FF96C7F8B2CFEFDA41FA3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TransformExtensions_tF7DDA7C52B354E79289FF96C7F8B2CFEFDA41FA3_CustomAttributesCacheGenerator_TransformExtensions_OrbitAround_m427AFBBED01A222959A499573BF3416EEB0BBA8C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_SetX_m628D5C7426C250CF1AA42EB7B4D5CD4C2AEF6FD2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_SetY_m4E8FBC009EFF33EF0A41B94012A34C4CBE0A2FB9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Add_mF90553B085B216B8063C319E7F5C8204150ED6A1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Add_m762B3B8DF1C23DE0AA2C3560276B2A8B41B63D4B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_AddX_m758A794B218B5C423592D51E268B663BBCC11962(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_AddY_mBB7E924E34BC6B1692D46C6056AFFECA78C72D1A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Multiply_mF1DFE84C51DCB8461D57DFEA89F8271451231BBE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_MultiplyX_mBDA6E3039AB952E5CBBA6392DCA76DBB53AD320A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_MultiplyY_m5D71509EA53C9E5E1EB99824003A55F582038DCE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipX_m207B2ECA3AFA9705787AF81675F193DDFEED1274(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipX_m9B3AD699449ACAEBC20A9FAF8E18C10D86D7AC8B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipY_m3D57D6A2C8E86B7932C2E2E31818ECDC8E7394E2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipY_mE5CE03C2A7D315D3A99DD3A328A4AD31437FBA22(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Clamp_m45CA9923A6E901E9382DDC7FE420214A9BFFD02B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Clamp01_mAC727F3DC44C853FDA19B266270580714034A817(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Abs_mD3F2F9E177B6FB04F7D6FF4C1C5AE863A9717B5D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_IsNaN_m2CEE8D61FDD43F32C53233AA95A589ED3E033EE4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_IsBetween_m12B3F6E076D165F71A3EA8653FF0D395D939065F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Random_m41E2C59D0F97940AAF95FFBFB970BD4D4F609284(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Random_m75CA36F81F110C038800A22EAFDE25668ABA9B25(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_SetX_m595EC12BB39254EA57783C91AD3ADCB4A653C8B0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_SetY_m0A7A88B104C6A031184C0EC70E9BB8834E818C6E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Add_m2C033D4117520A9FEE90D070A9102DD3C70BE2E6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Add_m7FA63B8E9AF73DA9D6F1839128884FB5C83A2DEA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_AddX_m0DB873F80F8A413EE690783034D438225AB3D164(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_AddY_m27B53E8C4E342CDC2365F88B86F21E7DE407D9F4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Multiply_mAB1021AA7564D90AF150F1F171BAD924F3B38450(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_MultiplyX_m972CCB60D21C531E0021216D2E4E147A7EA7C0A6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_MultiplyY_mF5AE7390ADC21BD6BAA2F0307910342360EE53ED(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipX_m60E7C5F685BB334192C017E7B59F26459A37B4E1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipY_mD2C6E9C5B1C152F266DE5AA51DDB1F299294DAE5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Clamp_mDDF1AEFA36425F97961E262C791A7118CFA080F8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Clamp01_m420330594EDA2001E2A9A5E43542B6C2934CC1DE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Abs_m443A22A719ECBC24B13A8C98DF0F643BB6699F35(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_IsNaN_m3BD979CB285D0FE1B7C92EC7C09A98DE3FEC439A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_IsBetween_m9BDF92254390AE9F6712D1911CFD269800874943(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Random_mC14C459831FE9B2D765229782B770F82DB2DC5D7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Random_mD7F87347B2F86A02C3D17910862F3FF2361A7B44(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetX_m81CF6035CEB02FC9564EC3FDA6F022DAFC315718(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetY_m80B1B14A61DFD9644F2738CFF13D2C0AE4E1B47C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetZ_mA4B15836E624115C548DC4DBC9C231B915425170(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Add_m129731B6B0B7D716826167689EF58602DE19DEB0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Add_m427C7F3B429DB4D3D6885A799BCB22A8BB746D10(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddX_m091C22A33637623414F59CF8E760945DC978FE0A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddY_m3DC1D188941755AA4B0EC34B71E1EB4F9443EE3F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddZ_m167854E93C83055C59F075127BC017ACC4BFCE30(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Multiply_m59A3EB0E5AC45440069BA4638FE5574337F16C24(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyX_m01BB07BBFF31318D2D904375EA6229EC98269AE7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyY_mD8206F4F19AC05F89F814A1A3A1FC066B8FAD3F7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyZ_m4125A5E09C216CAB14745C8B807416602546717B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipX_mE1C6AA3FB735AE73620ACAD1B024CE661DED4950(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipX_m1A831DF6FF2320D87367883EC610D1C0F9ADFFF3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipY_m947BF3D1C40A38EFBA386A04E0AE3D2BBEA34C5F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipY_m8B0B2087D7210C8C7E8478EE593DC76808C60F24(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipZ_mB79D528327D548D16D0EDF4054F0B123CBBE7A53(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipZ_mC50F3E941648FC54AECB6258E6A700F509223729(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Clamp_m560F956CF30672AAEBDE86DAE2151F22C3BA4650(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Clamp01_mA5FB3155EFFD290F0EF094B52B33D1073D6B031B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Abs_mE81EE8A5C7EE28268CF0D1B91AC614564E3F772F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_IsNaN_m8BBB8CB0F0A766C32ECD86496E0C09DF33EFCF6D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_IsBetween_m7AF18A71EECB9DC544FE13BACD8BF22CE82558EC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Random_m4DEBFEDD768D01361BD810FD1C154E142F2FEA3B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetX_m163A6AC84AEE45EF5BC9FAE6C13455CE7742CC6D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetY_m63B70C6B67A6D6B7ACA684FC65DC0D7B7FC7A9D0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetZ_m5EE056686D97AAD916B9C0B85DD362B93F993C51(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Add_mA7D11EB57D79A27310CA41C27F96C8E4351784A6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Add_mB57C1DC5B450AF7750A847B09F1FD340260D8287(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddX_mC7DED0E7E3A689D23932A95CD5E6635344009FEF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddY_m70CF1032AA0D84F1DE1BF94260CB0797023B4E7A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddZ_mF3ADC52DBBE7EA3D772E254573B7F3BB28C4525F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Multiply_m395208DA68CD04BC047C164060D1AD22FBE4AA40(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyX_mCFBFA0C826EE539A6EB978C03C7C3C4A79C68082(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyY_m49A31473D6CD98AA93115BAFAC87F01A5C742071(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyZ_m534D5268445D560788BC0051115F7FDDF7586DCA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipX_m43688C951EC6BD33AB086671DFA5FF4107126D63(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipY_mE1F7250AB3D50229D14DED692C2DB294658043F1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipZ_m82CBD59675BB1FA10165B5BF48472E1F2B213340(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Clamp_mB2DF32F829692E3ADE4E5BD158D2CFF86AFE7D3A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Clamp01_m2EA27A8E453821993BEB3EAB65B067EB8B11409C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Abs_m64AA0B24024192CF433F91B6B6EB9C7A402FDDD6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Random_m1C1CF68F0D0FC1AB9A6B6210ABD7B74CD803628F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_IsNaN_m6487769847AF0456E47459EF5D4EEE557AC23DE9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_IsBetween_mF858EF76A0CE1AEEA83E725B0DE0E53256D609D6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_SetX_m4488FC494CB2AF61DA8CA4B27325D5C6347D2A46(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_SetY_m88F360368C14B924564A67CB65176F828F88E65E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_SetZ_m5B05CBDF245A95D31DF067551B66F7D95CDA821D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_SetW_m58268842AF83B79D88E1ECD3769B6E04CA223956(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Add_m75644FE0B835214634EB48EF8A397874D6040CC1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Add_mB764E997232736FAF86908C91A26F422CF99150B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_AddX_m921E63F49FDA98902D1A3AFB611E42B1699733B0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_AddY_mC95FB316204D4DDEDE5E75E5C3B06E22E4C34C2D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_AddZ_m888AD463EF6716CE6A65360AD29EF75F9664C824(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_AddW_m3B5CBAAD90A8722B6884F299766CD5CDBBCBA98B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Multiply_m28E187B218F792582C81A26C2AA4C2BEED3AAD81(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_MultiplyX_mAA0033CABA21A8A4AC3D5A25F96427DA23981E04(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_MultiplyY_m2F195487328CDFEB4AB2F8CAC2F0528974012832(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_MultiplyZ_m6B49DF8E2A118803DED30A63BF92F00DCD59B9D4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_MultiplyW_m82DD4E43D50E114A94CAF9C07CE2FB82C366A77F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipX_m4FE0F1E12B406F3FD14AF98556E925E81AE7789C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipX_m075997348588562CD28F7D3792725F51CEB40220(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipY_m18F8D19C98CC1A8DFDE8784E33C29DE27D86E9B5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipY_m47B8E95F76846CF7B4FB52E781616DD2099A81BE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipZ_m7EA7F38B981936AED7E919AF0CD5280696E98560(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipZ_m4CCFC1573B94ADC023952CEDF009A143E999204B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipW_m74E8AD6CD37BF06788D3E664C8308BF37BA4F0F3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipW_mBC9BDF2CE1DF86A8EE1FDCAC8EE5707C80636DA8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Clamp_m4D46C45F7D1CD35ECDCD4FAACB54E027772BCF3E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Clamp01_m5D536C744535AA0842C612FB34084D106E758199(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Abs_m4934C92C8D119F43FBE87A923B0D6F69D70FCBFC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_IsNaN_m0D9B34D85906B29320C3A779695A5F3C49545615(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_IsBetween_m7760AE4387A1C698F72BC1EDEEBECF87039470AD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Random_m75E196B5C836C8D8C82CB32CF7A9D8F1BD024D4E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BoxDetector_tF8A6B2368DF1D13BC4985BD5DC88BBF9F2E33FC3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x44\x65\x74\x65\x63\x74\x6F\x72\x2F\x42\x6F\x78\x20\x44\x65\x74\x65\x63\x74\x6F\x72"), 1LL, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x44\x65\x74\x65\x63\x74\x6F\x72\x2F\x43\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x20\x44\x65\x74\x65\x63\x74\x6F\x72"), 0LL, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_U3COnEnterCollisionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_U3COnExitCollisionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_U3COnEnterCollision2DU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_U3COnExitCollision2DU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_get_OnEnterCollision_m552AE5930F972CF920E0767FECEEA6E8E404CCDB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_set_OnEnterCollision_m6F9F5443517CAE3BA616DAC973C338491D720DDA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_get_OnExitCollision_mFCE873DF650709BA935EA417D700D44B124233F7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_set_OnExitCollision_mABEA96E854787882105ECE55D298B4F958EFF49D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_get_OnEnterCollision2D_m65D34D5EE180EDF0087CD7B17B03C5F86BB83627(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_set_OnEnterCollision2D_mB1E65053C541FA44FFD1ACAE3AB159B5755A91D8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_get_OnExitCollision2D_mA070591D65C0365EB7EEB1938FB063D8F4D1E57D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_set_OnExitCollision2D_mAB23E7E7ABB23329F387123FD0F55B8393DE80B1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Detector_t81191080C8995EEB54E78173A1DD31087DD628CF_CustomAttributesCacheGenerator_total(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Detector_t81191080C8995EEB54E78173A1DD31087DD628CF_CustomAttributesCacheGenerator_U3CIsCollisionDetectorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Detector_t81191080C8995EEB54E78173A1DD31087DD628CF_CustomAttributesCacheGenerator_Detector_get_IsCollisionDetector_mF21FA01B1A8AB55B2CC0403AF849AAB2EDB119B8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Detector_t81191080C8995EEB54E78173A1DD31087DD628CF_CustomAttributesCacheGenerator_Detector_U3CDetect3DU3Eb__16_0_m4470C692412E4DB45B299A874A4D6EF37BB71B35(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Detector_t81191080C8995EEB54E78173A1DD31087DD628CF_CustomAttributesCacheGenerator_Detector_U3CDetect2DU3Eb__17_0_mA99BEFCE95AB60BBC004150C2CC3C982C547F218(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RayDetector_t00A4CC4AEB3D3C4202FF78764E02A14E35E5BC95_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x44\x65\x74\x65\x63\x74\x6F\x72\x2F\x52\x61\x79\x20\x44\x65\x74\x65\x63\x74\x6F\x72"), 3LL, NULL);
	}
}
static void RayDetector_t00A4CC4AEB3D3C4202FF78764E02A14E35E5BC95_CustomAttributesCacheGenerator_distance(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 0.0f, NULL);
	}
}
static void SphereDetector_t476728A4ECFB3F6F4D02B2E5DFDBA89945E1F54E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x44\x65\x74\x65\x63\x74\x6F\x72\x2F\x53\x70\x68\x65\x72\x65\x20\x44\x65\x74\x65\x63\x74\x6F\x72"), 2LL, NULL);
	}
}
static void SphereDetector_t476728A4ECFB3F6F4D02B2E5DFDBA89945E1F54E_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 0.0f, NULL);
	}
}
static void ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_U3CDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_U3CNoiseU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_ShakeController_get_Data_m9194007F30FE902F418E3719CC6589BAACBD2A97(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_ShakeController_set_Data_mCC4E4F1BDCA181EAA83C186945435EC73923CF95(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_ShakeController_get_Noise_m6430E01E097318FB62F2392358856AD0436AA177(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_ShakeController_set_Noise_mFB37CA32FC05BAE468506C2ED111B29AED5194BA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShakeData_t5F1C3E32FFD590898D62F2F07AF5F6E7543D5BD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x53\x68\x61\x6B\x65\x20\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x53\x68\x61\x6B\x65\x20\x44\x61\x74\x61"), NULL);
	}
}
static void ShakeData_t5F1C3E32FFD590898D62F2F07AF5F6E7543D5BD8_CustomAttributesCacheGenerator_amplitude(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 5.0f, NULL);
	}
}
static void ShakeData_t5F1C3E32FFD590898D62F2F07AF5F6E7543D5BD8_CustomAttributesCacheGenerator_frequency(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 20.0f, NULL);
	}
}
static void ShakeData_t5F1C3E32FFD590898D62F2F07AF5F6E7543D5BD8_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 5.0f, NULL);
	}
}
static void Shaker_t78A3FC34FC949275ADFCA0480C427A1E9567A1AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x53\x68\x61\x6B\x65\x72"), NULL);
	}
}
static void Shaker_t78A3FC34FC949275ADFCA0480C427A1E9567A1AB_CustomAttributesCacheGenerator_destroy(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Shaker_t78A3FC34FC949275ADFCA0480C427A1E9567A1AB_CustomAttributesCacheGenerator_U3CShakesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Shaker_t78A3FC34FC949275ADFCA0480C427A1E9567A1AB_CustomAttributesCacheGenerator_Shaker_get_Shakes_m16E0A104B3A0951DF9BA6D427289A34165C88736(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Shaker_t78A3FC34FC949275ADFCA0480C427A1E9567A1AB_CustomAttributesCacheGenerator_Shaker_set_Shakes_m69643E7D4F2FEC0A5B164550D74979D12E0D60D1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var), NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_destroy(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_index(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_U3CSoundU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_U3CSourceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_get_Sound_mBA2B474E8303D1A09B3EFB797D4ECC3C0932FC73(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_set_Sound_mAFFB12A928462D41B9316599893546B4D6FC993E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_get_Source_m17A382415CE8079DBE7279043BF410B844F45448(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_set_Source_m6B3FF429CE8B1FEEEE895EC0E499F15E53FE049A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_InCoroutine_m33D2FDA6432F130AC83F2D296F45F31229B72508(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_0_0_0_var), NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_OutCoroutine_m73884E8C7CEFB14569C11DB03BC001D6916673AA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_0_0_0_var), NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_SFXUpdate_m03BAC1FA20CDAF18E9942DA9944BDFFE203C6778(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_0_0_0_var), NULL);
	}
}
static void SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_CurveCoroutine_m9A698305897A7796C9BE34C2CEAD4189984F72EE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_0_0_0_var), NULL);
	}
}
static void U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator_U3CInCoroutineU3Ed__36__ctor_m768A77D62609B5219C8B27E84141983AAD3A3D76(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator_U3CInCoroutineU3Ed__36_System_IDisposable_Dispose_mB6F727542D4FE84C7FA54DB3A654762DF56997E3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator_U3CInCoroutineU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68B293F1F04F174F8AB3D39F3117B329F637258B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator_U3CInCoroutineU3Ed__36_System_Collections_IEnumerator_Reset_m1ABD37031B2464A2450F841151AE1ADF40CE3551(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator_U3CInCoroutineU3Ed__36_System_Collections_IEnumerator_get_Current_m721A47FB34BEB99D558CDB207E02E86FBDBBA28C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator_U3COutCoroutineU3Ed__37__ctor_mBF75D8F91859D02800985B782AD9B9061034A4CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator_U3COutCoroutineU3Ed__37_System_IDisposable_Dispose_mFC1E65CC0E5BED49103F47FA6074CB7CD88B2EFD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator_U3COutCoroutineU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA32CEBA53B1DE20AF3F1994A7936BE75142B8F71(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator_U3COutCoroutineU3Ed__37_System_Collections_IEnumerator_Reset_m7107C4BB8F4BD42DC550CA60ABB427255BAA6F27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator_U3COutCoroutineU3Ed__37_System_Collections_IEnumerator_get_Current_m6A992C1DF0F0D804C810939A396E501E79B35003(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator_U3CSFXUpdateU3Ed__38__ctor_m07428BBF9A91E3E67C36ED99F3FA91529FE4F06F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator_U3CSFXUpdateU3Ed__38_System_IDisposable_Dispose_m9EE4DCFF6221AAE6E63389B625CC72E7BEC38C04(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator_U3CSFXUpdateU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84E0A548F2DB3379A1F64D00FED15D945D35E777(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator_U3CSFXUpdateU3Ed__38_System_Collections_IEnumerator_Reset_m440C938D24AC7FC03EAB463992F1640D4A9E1780(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator_U3CSFXUpdateU3Ed__38_System_Collections_IEnumerator_get_Current_m386B1BBB881C24922403C0C99C403CF1BF7237D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator_U3CCurveCoroutineU3Ed__42__ctor_m7CBE424F8485C6F65BF9B61DC65E567D584FF738(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator_U3CCurveCoroutineU3Ed__42_System_IDisposable_Dispose_mAE910B8021B1C839BDD46C8091E823CCC14A1B49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator_U3CCurveCoroutineU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3D1317D299D2A110D9B57D9017DDA06E96B745D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator_U3CCurveCoroutineU3Ed__42_System_Collections_IEnumerator_Reset_mE505FA3A27B3D11891AC8BF8BB1BBB63FC2627D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator_U3CCurveCoroutineU3Ed__42_System_Collections_IEnumerator_get_Current_m5F69CE3B305929E8E2786EE42A96D602A656CCF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[2];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x53\x6F\x75\x6E\x64\x20\x45\x66\x66\x65\x63\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x53\x6F\x75\x6E\x64\x20\x45\x66\x66\x65\x63\x74"), NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_delayIn(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 60.0f, NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_delayOut(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 60.0f, NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_start(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_end(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_minCycles(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 10.0f, NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_maxCycles(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 10.0f, NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_volumeValue(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_spatialBlend(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_U3CControllerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_SoundEffect_get_Controller_m2A5FF64D3317DA9A1E49055A1B6CB8579591E804(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_SoundEffect_set_Controller_mEE20B5A111058B0F288672C8808E32209332B7F6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass47_0_tC90C0F8AAD49F6826A2DD1C4DA0F949EE7E6A378_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LookAt_t2056805C19591CD18EFE9C1D97AC0421E423B0E6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x4C\x6F\x6F\x6B\x20\x41\x74"), NULL);
	}
}
static void LookAt_t2056805C19591CD18EFE9C1D97AC0421E423B0E6_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 0.0f, NULL);
	}
}
static void MoveTo_t86637DBAAFB56DE66C210CD5D53637611D181EF2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x4D\x6F\x76\x65\x20\x54\x6F"), NULL);
	}
}
static void MoveTo_t86637DBAAFB56DE66C210CD5D53637611D181EF2_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 0.0f, NULL);
	}
}
static void MoveTo_t86637DBAAFB56DE66C210CD5D53637611D181EF2_CustomAttributesCacheGenerator_smoothing(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void RotateTo_t65C9DABDE8AB9EFE4B0A411EA1AD71FC60101174_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x52\x6F\x74\x61\x74\x65\x20\x54\x6F"), NULL);
	}
}
static void RotateTo_t65C9DABDE8AB9EFE4B0A411EA1AD71FC60101174_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 0.0f, NULL);
	}
}
static void Rotator_t20461B33805B1DA59607BA2E4D62BDD14BE47C7A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x52\x6F\x74\x61\x74\x6F\x72"), NULL);
	}
}
static void ScaleTo_tA00A4305801B9D90E277B03E97DC8AB488DF8685_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x53\x63\x61\x6C\x65\x20\x54\x6F"), NULL);
	}
}
static void ScaleTo_tA00A4305801B9D90E277B03E97DC8AB488DF8685_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 0.0f, NULL);
	}
}
static void ScaleTo_tA00A4305801B9D90E277B03E97DC8AB488DF8685_CustomAttributesCacheGenerator_smoothing(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void BoolVariable_tF8C40FAF31124AF9BACD424E8062BB27BD063C8C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x42\x6F\x6F\x6C"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x56\x61\x72\x69\x61\x62\x6C\x65\x73\x2F\x42\x6F\x6F\x6C"), NULL);
	}
}
static void FloatVariable_tEAD6CA0808B9960713AD68B18843A03F275D4A10_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x46\x6C\x6F\x61\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x56\x61\x72\x69\x61\x62\x6C\x65\x73\x2F\x46\x6C\x6F\x61\x74"), NULL);
	}
}
static void GlobalVariable_1_t2AC8186011BD6AF407254E62A334FC3650440CC1_CustomAttributesCacheGenerator_debug(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GlobalVariable_1_t2AC8186011BD6AF407254E62A334FC3650440CC1_CustomAttributesCacheGenerator_reset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GlobalVariable_1_t2AC8186011BD6AF407254E62A334FC3650440CC1_CustomAttributesCacheGenerator_defaultValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GlobalVariable_1_t2AC8186011BD6AF407254E62A334FC3650440CC1_CustomAttributesCacheGenerator_value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GlobalVariable_1_t2AC8186011BD6AF407254E62A334FC3650440CC1_CustomAttributesCacheGenerator_OnValueChanged(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void IntVariable_t50D8E15DAD44564D43630BE92684F4632225D52D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x49\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x56\x61\x72\x69\x61\x62\x6C\x65\x73\x2F\x49\x6E\x74"), NULL);
	}
}
static void StringVariable_t0E61F643B52429829118DFD3049BF2FDBBA6BA8A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x53\x74\x72\x69\x6E\x67"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x56\x61\x72\x69\x61\x62\x6C\x65\x73\x2F\x53\x74\x72\x69\x6E\x67"), NULL);
	}
}
static void Vector2Variable_t707AD3D040FD9E93BF767FCAA5AB9DC9B6E77A29_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x56\x65\x63\x74\x6F\x72\x32"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x56\x61\x72\x69\x61\x62\x6C\x65\x73\x2F\x56\x65\x63\x74\x6F\x72\x32"), NULL);
	}
}
static void Vector3Variable_tE319AA518CC531C8BF839DC1C99ABA0D0FEE46DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x56\x65\x63\x74\x6F\x72\x33"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x56\x61\x72\x69\x61\x62\x6C\x65\x73\x2F\x56\x65\x63\x74\x6F\x72\x33"), NULL);
	}
}
static void ObjectPool_t4437F4C7A1268DAD2CF8731860343C547A5503FA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x50\x61\x74\x74\x65\x72\x6E\x73\x2F\x4F\x62\x6A\x65\x63\x74\x20\x50\x6F\x6F\x6C"), NULL);
	}
}
static void GameEvent_t79C47F45E885DD7612D167DB261BD26140DDAA00_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x47\x61\x6D\x65\x20\x45\x76\x65\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x47\x61\x6D\x65\x20\x45\x76\x65\x6E\x74"), NULL);
	}
}
static void U3CU3Ec_t2DFAD0285E3F436318263D1C386120CF20DE2ED8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Observer_tB60671B12559177E8B736ADCA58E7729EDAF539B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x50\x61\x74\x74\x65\x72\x6E\x73\x2F\x4F\x62\x73\x65\x72\x76\x65\x72"), NULL);
	}
}
static void DontDestroyOnLoad_t51E207C7F768272639B2045C6BA1B788A2CFC3B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x74\x69\x70\x69\x78\x65\x6C\x2F\x50\x61\x74\x74\x65\x72\x6E\x73\x2F\x44\x6F\x6E\x74\x20\x44\x65\x73\x74\x72\x6F\x79\x20\x4F\x6E\x20\x4C\x6F\x61\x64"), NULL);
	}
}
static void Singleton_1_t10C86293B9768076580DF8EC56BFFF52FAE0A94C_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Singleton_1_t10C86293B9768076580DF8EC56BFFF52FAE0A94C_CustomAttributesCacheGenerator_Singleton_1_get_Instance_m9B250D55E1FA4980A38F89EF7215C48BE220A82B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Singleton_1_t10C86293B9768076580DF8EC56BFFF52FAE0A94C_CustomAttributesCacheGenerator_Singleton_1_set_Instance_mCFA1C0D4748D911A560087F21DE713A64BFD57A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[565] = 
{
	ParticleController_t1321350CE7B8866173FCA8B8C2FE5C1C02B591CB_CustomAttributesCacheGenerator,
	PostProcessController_tCE55D91AB09F6CCEECD15317D7D2E3A50625A404_CustomAttributesCacheGenerator,
	GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator,
	Bullet_t03F753BFC452ED62E4F21F7475BDC85A461477FB_CustomAttributesCacheGenerator,
	Cube_tDA5F7EABAB6759295837377645468CACD038E785_CustomAttributesCacheGenerator,
	Gun_tD5524FEE60C64BD8012CCA408F20D54E23D7BD7B_CustomAttributesCacheGenerator,
	InputController_t8DCE042BFEE3216C69A47E8CF5D2AD7BE61FC477_CustomAttributesCacheGenerator,
	LevelButton_t594042553BBB302EEAA6341F2970689FF9B4748F_CustomAttributesCacheGenerator,
	VRButton_t855BDA83829A1E12CF55166756ADB4E0BC8A8AF7_CustomAttributesCacheGenerator,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator,
	MusicController_tEB578E45F93A5B0DDC83BF784DDC31E03814123B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_tA1B456F005FDB7B7B948677C799572E20B4C318E_CustomAttributesCacheGenerator,
	ScoreController_t24C0514AB4B209C5BB043287493DE3EE9FA006C2_CustomAttributesCacheGenerator,
	TimeController_t9377123311BA6E26BA04D94350DEDBADC6224557_CustomAttributesCacheGenerator,
	Spawner_t33CA370368AF9D12D62931126F248AF6F1FD73D4_CustomAttributesCacheGenerator,
	U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator,
	U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator,
	AnimatorExtensions_t92B2B45FC1A8E51D177B9E36134F4E64949804DF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_tBFCF0F4DDEB27B1ABFAF894AF738C8D6F3887023_CustomAttributesCacheGenerator,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_1_tE9DDC0D8DB88BFB6C7158AB4A6C3017263E754DB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_1_tA394AF4946184D2047C3EBC69C5944F95A75535D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_1_tC99E4F3D281EBE5CE28DDCA379E11E5E8EB0843E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_1_t0A6B89C40011BC1D309B8DF388FDD33A8671B041_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_1_t111828C5E5D6576384977A84D13DE39135310775_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_1_t3174C326419FEFC31DE9BF72CC0AD50290714DD5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_1_t4141D646C6435C4695063A8AA492C67E99133F8D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_1_t16BA9A85E97E3452774BFB907BC1DB6182396BBB_CustomAttributesCacheGenerator,
	ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator,
	FloatExtensions_t11F738799DFB051D1DFE86130BF864B2E07D8BE9_CustomAttributesCacheGenerator,
	U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator,
	ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator,
	IntExtensions_t2AB6759A9C19F84E98B0DEC5AE664D1DEC2B0AE2_CustomAttributesCacheGenerator,
	ListExtensions_t0055BC97A88F83F00F9F9026C5ECFAC40F6F0386_CustomAttributesCacheGenerator,
	MonoBehaviourExtensions_t7EF7770B6CE16EE2F83ED2EA1627BB0C8DBECD25_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_tBDE4A152D6E676F268E9C01EF4EB07F14C11E0D4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t860599246BD98A8F8414D681A7233BA1CC9BA6D5_CustomAttributesCacheGenerator,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator,
	Rigidbody2DExtensions_t73F71A7890A5A39EE2BA80FB701ED5967B7825C7_CustomAttributesCacheGenerator,
	SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator,
	StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator,
	TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator,
	TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator,
	TransformExtensions_tF7DDA7C52B354E79289FF96C7F8B2CFEFDA41FA3_CustomAttributesCacheGenerator,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator,
	BoxDetector_tF8A6B2368DF1D13BC4985BD5DC88BBF9F2E33FC3_CustomAttributesCacheGenerator,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator,
	RayDetector_t00A4CC4AEB3D3C4202FF78764E02A14E35E5BC95_CustomAttributesCacheGenerator,
	SphereDetector_t476728A4ECFB3F6F4D02B2E5DFDBA89945E1F54E_CustomAttributesCacheGenerator,
	ShakeData_t5F1C3E32FFD590898D62F2F07AF5F6E7543D5BD8_CustomAttributesCacheGenerator,
	Shaker_t78A3FC34FC949275ADFCA0480C427A1E9567A1AB_CustomAttributesCacheGenerator,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator,
	U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator,
	U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator,
	U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator,
	U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass47_0_tC90C0F8AAD49F6826A2DD1C4DA0F949EE7E6A378_CustomAttributesCacheGenerator,
	LookAt_t2056805C19591CD18EFE9C1D97AC0421E423B0E6_CustomAttributesCacheGenerator,
	MoveTo_t86637DBAAFB56DE66C210CD5D53637611D181EF2_CustomAttributesCacheGenerator,
	RotateTo_t65C9DABDE8AB9EFE4B0A411EA1AD71FC60101174_CustomAttributesCacheGenerator,
	Rotator_t20461B33805B1DA59607BA2E4D62BDD14BE47C7A_CustomAttributesCacheGenerator,
	ScaleTo_tA00A4305801B9D90E277B03E97DC8AB488DF8685_CustomAttributesCacheGenerator,
	BoolVariable_tF8C40FAF31124AF9BACD424E8062BB27BD063C8C_CustomAttributesCacheGenerator,
	FloatVariable_tEAD6CA0808B9960713AD68B18843A03F275D4A10_CustomAttributesCacheGenerator,
	IntVariable_t50D8E15DAD44564D43630BE92684F4632225D52D_CustomAttributesCacheGenerator,
	StringVariable_t0E61F643B52429829118DFD3049BF2FDBBA6BA8A_CustomAttributesCacheGenerator,
	Vector2Variable_t707AD3D040FD9E93BF767FCAA5AB9DC9B6E77A29_CustomAttributesCacheGenerator,
	Vector3Variable_tE319AA518CC531C8BF839DC1C99ABA0D0FEE46DD_CustomAttributesCacheGenerator,
	ObjectPool_t4437F4C7A1268DAD2CF8731860343C547A5503FA_CustomAttributesCacheGenerator,
	GameEvent_t79C47F45E885DD7612D167DB261BD26140DDAA00_CustomAttributesCacheGenerator,
	U3CU3Ec_t2DFAD0285E3F436318263D1C386120CF20DE2ED8_CustomAttributesCacheGenerator,
	Observer_tB60671B12559177E8B736ADCA58E7729EDAF539B_CustomAttributesCacheGenerator,
	DontDestroyOnLoad_t51E207C7F768272639B2045C6BA1B788A2CFC3B2_CustomAttributesCacheGenerator,
	ParticleController_t1321350CE7B8866173FCA8B8C2FE5C1C02B591CB_CustomAttributesCacheGenerator_U3CPoolU3Ek__BackingField,
	GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_U3CDataU3Ek__BackingField,
	GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_U3CInPauseU3Ek__BackingField,
	Bullet_t03F753BFC452ED62E4F21F7475BDC85A461477FB_CustomAttributesCacheGenerator_force,
	Bullet_t03F753BFC452ED62E4F21F7475BDC85A461477FB_CustomAttributesCacheGenerator_gun,
	Gun_tD5524FEE60C64BD8012CCA408F20D54E23D7BD7B_CustomAttributesCacheGenerator_U3CShootingU3Ek__BackingField,
	LevelButton_t594042553BBB302EEAA6341F2970689FF9B4748F_CustomAttributesCacheGenerator_levelData,
	VRButton_t855BDA83829A1E12CF55166756ADB4E0BC8A8AF7_CustomAttributesCacheGenerator_OnEnter,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_clip,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_correctRed,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_forceRed,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_impactsRed,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_impactsBlue,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_speed,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_delay,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_minAmount,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_maxAmount,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_OnLoad,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_U3CHighscoreU3Ek__BackingField,
	TimeController_t9377123311BA6E26BA04D94350DEDBADC6224557_CustomAttributesCacheGenerator_U3CTimeU3Ek__BackingField,
	BaseObject_t2E7E8DF7B6AB09B998D7B321873C12ABF90265CC_CustomAttributesCacheGenerator_OnEnableObject,
	BaseObject_t2E7E8DF7B6AB09B998D7B321873C12ABF90265CC_CustomAttributesCacheGenerator_OnDisableObject,
	BaseObject_t2E7E8DF7B6AB09B998D7B321873C12ABF90265CC_CustomAttributesCacheGenerator_OnDestroyObject,
	CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_U3CCollider3DsU3Ek__BackingField,
	CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_U3CCollider2DsU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CBlackU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkBlueU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkGreenU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkAquaU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkRedU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkPurpleU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CGoldU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CGrayU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CDarkGrayU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CBlueU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CGreenU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CAquaU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CRedU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CLightPurpleU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CYellowU3Ek__BackingField,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_U3CWhiteU3Ek__BackingField,
	CustomEvent_t5B35408F28C7A79D4ACF840110EF0173112F3E91_CustomAttributesCacheGenerator_Event,
	CustomEvent_1_t4EEDFA083CB1C31478A549CDA764DE151FDDD112_CustomAttributesCacheGenerator_Event,
	CustomEvent_2_tB9D2FC3CCE07F79933DFE0A2DA47DE2B13BBBEB2_CustomAttributesCacheGenerator_Event,
	CustomEvent_3_t9A49C44D538F17341B1A83FC9EE42669C5BE0761_CustomAttributesCacheGenerator_Event,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_U3COnEnterCollisionU3Ek__BackingField,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_U3COnExitCollisionU3Ek__BackingField,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_U3COnEnterCollision2DU3Ek__BackingField,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_U3COnExitCollision2DU3Ek__BackingField,
	Detector_t81191080C8995EEB54E78173A1DD31087DD628CF_CustomAttributesCacheGenerator_total,
	Detector_t81191080C8995EEB54E78173A1DD31087DD628CF_CustomAttributesCacheGenerator_U3CIsCollisionDetectorU3Ek__BackingField,
	RayDetector_t00A4CC4AEB3D3C4202FF78764E02A14E35E5BC95_CustomAttributesCacheGenerator_distance,
	SphereDetector_t476728A4ECFB3F6F4D02B2E5DFDBA89945E1F54E_CustomAttributesCacheGenerator_radius,
	ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_U3CDataU3Ek__BackingField,
	ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_U3CNoiseU3Ek__BackingField,
	ShakeData_t5F1C3E32FFD590898D62F2F07AF5F6E7543D5BD8_CustomAttributesCacheGenerator_amplitude,
	ShakeData_t5F1C3E32FFD590898D62F2F07AF5F6E7543D5BD8_CustomAttributesCacheGenerator_frequency,
	ShakeData_t5F1C3E32FFD590898D62F2F07AF5F6E7543D5BD8_CustomAttributesCacheGenerator_duration,
	Shaker_t78A3FC34FC949275ADFCA0480C427A1E9567A1AB_CustomAttributesCacheGenerator_destroy,
	Shaker_t78A3FC34FC949275ADFCA0480C427A1E9567A1AB_CustomAttributesCacheGenerator_U3CShakesU3Ek__BackingField,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_destroy,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_index,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_U3CSoundU3Ek__BackingField,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_U3CSourceU3Ek__BackingField,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_delayIn,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_delayOut,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_start,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_end,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_minCycles,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_maxCycles,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_volumeValue,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_spatialBlend,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_U3CControllerU3Ek__BackingField,
	LookAt_t2056805C19591CD18EFE9C1D97AC0421E423B0E6_CustomAttributesCacheGenerator_speed,
	MoveTo_t86637DBAAFB56DE66C210CD5D53637611D181EF2_CustomAttributesCacheGenerator_speed,
	MoveTo_t86637DBAAFB56DE66C210CD5D53637611D181EF2_CustomAttributesCacheGenerator_smoothing,
	RotateTo_t65C9DABDE8AB9EFE4B0A411EA1AD71FC60101174_CustomAttributesCacheGenerator_speed,
	ScaleTo_tA00A4305801B9D90E277B03E97DC8AB488DF8685_CustomAttributesCacheGenerator_speed,
	ScaleTo_tA00A4305801B9D90E277B03E97DC8AB488DF8685_CustomAttributesCacheGenerator_smoothing,
	GlobalVariable_1_t2AC8186011BD6AF407254E62A334FC3650440CC1_CustomAttributesCacheGenerator_debug,
	GlobalVariable_1_t2AC8186011BD6AF407254E62A334FC3650440CC1_CustomAttributesCacheGenerator_reset,
	GlobalVariable_1_t2AC8186011BD6AF407254E62A334FC3650440CC1_CustomAttributesCacheGenerator_defaultValue,
	GlobalVariable_1_t2AC8186011BD6AF407254E62A334FC3650440CC1_CustomAttributesCacheGenerator_value,
	GlobalVariable_1_t2AC8186011BD6AF407254E62A334FC3650440CC1_CustomAttributesCacheGenerator_OnValueChanged,
	Singleton_1_t10C86293B9768076580DF8EC56BFFF52FAE0A94C_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	ParticleController_t1321350CE7B8866173FCA8B8C2FE5C1C02B591CB_CustomAttributesCacheGenerator_ParticleController_get_Pool_m0B06778AC43BA0AEEBCCA2900A6774581F31E4FA,
	ParticleController_t1321350CE7B8866173FCA8B8C2FE5C1C02B591CB_CustomAttributesCacheGenerator_ParticleController_set_Pool_m358926DCB555DCBF8394FB3B12239690FFB76A1F,
	GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_GameManager_get_Data_m1B7A20844D301ED83EA5355B5B9F96F368A317D7,
	GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_GameManager_set_Data_m10FF27828C8F24B6064BEFEB9A77DFD4BA72DE66,
	GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_GameManager_get_InPause_m9B5C419C01769D13A140FE124FEF1A0CBBA89E16,
	GameManager_tD03B225F03442604451AA3D8A3E43E3A4795496B_CustomAttributesCacheGenerator_GameManager_set_InPause_mF8A3587DFF67477D609E2F31EDC831AF9EB91828,
	Gun_tD5524FEE60C64BD8012CCA408F20D54E23D7BD7B_CustomAttributesCacheGenerator_Gun_get_Shooting_mBADD5CED17DC9B9D31EE0353C4704F57F58C9EA0,
	Gun_tD5524FEE60C64BD8012CCA408F20D54E23D7BD7B_CustomAttributesCacheGenerator_Gun_set_Shooting_m0BD4A2DC332628D534B55DF5C302EC80B227765D,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_LevelData_get_Highscore_m2572FFE3A1F87CF7E6385159C9E0D5C161B9E73D,
	LevelData_t60FB29F5C07D4A3518C0674B71AF7BBD3DECD25F_CustomAttributesCacheGenerator_LevelData_set_Highscore_m4109F0FD95BBEEC422595FCB902CCD18B4AF6516,
	TimeController_t9377123311BA6E26BA04D94350DEDBADC6224557_CustomAttributesCacheGenerator_TimeController_get_Time_mADF42903A4A10FC3BCF2C091D9B9B7206210E99F,
	TimeController_t9377123311BA6E26BA04D94350DEDBADC6224557_CustomAttributesCacheGenerator_TimeController_set_Time_mEFD6F7166CF29EE301EF91611B3CFD4F0621AFFD,
	Spawner_t33CA370368AF9D12D62931126F248AF6F1FD73D4_CustomAttributesCacheGenerator_Spawner_SpawnCoroutine_m549896FE8F0AD5E66BAB22A71859C5AA2247FA06,
	Spawner_t33CA370368AF9D12D62931126F248AF6F1FD73D4_CustomAttributesCacheGenerator_Spawner_DestroyCoroutine_m6300439302DA4A94603DB451CD02278474719315,
	U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator_U3CSpawnCoroutineU3Ed__8__ctor_mFD1313F3B32AB15AB720B852BC14652BED23B696,
	U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator_U3CSpawnCoroutineU3Ed__8_System_IDisposable_Dispose_m0BB4E8AAEB33F52178252D89D5FBE31D4BB5FEBD,
	U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator_U3CSpawnCoroutineU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mADB539B627C96166A7BA5D42F374E9D2D5524F01,
	U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator_U3CSpawnCoroutineU3Ed__8_System_Collections_IEnumerator_Reset_m8BC70F40D5205DA9353062F93CBC3ABD94A7847E,
	U3CSpawnCoroutineU3Ed__8_t2D043D97F8030230DC5B38E9363655A4B624CB1B_CustomAttributesCacheGenerator_U3CSpawnCoroutineU3Ed__8_System_Collections_IEnumerator_get_Current_m4FC7E752EC618871E9FDD1D27472408107BB2BA3,
	U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator_U3CDestroyCoroutineU3Ed__9__ctor_m57FF4656A8EB51BC48F56B2FC64168D12A859A14,
	U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator_U3CDestroyCoroutineU3Ed__9_System_IDisposable_Dispose_m6B5A6979C4B1ACCDFBC8D2E62078CD5475E04D17,
	U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator_U3CDestroyCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD98FDE193B09AB90ECDCEB68BCAC51245E244437,
	U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator_U3CDestroyCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m581161E85D7AF70C0E9FAACF3662C90C27D64497,
	U3CDestroyCoroutineU3Ed__9_tE0F0DDC014D7197878AD8EDD5759940B4069D657_CustomAttributesCacheGenerator_U3CDestroyCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mEBFEDA69992E0957E47A514D7843E67440004F28,
	CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_CollisionShape_get_Collider3Ds_m3B1665EFB9A0842AA9165F01D75180104EB8C5BE,
	CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_CollisionShape_set_Collider3Ds_m1AFDAE818DEE62300CC1B3CDC7DFB3BED593ABF4,
	CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_CollisionShape_get_Collider2Ds_mAE9734F5A82AD0A329CE26024BF9933AD710BA6A,
	CollisionShape_tA89AE8A4440153B1937895AD23A6B065907FE40F_CustomAttributesCacheGenerator_CollisionShape_set_Collider2Ds_mF8CEBB4D0E430D3CF05648F0AB850A693B5DAD08,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Black_mAA43214B64EB771E63E91EE7E9599E2E0A11FC80,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Black_m9E303B4234A56F5CD0197FF9897B983560774886,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkBlue_m8DA4C1F9CBC8876B06B560F692F0831F6345DAEE,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkBlue_mBAC57C2DC67AC8B0347BDB973978785F1678EC14,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkGreen_mE6CDB2C12ED02884C481D3BAF1F72BAC600488BB,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkGreen_mD5AA93D92E07A08BE6E682548086EB0820B86FB6,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkAqua_m4459622E91F8B377D8D5B05D8EFEB3AE9BDFA07C,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkAqua_mE14F05D00B594DAA31F6C81B46898A7DD8638299,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkRed_m16C2014D3A2802DC6DD092DED66B4CBAAFAFD168,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkRed_mB1216D5C094AEB9BD4F8B12CFFCFE6742B73DA1C,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkPurple_m8C8C52E33789C5ADF3BEA2194F801B9582BF569B,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkPurple_mF9F855C820B992667C9F465994F2E53E3B27EE85,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Gold_m746E9979E1BA38E14C359340DB2C31DECD25830C,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Gold_m55DF49C197C4B7CE5169F28E327E8EBCCCD94DB8,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Gray_mF8233FB0AA5C8525006ECFC4244624DA321EB2DE,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Gray_m3D5F0D102AC9B1EE88DE051F78CE60D11F8D31A2,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_DarkGray_m13B0103AEC08C4A8877BC17C1221EA2D347516BB,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_DarkGray_mD084F5B587B5EDA7A9787747E78705EC4C2DA8AD,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Blue_m616240B6A1145A2B771AC140E5C18C530A83DB8F,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Blue_m9F4079D5C6866780DCAF058D8853DC5EA7C59829,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Green_m17BAC4AE541DEB4D615F8DEA46DA076E06875CE6,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Green_mA92664CE44C2759E464DEA4006F1FC05ED0F7701,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Aqua_m892EB4654255457958BB0B3048B20944844D93A9,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Aqua_m0BEB1267F6AAA73B2131D10894F9204DF1F6AC86,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Red_mFFD3D6960EADDC9B5C6F040C34EDBCEE4F794B46,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Red_m25C2338D066C18BA0D592069DD25349B753D3A1B,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_LightPurple_mE52D43E2B72D28197B362D188A81BE9D2DADE22A,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_LightPurple_m225FBECE0B2B2EE07B94DF923B764495B33675E7,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_Yellow_m0ECD8AE27E256F35C20B31606FE4A8014A50435E,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_Yellow_mB22D624E1720AF8E55B12A07535B36FF12645B40,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_get_White_mF9CC4AE9901408F590F58BF6D909A81DF4C1C1AA,
	Colors_tA594BBC4F655FE1B889F4DA1AECC5A2C40358505_CustomAttributesCacheGenerator_Colors_set_White_mA6F60FAC4252EB2DB23A4C64D4EF9303EFD286EF,
	AnimatorExtensions_t92B2B45FC1A8E51D177B9E36134F4E64949804DF_CustomAttributesCacheGenerator_AnimatorExtensions_HasParameter_m65C2A853A157D11E89D30CA699DF352245CA502D,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Shuffle_mA14E8F7784445218B0D50F0B1A0AF2DB48FE6FF3,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Random_m60D6955470B9DD6FB35BB678C1EEBC7D4AC9B339,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Add_mF283468779923CF5A38689E03D89F7CC91F9F346,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Add_mD0079738AB79BEE34A4B975850F74693F7F4319A,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Insert_m534846A9101A4F536A0387A8F987D3CA89EB60C7,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Insert_mC1A161A6D5844A4594D1D5820ABAC8F0CF3891CD,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Fusion_m8DCC32AABE8CBCC37FAAB9CF60395638B652C07C,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Get_m0B05DFF32B4017A1CED013A30B7B595EFB32FC8F,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_RemoveAt_mF370866441AE10765F61DB805DAAAAAA75A25F2E,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Remove_m5F45260BA2F1B80FCF7BBE9A24764B0DAC2D1603,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Remove_m49F6441C0BE6CD8F5FE85A7E52B60CBF2F2D8C9E,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Remove_m39D0B84C66460E3C0FF4B79FF23EAD4E7EEC3055,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_FindAll_m8F4FA9EC9EDF4E1F20ABB88E88E8A754857A6E97,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_FindAll_m6F1DA872BADCAD84F6E43DF230917B1B2090DD92,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_FindAll_m5ACBEB97B5A4BCB89A561F3B5E85BFCFBF2C13A7,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_First_m8B9A2522669365FD7AECA1473602C5C9D68EFE9F,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Last_m1E3C4C6F29316F57E3960179CB70422B81D8A2BD,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Find_m9F13B38E8CE9C3B3C92C05939F073ABBBE7580CC,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Find_mFFAB56812B612DCC241BD058B19C52037A32BD4E,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Maximum_mC9AF1E12D99FB1704B2E28ED6F44ECBCD8B8C578,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Minimum_m30DA7B04AF45F7995F87EC801AA040531F1F526C,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Index_m226898DBDEAA4EA9CB04C44BF1BCD0EF9DBCB178,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Index_m713BD5160380271C76860C18A541C8F2A6771E8E,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Contains_mC735778C64AE14948C6B89EBF82BE6FC70AB06A5,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Contains_m3845D3428047503C269BD91DAE6292126C79ED58,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Each_m9118C7E1F1DD142B92653226908C72A78F801276,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Sort_mB9456DFF0945D3B7AA26489EC10F8AA77E99BF85,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Reverse_m3C4C012A50C53098AF290B70A0F80857CC535B3A,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Swap_m4AA0F5D4D298ABB291E437E226F4C847735E3B25,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_Swap_m94D017C3BCFAF02BA73F2FD0EED60B2BCECFF26F,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_InBounds_m1533CBE553FD2EAAD8DE422ADC50627FFACA2C65,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_IsNull_m24145C25BAB281FFB0159AEEABDA43EDFFAFEB65,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_IsNull_mA2AD6F3048466572F7AA2266F2427E5C40D74D7C,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_IsEmpty_m66C48A0FBAEEF60C759AF9B2D604F224A704C840,
	ArrayExtensions_tC81A707864F13AD5BFC2221DB658812CB6C0AB94_CustomAttributesCacheGenerator_ArrayExtensions_IsNullOrEmpty_mC6C8AAD6031CFEA93BC012365F9DBE47C5CAE862,
	ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_Set_m6AFA22FCF6B5EC500255C9FEEE6935D9E4021BD7,
	ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetR_mFFA620CE382CA0CA7BB52C3ABD3CD737E5EFB6E2,
	ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetR_m09BA6159DEDCCBE2DBEAB9C49F22CA6007323C63,
	ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetG_mBB4DF67245D4EB6913B2D3E5D852969D83A16498,
	ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetG_m8DF4C4FC264EFDF9FCF6F714AE3B5ECD98E9BF0D,
	ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetB_m84DBE0E438C84E3D584E97C58B969A8979DA5DD9,
	ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetB_m9A58A226B85B62796E7A68CF55DFCC9C2BBA2993,
	ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetA_mA169AE6EED6A543E3ABD2A113EF5308D4BC1287F,
	ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_SetA_m963C90656799BD83F9FE4C1FAAB5A3FA9DAE1608,
	ColorExtensions_t69CB90A553C96BD1E9952D53CE7480CC9D4FEDE6_CustomAttributesCacheGenerator_ColorExtensions_ToHex_m59386FA193D9DD3507810E2C7AFE2F712D5C2290,
	FloatExtensions_t11F738799DFB051D1DFE86130BF864B2E07D8BE9_CustomAttributesCacheGenerator_FloatExtensions_Abs_mC78DD95AA8FAA3082D8C41884BDD06D5FA9ABFBF,
	FloatExtensions_t11F738799DFB051D1DFE86130BF864B2E07D8BE9_CustomAttributesCacheGenerator_FloatExtensions_IsBetween_mC6515CC784721FCA19FE9D96C18637B07F3B0AD3,
	FloatExtensions_t11F738799DFB051D1DFE86130BF864B2E07D8BE9_CustomAttributesCacheGenerator_FloatExtensions_IsOut_m2EB229AB49E1964E39BFC1D5A29275493E142A75,
	FloatExtensions_t11F738799DFB051D1DFE86130BF864B2E07D8BE9_CustomAttributesCacheGenerator_FloatExtensions_Lerp_m4FFD0C6736D360C71851DEC02866847F925BE1D0,
	U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator_U3CLerpU3Ed__3__ctor_m17137A1ECDE2DC1B5E0B05AC6B12D70873286ED9,
	U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator_U3CLerpU3Ed__3_System_IDisposable_Dispose_m216C87FD0A759BDA982FC212D1E1551A499A8C4C,
	U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator_U3CLerpU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16B7E50FF3A9EF14AFAC9E1E0DC2CF5F1B9DB67A,
	U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator_U3CLerpU3Ed__3_System_Collections_IEnumerator_Reset_mC4E3EC46B4A0646C6737E4924FCDADD09FEE512E,
	U3CLerpU3Ed__3_t9775751A8D71702782BBC4C7AE24CFFF7C76BA5C_CustomAttributesCacheGenerator_U3CLerpU3Ed__3_System_Collections_IEnumerator_get_Current_m4BB5AD0EB5DE1B3306ECFF7DC883012CCF9DE601,
	ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetR_mFAEBA61A20E6428F999A17E03AF36B2503A2C431,
	ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetR_m629E89B98E610E8CB83CF42E6367B7D3A14A6A69,
	ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetG_m382DA12A44594B14636CEADB0818D97BE178936B,
	ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetG_mD5DE779F22269932C230DA10B6E58E160887C71E,
	ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetB_m988B7B9526387E85ED2B17DA538E59E90091ADEA,
	ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetB_m49026135CCEAD64A953F960D16CC47AFEA57D973,
	ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetA_m70F77E629B8B45D00ABF861A762F2BA764880F67,
	ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetA_mAA23378BAB12FA75FF36348E3C2742CB8FBE263A,
	ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetColor_m5E5CCF0BA62A2AEB34DCA633FD9B4E63A4FB1E61,
	ImageExtensions_tF2BAC68D46B0EF96EA75D2E94E456B70ED8DB054_CustomAttributesCacheGenerator_ImageExtensions_SetColor_mBEE20DCF91C9119E14EADCA21171A4690B603412,
	IntExtensions_t2AB6759A9C19F84E98B0DEC5AE664D1DEC2B0AE2_CustomAttributesCacheGenerator_IntExtensions_Abs_m09093FE68577FEDF3E9026C4D1A27B3CCEAA3193,
	IntExtensions_t2AB6759A9C19F84E98B0DEC5AE664D1DEC2B0AE2_CustomAttributesCacheGenerator_IntExtensions_IsBetween_mC4EDD39F5B7C8CC2ED88735414F86E9FA009A509,
	IntExtensions_t2AB6759A9C19F84E98B0DEC5AE664D1DEC2B0AE2_CustomAttributesCacheGenerator_IntExtensions_IsOut_m5C3BBD203A78F7FFA3B5B0C3C569CFE888A67C21,
	ListExtensions_t0055BC97A88F83F00F9F9026C5ECFAC40F6F0386_CustomAttributesCacheGenerator_ListExtensions_Shuffle_m42B1A28752BEA487C5B409C0591671DC5D6853AB,
	ListExtensions_t0055BC97A88F83F00F9F9026C5ECFAC40F6F0386_CustomAttributesCacheGenerator_ListExtensions_Random_m21815B793D9B14F272FC058BF2349E1BA802D725,
	ListExtensions_t0055BC97A88F83F00F9F9026C5ECFAC40F6F0386_CustomAttributesCacheGenerator_ListExtensions_First_m98FB80BB01DF1AAEAD047BFDCFD5D437EDE76C54,
	ListExtensions_t0055BC97A88F83F00F9F9026C5ECFAC40F6F0386_CustomAttributesCacheGenerator_ListExtensions_Last_m2C6AF04A05A963E5F83E3D0854A82AEAF963969F,
	MonoBehaviourExtensions_t7EF7770B6CE16EE2F83ED2EA1627BB0C8DBECD25_CustomAttributesCacheGenerator_MonoBehaviourExtensions_StartCoroutines_mDCD7FE5F426A916EBB16514473D000A7FDF5DC91,
	MonoBehaviourExtensions_t7EF7770B6CE16EE2F83ED2EA1627BB0C8DBECD25_CustomAttributesCacheGenerator_MonoBehaviourExtensions_StopCoroutines_mB4D9D1AF8DBE31F6B589A8CFB44D9993B14054E7,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_Log_m61CF3C51D0F65245B5E8D769788F35EE3339022B,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogNotification_m0968F3F09B27243BA90F851446E9D978A2031EDD,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogSuccess_mDE4CFC8CEFB0AF4619AC6871A70F09E8FE2F19C8,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogWarning_mDB995A3CE3662832CD9097225D442C73A6FF2D4D,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogError_m953CE7BACA44BA1932BF876D8B1ADC9612450256,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Zero_m0F12B09FBEB3B559E1D64904A9121BF1B38ACF23,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Up_mBB3A995893817FEBD828D45099EDFC5A84BBEF5D,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Up_mC52895A6ACD4B3F51662DB8C74F910B1C0CC90B8,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Up_m06E359EA967177DD74342E90C37DC889F6F83F00,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Up90_mABB571A0047C0DAE30CCC9BB65A52CB2815F8A9C,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Up180_mE140FDDE5BE7988867B33A14E09B28D74EA89A9F,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Down_m153776DFF67216F955E7138922B3D4146382320E,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Down_m90995D193F285DB9A33D12E5BB909990F3A7FD6E,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Down_mEB06E21CAB8262A461537A80F8F98992C94F510F,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Down90_m0A7CFA1EFE1E1DE498622A5B60387EBB13509B1F,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Down180_m21AC9C0569C619D1EA8F56B7FAC4C4D0DEC5C4C0,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Left_m947B5E58A6C2DFCF74DD4A8756424E221C66966D,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Left_mDA42E54985429B0F3E23DC9BDF3862B3A0DF41FD,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Left_m5C2BBF623267EB0C3EB1240265BC924DF4C35EB5,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Left90_mBCEC0324C5BE378CCF01882DB9C49E3C88D7C453,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Left180_m10F0F71ABE7471EE31D93784997104EE94F6C43B,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Right_m776B9F0569D3F2244DD5A97A4E89DF3AB16EF818,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Right_m9453E79E22C616CB1986E5C1EA0D039AEAEF6C95,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Right_m14FF1F9324335431168EF73FF6041792A6EA04DF,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Right90_m9BD36C19ED4067966DE28AD70E61D36681B2BFE4,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Right180_m405E1E80C2FE3F6C2E7942AB09D4B7D85151825C,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Forward_mDCE957BA188393041286E10CE0FE787BD523BB88,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Forward_m7000615890CAEC4AC45C91A66A68BD52BBA39BC4,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Forward_m65940725192A7DA0D955C067F3BFEE3E6DA86B90,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Forward90_m80D0BF85B5BB5CF21A19B54D20E554C6B34BDD67,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Forward180_m199E6031F53F23DE333755BD198D81DAC664D883,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Backward_m52DF472DFAAFDE4E506D3A7BB06C9E75D3299C73,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Backward_m70BA954DD7F1A36CA375FDCDBF0D441AF5D0880B,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Backward_m24D1D22CDA1132F002B0CF76948A15ED2C5A5F58,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Backward90_mE14A87E0BEA5D911553B940C41D7E599759B7832,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Backward180_m6FB603BF64C76C05CCE51CEC35248AAA3D18B3CD,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Add_m4CAA4D499323047A559C20D57473B45815F0E648,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Add_m336CEF5746B11CFA9D83A148A0DE496862D7ECCB,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Add_m7084BA1762C4F4381CF2FCF9635E888E1F498D9C,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Add_mE46FCEB42C92B5163F71B313073F2E814BF0E50D,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Add_mF713C20D639724911F419DEC5DE3EEB0FD18AACA,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Subtract_m4CF2B58CBBCE57D0DBB4AC1F1D5F777888657F04,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Subtract_m2B59E0F86BD21F529B10BB208801F15F424484A8,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Subtract_m2C3BAB564A9569BE9BB425835FFB040873AFB8C3,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Subtract_m4F04ED216472FDD481FA50F2DE5B53808BC31365,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Subtract_mB6942626E492BEB8FE232C43F6D497D96FFF01E9,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Multiply_m67D9D8633883D8E551D25EB7269C46FF725782F7,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Multiply_mAA96F55CF284FD2C99FD1035CC1CCE8673E662BD,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Multiply_m9227C99233A1B254EFBFA0117E89363C1C0A0BE8,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Multiply_mB7D0A683328EB6D69EAD60E2095E48AFBFACAB27,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Multiply_m8E8913D8912390CE6C9EDE0E562AC4FF1E7AE2E6,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Divide_m5EFFEB89FB0B5889C6AC64A4326A492F1A02D02C,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Divide_m9EA4388DBF3ABE2DFA8ED007A4F6331228410D36,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Divide_m357BD1B27735729427DA56C846D3019977D260F2,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Divide_m07FA50A7EBDF4523A9309D15094C76030A78D215,
	QuaternionExtensions_tE24F3019455FB00BF87BD3E7E19CAFE69960816C_CustomAttributesCacheGenerator_QuaternionExtensions_Divide_mE439CBA5A34972F59CEF509C0490D8C7C9C5309E,
	Rigidbody2DExtensions_t73F71A7890A5A39EE2BA80FB701ED5967B7825C7_CustomAttributesCacheGenerator_Rigidbody2DExtensions_AddExplosionForce_m8E9BDAD79B943D7B97E0FC5EE0EE5AD0599454DE,
	SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetR_m8BFF8AFDC263CA0BC4A40FC5D06B0A32E1E76A69,
	SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetR_m6EEF8BA1DE7852327357E5A7A721337679DA62EF,
	SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetG_mB5A0183B6B695701304581A15FB9ECCB39CBFA8B,
	SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetG_mAC079782001B1E6832445F6F80514071D8901385,
	SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetB_m9284EF24294550B3ED66698DC6DEA83EEC5B182E,
	SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetB_m757487CD1F97AA94AAB9BCF75CAC56833504184A,
	SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetA_mC94B3648CF295E68D03A4179CA232D1A2F06EC20,
	SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetA_m54BAD889A436F0F488D6F45B573F1FBF94916770,
	SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetColor_m7CD01B62F84106674BA6973641776B6CD3A6A7FA,
	SpriteRendererExtensions_t4972FB12A6AE70BE87B8E2C57F234DF596E29C58_CustomAttributesCacheGenerator_SpriteRendererExtensions_SetColor_m5C7E8A42F030A1A8A564332F06936ED007048C95,
	StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_Divide_m2BCC8258257E8616F53F6DA9B04E10FE1936641D,
	StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_Color_mB8C917AEDFC621F07E4ED594A654FD6A40A7ED11,
	StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_ToColor_mFE27F82901872E6F765BC8EDC12B048CC0DD1B74,
	StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_ToEnum_mA06CE7F87E469E941591617556D41DABE2ADC755,
	StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_IsHex_mDDC2251E712409026DB88D83386F4B84A26D1022,
	StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_IsAlpha_m74204F1AA74D46F02EA8E89C5C00B403EE6D30F7,
	StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_IsNumeric_mFF043632B4F5E1742BE82FC3B006578C92B0E04A,
	StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_IsAlphanumeric_m901B743AF3B9C962E9C4453323D1E865F752B66D,
	StringExtensions_t66FA1F5E269B4F6629B877775840C74B7FABC990_CustomAttributesCacheGenerator_StringExtensions_IsValidInput_m88CD9E58498F3640C2DFC0D451C6F5DAEEC39EDB,
	TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetR_mB4B2957AC500366941B17B7568231D351317482B,
	TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetR_m94BBFE8352782D5DD510CBFE8BF650B31F7AA115,
	TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetG_m2039EFA377D9DF24CA2C824EC1185737E8E92DCD,
	TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetG_m96C93CAA18EF4F9ED87F9617D2EE1453B1C014F6,
	TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetB_mE795683BBB5EC70F256F18F1622BF46966D49690,
	TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetB_m059A5D2AF784EE97EC7B7E52F7CCFFFBAE403E0E,
	TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetA_m291C7CDB9C0C7C0AF7F7B2B009E7D68EA1EECC5C,
	TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetA_mE8D0A7075CD7AA9D35E37F50AF49BF1DA9443ED4,
	TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetColor_m37619C9062B93B5A410F9336C050FBCFE614670A,
	TMPTextExtensions_t3008A1AE57387049D2A1D6E2CA507C372AE28F4F_CustomAttributesCacheGenerator_TMPTextExtensions_SetColor_m7553DCBF072EBFC73BEF0EDD99102742A475F0E0,
	TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetR_m3C5E804D9E55689CB388ECDEEB0084E1474BA8DA,
	TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetR_m1EADCE814318C3D20603DFBAC59C2A7FFAF7E8BB,
	TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetG_m83D9666D54A195A448B8403A01224410D1000F6C,
	TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetG_mC90BD24E6A8221AE2369CD5AF190FA771DA10C78,
	TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetB_m48F032B423EBAF11900517D6C1E1525D2ED1DCB3,
	TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetB_mBC36FDFBD6A12DCB5FEFC72DEECF90D16A65A49A,
	TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetA_m85362D6B891357D09337508D77EA2FEA3D219566,
	TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetA_m40873B329DFE4CCB63BE5BC83FEE7136DAC64C9D,
	TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetColor_m0C70BA3F433326639E054B20FD170A50D91D0A50,
	TextExtensions_tDBF2A3BEA56C3F709804D466624C9D31FC231D09_CustomAttributesCacheGenerator_TextExtensions_SetColor_m71E41A2A11BF53243929659738C52ADA9F1DC9D3,
	TransformExtensions_tF7DDA7C52B354E79289FF96C7F8B2CFEFDA41FA3_CustomAttributesCacheGenerator_TransformExtensions_OrbitAround_m427AFBBED01A222959A499573BF3416EEB0BBA8C,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_SetX_m628D5C7426C250CF1AA42EB7B4D5CD4C2AEF6FD2,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_SetY_m4E8FBC009EFF33EF0A41B94012A34C4CBE0A2FB9,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Add_mF90553B085B216B8063C319E7F5C8204150ED6A1,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Add_m762B3B8DF1C23DE0AA2C3560276B2A8B41B63D4B,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_AddX_m758A794B218B5C423592D51E268B663BBCC11962,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_AddY_mBB7E924E34BC6B1692D46C6056AFFECA78C72D1A,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Multiply_mF1DFE84C51DCB8461D57DFEA89F8271451231BBE,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_MultiplyX_mBDA6E3039AB952E5CBBA6392DCA76DBB53AD320A,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_MultiplyY_m5D71509EA53C9E5E1EB99824003A55F582038DCE,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipX_m207B2ECA3AFA9705787AF81675F193DDFEED1274,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipX_m9B3AD699449ACAEBC20A9FAF8E18C10D86D7AC8B,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipY_m3D57D6A2C8E86B7932C2E2E31818ECDC8E7394E2,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipY_mE5CE03C2A7D315D3A99DD3A328A4AD31437FBA22,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Clamp_m45CA9923A6E901E9382DDC7FE420214A9BFFD02B,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Clamp01_mAC727F3DC44C853FDA19B266270580714034A817,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Abs_mD3F2F9E177B6FB04F7D6FF4C1C5AE863A9717B5D,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_IsNaN_m2CEE8D61FDD43F32C53233AA95A589ED3E033EE4,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_IsBetween_m12B3F6E076D165F71A3EA8653FF0D395D939065F,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Random_m41E2C59D0F97940AAF95FFBFB970BD4D4F609284,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Random_m75CA36F81F110C038800A22EAFDE25668ABA9B25,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_SetX_m595EC12BB39254EA57783C91AD3ADCB4A653C8B0,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_SetY_m0A7A88B104C6A031184C0EC70E9BB8834E818C6E,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Add_m2C033D4117520A9FEE90D070A9102DD3C70BE2E6,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Add_m7FA63B8E9AF73DA9D6F1839128884FB5C83A2DEA,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_AddX_m0DB873F80F8A413EE690783034D438225AB3D164,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_AddY_m27B53E8C4E342CDC2365F88B86F21E7DE407D9F4,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Multiply_mAB1021AA7564D90AF150F1F171BAD924F3B38450,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_MultiplyX_m972CCB60D21C531E0021216D2E4E147A7EA7C0A6,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_MultiplyY_mF5AE7390ADC21BD6BAA2F0307910342360EE53ED,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipX_m60E7C5F685BB334192C017E7B59F26459A37B4E1,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_FlipY_mD2C6E9C5B1C152F266DE5AA51DDB1F299294DAE5,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Clamp_mDDF1AEFA36425F97961E262C791A7118CFA080F8,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Clamp01_m420330594EDA2001E2A9A5E43542B6C2934CC1DE,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Abs_m443A22A719ECBC24B13A8C98DF0F643BB6699F35,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_IsNaN_m3BD979CB285D0FE1B7C92EC7C09A98DE3FEC439A,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_IsBetween_m9BDF92254390AE9F6712D1911CFD269800874943,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Random_mC14C459831FE9B2D765229782B770F82DB2DC5D7,
	Vector2Extensions_tBD464A11C14C9475279F384ED8078E3A166CA1DD_CustomAttributesCacheGenerator_Vector2Extensions_Random_mD7F87347B2F86A02C3D17910862F3FF2361A7B44,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetX_m81CF6035CEB02FC9564EC3FDA6F022DAFC315718,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetY_m80B1B14A61DFD9644F2738CFF13D2C0AE4E1B47C,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetZ_mA4B15836E624115C548DC4DBC9C231B915425170,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Add_m129731B6B0B7D716826167689EF58602DE19DEB0,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Add_m427C7F3B429DB4D3D6885A799BCB22A8BB746D10,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddX_m091C22A33637623414F59CF8E760945DC978FE0A,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddY_m3DC1D188941755AA4B0EC34B71E1EB4F9443EE3F,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddZ_m167854E93C83055C59F075127BC017ACC4BFCE30,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Multiply_m59A3EB0E5AC45440069BA4638FE5574337F16C24,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyX_m01BB07BBFF31318D2D904375EA6229EC98269AE7,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyY_mD8206F4F19AC05F89F814A1A3A1FC066B8FAD3F7,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyZ_m4125A5E09C216CAB14745C8B807416602546717B,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipX_mE1C6AA3FB735AE73620ACAD1B024CE661DED4950,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipX_m1A831DF6FF2320D87367883EC610D1C0F9ADFFF3,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipY_m947BF3D1C40A38EFBA386A04E0AE3D2BBEA34C5F,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipY_m8B0B2087D7210C8C7E8478EE593DC76808C60F24,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipZ_mB79D528327D548D16D0EDF4054F0B123CBBE7A53,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipZ_mC50F3E941648FC54AECB6258E6A700F509223729,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Clamp_m560F956CF30672AAEBDE86DAE2151F22C3BA4650,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Clamp01_mA5FB3155EFFD290F0EF094B52B33D1073D6B031B,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Abs_mE81EE8A5C7EE28268CF0D1B91AC614564E3F772F,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_IsNaN_m8BBB8CB0F0A766C32ECD86496E0C09DF33EFCF6D,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_IsBetween_m7AF18A71EECB9DC544FE13BACD8BF22CE82558EC,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Random_m4DEBFEDD768D01361BD810FD1C154E142F2FEA3B,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetX_m163A6AC84AEE45EF5BC9FAE6C13455CE7742CC6D,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetY_m63B70C6B67A6D6B7ACA684FC65DC0D7B7FC7A9D0,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_SetZ_m5EE056686D97AAD916B9C0B85DD362B93F993C51,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Add_mA7D11EB57D79A27310CA41C27F96C8E4351784A6,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Add_mB57C1DC5B450AF7750A847B09F1FD340260D8287,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddX_mC7DED0E7E3A689D23932A95CD5E6635344009FEF,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddY_m70CF1032AA0D84F1DE1BF94260CB0797023B4E7A,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_AddZ_mF3ADC52DBBE7EA3D772E254573B7F3BB28C4525F,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Multiply_m395208DA68CD04BC047C164060D1AD22FBE4AA40,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyX_mCFBFA0C826EE539A6EB978C03C7C3C4A79C68082,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyY_m49A31473D6CD98AA93115BAFAC87F01A5C742071,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_MultiplyZ_m534D5268445D560788BC0051115F7FDDF7586DCA,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipX_m43688C951EC6BD33AB086671DFA5FF4107126D63,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipY_mE1F7250AB3D50229D14DED692C2DB294658043F1,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_FlipZ_m82CBD59675BB1FA10165B5BF48472E1F2B213340,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Clamp_mB2DF32F829692E3ADE4E5BD158D2CFF86AFE7D3A,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Clamp01_m2EA27A8E453821993BEB3EAB65B067EB8B11409C,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Abs_m64AA0B24024192CF433F91B6B6EB9C7A402FDDD6,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_Random_m1C1CF68F0D0FC1AB9A6B6210ABD7B74CD803628F,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_IsNaN_m6487769847AF0456E47459EF5D4EEE557AC23DE9,
	Vector3Extensions_t6BB25B62C56EDF2DCC41E9A47F10858962B289AC_CustomAttributesCacheGenerator_Vector3Extensions_IsBetween_mF858EF76A0CE1AEEA83E725B0DE0E53256D609D6,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_SetX_m4488FC494CB2AF61DA8CA4B27325D5C6347D2A46,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_SetY_m88F360368C14B924564A67CB65176F828F88E65E,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_SetZ_m5B05CBDF245A95D31DF067551B66F7D95CDA821D,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_SetW_m58268842AF83B79D88E1ECD3769B6E04CA223956,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Add_m75644FE0B835214634EB48EF8A397874D6040CC1,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Add_mB764E997232736FAF86908C91A26F422CF99150B,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_AddX_m921E63F49FDA98902D1A3AFB611E42B1699733B0,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_AddY_mC95FB316204D4DDEDE5E75E5C3B06E22E4C34C2D,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_AddZ_m888AD463EF6716CE6A65360AD29EF75F9664C824,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_AddW_m3B5CBAAD90A8722B6884F299766CD5CDBBCBA98B,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Multiply_m28E187B218F792582C81A26C2AA4C2BEED3AAD81,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_MultiplyX_mAA0033CABA21A8A4AC3D5A25F96427DA23981E04,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_MultiplyY_m2F195487328CDFEB4AB2F8CAC2F0528974012832,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_MultiplyZ_m6B49DF8E2A118803DED30A63BF92F00DCD59B9D4,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_MultiplyW_m82DD4E43D50E114A94CAF9C07CE2FB82C366A77F,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipX_m4FE0F1E12B406F3FD14AF98556E925E81AE7789C,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipX_m075997348588562CD28F7D3792725F51CEB40220,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipY_m18F8D19C98CC1A8DFDE8784E33C29DE27D86E9B5,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipY_m47B8E95F76846CF7B4FB52E781616DD2099A81BE,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipZ_m7EA7F38B981936AED7E919AF0CD5280696E98560,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipZ_m4CCFC1573B94ADC023952CEDF009A143E999204B,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipW_m74E8AD6CD37BF06788D3E664C8308BF37BA4F0F3,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_FlipW_mBC9BDF2CE1DF86A8EE1FDCAC8EE5707C80636DA8,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Clamp_m4D46C45F7D1CD35ECDCD4FAACB54E027772BCF3E,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Clamp01_m5D536C744535AA0842C612FB34084D106E758199,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Abs_m4934C92C8D119F43FBE87A923B0D6F69D70FCBFC,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_IsNaN_m0D9B34D85906B29320C3A779695A5F3C49545615,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_IsBetween_m7760AE4387A1C698F72BC1EDEEBECF87039470AD,
	Vector4Extensions_t07A19FBCF59E288D750C265DBDBD05CB292BAFA6_CustomAttributesCacheGenerator_Vector4Extensions_Random_m75E196B5C836C8D8C82CB32CF7A9D8F1BD024D4E,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_get_OnEnterCollision_m552AE5930F972CF920E0767FECEEA6E8E404CCDB,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_set_OnEnterCollision_m6F9F5443517CAE3BA616DAC973C338491D720DDA,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_get_OnExitCollision_mFCE873DF650709BA935EA417D700D44B124233F7,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_set_OnExitCollision_mABEA96E854787882105ECE55D298B4F958EFF49D,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_get_OnEnterCollision2D_m65D34D5EE180EDF0087CD7B17B03C5F86BB83627,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_set_OnEnterCollision2D_mB1E65053C541FA44FFD1ACAE3AB159B5755A91D8,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_get_OnExitCollision2D_mA070591D65C0365EB7EEB1938FB063D8F4D1E57D,
	CollisionDetector_t4999086E3EFA8F29C5E385967B0EB15451BB89E8_CustomAttributesCacheGenerator_CollisionDetector_set_OnExitCollision2D_mAB23E7E7ABB23329F387123FD0F55B8393DE80B1,
	Detector_t81191080C8995EEB54E78173A1DD31087DD628CF_CustomAttributesCacheGenerator_Detector_get_IsCollisionDetector_mF21FA01B1A8AB55B2CC0403AF849AAB2EDB119B8,
	Detector_t81191080C8995EEB54E78173A1DD31087DD628CF_CustomAttributesCacheGenerator_Detector_U3CDetect3DU3Eb__16_0_m4470C692412E4DB45B299A874A4D6EF37BB71B35,
	Detector_t81191080C8995EEB54E78173A1DD31087DD628CF_CustomAttributesCacheGenerator_Detector_U3CDetect2DU3Eb__17_0_mA99BEFCE95AB60BBC004150C2CC3C982C547F218,
	ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_ShakeController_get_Data_m9194007F30FE902F418E3719CC6589BAACBD2A97,
	ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_ShakeController_set_Data_mCC4E4F1BDCA181EAA83C186945435EC73923CF95,
	ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_ShakeController_get_Noise_m6430E01E097318FB62F2392358856AD0436AA177,
	ShakeController_tE01227E85D2E1C5D68B622052D5040AF4B5D66DE_CustomAttributesCacheGenerator_ShakeController_set_Noise_mFB37CA32FC05BAE468506C2ED111B29AED5194BA,
	Shaker_t78A3FC34FC949275ADFCA0480C427A1E9567A1AB_CustomAttributesCacheGenerator_Shaker_get_Shakes_m16E0A104B3A0951DF9BA6D427289A34165C88736,
	Shaker_t78A3FC34FC949275ADFCA0480C427A1E9567A1AB_CustomAttributesCacheGenerator_Shaker_set_Shakes_m69643E7D4F2FEC0A5B164550D74979D12E0D60D1,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_get_Sound_mBA2B474E8303D1A09B3EFB797D4ECC3C0932FC73,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_set_Sound_mAFFB12A928462D41B9316599893546B4D6FC993E,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_get_Source_m17A382415CE8079DBE7279043BF410B844F45448,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_set_Source_m6B3FF429CE8B1FEEEE895EC0E499F15E53FE049A,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_InCoroutine_m33D2FDA6432F130AC83F2D296F45F31229B72508,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_OutCoroutine_m73884E8C7CEFB14569C11DB03BC001D6916673AA,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_SFXUpdate_m03BAC1FA20CDAF18E9942DA9944BDFFE203C6778,
	SoundController_tC3D0A78F7722DCDC56BEE81992433E8050C4456B_CustomAttributesCacheGenerator_SoundController_CurveCoroutine_m9A698305897A7796C9BE34C2CEAD4189984F72EE,
	U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator_U3CInCoroutineU3Ed__36__ctor_m768A77D62609B5219C8B27E84141983AAD3A3D76,
	U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator_U3CInCoroutineU3Ed__36_System_IDisposable_Dispose_mB6F727542D4FE84C7FA54DB3A654762DF56997E3,
	U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator_U3CInCoroutineU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68B293F1F04F174F8AB3D39F3117B329F637258B,
	U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator_U3CInCoroutineU3Ed__36_System_Collections_IEnumerator_Reset_m1ABD37031B2464A2450F841151AE1ADF40CE3551,
	U3CInCoroutineU3Ed__36_t26C4702D3E5929B9E550563FC3ECE5423EBF166D_CustomAttributesCacheGenerator_U3CInCoroutineU3Ed__36_System_Collections_IEnumerator_get_Current_m721A47FB34BEB99D558CDB207E02E86FBDBBA28C,
	U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator_U3COutCoroutineU3Ed__37__ctor_mBF75D8F91859D02800985B782AD9B9061034A4CC,
	U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator_U3COutCoroutineU3Ed__37_System_IDisposable_Dispose_mFC1E65CC0E5BED49103F47FA6074CB7CD88B2EFD,
	U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator_U3COutCoroutineU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA32CEBA53B1DE20AF3F1994A7936BE75142B8F71,
	U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator_U3COutCoroutineU3Ed__37_System_Collections_IEnumerator_Reset_m7107C4BB8F4BD42DC550CA60ABB427255BAA6F27,
	U3COutCoroutineU3Ed__37_tCFC57C2637D7B1BD05FCD5A3E6B24C95CB73BC6A_CustomAttributesCacheGenerator_U3COutCoroutineU3Ed__37_System_Collections_IEnumerator_get_Current_m6A992C1DF0F0D804C810939A396E501E79B35003,
	U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator_U3CSFXUpdateU3Ed__38__ctor_m07428BBF9A91E3E67C36ED99F3FA91529FE4F06F,
	U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator_U3CSFXUpdateU3Ed__38_System_IDisposable_Dispose_m9EE4DCFF6221AAE6E63389B625CC72E7BEC38C04,
	U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator_U3CSFXUpdateU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84E0A548F2DB3379A1F64D00FED15D945D35E777,
	U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator_U3CSFXUpdateU3Ed__38_System_Collections_IEnumerator_Reset_m440C938D24AC7FC03EAB463992F1640D4A9E1780,
	U3CSFXUpdateU3Ed__38_t78DF52FC749D83FB68A89B40ACEDEE193F1345C7_CustomAttributesCacheGenerator_U3CSFXUpdateU3Ed__38_System_Collections_IEnumerator_get_Current_m386B1BBB881C24922403C0C99C403CF1BF7237D2,
	U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator_U3CCurveCoroutineU3Ed__42__ctor_m7CBE424F8485C6F65BF9B61DC65E567D584FF738,
	U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator_U3CCurveCoroutineU3Ed__42_System_IDisposable_Dispose_mAE910B8021B1C839BDD46C8091E823CCC14A1B49,
	U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator_U3CCurveCoroutineU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3D1317D299D2A110D9B57D9017DDA06E96B745D,
	U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator_U3CCurveCoroutineU3Ed__42_System_Collections_IEnumerator_Reset_mE505FA3A27B3D11891AC8BF8BB1BBB63FC2627D8,
	U3CCurveCoroutineU3Ed__42_t8157D9962FE5251EA4187EB217807B6CB6F573FD_CustomAttributesCacheGenerator_U3CCurveCoroutineU3Ed__42_System_Collections_IEnumerator_get_Current_m5F69CE3B305929E8E2786EE42A96D602A656CCF8,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_SoundEffect_get_Controller_m2A5FF64D3317DA9A1E49055A1B6CB8579591E804,
	SoundEffect_t008E2E86BF765B2EDB4246372C4F5A6AFAB3F50B_CustomAttributesCacheGenerator_SoundEffect_set_Controller_mEE20B5A111058B0F288672C8808E32209332B7F6,
	Singleton_1_t10C86293B9768076580DF8EC56BFFF52FAE0A94C_CustomAttributesCacheGenerator_Singleton_1_get_Instance_m9B250D55E1FA4980A38F89EF7215C48BE220A82B,
	Singleton_1_t10C86293B9768076580DF8EC56BFFF52FAE0A94C_CustomAttributesCacheGenerator_Singleton_1_set_Instance_mCFA1C0D4748D911A560087F21DE713A64BFD57A4,
	BaseObject_t2E7E8DF7B6AB09B998D7B321873C12ABF90265CC_CustomAttributesCacheGenerator_BaseObject_StartCoroutines_mBCE8DB63DA71A918AEC8B7699C5DF55CC03BBFB5____coroutines0,
	BaseObject_t2E7E8DF7B6AB09B998D7B321873C12ABF90265CC_CustomAttributesCacheGenerator_BaseObject_StopCoroutines_m8CC10400B83317C0C06FEB3C7C9A07835E9E8C6B____coroutines0,
	MonoBehaviourExtensions_t7EF7770B6CE16EE2F83ED2EA1627BB0C8DBECD25_CustomAttributesCacheGenerator_MonoBehaviourExtensions_StartCoroutines_mDCD7FE5F426A916EBB16514473D000A7FDF5DC91____coroutines1,
	MonoBehaviourExtensions_t7EF7770B6CE16EE2F83ED2EA1627BB0C8DBECD25_CustomAttributesCacheGenerator_MonoBehaviourExtensions_StopCoroutines_mB4D9D1AF8DBE31F6B589A8CFB44D9993B14054E7____coroutines1,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_Log_mE2360D17840FEC4124F24EA2B46D2FFB0838D5A9____msg3,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_Log_m61CF3C51D0F65245B5E8D769788F35EE3339022B____msg1,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogNotification_m0968F3F09B27243BA90F851446E9D978A2031EDD____msg1,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogSuccess_mDE4CFC8CEFB0AF4619AC6871A70F09E8FE2F19C8____msg1,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogWarning_mDB995A3CE3662832CD9097225D442C73A6FF2D4D____msg1,
	ObjectExtensions_tE73BD97070273373C4CF88F8D975494AD7E67DA0_CustomAttributesCacheGenerator_ObjectExtensions_LogError_m953CE7BACA44BA1932BF876D8B1ADC9612450256____msg1,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
