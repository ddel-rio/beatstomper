## Game

Beat Stomper is a musical VR mini-game made with Unity and C#.

The player must try to score as many points as possible before the end of the song by shooting cubes that move to the rhythm of the music around them.

---

## Project

The project is fully parameterized with Scriptable Objects and Events so it is very easy to scale in the future.

Many resources, such as materials and particles, are optimized for VR and cube generation is done using Object Pool.

It also includes part of my personal framework called Antipixel Framework, which speeds up the development a lot.

The repository includes both the source code and the executable in case you want to test the game directly.

---

## Screenshots


![Scheme](https://i.imgur.com/a8GOdot.gif)
![Scheme](https://i.imgur.com/q6MZfBB.png)

![Scheme](https://i.imgur.com/S8PiJf8m.png)
![Scheme](https://i.imgur.com/MDmIoq0m.png)
![Scheme](https://i.imgur.com/2dHrFI8m.png)

![Scheme](https://i.imgur.com/LL17RHQl.png)